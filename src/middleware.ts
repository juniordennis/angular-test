import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NavbarComponent } from './client/components/navbar/navbar.component';
import { UserService } from './client/services/user.service';

@Injectable()
export class LoggedInMiddleware implements CanActivate {
  user: any;
  constructor(private router: Router, private navbarComponent: NavbarComponent, private userService: UserService) {}

  canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<boolean> {
    return this.userService.getByToken().map((user: any) => {
      if (user) {
        return true;
      } else {
        // not logged in so redirect to login page
        this.router.navigate(['']);
        return false;
      }
    }).catch((err: any, caught: Observable<boolean>): Observable<boolean> => {
      if (err.code == 410) {
        // not logged in
        this.router.navigate(['']);
        return Observable.of(false);
      } else {
        this.router.navigate(['']);
        return Observable.of(false);
      }
    }).take(1);
  }
}

@Injectable()
export class VerifiedMiddleware implements CanActivate {
  user: any; // should be User model
  constructor(private router: Router, private navbarComponent: NavbarComponent, private userService: UserService) {}

  canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<boolean> {
    return this.userService.getByToken().map((user) => {
      if (user && user.verification && !user.verification.verified) {
        this.router.navigate(['verification/' + user._id]);
        return false;
      } else {
        return true;
      }
    }).catch((err: any, caught: Observable<boolean>): Observable<boolean> => {
      if (err.code == 410) {
        // not logged in
        this.router.navigate(['']);
        return Observable.of(false);
      } else {
        this.router.navigate(['']);
        return Observable.of(false);
      }
    }).take(1);
  }
}

@Injectable()
export class AsAdminMiddleware implements CanActivate {
  user: any; // should be User model
  constructor(private router: Router, private userService: UserService) {}

  canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<boolean> {
    return this.userService.getByToken().map((user) => {
      if (user && user.role && user.role == 'admin') {
        return true;
      } else {
        this.router.navigate(['']);
        return false;
      }
    }).catch((err: any, caught: Observable<boolean>): Observable<boolean> => {
      if (err.code == 410) {
        // not logged in
        this.router.navigate(['admin/login']);
        return Observable.of(false);
      } else {
        this.router.navigate(['admin/login']);
        return Observable.of(false);
      }
    }).take(1);
  }
}
