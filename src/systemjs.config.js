/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
var isPublic = typeof window != "undefined";
(function (global) {
  System.config({
    paths: {
      // paths serve as alias
      'npm:': (isPublic) ? '/' : 'node_modules/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      app: 'dist',

      // angular bundles
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

      // other libraries
      'ng2-bootstrap': 'npm:ng2-bootstrap/bundles/ngx-bootstrap.umd.js',
      'ng2-facebook-sdk': 'npm:ng2-facebook-sdk',
      'nouislider': 'npm:nouislider',
      'ng2-nouislider': 'npm:ng2-nouislider',
      'angular2-notifications': 'npm:angular2-notifications',
      'moment': 'npm:moment/moment.js',
      'rxjs': 'npm:rxjs',
      'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
      'primeng': 'npm:primeng',
      'jquery' : 'npm:jquery/dist/jquery.js',
      'slick-carousel' : 'npm:slick-carousel',
      'intl-tel-input' : 'npm:intl-tel-input',
      "ng2-ckeditor": "npm:ng2-ckeditor",
      "underscore" : "npm:underscore/underscore.js",
      "angular2-google-maps/core" : "npm:angular2-google-maps/core/core.umd.js",
      'angular2-fullcalendar': 'npm:angular2-fullcalendar',
      'fullcalendar' : 'npm:fullcalendar/dist/',
      'angular2-signaturepad': 'npm:angular2-signaturepad',
      'signature_pad' : 'npm:signature_pad/dist/',
      'rxjs-ddp-client' : 'npm:rxjs-ddp-client/dist',
      'ejson': 'npm:ejson',
      'dropzone': 'npm:dropzone',
      'ng2-pdf-viewer': 'npm:ng2-pdf-viewer',
      'pdfjs-dist': 'npm:pdfjs-dist',
      'markerwithlabel': 'npm:markerwithlabel/index.js',
      'ngx-popover': 'npm:ngx-popover',
      'ng2-sharebuttons': 'npm:ng2-sharebuttons/bundles/',
      'ng2-select': 'npm:ng2-select/',
      // 'angular-google-maps-marker/core': 'npm:angular-google-maps-marker/core/core.umd.js',
      'ng2-meta' : 'npm:ng2-meta/dist/index.js',
      'socket.io-client': 'npm:socket.io-client/dist/socket.io.js',
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        defaultExtension: 'js'
      },
      rxjs: {
        defaultExtension: 'js'
      },
      primeng: {
        defaultExtension: 'js'
      },
      'angular2-notifications': {
        "main": 'components.js',
        "defaultExtension": 'js'
      },
      'nouislider': {
        "main": 'distribute/nouislider.js',
        "defaultExtension": 'js'
      },
      'ng2-nouislider': {
        "main": 'src/nouislider.js',
        "defaultExtension": 'js'
      },
      'ng2-ion-range-slider': {
        "main": "index.js",
        "defaultExtension": "js"
      },
      'ion-rangeslider': {
        "defaultExtension": "js"
      },
      "pdfjs-dist": {
        'defaultExtension': 'js'
      },
      "ng2-ckeditor": {
        "main": "lib/index.js",
        "defaultExtension": "js"
      },
      "ng2-pdf-viewer": {
        "main": "dist/index.js",
        "defaultExtension": "js"
      },
      "ng2-facebook-sdk": {
        "main": "dist/index.js",
        "defaultExtension": "js"
      },
      "slick-carousel": {
        "main": "slick/slick.js",
        "defaultExtension": "js"
      },
      "intl-tel-input": {
        "main": "index.js",
        "defaultExtension": "js"
      },
      "dropzone": {
        "main": "dist/dropzone.js",
        "defaultExtension": "js"
      },
      'fullcalendar': {
        main: 'fullcalendar.js',
        defaultExtension: 'js'
      },
      'angular2-fullcalendar': {
        main: 'angular2-fullcalendar.js',
        defaultExtension: 'js'
      },
      'angular2-signaturepad': {
        main: 'index.js',
        defaultExtension: 'js'
      },
      'signature_pad': {
        main: 'signature_pad.js',
        defaultExtension: 'js'
      },
      'rxjs-ddp-client': {
        main: 'index.js',
        defaultExtension: 'js'
      },
      'ejson': {
        main: 'index.js',
        defaultExtension: 'js'
      },
      'ngx-popover': {
        main: 'index.js',
        defaultExtension: 'js'
      },
      'ng2-sharebuttons':{
        main : 'ng2-sharebuttons.umd.js',
        defaultExtension : 'js'
      },
      'ng2-select':{
        main : 'ng2-select.js',
        defaultExtension : 'js'
      },
    }
  });
})(this);
