import { Developments } from '../models/developments';

export const DEVELOPMENTS: Developments[] = [
	{ id: '1', name: '1 LOFT', slug: '', number_of_units: 1, properties: [{ id: '1', development: '1', address: {
	floor: '', unit: '', block_number: 100, street_name: '', postal_code: 200, coordinates: [''], country: '', full_address: 'Belyen Garden',
	type: ''}, details: {size_sqf: 220, size_sqm: 120, bedroom: 0, bathroom: 0, price: 2000, psqft: 20000, psqm: 23000,
	available: '', furnishing: '', description: '', type: '', sale_date: '', property_type: '', tenure: '', completion_date: '',
	type_of_sale: '', purchaser_address_indicator: '', planning_region: '', planning_area: ''} }], tenure: 'awefawe', age: 1, planning_region: 'central reqion',
	planning_area: 'gelyang', type_of_area: '10', postal_district: '10'},

	{ id: '2', name: '2 LOFT', slug: '', number_of_units: 1, properties: [{ id: '1', development: '1', address: {
	floor: '', unit: '', block_number: 100, street_name: '', postal_code: 100, coordinates: [''], country: '', full_address: 'Belyen Garden',
	type: ''}, details: {size_sqf: 0, size_sqm: 0, bedroom: 0, bathroom: 0, price: 2000, psqft: 20000, psqm: 23000,
	available: '', furnishing: '', description: '', type: '', sale_date: '', property_type: '', tenure: '', completion_date: '',
	type_of_sale: '', purchaser_address_indicator: '', planning_region: '', planning_area: ''} }], tenure: 'awefawe', age: 1, planning_region: 'central reqion',
	planning_area: 'gelyang', type_of_area: '10', postal_district: '10'}
];