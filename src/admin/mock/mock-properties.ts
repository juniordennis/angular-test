import { Properties } from '../models/properties';

export const PROPERTIES: Properties[] = [
	{ id: '1', development: '1 LOFT', address: {floor: '', unit: '', block_number: 100, street_name: '', postal_code: 200, coordinates: [''], country: '', full_address: 'Belyen Garden',
	type: ''}, details: {size_sqf: 220, size_sqm: 120, bedroom: 0, bathroom: 0, price: 2000, psqft: 20000, psqm: 23000,
	available: '', furnishing: '', description: '', type: '', sale_date: '', property_type: '', tenure: '', completion_date: '',
	type_of_sale: '', purchaser_address_indicator: '', planning_region: '', planning_area: ''} },

	{ id: '2', development: '1 King Albelrt', address: {floor: '', unit: '', block_number: 100, street_name: '', postal_code: 200, coordinates: [''], country: '', full_address: 'Belyen Garden',
	type: ''}, details: {size_sqf: 220, size_sqm: 120, bedroom: 0, bathroom: 0, price: 2000, psqft: 20000, psqm: 23000,
	available: '', furnishing: '', description: '', type: '', sale_date: '', property_type: '', tenure: '', completion_date: '',
	type_of_sale: '', purchaser_address_indicator: '', planning_region: '', planning_area: ''} }
];