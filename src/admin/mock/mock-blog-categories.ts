import { BlogCategories } from '../models/blog-categories';

export const CATEGORIES: BlogCategories[] = [
	{ id: '1', name: 'Furniture', description: 'Nothing',created_by : { id: '1' , username: '', email: '', password: '',
	 salt: '', phone: '', role: '', verification: {verified: false, verified_by: '', verified_date: '', expires: '',
	 code: ''}, created_at: '19/09/2018', report_user: { reported: false, count_report: 0 } }, created_at: '19/09/2017' },

	{ id: '2', name: 'Modern live', description: 'Nothing',created_by : { id: '1' , username: '', email: '', password: '',
	 salt: '', phone: '', role: '', verification: {verified: false, verified_by: '', verified_date: '', expires: '',
	 code: ''}, created_at: '19/09/2018', report_user: { reported: false, count_report: 0 } }, created_at: '19/09/2017' }
];