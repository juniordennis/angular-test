import { Users } from '../models/users';

export const USERS: Users[] = [
	{ id : '1', username : 'Jonny', email : 'iskandarjony65@gmail.com', password : 'admin', salt : '', phone : '08123421341',
	 role : 'user', verification : { verified : false, verified_by : '', verified_date : '18/09/2018', expires : '', code : '' }, created_at : '19/09/2018', report_user: { reported: false, count_report: 0 } },
	 { id : '2', username : 'Andi', email : 'Andi@gmail.com', password : 'Andi', salt : '', phone : '081233421341',
	 role : 'user', verification : { verified : true, verified_by : 'Test', verified_date : '21/02/2017', expires : '21/02/2019', code : 'A1' }, created_at : '19/02/2017', report_user: { reported: false, count_report: 0 } }
];