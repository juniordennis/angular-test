import { Appointments } from '../models/appointments';

export const APPOINTMENTS: Appointments[] = [
	{ id: '1', room_id: '1', landlord: { id: '1' , username: 'Jonny', email: '', password: '', salt: '', phone: '', role: '',
	verification: {verified: false, verified_by: '', verified_date: '', expires: '', code: ''}, created_at: '', report_user: { reported: false, count_report: 0 } },
	tenant: { id: '1' , username: 'Andi', email: '', password: '', salt: '', phone: '', role: '',
	verification: {verified: false, verified_by: '', verified_date: '', expires: '', code: ''}, created_at: '', report_user: { reported: false, count_report: 0 } },
	property: { id: '1', development: '1', address: {
	floor: '', unit: '', block_number: 100, street_name: '', postal_code: 200, coordinates: [''], country: '', full_address: 'Belyen Garden',
	type: ''}, details: {size_sqf: 220, size_sqm: 120, bedroom: 0, bathroom: 0, price: 2000, psqft: 20000, psqm: 23000,
	available: '', furnishing: '', description: '', type: '', sale_date: '', property_type: '', tenure: '', completion_date: '',
	type_of_sale: '', purchaser_address_indicator: '', planning_region: '', planning_area: ''} }, schedule: '',chosen_time: { date: '', from: '', to: '' }, status: ''}
];