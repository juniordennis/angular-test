import { Banks } from '../models/banks';

export const BANKS: Banks[] = [
	{ id: '1', code: '123', name: 'BCA', description: 'Testing', created_at: '19/09/2017' },
	{ id: '2', code: '09091', name: 'DANAMAON', description: 'Testing Testing', created_at: '25/09/2017' }
];