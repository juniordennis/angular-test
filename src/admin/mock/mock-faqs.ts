import { Faqs } from '../models/faqs';

export const FAQS: Faqs[] = [
	{ id: '1', question: 'Can I use an agent and your platform concurrently?',
	answer: 'Yes. There is nothing stopping you from using Staysmart and an agent concurrently. We believe both landlords and tenants have the right to use all means and channels in achieving their objective.',
	created_at: '19/09/2017', for: 'Landlord', created_by : { id: '1' , username: '', email: '', password: '', salt: '', phone: '', role: '', verification: {verified: false, verified_by: '', verified_date: '', expires: '', code: ''}, created_at: '', report_user: { reported: false, count_report: 0 } } },

	{ id: '1', question: 'Do I have to do the paper work myself?',
	answer: 'Our platform provides standardized template of the typical leasing documents - Letter of Intent (LOI) and Tenancy Agreement (TA) - that allows you to do up the paperwork seamlessly.',
	created_at: '19/09/2017', for: 'Landlord', created_by : { id: '1' , username: '', email: '', password: '', salt: '', phone: '', role: '', verification: {verified: false, verified_by: '', verified_date: '', expires: '', code: ''}, created_at: '', report_user: { reported: false, count_report: 0 } } } 
];