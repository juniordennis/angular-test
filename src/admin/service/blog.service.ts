import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Blogs } from '../modelsV2/blogs';
import { url } from '../global';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BlogService {
  constructor(private http : Http) {}
	data: any;

  getBlogs() {
    return this.http.get(url + 'blogs', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getById(id:string) {    
    return this.http.get(url + 'blogs/' + id, this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  create(value:any): Promise<Blogs> {
    return this.http.post(url +  'blogs', value, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  update(value:Blogs): Promise<Blogs> {
    return this.http.put(url + 'blogs/update/' + value._id,value, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  delete(id:any): Promise<void> {
    return this.http.delete(url + 'blogs/' + id, this.jwt())
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  private jwt() {
    let currentUser = JSON.parse(localStorage.getItem('authToken'));
    if (currentUser && currentUser.token) {
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
        return new RequestOptions({ headers: headers });
    }
  }
}
