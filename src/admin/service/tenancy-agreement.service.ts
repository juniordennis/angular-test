import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Agreements } from '../modelsV2/agreements';
import { TENANCY_AGREEMENTS } from '../mock/mock-tenancy-agreements';
import { url } from '../global';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class TenancyAgreementService {
  constructor(private http : Http) {}
	data: any;

  getAgreementById(id:string) {
    return this.http.get(url + 'agreements/' + id, this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getTAById(id:string) {    
    return this.http.get(url + 'ta/' + id, this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getAll() {
    return this.http.get(url + 'agreements', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getTenancyAgreement(id:any): Promise<Agreements[]> {
  	for (var i = 0; i < TENANCY_AGREEMENTS.length; i++) {
  		if (TENANCY_AGREEMENTS[i].id == id) {
  			this.data = TENANCY_AGREEMENTS[i];
  		}
  	}
  	return Promise.resolve(this.data);
  }

  getUserTenancyAgreements(id:any): Promise<Agreements[]> {
    for (var i = 0; i < TENANCY_AGREEMENTS.length; i++) {
      if (TENANCY_AGREEMENTS[i].id == id) {
        this.data = TENANCY_AGREEMENTS[i];
      }
    }
    return Promise.resolve(this.data);
  }

  acceptPayment(body:any, id:string) {
    return this.http.post(url + 'agreement/payment/accepted/' +  id, body, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  rejectPayment(body:any, id:string) {
    return this.http.put(url + 'agreement/payment/rejected/' +  id, body, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  delete(id:any): Promise<void> {
    return this.http.delete(url + 'agreements/' + id, this.jwt())
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  private jwt() {
    let currentUser = JSON.parse(localStorage.getItem('authToken'));
    if (currentUser && currentUser.token) {
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
        return new RequestOptions({ headers: headers });
    }
  }
}