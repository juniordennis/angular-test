import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Users } from '../modelsV2/users';
import { url } from '../global';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {
  constructor(private http : Http) {}
	data: any;

  getByToken(){
    return this.http.get(url + 'me', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getById(id:string) {
    return this.http.get(url + 'users/' + id, this.jwt())
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getUsers() {
    return this.http.get(url + 'users', this.jwt())
          .map((res:Response) => res.json())
          .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  create(value:any): Promise<Users> {
  	return this.http.post(url +  'users', value, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
	}

	update(value:Users): Promise<Users> {
    return this.http.put(url + 'users/update/' + value._id,value, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
    }

	delete(id:string): Promise<void> {
		return this.http.delete(url + 'users/' + id, this.jwt())
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
	}

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  private jwt() {
    let currentUser = JSON.parse(localStorage.getItem('authToken'));
    if (currentUser && currentUser.token) {
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
        return new RequestOptions({ headers: headers });
    } else {
      return new RequestOptions();
    }
  }

}
