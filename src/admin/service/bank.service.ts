import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Banks } from '../modelsV2/banks';
import { url } from '../global';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BankService {
  constructor(private http : Http) {}
	data: any;

  getBanks() {
    return this.http.get(url + 'banks', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getById(id:string){    
    return this.http.get(url + 'banks/' + id, this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  create(value:any): Promise<Banks> {
    return this.http.post(url +  'banks', value, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

	update(value:Banks): Promise<Banks> {
    return this.http.put(url + 'banks/update/' + value._id,value, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  delete(id:any): Promise<void> {
    return this.http.delete(url + 'banks/' + id, this.jwt())
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  private jwt() {
    let currentUser = JSON.parse(localStorage.getItem('authToken'));
    if (currentUser && currentUser.token) {
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
        return new RequestOptions({ headers: headers });
    }
  }
}