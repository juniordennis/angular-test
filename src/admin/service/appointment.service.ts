import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { url } from '../global';
import { Appointments } from '../models/appointments';
import { APPOINTMENTS } from '../mock/mock-appointments';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AppointmentService {
	data: any;

  constructor(private http: Http) {}

  getAll(){
    return this.http.get(url + 'appointments', this.jwt())
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getAppointments(): Promise<Appointments[]> {
    return Promise.resolve(APPOINTMENTS);
  }

  getAppointment(id:any): Promise<Appointments[]> {
  	for (var i = 0; i < APPOINTMENTS.length; i++) {
  		if (APPOINTMENTS[i].id == id) {
  			this.data = APPOINTMENTS[i];
  		}
  	}
  	return Promise.resolve(this.data);
  }

  create(value:any): Promise<Appointments[]> {
 		// return this.http.post(url +  '', value, this.jwt())
   //    .toPromise()
   //    .then(res => res.json().data)
   //    .catch(this.handleError);
  	APPOINTMENTS.push(value);
    return Promise.resolve(APPOINTMENTS);
	}

	update(id:any,value:any): Promise<Appointments[]> {
   	// return this.http.post(url + '' + id,value, this.jwt())
    //   .toPromise()
    //   .then(res => res.json().data)
    //   .catch(this.handleError);
    return Promise.resolve(APPOINTMENTS);
	}

	delete(id:string): Promise<void> {
		return this.http.delete(url + 'appointments/' + id, this.jwt())
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
	}

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}