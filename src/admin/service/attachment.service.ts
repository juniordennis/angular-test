import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Attachments } from '../modelsV2/attachments';
import { url } from '../global';
import 'rxjs/add/operator/toPromise';
 
@Injectable()
export class AttachmentService {
    constructor(private http: Http) {}

    getAll(){
        return this.http.get(url + 'attachments', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    getById(id:string){
        return this.http.get(url + 'attachments/' + id, this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    create(body:any){
        return this.http.post(url +  'attachments', body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    update(body:any){
        return this.http.post(url + 'attachments/update/' + body._id,body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    delete(id: string): Promise<void> {
        return this.http.delete(url + 'attachments/' + id, this.jwt())
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    downloadPDF(link:string): any {
        return this.http.get(link, { responseType: ResponseContentType.Blob }).map(
        (res:any) => {
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }

}