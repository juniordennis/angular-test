import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Properties } from '../modelsV2/properties';
import { PROPERTIES } from '../mock/mock-properties'
import { url } from '../global';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PropertyService {
  constructor(private http : Http) {}
	data: any;

  getAllDevelopment() {
    return this.http.get(url + 'developments', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getAllProperty() {
    return this.http.get(url + 'properties', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getProperty(id:any): Promise<Properties[]> {
  	for (var i = 0; i < PROPERTIES.length; i++) {
  		if (PROPERTIES[i].id == id) {
  			this.data = PROPERTIES[i];
  		}
  	}
  	return Promise.resolve(this.data);
  }

  getPropertyById(id:any) {
    return this.http.get(url + 'properties/' + id, this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  approve(body:any, id:string) {
    return this.http.put(url + 'properties/approve/' +  id, body, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  reject(body:any, id:string) {
    return this.http.put(url + 'properties/reject/' + id, body, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  create(value:any): Promise<Properties> {
    return this.http.post(url +  'properties/', value, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  update(value:any, id:string): Promise<Properties> {
    return this.http.put(url +  'properties/update/' + id, value, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  delete(id:any): Promise<void> {
    return this.http.delete(url + 'properties/' + id, this.jwt())
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  checkDraft(){
    return this.http.get(url + 'properties/draft', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  private jwt() {
    let currentUser = JSON.parse(localStorage.getItem('authToken'));
    if (currentUser && currentUser.token) {
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
        return new RequestOptions({ headers: headers });
    }
  }
}