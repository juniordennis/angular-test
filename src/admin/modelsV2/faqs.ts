import { Users } from './index';

export class Faqs {
	_id : string;
	question : string;
	answer : string;
	for : string;
	created_at : string;
	created_by : Users;
}