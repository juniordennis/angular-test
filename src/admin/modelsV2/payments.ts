import { Attachments } from './index'

export class Payments {
	_id : string;
	type : string;
	fee : [{
		code : string,
		name : string,
		amount : number,
		received_amount : number,
		needed_refund : boolean,		
		refunded : boolean,
		created_at : string,
		updated_at : string
	}];
	attachment: {
		payment : Attachments,
		payment_confirm : Attachments,
		refund_confirm : Attachments
	};
	status : string;
	remarks : string
}