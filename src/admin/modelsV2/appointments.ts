import { Users,Properties } from './index'

export class Appointments {
	_id : string;
	room_id : string;
	landlord : Users;
	tenant : Users;
	property : Properties;
	schedule : string;
	chosen_time : {
		date : string,
		from : string,
		to : string
	};
	status : string;
}