import { Attachments } from './index';

export class Amenities {
	_id : string;
	name : string;
	description : string;
	icon : Attachments;
	created_at : string;
}