import { Properties } from './index'

export class Developments {
	_id : string;
	name : string;
	slug : string;
	number_of_units : number;
	properties : [ Properties ];
	tenure : string;
	age : number;
	planning_region : string;
	planning_area : string;
	type_of_area : string;
	postal_district : number; 
	address : {
		block_number : string,
		street_name : string,
		postal_code : string,
		coordinates : string,
		country : string,
		full_address : string,
		type : string
	};
}