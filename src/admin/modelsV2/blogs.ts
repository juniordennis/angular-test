import { Users, Attachments, BlogCategories, Comments } from './index';

export class Blogs {
	_id : string;
	cover : Attachments;
	category : BlogCategories;
	title : string;
	slug : string;	
	source : string;
	content : string;
	comments : [Comments];
	created_at : string;
	created_by : Users;
}