import { Attachments,Banks,Companies,Properties,Agreements,ChatRooms } from './index';

export class Users {
	_id : string;
	username : string;
	email : string;
	password : string;
	phone : string;
	role : string;
	picture : Attachments;
	verification : {
		verified : boolean,
		verified_date : string,
		expires : string,
		code : string
	};
	tenant : {
		data : {
			name : string,
			identification_type : string,
			identification_number : string,
			identification_proof : {
				front : Attachments,
				back : Attachments
			},
			bank_account : {
				bank : Banks,
				name : string,
				no : number
			}
		},
		histories : [{
			date : string,
			data : {}
		}]
	};
	landlord : {
		data : {
			name : string,
			identification_type : string,
			identification_number : string,
			identification_proof : {
				front : Attachments,
				back : Attachments
			},
			owners : [{
				name : string,
				identification_type : string,
				identification_number : string,
				identification_proof : {
					front : Attachments,
					back : Attachments
				}
			}],
			company : Companies,
			bank_account : {
				bank : Banks,
				name : string,
				no : number
			}
		},
		histories : [{
			date : string,
			data : {}
		}]
	};
	owned_properties : [ Properties ];
	rented_properties : [{
		until : string,
		property : Properties,
		agreements : Agreements
	}];
	managed_properties : Properties;
	chat_rooms : ChatRooms;
	agreements : [ Agreements ];
	dreamtalk : [{
		loginId : string,
		loginToken : string,
		loginTokenExpires : string
	}];
	blocked_users : Users;
	companies : Companies;
	shortlisted_property : Properties;
	service : {
		facebook : {
			id : string,
			token : string
		}
	};
	reset_password : {
		token : string,
		created_at : string,
		expired_at : string
	}
	created_at : string;
}