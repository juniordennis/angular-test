import { Users, Properties, Appointments, Attachments, Payments } from './index'

export class Agreements {
	_id : string;
	landlord : Users;
	tenant : Users;
	property : Properties;
	appointment : Appointments;
	letter_of_intent : {
		data : {
			monthly_rental : number,
			term_lease : number,
			date_commencement : string,
			requirements : [ string ],
			populate_tenant : boolean,
			landlord : {
				full_name : string,
				type : string,
				identification_number : string
				identity_front : Attachments,
				identity_back : Attachments,
			},
			tenant : {
				full_name : string,
				type : string,
				identification_number : string
				identity_front : Attachments,
				identity_back : Attachments
			},
			gfd_amount: number,
			sd_amount: number,
			security_deposit: number,
			term_payment: number,
			minor_repair_cost: number,
			lapse_offer: number,
			term_lease_extend: number,
			appointment : Appointments,
			property : Properties,
			confirmation : {
				tenant : {
					sign : string,
					date : string,
					remarks : string
				},
				landlord : {
					sign : string,
					date : string,
					remarks : string
				}
			},
			payment : Payments,
			status : string,
			created_at : string
		},
		histories : [{
			date : string,
			data : {}
		}]
	};
	tenancy_agreement : {
		data : {
			confirmation : {
				tenant : {
					sign : string,
					date : string,
					remarks : string
				},
				landlord : {
					sign : string,
					date : string,
					remarks : string
				}
			},
			payment : Payments,
			status : string,
			created_at : string
		},
		histories : [{
			date : string,
			data : {}
		}]
	};
	inventory_list : {
		data : {
			confirmation : {
				tenant : {
					sign : string,
					date : string,
					remarks : string
				},
				landlord : {
					sign : string,
					date : string,
					remarks : string
				}
			},
			status : string,
			property : Properties,
			created_at : string,
			list : [{
				name : string,
				items : [{
					name : string,
					quantity : number,
					remark : number,
					attachments : Attachments,
					landlord_check : boolean,
					tenant_check : boolean
				}]
			}]
		},
		histories : [{
			date : string,
			data : {}
		}]
	};
}