import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Agreements } from '../../modelsV2/agreements';
import { AttachmentService } from '../../../client/services/attachment.service';
import { AgreementService } from '../../../client/services/agreement.service';
import { AlertService, UserService } from '../../service/index';
import { NotificationsService } from 'angular2-notifications';
import { Observable} from 'rxjs/Observable';
import { url } from '../../global';
import "rxjs/add/operator/takeWhile";
import { SharedService } from '../../../client/services/shared.service';

@Component({
    selector: 'user-information-loi',
    templateUrl : './detail-letter-of-intent.component.html',
    
    providers: [AgreementService, AlertService, UserService],
})

export class AdminLetterOfIntentDetailComponent implements OnInit {
    myForm : FormGroup;
    letterOfIntents: Agreements[];
    dataPayment: any = {};
    dataAgreement: any = {};
    users: any;
    loi_id: any;
    id: any;
    data: {};
    isAccept: boolean;
    isReject: boolean;
    gfd_sda: any;
    valueTransfer: any;
    url: any;
    model: any = {};
    subscription: boolean = true;
    header: any;
    contentClass: string = 'content';

    @ViewChild('modalAccept') modalAccept:any;
    @ViewChild('modalReject') modalReject:any;

    constructor(private formbuilder: FormBuilder,private agreementService: AgreementService, private router: Router, private activateRoute: ActivatedRoute,
        private alertService: AlertService, private userService: UserService, private notificationsService: NotificationsService, 
        private attachmentService: AttachmentService, private sharedService: SharedService) 
    { 
        this.sharedService.collapse$.subscribe((collapse) => {
            if (collapse === true) {
                this.contentClass = 'content resize-margin-left';
            }
            else {
                this.contentClass = 'content'; 
            }
        }); 
    }

    ngOnInit() {
        this.activateRoute.params
        .takeWhile(()=> this.subscription)
        .subscribe((params: Params) => {
            this.loi_id = params['id'];
        });

        this.getLOI();
        this.getAgreement();

        this.isAccept = false;
        this.isReject = false;
        this.url = url;
        this.model.type = "letter_of_intent";

        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
    }

    getAgreement() {
        this.agreementService.getById(this.loi_id)
        .takeWhile(()=> this.subscription)
        .subscribe(agreement => {
            this.dataAgreement = agreement;
            console.log(this.dataAgreement);
        });
    }

    getLOI() {
        this.agreementService.getLOI(this.loi_id)
        .takeWhile(()=> this.subscription)
        .subscribe(payment => {
            this.dataPayment = payment;
            console.log(this.dataPayment);
        });
    }

    openAcceptModal() {
        if (!this.model.payment_confirm) {
            this.notificationsService.error(
                '',
                'Upload the photo first before submit the data',
                )
        } else if (!this.model.receive_payment) {
            this.notificationsService.error(
                '',
                'Fee is required',
                )
        } else if ((this.dataPayment.payment.fee[0].amount + this.dataPayment.payment.fee[1].amount)> this.model.receive_payment) {
            this.notificationsService.error(
                '',
                'Payment transfered is not enough',
                )
        } else {
            this.modalAccept.show()
        }
    }

    hideAcceptModal() {
        this.modalAccept.hide()
    }

    openRejectModal() {
        if (!this.model.receive_payment || !this.model.remarks) {
            this.notificationsService.error(
                '',
                'Please fill out remarks and transferred value',
                )
        } else {
            this.modalReject.show()
        }
    }

    hideRejectModal() {
        this.modalReject.hide()
    }

    acceptPayment() {
        this.agreementService.acceptPayment(this.loi_id, this.model)
        .then(
            res => {
                this.notificationsService.success(
                    'Successfully',
                    'Payment accepted',
                    )
            },
            error => {
                this.notificationsService.error(
                    'Error',
                    'Accept payment failed',
                    )
                console.log(error);
            }
        );
        this.isAccept = false;
        this.hideAcceptModal();
        this.getLOI();
    }

    rejectPayment() {
        this.agreementService.rejectPayment(this.loi_id, this.model)
        .then(
            res => {
                this.notificationsService.success(
                    'Successfully',
                    'Payment rejected',
                    )
            },
            error => {
                this.notificationsService.error(
                    'Error',
                    'Reject payment failed',
                    )
                console.log(error);
            }
        );
        this.isReject = false;
        this.hideRejectModal();
        this.getLOI();
    }

    viewUserDetail(id:any) {
        this.router.navigate(['admin/user/' + id + '/detail']);
    }

    backToLOI() {
        this.router.navigate(['admin/letter_of_intent']);
    }

    handleFileSuccess(event:any, image:any): void {
        console.log('success', event, image);
        this.model.payment_confirm = event.file.uploadedId;
    }

    handleFileFailure(event:any, image:any): void {
        console.log('fail', event, image);
    }

    handleFileSending(event:any, image:any): void {
        console.log('send', event, image);
    }

    handleFileAdded(event:any): void {
        console.log('add', event);
    }

    handleFileRemoved(event:any): void {
        console.log('remove', event);
        this.attachmentService.delete(event.file.uploadedId)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}