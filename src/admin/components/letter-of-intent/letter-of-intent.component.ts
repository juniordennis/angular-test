import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Agreements } from '../../modelsV2/agreements';
import { AgreementService } from '../../../client/services/agreement.service';
import { AttachmentService } from '../../../client/services/attachment.service';
import { url } from '../../../global';
import { NotificationsService } from 'angular2-notifications';
import { SharedService } from '../../../client/services/shared.service';
import "rxjs/add/operator/takeWhile";

@Component({
    selector: 'my-loi',
    templateUrl : './letter-of-intent.component.html',
    
    providers: [AgreementService]
})

export class AdminLetterOfIntentComponent implements OnInit {
    subscription: boolean = true;
    letterOfIntents: Agreements[];
    report: any;
    loi: any;
    isReport: boolean;
    link: string;
	model: any = {};
    header: any;
    imageURL: string;
    contentClass: string = 'content';

    @ViewChild('modalRefund') modalRefund:any;
    @ViewChild('modalImage') modalImage:any;

    constructor(
        private agreementService: AgreementService, 
        private router: Router,
        private attachmentService: AttachmentService,
        private notificationsService: NotificationsService,
        private sharedService: SharedService
        ) 
    { 
        this.sharedService.collapse$.subscribe((collapse) => {
            if (collapse === true) {
                this.contentClass = 'content resize-margin-left';
            }
            else {
                this.contentClass = 'content'; 
            }
        }); 
    }

    ngOnInit(): void {
        this.isReport = false;
        this.getAll();
        this.link = url + 'attachments';

        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
    }

    getAll(): void {
        this.agreementService.getAllAdmin().takeWhile(()=> this.subscription).subscribe(agreement => { 
            this.letterOfIntents = agreement.filter((data:any) => data.letter_of_intent.data);
            console.log(this.letterOfIntents, agreement);
        });
    }

    getReport(id:string) {
        this.isReport = true;
        this.agreementService.getLoiReport(id)
        .takeWhile(()=> this.subscription)
        .subscribe(report => {
            this.report = report;
        });
        this.getLOI(id);
    }

    openModalRefund(id:string) {
        this.getLOI(id);
        this.modalRefund.show();
    }

    hideModalRefund() {
        this.modalRefund.hide();
    }

    openModalImage(urlImage:string) {
        this.imageURL = urlImage;
        this.modalImage.show();
        console.log(this.imageURL,urlImage,'urlimage')
    }
    hideModalImage() {
        this.modalImage.hide();
    }


    getLOI(id:string) {
        this.agreementService.getLOI(id)
        .takeWhile(()=> this.subscription)
        .subscribe(loi => {
            this.loi = loi;
        });
    }

    viewInformation(id:string) {
        this.router.navigate(['admin/letter_of_intent/details/' + id]);
    }

    uploadRefundPayment(): void {
        console.log(this.model)
        if (!this.model.refund_confirm) {
            this.notificationsService.error(
                'Error',
                'Please Upload Proof before Confirm!',
                )
        } else {
            this.agreementService.uploadRefundProof(this.loi.payment._id, this.model)
            .then(
                data => {
                    this.notificationsService.success(
                        'Success',
                        'Refund Proof Uploaded',
                        )
                },
                error => {
                    this.notificationsService.error(
                        'Error',
                        'Server error, please try again later',
                        )
                }
            );
            this.hideModalRefund()
            this.getAll();
        }
    }

    handleFileSuccess(event:any): void {
        this.model.refund_confirm = event.file.uploadedId
      }

    handleFileFailure(event:any): void {
        console.log('fail', event);
    }

    handleFileSending(event:any): void {
        console.log('send', event);
    }

    handleFileAdded(event:any): void {
        console.log('add', event);
    }

    handleFileRemoved(event:any, type:any): void {
        this.attachmentService.delete(event.uploadedId)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
