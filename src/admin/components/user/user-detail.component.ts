import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Users } from '../../modelsV2/users';
import { UserService, AlertService } from '../../service/index';
import { Observable} from 'rxjs/Observable';
import { SharedService } from '../../../client/services/shared.service';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'user-detail',
  templateUrl : './user-detail.component.html',
  
  providers: [UserService, AlertService]
})

export class AdminUserDetailComponent implements OnInit {
  subscription: boolean = true;
  users: Users[] = [];
  userId: any;
  userDetail: any;
  isTenant: any;
  isLandlord: any;
  isCompany: any;
  isProperty:any;
  contentClass:string = 'content';

  constructor(private userService: UserService, private router: Router, private activateRoute: ActivatedRoute,
              private alertService: AlertService,  private sharedService: SharedService) 
    {
      this.sharedService.collapse$.subscribe((collapse) => {
          if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
          }
          else {
            this.contentClass = 'content'; 
          }
      }); 
    }

  ngOnInit(): void {
    this.activateRoute.params.takeWhile(()=> this.subscription).subscribe((params: Params) => {
      this.userId = params['id'];
    });
    if (this.userId) {
      this.getDataUser();
    }
  }

  getDataUser(): void {
    this.userService.getById(this.userId)
        .takeWhile(()=> this.subscription)
        .subscribe(user => {
        this.userDetail = user;
        this.isTenant = this.userDetail.tenant;
        this.isLandlord = this.userDetail.landlord;
        this.isCompany = this.userDetail.companies;
        this.isProperty = this.userDetail.owned_properties;
      });
  }


  backToUser() {
    this.router.navigate(['admin/user']);
  }

  ngOnDestroy() {
    this.subscription = false;
  }
}
