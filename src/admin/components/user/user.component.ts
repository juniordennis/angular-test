import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Users } from '../../modelsV2/users';
import { UserService, AlertService } from '../../service/index';
import { Observable} from 'rxjs/Observable';
import { SharedService } from '../../../client/services/shared.service';
import 'rxjs/add/operator/takeWhile';

@Component({
  selector: 'my-user',
  templateUrl : './user.component.html',
  
  providers: [UserService, AlertService]
})

export class AdminUserComponent implements OnInit {
  subscription: boolean = true;
	users: Users[] = [];
  userId: any;
  userDetail: any;
  contentClass:string = 'content';

  constructor(private userService: UserService, private router: Router, private activateRoute: ActivatedRoute,
              private alertService: AlertService,  private sharedService: SharedService) 
    {
      this.sharedService.collapse$.subscribe((collapse) => {
          if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
          }
          else {
            this.contentClass = 'content'; 
          }
      }); 
    }

  ngOnInit(): void {
    this.getUsers();
    this.activateRoute.params.takeWhile(()=> this.subscription).subscribe((params: Params) => {
      this.userId = params['id'];
    });
    if (this.userId) {
      this.userService.getById(this.userId)
        .takeWhile(()=> this.subscription)
        .subscribe(user => {
        this.userDetail = user;
      });
    }
  }

  getUsers(): void {
    this.userService.getUsers().takeWhile(()=> this.subscription).subscribe(users => { 
        this.users = users;
    });
  }

  deleteUser(value: Users): void {
    this.userService.delete(value._id)
    .then(response => {
        this.alertService.success('Delete user successful', true);
        this.getUsers();
      },
      error => {
        alert(`The user could not be deleted, server Error.`);
      }
    );
  }

  editUser(value:Users): void {
    this.router.navigate(['admin/user/' + value._id + '/edit']);
  }

  viewUser(value:Users): void {
    this.router.navigate(['admin/user/' + value._id + '/detail']);
  }

  backToUser() {
    this.router.navigate(['admin/user']);
  }

  ngOnDestroy() {
    this.subscription = false;
  }
}
