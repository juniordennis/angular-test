import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { AdminEqualValidator } from './equal-validator.directive';
import { Users } from '../../modelsV2/users';
import { UserService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';
import 'rxjs/add/operator/switchMap';
declare var $ : any;
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'my-user',
  templateUrl : './edit-user.component.html',
  styleUrls: ['./../../../public/assets/admin/components/user.css', './../../../public/assets/admin/css/style.min.css','./../../../public/assets/admin/css/style-responsive.min.css',],
  providers: [UserService, AlertService]
})

export class AdminEditUserComponent {
  subscription: boolean = true;
  myForm : FormGroup;
  users: Users[];
  dataUser: any = {};
  userId: any;
  model: any = {};
  contentClass: string = 'content';

  constructor(private formbuilder: FormBuilder, private userService : UserService, private alertService : AlertService,
              private activateRoute: ActivatedRoute, private location: Location, private router: Router, private sharedService: SharedService) 
  { 
    this.location = location; 
    this.sharedService.collapse$.subscribe((collapse) => {
      if (collapse === true) {
        this.contentClass = 'content resize-margin-left';
      }
      else {
        this.contentClass = 'content'; 
      }
    }); 
  }


  ngOnInit(): void {
    $("#phone").intlTelInput();

    this.activateRoute.params.takeWhile(()=> this.subscription).subscribe((params: Params) => {
      this.userId = params['id'];
    });

    if (this.userId != null) {
      this.userService.getById(this.userId)
        .takeWhile(()=> this.subscription)
        .subscribe(user => {
        this.dataUser = user;
      });
    }

		this.myForm = this.formbuilder.group({
			username : ['', Validators.required],
            email : ['', Validators.required],
            password : ['', Validators.required],
            confirmPassword : ['', Validators.required],
            phone : ['', Validators.required],
            role : 'admin'
		});
	}

	createUser(): void {
		this.userService.create(this.dataUser)
    .then(
        data => {
            this.alertService.success('Create user successful', true);
            this.router.navigate(['/admin/user']);
        },
        error => {
            console.log(error);
            alert(`The user could not be save, server Error.`);
        }
    );
	}

  updateUser(): void {
    this.userService.update(this.dataUser)
    .then(
      response => {
        this.alertService.success('Update development successful', true);
        this.router.navigate(['/admin/user']);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  backToUser() {
    this.router.navigate(['admin/user']);
  }

  ngOnDestroy() {
    this.subscription = false;
  }
}
