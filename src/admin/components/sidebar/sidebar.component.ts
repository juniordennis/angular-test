import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../client/services/user.service';
import { SharedService } from '../../../client/services/shared.service';

@Component({
    selector: 'my-sidebar',
    templateUrl : './sidebar.component.html',
    styleUrls: ['./../../../public/assets/admin/css/animate.min.css','./../../../public/assets/admin/components/sidebar.css',
    './../../../public/assets/admin/css/style.min.css','./../../../public/assets/admin/css/style-responsive.min.css'],
})

export class AdminSidebarComponent implements OnInit {
	user: any;
    isCollapsed:boolean = true;
    flexWidth:string = 'sidebar admin-sidebar';
    windowSize:number;
    notMobile:boolean = true;
    flexBg:string = 'sidebar-bg';

    constructor(
        private userService: UserService,
        private sharedService: SharedService,
        ) {
            this.sharedService.collapse$.subscribe((collapse) => {
                this.toggle(collapse);
            });
            this.windowSize = window.innerWidth;
        }

    ngOnInit() {
        this.getUser();
        if (this.windowSize < 768) {
            this.notMobile = false;
            this.isCollapsed = false;
            this.toggle(this.isCollapsed);
        }
        else {
            this.notMobile = true;
            this.isCollapsed = true;
            this.toggle(this.isCollapsed);   
        }
    }

    toggle(collapse: boolean) {
        this.isCollapsed = collapse;
        this.resizeChange('toggle');
        this.isCollapsed = !collapse;
    }

    getUser() {
        this.userService.getByToken().subscribe(user => { 
            this.user = user;
        });
    }

    onResize(event:any) {
        this.windowSize = event.target.innerWidth;
        this.resizeChange('resize');
    }

    resizeChange(type:string) {
        let collapsed:boolean;
        if (type == 'toggle') {
            collapsed = this.isCollapsed;
        }
        else {
            collapsed = !this.isCollapsed;
        }
        if (this.windowSize > 767) {
            this.notMobile = true;
            if (collapsed === true) {
                this.flexWidth = 'sidebar admin-sidebar min-width';
                this.flexBg = 'sidebar-bg min-width';
            }
            else {
                this.flexWidth = 'sidebar admin-sidebar';
                this.flexBg = 'sidebar-bg';
            }
        }
        else {
            this.notMobile = false;
            if (collapsed === true) {
                this.flexWidth = 'sidebar admin-sidebar min-width left-0 sidebar-z-index sidebar-padding-top';
                this.flexBg = 'sidebar-bg min-width left-0 bg-z-index';
            }
            else {
                this.flexWidth = 'sidebar admin-sidebar';
                this.flexBg = 'sidebar-bg';
            }
        }
    }
}
