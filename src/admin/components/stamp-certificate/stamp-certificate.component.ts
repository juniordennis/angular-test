import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Agreements } from '../../models/agreements';
import { TenancyAgreementService } from '../../service/index';
import { Observable} from 'rxjs/Observable';
import { SharedService } from '../../../client/services/shared.service';

@Component({
  selector: 'stamp-certificate',
  templateUrl : './stamp-certificate.component.html',
  
  providers: [TenancyAgreementService],
})

export class AdminStampCertificateComponent implements OnInit {
  myForm : FormGroup;
	tenancyAgreements: Agreements[];
  dataPayment: any = {};
  ta_id: any;
  contentClass: string = 'content';

  constructor(private formbuilder: FormBuilder,private tenancyAgreementService: TenancyAgreementService, private router: Router, private activateRoute: ActivatedRoute, private sharedService: SharedService) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.activateRoute.params.subscribe((params: Params) => {
      this.ta_id = params['id'];
    });
  }

  backToTA() {
    this.router.navigate(['admin/tenancy_agreement']);
  }

  handleFileSuccess(event:any, image:any): void {
    console.log('success', event, image);
  }

  handleFileFailure(event:any, image:any): void {
    console.log('fail', event, image);
  }

  handleFileSending(event:any, image:any): void {
    console.log('send', event, image);
  }

  handleFileAdded(event:any): void {
    console.log('add', event);
  }

  handleFileRemoved(event:any): void {
    console.log('remove', event);
  }
}