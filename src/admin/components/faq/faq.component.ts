import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Faqs } from '../../modelsV2/faqs';
import { FaqService, AlertService } from '../../service/index';
import "rxjs/add/operator/takeWhile";
import { SharedService } from '../../../client/services/shared.service';

@Component({
  selector: 'my-faq',
  templateUrl : './faq.component.html',
  
  providers: [FaqService, AlertService]
})

export class AdminFaqComponent implements OnInit {
  subscription: boolean = true;
  faqs: Faqs[];
  contentClass: string = 'content';

  constructor(private faqService: FaqService, private alertService: AlertService, private router: Router, private sharedService: SharedService) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.getFaqs();
  }

  getFaqs() {
    this.faqService.getFaqs().takeWhile(()=> this.subscription).subscribe(faqs => { 
        this.faqs = faqs;
        console.log(faqs) ;
    });
  }

  deleteFaq(value:Faqs): void {
    this.faqService.delete(value._id)
    .then(response => {
        this.alertService.success('Delete faq successful', true);
        this.getFaqs();
      },
      error => {
        alert(`The faq could not be deleted, server Error.`);
      }
    );
  }

  editFaq(value:Faqs): void {
    this.router.navigate(['admin/faq/' + value._id + '/edit']);
  }

  ngOnDestroy() {
    this.subscription = false;
  }
}
