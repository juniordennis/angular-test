import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Faqs } from '../../modelsV2/faqs';
import { FaqService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';
import 'rxjs/add/operator/switchMap';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'my-faq',
  templateUrl : './edit-faq.component.html',
  
  providers: [FaqService, AlertService]
})

export class AdminEditFaqComponent {
  subscription: boolean = true;
	myForm : FormGroup;
	faqs: Faqs[];
  dataFaq: any = {};
  faqId: any;
  model: any = {};
  contentClass: string = 'content';

	constructor(private formbuilder: FormBuilder, private faqService: FaqService, private alertService: AlertService,
              private activateRoute: ActivatedRoute, private location: Location, private router: Router, private sharedService: SharedService) 
  { 
    this.location = location; 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

	ngOnInit(): void {
    this.activateRoute.params.takeWhile(()=> this.subscription).subscribe((params: Params) => {
      this.faqId = params['id'];
    });

    if (this.faqId != null) {
      this.faqService.getById(this.faqId)
        .takeWhile(()=> this.subscription)
        .subscribe(faq => {
        this.dataFaq = faq;
      });
    }

		this.myForm = this.formbuilder.group({
      question : ['', Validators.required],
      answer : ['', Validators.required],
      for : ['', Validators.required]
		});
	}

	createFaq(): void {
    if (this.dataFaq.for == true) {
      this.dataFaq.for = "landlord";
    }
    else {
      this.dataFaq.for = "tenant";
    }
    this.faqService.create(this.dataFaq)
    .then(
      data => {
        this.alertService.success('Create faq successful', true);
        this.router.navigate(['/admin/faq']);
      },
      error => {
        console.log(error);
        alert(`The faq could not be save, server Error.`);
      }
    );
	}

  updateFaq(): void {
    if (this.dataFaq.for == true) {
      this.dataFaq.for = "landlord";
    }
    else {
      this.dataFaq.for = "tenant";
    }
    this.faqService.update(this.dataFaq)
    .then(
      response => {
        this.alertService.success('Update faq successful', true);
        this.router.navigate(['/admin/faq']);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  back() {
    this.router.navigate(['/admin/faq']);
  }

  ngOnDestroy() {
    this.subscription = false;
  }
}
