import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { BlogCategories } from '../../modelsV2/blog-categories';
import { BlogCategoryService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'my-category',
  templateUrl : './edit-blog-category.component.html',
  
  providers: [BlogCategoryService, AlertService]
})

export class AdminEditBlogCategoryComponent {
	myForm : FormGroup;
	blogCategories: BlogCategories[];
  dataBlogCategory: any = {};
  blogCategoryId: any;
  model: any = {};
  contentClass: string = 'content';

	constructor(private formbuilder: FormBuilder, private blogCategoryService: BlogCategoryService, private alertService: AlertService,
              private activateRoute: ActivatedRoute, private location: Location, private router: Router, private sharedService: SharedService) 
  { 
    this.location = location; 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

	ngOnInit(): void {
    this.activateRoute.params.subscribe((params: Params) => {
      this.blogCategoryId = params['id'];
    });

    if (this.blogCategoryId != null) {
      this.blogCategoryService.getById(this.blogCategoryId)
        .subscribe(category => {
        this.dataBlogCategory = category;
      });
    }

		this.myForm = this.formbuilder.group({
      name : ['', Validators.required],
      description : ['', Validators.required]
		});
	}

	createBlogCategory(): void {
    this.blogCategoryService.create(this.dataBlogCategory)
    .then(
      data => {
          this.alertService.success('Create blog category successful', true);
          this.router.navigate(['/admin/category']);
      },
      error => {
          console.log(error);
          alert(`The blog category could not be save, server Error.`);
      }
    );
  }

  updateBlogCategory(): void {
    this.blogCategoryService.update(this.dataBlogCategory)
    .then(
      response => {
        this.alertService.success('Update blog category successful', true);
        this.router.navigate(['/admin/category']);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
}
