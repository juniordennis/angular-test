import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BlogCategories } from '../../modelsV2/blog-categories';
import { BlogCategoryService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';

@Component({
  selector: 'my-category',
  templateUrl : './blog-category.component.html',
  
  providers: [BlogCategoryService, AlertService]
})

export class AdminBlogCategoryComponent implements OnInit {
	blogCategories: BlogCategories[];
  contentClass: string = 'content';

  constructor(private blogCategoryService: BlogCategoryService, private router: Router, private alertService: AlertService, private sharedService: SharedService) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.getBlogCategories();
  }

  getBlogCategories() {
    this.blogCategoryService.getBlogCategories().subscribe(blogCategories => { 
        this.blogCategories = blogCategories;
        console.log(blogCategories) ;
    });
  }

  deleteBlogCategory(value:BlogCategories): void {
    this.blogCategoryService.delete(value._id)
      .then(response => {
        this.alertService.success('Delete blog category successful', true);
        this.getBlogCategories();
      },
      error => {
        alert(`The blog category could not be deleted, server Error.`);
      }
    );
  }

  editBlogCategory(value:BlogCategories): void {
    this.router.navigate(['admin/category/' + value._id + '/edit']);
  }
}
