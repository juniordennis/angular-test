import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminComponent } from '../../app/admin.component';
import { AuthenticationService } from '../../../client/services/authentication.service';
import { UserService } from '../../../client/services/user.service';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'my-login',
  templateUrl : './login.component.html',
  styleUrls: ['./../../../public/assets/admin/components/login.css'],
  providers: [AuthenticationService, UserService, AdminComponent]
})

export class AdminLoginComponent {
    subscription: boolean = true;
    myForm : FormGroup;
    name:any;
    authToken:any;
    user: any = {};
    returnUrl: string;
    errorMessage: string;

	constructor(
        private formbuilder: FormBuilder,
        private route: ActivatedRoute, 
        private router: Router, 
        private authService: AuthenticationService,
        private userService: UserService, 
        private adminComponent: AdminComponent,
        ) {}

	ngOnInit() {
        // this.authService.logout();
        this.myForm = this.formbuilder.group({
            username : ['', Validators.required],
            password : ['', Validators.required]
        });

        if (localStorage.getItem('authToken')) {
            this.userService.getByToken()
            .takeWhile(()=> this.subscription)
            .subscribe(user => {
                if(user.role == 'admin') {
                    this.router.navigate(['admin/dashboard']);
                } else {
                    this.router.navigate(['']);
                }
            })
        }
    }

    login() {
        this.authService.login(this.user.username, this.user.password)
            .takeWhile(()=> this.subscription)
            .subscribe(
                data => {
                    console.log('Log in successfully')
                    this.router.navigate(['admin/dashboard']);
                },
                error => {
                    this.errorMessage = error;
                    console.log(error,'error login');
                });
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
