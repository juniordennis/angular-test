import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Developments } from '../../models/developments';
import { AttachmentService, DevelopmentService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'my-development',
  templateUrl : './edit-development.component.html',
  
  providers: [AttachmentService, DevelopmentService, AlertService]
})

export class AdminEditDevelopmentComponent {
	myForm : FormGroup;
  formUnit : FormGroup;
	developments: Developments[];
  dataDevelopment: any = {};
  dataUnitDevelopment: any = {};
  developmentId: any;
  unitDevelopmentId: any;
  model: any = {};
  url: string;
  typeDev: boolean;
  lat: any;
  lng: any;
  contentClass: string = 'content';

	constructor(private formbuilder: FormBuilder, private attachmentService: AttachmentService, private developmentService: DevelopmentService, private alertService: AlertService,
              private activateRoute: ActivatedRoute, private location: Location, private router: Router, private sharedService: SharedService) 
  { 
    this.location = location; 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

	ngOnInit(): void {
    this.activateRoute.params.subscribe((params: Params) => {
      this.developmentId = params['id'];
    });

    if (this.developmentId != undefined) {
      this.developmentService.getById(this.developmentId)
        .subscribe(development => {
        this.dataDevelopment = development;
      });
    }

    this.url = this.router.url;
    if (this.url == "/admin/development/" + this.developmentId + "/edit" || this.url == "/admin/development/new") {
      this.typeDev = true;
  		this.myForm = this.formbuilder.group({
        name : ['', Validators.required],
        type_of_area : [''],
        tenure : [''],
        planning_area : [''],
        age : [''],
        number_of_units : [''],
        description : ['']
  		});
    }
    else {
      this.typeDev = false;
      this.formUnit = this.formbuilder.group({
        address : ['', Validators.required],
        block_number : ['', Validators.required],
        street_name : ['', Validators.required],
        floor : ['', Validators.required],
        unit : ['', Validators.required],
        postal_code : ['', Validators.required],
        size_sqm : ['', Validators.required],
        size_sqf : ['', Validators.required],
        price : ['', Validators.required],
        psqft : ['', Validators.required],
        psqm : ['', Validators.required],
        sale_date : ['', Validators.required],
        type : ['', Validators.required],
        tenure : ['', Validators.required],
        completion_date : ['', Validators.required],
        type_of_sale : ['', Validators.required],
        purchaser_address_indicator : ['', Validators.required],
        planning_region : ['', Validators.required],
        planning_area : ['', Validators.required]
      });
      this.lat = 51.678418;
      this.lng = 7.809007;
    }
	}

	createDevelopment(): void {
		this.developmentService.create(this.dataDevelopment)
    .then(
      data => {
          this.alertService.success('Create development successful', true);
          this.router.navigate(['/admin/development']);
      },
      error => {
          console.log(error);
          alert(`The development could not be save, server Error.`);
      }
    );
	}

  updateDevelopment(): void {
    this.developmentService.update(this.dataDevelopment)
    .then(
      response => {
        this.alertService.success('Update development successful', true);
        this.router.navigate(['/admin/development']);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  // createUnitDevelopment(): void {
  //   this.developmentService.createUnit(this.dataUnitDevelopment).then(
  //     data => this.router.navigate(['admin/development'])
  //   );
  // }

  // updateUnitDevelopment(id:any): void {
  //   this.developmentService.updateUnit(id,this.dataUnitDevelopment).then(
  //     data => this.router.navigate(['admin/development'])
  //   );
  // }

  backToDevelopment() {
    this.router.navigate(['admin/development']);
  }

  handleFileSuccess(event:any, image:any): void {
    console.log('success', event, image);
    this.attachmentService.getById(event.file.uploadedId)
      .subscribe(data=>{
        console.log(data);
      })
  }

  handleFileFailure(event:any, image:any): void {
    console.log('fail', event, image);
  }

  handleFileSending(event:any, image:any): void {
    console.log('send', event, image);
  }

  handleFileAdded(event:any): void {
    console.log('add', event);
  }

  handleFileRemoved(event:any): void {
    console.log('remove', event);
    this.attachmentService.delete(event.uploadedId)
    .then(
        response => {
            console.log(response);
        },
        error => {
            console.log(error);
        }
    );
  }
}
