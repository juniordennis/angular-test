import { Component, OnInit, Input, Output } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';

import { Developments } from '../../modelsV2/developments';
import { Properties } from '../../modelsV2/properties';
import { DevelopmentService, PropertyService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';

@Component({
  selector: 'my-development',
  templateUrl : './development.component.html',
  
  providers: [DevelopmentService, AlertService, PropertyService]
})

export class AdminDevelopmentComponent implements OnInit {
	developments: Developments[];
  detailDev: Developments[];
  unitDev: Developments[];
  developmentId: any;
  url: string;
  isDetailDev: boolean;
  contentClass: string = 'content';

  constructor(private developmentService: DevelopmentService, private router: Router, private activateRoute: ActivatedRoute,
              private alertService: AlertService, private propertyService: PropertyService, private sharedService: SharedService) 
  {
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.url = this.router.url;
    this.getDevelopments();
    this.activateRoute.params.subscribe((params: Params) => {
      this.developmentId = params['id'];
    });
    if (this.developmentId) {
      if (this.url == "/admin/development/"+ this.developmentId +'/detail') {
        this.developmentService.getById(this.developmentId)
          .subscribe(development => {
          this.detailDev = development;
        });
      }
      else {
        // this.getUnitDevelopment(this.developmentId);
      }
    }
    if (this.developmentId) {
      if (this.url == "/admin/development/"+ this.developmentId +'/unit') {
        this.developmentService.getById(this.developmentId)
          .subscribe(development => {
          this.unitDev = development.properties;
          console.log(development.properties);
        });
      }
      else {
        // this.getUnitDevelopment(this.developmentId);
      }
    }
  }


  // getDetailDevelopment(id:any): void {
  //   this.developmentService.viewDevelopment(id).then(developments => {
  //     this.detailDev = developments;
  //     this.age = this.detailDev['age'];
  //     this.postal_district = this.detailDev['postal_district'];
  //     this.tenure = this.detailDev['tenure'];
  //     this.planning_area = this.detailDev['planning_area'];
  //     this.no_of_unit = this.detailDev['no_of_unit'];
  //     this.isDetailDev = true;
  //   });
  // }

  // getUnitDevelopment(id:any): void {
  //   this.developmentService.viewUnitDevelopment(id).then(units => {
  //     this.unitDev = units;
  //     this.isUnitDev = true;
  //     console.log(this.unitDev);
  //   });
  // }

  getDevelopments(): void {
    this.developmentService.getDevelopments().subscribe(developments => { 
        this.developments = developments;
        console.log(developments) ;
    });
  }

  deleteDevelopment(value:Developments): void {
    this.developmentService.delete(value._id)
      .then(response => {
        this.alertService.success('Delete development successful', true);
        this.getDevelopments();
      },
      error => {
        alert(`The development could not be deleted, server Error.`);
      }
    );
  }

  // deleteUnit(value:Developments): void {
  //   this.developmentService.deleteUnit(value._id,value).then();
  // }

  editDevelopment(value:Developments): void {
    this.router.navigate(['admin/development/' + value._id + '/edit']);
  }

  viewDevelopment(value:Developments): void {
    this.router.navigate(['admin/development/' + value._id + '/detail']);
  }

  viewUnit(value:Developments): void {
    this.router.navigate(['admin/development/' + value._id + '/unit']);
  }

  // editUnit(value:Developments): void {
  //   this.router.navigate(['admin/development/unit/' + value._id + '/edit'])
  // }
}