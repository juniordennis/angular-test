import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment'
import { Appointments } from '../../models/appointments';
import { AppointmentService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';
import "rxjs/add/operator/takeWhile";

@Component({
    selector: 'my-appointment',
    templateUrl : './appointment.component.html',
    
    providers: [AppointmentService]
})

export class AdminAppointmentComponent implements OnInit {
	appointments: any;
    appointmentsModalId: string;
    subscription: boolean = true;
    contentClass: string = 'content';

    constructor(private appointmentService: AppointmentService, private router: Router, private sharedService: SharedService) 
    { 
        this.sharedService.collapse$.subscribe((collapse) => {
            if (collapse === true) {
                this.contentClass = 'content resize-margin-left';
            }
            else {
                this.contentClass = 'content'; 
            }
        }); 
    }

    @ViewChild('modalDelete') modalDelete:any;

    ngOnInit(): void {
        this.getAll();
    }

    getAll(){
        this.appointmentService.getAll().takeWhile(()=> this.subscription).subscribe(appointments => { 
            this.appointments = appointments;
            for (var z = 0; z < this.appointments.length; ++z) {
                this.appointments[z].chosen_time.date = moment(this.appointments[z].chosen_time.date, "dddd, DD-MM-YYYY").format("DD-MM-YYYY");
            }
            

            
        });
    }

    delete(){
        this.appointmentService.delete(this.appointmentsModalId)
        .then(
            response => {
                this.hideModalDelete();
                this.ngOnInit();
            },
            error => {
                console.log(error)
            }
        );
    }

    showModalDelete(id:string):void {
        this.modalDelete.show();
        this.appointmentsModalId = id;
    }

    hideModalDelete():void {
        this.modalDelete.hide();
        this.appointmentsModalId = '';
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}