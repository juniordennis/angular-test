import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgreementService } from '../../../client/services/agreement.service';
import { AttachmentService } from '../../../client/services/attachment.service';
import { NotificationsService } from 'angular2-notifications';
import { SharedService } from '../../../client/services/shared.service';
import { url } from '../../../global';
import "rxjs/add/operator/takeWhile";

@Component({
    selector: 'my-certificate',
    templateUrl : './certificate.component.html',
    
})

export class AdminCertificateComponent implements OnInit {
    subscription: boolean = true;
    agreements: any;
    model: any = {};
    link: string;
    imageURL: string;
    agreementId: string;
    agreementTemp: any;
    agreement: any;
    header: any;
    contentClass: string = 'content';

    @ViewChild('modalPayment') modalPayment:any;
    @ViewChild('modalCertificate') modalCertificate:any;
    @ViewChild('modalImage') modalImage:any;

    constructor(
        private router: Router,
        private agreementService: AgreementService,
        private attachmentService: AttachmentService,
        private notificationsService: NotificationsService,
        private sharedService: SharedService
        ) 
    { 
        this.sharedService.collapse$.subscribe((collapse) => {
            if (collapse === true) {
                this.contentClass = 'content resize-margin-left';
            }
            else {
                this.contentClass = 'content'; 
            }
        }); 
    }

    ngOnInit() {
        this.getAll();
        this.link = url + 'attachments';

        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
    }

    getAll() {
        this.agreementService.getCertificateList().takeWhile(()=> this.subscription).subscribe(agreements => { 
            this.agreements = agreements;
        });
    }

    getAgreement(id:string) {
        this.agreementService.getCertificateList().takeWhile(()=> this.subscription).subscribe(agreements => { 
            this.agreementTemp = agreements.filter((data:any)=> data.idagreement == id);
            this.agreement = this.agreementTemp[0];
        });
    }

    openModalPayment(id:string) {
        this.getAgreement(id);
        this.agreementId = id;
        this.modalPayment.show();
    }
    hideModalPayment() {
        this.modalPayment.hide();
    }

    openModalCertificate(id:string) {
        this.agreementId = id;
        this.modalCertificate.show();
    }
    hideModalCertificate() {
        this.modalCertificate.hide();
    }

    openModalImage(urlImage:string) {
        this.imageURL = urlImage;
        this.modalImage.show();
    }
    hideModalImage() {
        this.modalImage.hide();
    }

    uploadPaymentProof() {
        if(this.model.attachment) {
            this.agreementService.uploadProofLandlord(this.agreementId,this.model)
            .then(
                response => {
                    this.notificationsService.success(
                        'Success',
                        'Proof Uploaded',
                        )
                    console.log(response);
                },
                error => {
                    console.log(error);
                }
                );
            this.getAll();
            this.hideModalPayment();
        } else {
            this.notificationsService.error('','Please upload the image first')
        }
    }

    uploadCertificate() {
        this.agreementService.uploadStampCertificate(this.agreementId,this.model)
        .then(
            response => {
                this.notificationsService.success(
                    'Success',
                    'Certificate Uploaded',
                    )
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
        this.getAll();
        this.hideModalCertificate();
    }

    handleFileSuccessCertificate(event:any): void {
        this.model.stamp_certificate = event.file.uploadedId
    }

    handleFileSuccessPayment(event:any): void {
        this.model.attachment = event.file.uploadedId
        console.log(this.model.attachment,'attachment')
    }

    handleFileFailure(event:any): void {
        console.log('fail', event);
    }

    handleFileSending(event:any): void {
        console.log('send', event);
    }

    handleFileAdded(event:any): void {
        console.log('add', event);
    }

    handleFileRemoved(event:any, type:any): void {
        this.attachmentService.delete(event.uploadedId)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}