import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SharedService } from '../../../client/services/shared.service';
import { Amenities } from '../../modelsV2/amenities';
import { AmenityService, AlertService } from '../../service/index';

@Component({
  selector: 'my-amenity',
  templateUrl : './amenity.component.html',
  
  providers: [AmenityService, AlertService]
})

export class AdminAmenityComponent implements OnInit {
	amenities: Amenities[];
  contentClass: string = 'content';

  constructor(private amenityService: AmenityService, private router: Router, private alertService: AlertService, private sharedService: SharedService) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.getAmenities();
  }

  getAmenities(): void {
    this.amenityService.getAmenities().subscribe(amenities => { 
      this.amenities = amenities;
    });
  }

  deleteAmenity(value:Amenities): void {
    this.amenityService.delete(value._id)
      .then(response => {
        this.alertService.success('Delete bank successful', true);
        this.getAmenities();
      },
      error => {
        alert(`The bank could not be deleted, server Error.`);
      }
    );
  }


  editAmenity(value:Amenities): void {
    this.router.navigate(['admin/amenity/' + value._id + '/edit']);
  }
}
