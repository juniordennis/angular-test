import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { SharedService } from '../../../client/services/shared.service';
import { Amenities } from '../../models/amenities';
import { AmenityService, AttachmentService, AlertService } from '../../service/index';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'my-amenity',
  templateUrl : './edit-amenity.component.html',
  
  providers: [AmenityService, AttachmentService, AlertService]
})

export class AdminEditAmenityComponent {
	myForm : FormGroup;
	amenities: Amenities[];
  dataAmenity: any = {};
  amenityId: any;
  header: any;
  model: any = {};
  contentClass: string = 'content';

	constructor(private formbuilder: FormBuilder, private amenityService : AmenityService, private alertService: AlertService, private attachmentService: AttachmentService,
              private activateRoute: ActivatedRoute, private location: Location, private router: Router, private sharedService: SharedService) 
  { 
    this.location = location; 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

	ngOnInit(): void {
    this.activateRoute.params.subscribe((params: Params) => {
      this.amenityId = params['id'];
    });

    if (this.amenityId != null) {
      this.amenityService.getById(this.amenityId)
        .subscribe(amenity => {
        this.dataAmenity = amenity;
      });
    }

		this.myForm = this.formbuilder.group({
      name : ['', Validators.required],
      description : ['', Validators.required]
		});

    let currentUser = JSON.parse(localStorage.getItem('authToken'));
    if (currentUser && currentUser.token) {
      this.header = {'Authorization': 'Bearer ' + currentUser.token}
    }
	}

	createAmenity(): void {
    console.log(this.dataAmenity);
    this.amenityService.create(this.dataAmenity)
    .then(
      data => {
          this.alertService.success('Create amenity successful', true);
          this.router.navigate(['/admin/amenity']);
      },
      error => {
          console.log(error);
          alert(`The amenity could not be save, server Error.`);
      }
    );
  }

  updateAmenity(): void {
    this.amenityService.update(this.dataAmenity)
    .then(
      response => {
        this.alertService.success('Update amenity successful', true);
        this.router.navigate(['/admin/amenity']);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  handleFileSuccess(event:any, image:any): void {
    console.log('success', event, image);
    this.attachmentService.getById(event.file.uploadedId)
      .subscribe(data=>{
          console.log(data)
          this.dataAmenity.icon = data;
      })
  }

  handleFileFailure(event:any, image:any): void {
    console.log('fail', event, image);
  }

  handleFileSending(event:any, image:any): void {
    console.log('send', event, image);
  }

  handleFileAdded(event:any): void {
    console.log('add', event);
  }

  handleFileRemoved(event:any): void {
    console.log('remove', event);
    this.attachmentService.delete(event.uploadedId)
    .then(
        response => {
            console.log(response);
        },
        error => {
            console.log(error);
        }
    );
  }
}
