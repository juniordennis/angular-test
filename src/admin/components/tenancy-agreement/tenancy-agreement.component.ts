import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AgreementService } from '../../../client/services/agreement.service';
import { Agreements } from '../../modelsV2/agreements';
import { TenancyAgreementService } from '../../service/index';
import { AttachmentService } from '../../../client/services/attachment.service';
import { NotificationsService } from 'angular2-notifications';
import { SharedService } from '../../../client/services/shared.service';
import { url } from '../../../global';
import "rxjs/add/operator/takeWhile";

@Component({
    selector: 'my-ta',
    templateUrl : './tenancy-agreement.component.html',
    
    providers: [AgreementService]
})

export class AdminTenancyAgreementComponent implements OnInit {
    subscription: boolean = true;
	tenancyAgreements: Agreements[];
    report: any;
    ta: any;
    link: string;
    model: any = {};
    isReport: boolean;
    imageURL: string;
    header: any;
    agreement: any;
    contentClass: string = 'content';

    @ViewChild('modalReference') modalReference:any;
    @ViewChild('modalRefund') modalRefund:any;
    @ViewChild('modalTransfer') modalTransfer:any;

    constructor(
        private router: Router,
        private agreementService: AgreementService,
        private attachmentService: AttachmentService,
        private notificationsService: NotificationsService,
        private sharedService: SharedService
        ) 
    { 
        this.sharedService.collapse$.subscribe((collapse) => {
            if (collapse === true) {
                this.contentClass = 'content resize-margin-left';
            }
            else {
                this.contentClass = 'content'; 
            }
        }); 
    }

    ngOnInit(): void {
        this.isReport = false;
        this.getAll();
        this.link = url + 'attachments';

        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
    }

    getAll(): void {
        this.agreementService.getAllAdmin().takeWhile(()=> this.subscription).subscribe(agreements => { 
            this.tenancyAgreements = agreements;
            console.log(this.tenancyAgreements,agreements,'agreements')
        });
    }

    getReport(id:string) {
        this.isReport = true;
        this.agreementService.getTaReport(id)
        .takeWhile(()=> this.subscription)
        .subscribe(report => {
            this.report = report;
        });
        this.getTA(id);
    }

    getTA(id:string) {
        this.agreementService.getTA(id)
        .takeWhile(()=> this.subscription)
        .subscribe(ta => {
            this.ta = ta;
        });
    }

    getAgreement(id:string) {
        this.agreementService.getById(id)
        .takeWhile(()=> this.subscription)
        .subscribe(agreement => {
            this.agreement = agreement;
        });
    }

    openModalReference(id:string) {
        this.imageURL = id;
        this.modalReference.show();
    }

    openModalRefund(id:string) {
        this.getTA(id);
        this.modalRefund.show();
    }

    hideModalRefund() {
        this.modalRefund.hide();
    }

    openModalTransfer(id:string) {
        this.getAgreement(id);
        this.modalTransfer.show();
    }

    hideModalTransfer() {
        this.modalTransfer.hide();
    }

    uploadReferencePayment() {
        console.log(this.model)
        if (this.model.refund_confirm == undefined) {
            this.notificationsService.error(
                'Error',
                'Please Upload Proof before Confirm!',
                )
        } else {
            this.agreementService.uploadRefundProof(this.ta.payment._id, this.model)
            .then(
                data => {
                    this.notificationsService.success(
                        'Success',
                        'Refund Proof Uploaded',
                        )
                    this.hideModalRefund()
                },
                error => {
                    this.notificationsService.error(
                        'Error',
                        'Server error, please try again later',
                        )
                }
            );
        }
    }

    uploadTransfertoLandlord() {
        if(this.model.transfer) {
            this.notificationsService.success(
                'Success',
                'Transfer uploaded',
                )
            this.hideModalTransfer();
        } else {
            this.notificationsService.error(
                '',
                'Please upload the image',
                )
        }
    }

    viewInformation(value:any) {
        this.router.navigate(['admin/tenancy_agreement/details/' + value._id]);
    }

    handleFileSuccessRefund(event:any): void {
        this.model.refund_confirm = event.file.uploadedId
    }

    handleFileSuccessTransfer(event:any): void {
        this.model.transfer = event.file.uploadedId
    }

    handleFileFailure(event:any): void {
        console.log('fail', event);
    }

    handleFileSending(event:any): void {
        console.log('send', event);
    }

    handleFileAdded(event:any): void {
        console.log('add', event);
    }

    handleFileRemoved(event:any, type:any): void {
        this.attachmentService.delete(event.uploadedId)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
