import { Component, ViewChild, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BrowserModule, DomSanitizer,SafeResourceUrl} from '@angular/platform-browser';

import { Agreements } from '../../models/agreements';
import { AgreementService } from '../../../client/services/agreement.service';
import { AttachmentService } from '../../../client/services/attachment.service';
import { TenancyAgreementService, AlertService } from '../../service/index';
import { Observable} from 'rxjs/Observable';
import { NotificationsService } from 'angular2-notifications';
import { url } from '../../global';
import { SharedService } from '../../../client/services/shared.service';
import "rxjs/add/operator/takeWhile";

@Component({
    selector: 'user-information-loi',
    templateUrl : './detail-tenancy-agreement.component.html',
    
    providers: [TenancyAgreementService, AlertService],
})

export class AdminTenancyAgreementDetailComponent implements OnInit {
    myForm : FormGroup;
    model: any = {};
    tenancyAgreements: Agreements[];
    dataPayment: any = {};
    dataAgreement: any = {};
    users: any;
    ta_id: any;
    id: any;
    data: {};
    valueTransfer: any;
    pdfSrc = '../../../public/assets/images/intro.pdf';
    gfd_sda: any;
    isAccept: boolean;
    isReject: boolean;
    url: any;
    pageurl:SafeResourceUrl;
    header: any;
    subscription: boolean = true;
    contentClass: string = 'content';

    @ViewChild('modalAccept') modalAccept:any;
    @ViewChild('modalReject') modalReject:any;

    constructor(private domSanitizer: DomSanitizer,private formbuilder: FormBuilder,private tenancyAgreementService: TenancyAgreementService, private router: Router, 
        private activateRoute: ActivatedRoute, private alertService: AlertService, private notificationsService: NotificationsService,
        private agreementService: AgreementService,
        private attachmentService: AttachmentService,
        private sharedService: SharedService
        ) 
    {
        this.sharedService.collapse$.subscribe((collapse) => {
            if (collapse === true) {
                this.contentClass = 'content resize-margin-left';
            }
            else {
                this.contentClass = 'content'; 
            }
        }); 
    }

    ngOnInit() {
        this.activateRoute.params
        .takeWhile(()=> this.subscription)
        .subscribe((params: Params) => {
            this.ta_id = params['id'];
        });
        
        this.getTA();
        this.getAgreement();

        this.isAccept = false;
        this.isReject = false;
        this.url = url;
        this.pageurl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.pdfSrc);
        this.model.type = "tenancy_agreement";

        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
    }

    getAgreement() {
        this.agreementService.getById(this.ta_id)
        .takeWhile(()=> this.subscription)
        .subscribe(agreement => {
            this.dataAgreement = agreement;
            console.log(this.dataAgreement,'dataAgreement');
        });
    }

    getTA() {
        this.agreementService.getTA(this.ta_id)
        .takeWhile(()=> this.subscription)
        .subscribe(payment => {
            this.dataPayment = payment;
            console.log(this.dataPayment,'dataPayment');
        });
    }

    viewUserDetail(id:any) {
        this.router.navigate(['admin/user/' + id + 'detail']);
    }

    backToTA() {
        this.router.navigate(['admin/tenancy_agreement']);
    }

    openAcceptModal() {
        if (!this.model.payment_confirm) {
            this.notificationsService.error(
                '',
                'Upload the photo first before submit the data',
                )
        } else if (!this.model.receive_payment) {
            this.notificationsService.error(
                '',
                'Fee is required',
                )
        } else if (this.dataPayment.payment.fee[0].amount > this.model.receive_payment) {
            this.notificationsService.error(
                '',
                'Payment transfered is not enough',
                )
        } else {
            this.modalAccept.show()
        }
    }

    hideAcceptModal() {
        this.modalAccept.hide()
    }

    openRejectModal() {
        if (!this.model.receive_payment || !this.model.remarks) {
            this.notificationsService.error(
                '',
                'Please fill out remarks and transferred value',
                )
        } else {
            this.modalReject.show()
        }
    }

    hideRejectModal() {
        this.modalReject.hide()
    }

    acceptPayment() {
        this.agreementService.acceptPayment(this.ta_id, this.model)
        .then(
            res => {
                this.notificationsService.success(
                    'Successfully',
                    'Payment accepted',
                    )
            },
            error => {
                this.notificationsService.error(
                    'Error',
                    'Accept payment failed',
                    )
                console.log(error);
            }
        );
        this.isAccept = false;
        this.hideAcceptModal()
        this.getTA();
    }

    rejectPayment() {
        this.agreementService.rejectPayment(this.ta_id, this.model)
        .then(
            res => {
                this.notificationsService.success(
                    'Successfully',
                    'Payment rejected',
                    )
            },
            error => {
                this.notificationsService.error(
                    'Error',
                    'Reject payment failed',
                    )
                console.log(error);
            }
        );
        this.isReject = false;
        this.hideRejectModal()
        this.getTA();
    }

    handleFileSuccess(event:any, image:any): void {
        console.log('success', event, image);
        this.model.payment_confirm = event.file.uploadedId;
    }

    handleFileFailure(event:any, image:any): void {
        console.log('fail', event, image);
    }

    handleFileSending(event:any, image:any): void {
        console.log('send', event, image);
    }

    handleFileAdded(event:any): void {
        console.log('add', event);
    }

    handleFileRemoved(event:any): void {
        console.log('remove', event);
        this.attachmentService.delete(event.file.uploadedId)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}