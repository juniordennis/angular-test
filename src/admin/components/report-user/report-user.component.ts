import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';

import { ReportUsers } from '../../models/report-users';
import { ReportUserService } from '../../service/index';
import { NotificationsService } from 'angular2-notifications';
import { SharedService } from '../../../client/services/shared.service';

@Component({
  selector: 'my-report',
  templateUrl : './report-user.component.html',
  
  providers: [ReportUserService]
})

export class AdminReportUserComponent implements OnInit {
	reportUsers: any = [];
  reportUsersToShow: any = [];
  reportUserDetail: any = null;
  url: string;
  contentClass: string = 'content';

  constructor(
    private reportUserService: ReportUserService, 
    private router: Router, 
    private activateRoute: ActivatedRoute, 
    private _notificationsService: NotificationsService,
    private sharedService: SharedService
  ) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.getReportUsers();
    this.url = this.router.url;
  }

  getReportUsers(): void {
    this.reportUserService.getAll().subscribe((data:any) => {
      var groupName: any = null;
      this.reportUsersToShow = [];
      this.reportUsers = data;
        for (var i = 0; i < this.reportUsers.length; i++){
            if (this.reportUsers[i].reported.username!=groupName) {
                this.reportUsersToShow.push({
                        username        :  this.reportUsers[i].reported.username,
                        data : []
                    });
            }
            groupName = this.reportUsers[i].reported.username;
        }

        for (var i = 0; i < this.reportUsersToShow.length; i++) {
            var filterReport = this.reportUsers.filter(
                                (data:any) => data.reported.username == this.reportUsersToShow[i].username
                              )

            for (var j = 0; j < filterReport.length; j++){
                this.reportUsersToShow[i].data.push(filterReport[j]);
            }

        }      
    });
    
  }

  deleteReport(value: any): void {
    // var error: number = 0;
    // for (var i = 0; i < value.data.length; ++i) {
    //   this.reportUserService.delete(value.data[i]._id).then(
    //     data => {},
    //     error=> {
    //       error += 1;
    //     }
    //   );
    // }
    // if(error == 0){
    //    this._notificationsService.success(
    //                         'Success',
    //                         'User has been reported',
    //             )
    //    this.ngOnInit();
    // }else{
    //    this._notificationsService.error(
    //                         'Error',
    //                         'Server error',
    //             )
    // }

    this.reportUserService.delete(value.data[0].reported._id).then(
        data => {
              this._notificationsService.success(
                            'Success',
                            'User has been reported',
                )
              this.ngOnInit();
        },
        error=> {
          this._notificationsService.error(
                            'Error',
                            'Server error',
                )
        }
      );
  }

  reportsUser(value: any): void {
    this.reportUserService.reportsUser(value.data[0].reported._id).then(
      data =>{
        this._notificationsService.success(
                            'Success',
                            'Succes report user',
                )
        this.ngOnInit();
      },
      error => {
        this._notificationsService.error(
                            'Error',
                            'Server error',
                )
      }
    );
  }

  viewReport(value:any): void {
    this.reportUserDetail = value.data;
  }

  backToReport() {
    this.reportUserDetail = null;
  }
}
