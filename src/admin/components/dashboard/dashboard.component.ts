import { Component } from '@angular/core';
import { SharedService } from '../../../client/services/shared.service';
@Component({
  selector: 'my-dashboard',
  templateUrl : './dashboard.component.html',
  
})

export class AdminDashboardComponent {
  contentClass: string = 'content';

  constructor(private sharedService: SharedService) 
  {
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

	GoogleAnalytic(){
    window.open('https://analytics.google.com/analytics/web/');
  }
  MigrateChats(){
  	console.log("EF");
  }
}
