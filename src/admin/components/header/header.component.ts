import { Component, OnInit } from '@angular/core';
import { Hero } from './hero';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../client/services/authentication.service';
import { UserService } from '../../../client/services/user.service';
import { SharedService } from '../../../client/services/shared.service';
import { AdminSidebarComponent } from '../sidebar/sidebar.component';

@Component({
    selector: 'my-header',
    templateUrl : './header.component.html',
    styleUrls: ['./../../../public/assets/admin/css/animate.min.css','./../../../public/assets/admin/components/header.css',
    './../../../public/assets/admin/css/style.min.css','./../../../public/assets/admin/css/style-responsive.min.css'],
    providers: [AuthenticationService, AdminSidebarComponent]
})

export class AdminHeaderComponent implements OnInit {
    user: any;

    constructor(
        private authService: AuthenticationService,
        private router: Router,
        private sharedService: SharedService,
        private sidebar: AdminSidebarComponent,
        private userService: UserService,
        ) {}

    ngOnInit() {
        this.getUser();
    }

    getUser() {
        this.userService.getByToken().subscribe(user => { 
            this.user = user;
        });
    }

    switchSidebar() {
        let style = this.sharedService.changeAdminSidebarClass();
    }

    toggleSidebar() {
        this.sharedService.changeAdminSidebarClass();   
    }

    logout(){
        this.authService.logout()
        this.router.navigate(['/admin/login']);
    }
}