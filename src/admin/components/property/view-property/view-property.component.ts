import { Component, OnInit, Input, Output, ViewChild } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Properties } from '../../../modelsV2/properties';
import { PropertyService, AlertService, AttachmentService } from '../../../service/index';
import { Observable} from 'rxjs/Observable';
import { BrowserModule, DomSanitizer,SafeResourceUrl } from '@angular/platform-browser';
import { GoogleMapsAPIWrapper, MapsAPILoader } from 'angular2-google-maps/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { url, googleMapStyles } from '../../../global';
import { NotificationsService } from 'angular2-notifications';
import { ModalDirective } from 'ng2-bootstrap';
import { LightboxModule } from 'primeng/primeng';
import { SharedService } from '../../../../client/services/shared.service';


@Component({
  selector: 'view-property',
  templateUrl : './view-property.component.html',
  styleUrls: ['./../../../../public/assets/admin/components/user.css','./../../../../public/assets/admin/components/blog.css',
              './../../../../public/assets/admin/components/development.css','./../../../../public/assets/admin/components/property.css'],
  providers: [PropertyService, AlertService, AttachmentService],
  styles: [`
      :host >>>  p-lightbox img {
        max-width: 100%;
      }
      :host >>> .ui-lightbox-content{
        max-width: 560px !important;
        width:100% !important;
        height:100% !important;
      }
  `]
})

export class AdminViewPropertyComponent implements OnInit {
  @ViewChild('myMap') myMap:any;
  @ViewChild('modalReject') public modalReject: ModalDirective;
  @ViewChild('modalApprove') public modalApprove: ModalDirective;
  properties: Properties[];
  dataProperty: any;
  dataUser: any;
  amenitiesPreview: any[]= [];
  propertyId: any;
  zoom: number = 15;
  maxZoom: number = 10;
  lat: number;
  lng: number;
  reported: any;
  user: any;
  pageurl:SafeResourceUrl;
  public customStyle  = googleMapStyles;
  model: any = {};
  link: any = url+'attachments';
  rejectSubmitted: boolean = false;
  loading: boolean = false;
  header: any;
  imageUser: any[];
  imageLandlordFront: any[];
  imageLandlordBack: any[];
  imageCertificate: any[];
  contentClass: string = 'content';

  constructor(private propertyService: PropertyService, private router: Router, private activateRoute: ActivatedRoute, private attachmentService: AttachmentService,
              private domSanitizer: DomSanitizer,private formbuilder: FormBuilder, private alertService: AlertService, private _notificationsService: NotificationsService, private sharedService: SharedService) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.activateRoute.params.subscribe((params: Params) => {
      this.propertyId = params['id'];
    });
    this.getProperty(this.propertyId);
    let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
  }

  getProperty(id:string):void {
    this.propertyService.getPropertyById(id)
      .subscribe(property => {
      this.dataProperty = property;
      this.lat = +this.dataProperty.address.coordinates[1];
      this.lng = +this.dataProperty.address.coordinates[0];
      console.log(this.lat+' '+this.lat)
      this.reported = this.dataProperty.owner.user.blocked_users;
      console.log(this.dataProperty);
      if(this.dataProperty.confirmation.status != 'pending'){
        if(this.dataProperty.confirmation.proof.type == 'application/pdf'){
          this.pageurl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.dataProperty.confirmation.proof.url);
        }else if(this.dataProperty.confirmation.proof.type == 'image/jpeg' || this.dataProperty.confirmation.proof.type == 'image/png'){
          this.imageCertificate = [];
          this.imageCertificate.push({source: this.dataProperty.confirmation.proof.url, thumbnail: this.dataProperty.confirmation.proof.url, title:this.dataProperty.confirmation.proof.name});
        }
      }
      if(this.dataProperty.owner.user.picture){
        this.imageUser = [];
        this.imageUser.push({source: this.dataProperty.owner.user.picture.url, thumbnail: this.dataProperty.owner.user.picture.url, title:this.dataProperty.owner.user.picture.name});
      }
      if(this.dataProperty.owner.user.landlord.data.identification_proof.front != null){
        this.imageLandlordFront = [];
        this.imageLandlordFront.push({source: this.dataProperty.owner.user.landlord.data.identification_proof.front.url, thumbnail: this.dataProperty.owner.user.landlord.data.identification_proof.front.url, title:this.dataProperty.owner.user.landlord.data.identification_proof.front.name});
      }
      if(this.dataProperty.owner.user.landlord.data.identification_proof.back != null){
        this.imageLandlordBack = [];
        this.imageLandlordBack.push({source: this.dataProperty.owner.user.landlord.data.identification_proof.back.url, thumbnail: this.dataProperty.owner.user.landlord.data.identification_proof.back.url, title:this.dataProperty.owner.user.landlord.data.identification_proof.back.name});
      }
    });
  }

  approve(id:string): void {
    console.log(this.model)
    if (!this.model.proofId) {
      this._notificationsService.error(
                            'Error',
                            'Please Upload Proof before Confirm!',
                )
      this.modalApprove.hide();
    }
    else {
      this.loading = true;
      this.propertyService.approve(this.model, id)
      .then(
        data => {
             this._notificationsService.success(
                            'Success',
                            'Listing confirmed',
                )
            this.getProperty(id);
            this.modalApprove.hide();
            this.loading = false;
        },
        error => {
            this._notificationsService.error(
                            'Error',
                            'Server error, please try again later',
                )
            this.loading = false;
        }
      );
    }
  }

  reject(id:string): void {
    console.log(this.model)
    if (!this.model.proofId) {
       this._notificationsService.error(
                            'Error',
                            'Please Upload Proof before Confirm!',
                )
       this.modalReject.hide();
    }
    else{
      if(this.model.remarks){
        this.loading = true;
        this.propertyService.reject(this.model, id)
          .then(
            data => {
                this._notificationsService.success(
                                'Success',
                                'Listing rejected',
                    )
                this.getProperty(id);
                this.modalReject.hide();
                this.rejectSubmitted = false;
                this.loading = false;
            },
            error => {
                this._notificationsService.error(
                                'Error',
                                'Server error, please try again later',
                    )
                this.rejectSubmitted = false;
            }
          );
      }else{
        this._notificationsService.error(
                            'Error',
                            'Please Fill out reason to reject!',
                )
      }
    }
  }

  backToProperty() {
    this.router.navigate(['admin/property']);
  }

  handleFileSuccess(event:any, image:any): void {
    console.log('success', event, image);
    this.model.proofId = event.file.uploadedId;
  }

  handleFileFailure(event:any, image:any): void {
    console.log('fail', event, image);
  }

  handleFileSending(event:any, image:any): void {
    console.log('send', event, image);
  }

  handleFileAdded(event:any): void {
    console.log('add', event);
  }

  handleFileRemoved(event:any): void {
    this.model.proofId = null;;
    console.log('remove', event);
    this.attachmentService.delete(event.uploadedId)
    .then(
        response => {
            console.log(response);
        },
        error => {
            console.log(error);
        }
    );
  }

  openModal(type:string){
    if(!this.model.proofId){
        this._notificationsService.error(
                            'Error',
                            'Please Upload Proof before Confirm!',
        )
    }else{
      if(type=="reject"){
        this.modalReject.show();
      }else{      
        this.modalApprove.show();
      }
    }
  }
  viewPdf(){
    this.attachmentService.downloadPDF(this.dataProperty.confirmation.proof.url).subscribe(
          (res:any) => {
            var fileURL = URL.createObjectURL(res);
            window.open(fileURL);
          }
      );
  }
}