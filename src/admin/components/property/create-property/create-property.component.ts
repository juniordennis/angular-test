import { Component, OnInit, ViewChild, NgZone, ElementRef, HostListener, Inject } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core';
import { MapsAPILoader } from 'angular2-google-maps/core';
import { Observable, Observer } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { PropertyService, AmenityService, AttachmentService, UserService } from '../../../service/index';
declare var google: any;
import { SelectItem } from 'primeng/primeng';
import { url, googleMapStyles } from '../../../global';
import { DOCUMENT } from "@angular/platform-browser";
import { SharedService } from '../../../../client/services/shared.service';
// import { PageScrollService, PageScrollInstance} from 'ng2-page-scroll';


@Component({
  selector: 'property',
  templateUrl: './create-property.component.html',
  styles: [`
    centered {
        display: block;
        margin-left: auto;
        margin-right: auto
    }
    :host >>> .popover {
      background-color: #f0f0f0;
      color: #848a93;
    }
    :host >>> .popover>.arrow:after {
      border-top-color: #009688;
    }
    .required-dropzone {
        background-color: #1d85ff;
    }
  `],
  providers: [PropertyService, AmenityService, AttachmentService, UserService]
})

export class AdminCreatePropertyComponent implements OnInit {
    @ViewChild('myMap') myMap:any;
    @ViewChild('descriptionInput') descriptionInput:ElementRef;
    @ViewChild('living1') living1:any;
    isPage:number=1;
    isIndividual: boolean;
    isCompany: boolean;
    model: any = {};
    public form: FormGroup;
    geocoder: any;
    filtered :boolean = false;
    zoom: number = 11;
    maxZoom: number = 15;
    lat: number;
    lng: number;
    developments: any[]= []
    developmentsForSelectInput: any[]=[];
    public customStyle  = googleMapStyles;
    selectedDevelopment: any;
    development: any;
    amenities: any[]= []
    amenitiesPreview: any[]= []
    picturesPreview: any= {
        living:'',
        dining:'',
        bed:'',
        toilet:'',
        kitchen:''
    }
    link: any = url+'attachments';
    header: any;
    pictures:any = {};
    front: any = null;
    back: any = null;
    elementFrontOwners: string[]=[];
    elementBackOwners: string[]=[];
    elementFrontShareholders: string[]=[];
    elementBackShareholders: string[]=[];
    elementCompanyDocuments: string[]=[];
    idFrontOwners: string[]=[];
    idBackOwners: string[]=[];
    idFrontShareholders: string[]=[];
    idBackShareholders: string[]=[];
    idCompanyDocument: string[]=[];
    ownerFront: boolean = false;
    ownerBack: boolean = false;
    schedules: any = [];
    documentsGhost: any = [];
    userInfo: any = {};
    step3Valid: boolean = false;
    time_h: any[] = [
                        {label: '01', value: '1'},
                        {label: '02', value: '2'},
                        {label: '03', value: '3'},
                        {label: '04', value: '4'},
                        {label: '05', value: '5'},
                        {label: '06', value: '6'},
                        {label: '07', value: '7'},
                        {label: '08', value: '8'},
                        {label: '09', value: '9'},
                        {label: '10', value: '10'},
                        {label: '11', value: '11'},
                        {label: '12', value: '12'},
                        {label: '13', value: '13'},
                        {label: '14', value: '14'},
                        {label: '15', value: '15'},
                        {label: '16', value: '16'},
                        {label: '17', value: '17'},
                        {label: '18', value: '18'},
                        {label: '19', value: '19'},
                        {label: '20', value: '20'},
                        {label: '21', value: '21'},
                        {label: '22', value: '22'},
                        {label: '23', value: '23'},
                        {label: '24', value: '24'},
                    ]
    id_property: string;
    editPage: boolean = false;
    selectedCompany: any= 'new';
    step1submitted:boolean= false;
    step2submitted:boolean= false;
    step3submitted:boolean= false;
    step4submitted:boolean= false;
    errorTimeTo:boolean = false;
    errorTime2:boolean = false;
    errorTime:boolean = false;
    errorTimeFrom:boolean = false;
    errorTimeTo2:boolean = false;
    errorDays:boolean = false;
    errorDaysElement:any = [];
    step4Data:number = 0;
    step4Message:boolean = false;
    globalError:number = 0;
    globalErrorMessage:boolean = false;
    days_selected:any = [];
    time_selected:any = [];
    errorStep4: number = 0;
    currentPosition: number=0;
    addressReadOnly: boolean=false;
    livingReqMessage: string;
    diningReqMessage: string;
    bedroomReqMessage: string;
    bathroomReqMessage: string;
    kitchenReqMessage: string;
    optionalMessage: string = "Drop files here to upload (Optional)";
    reqPictureInnerHtml = "<div class='dz-default dz-message'><span>Drop files here to upload (Required)</span></div>";
    optPictureInnerHtml = "<div class='dz-default dz-message'><span>Drop files here to upload (Optional)</span></div>";
    disableNextButton: boolean = false;
    contentClass: string = 'content';

    constructor(
                private router: Router,
                public  mapsApiLoader: MapsAPILoader,
                private propertyService: PropertyService,
                private amenityService: AmenityService,
                private attachementService: AttachmentService,
                private userService: UserService,
                private route: ActivatedRoute,
                private _notificationsService: NotificationsService,
                private _zone: NgZone,
                @Inject(DOCUMENT) private document: any,
                private elementRef:ElementRef,
                private sharedService: SharedService
                // private pageScrollService: PageScrollService
                ) {
        this.sharedService.collapse$.subscribe((collapse) => {
            if (collapse === true) {
                this.contentClass = 'content resize-margin-left';
            }
            else {
                this.contentClass = 'content'; 
            }
        }); 
        this.mapsApiLoader.load().then(() => {
            this.geocoder = new google.maps.Geocoder();
            // let autocomplete = new google.maps.places.Autocomplete(this.addressAutoComplete.nativeElement, {});
            // google.maps.event.addListener(this.geocoder, 'place_changed', () => {
            //     let place = autocomplete.getPlace();
            //     this.lat= place.geometry.location.lat();
            //     this.lng= place.geometry.location.lng();
            // });
        });
    }

    ngOnInit():void{
        this.route.params.subscribe(params => {
            this.id_property = params['id'];
        });
        this.lat= 1.352083;
        this.lng= 103.819836;

        this.pictures = {};
        this.pictures.living  =[{id:null, src:""},{id:null, src:''},{id:null, src:''},{id:null, src:''}]
        this.pictures.dining  =[{id:null, src:''},{id:null, src:''},{id:null, src:''},{id:null, src:''}]
        this.pictures.bed     =[{id:null, src:''},{id:null, src:''},{id:null, src:''},{id:null, src:''}]
        this.pictures.toilet  =[{id:null, src:''},{id:null, src:''},{id:null, src:''},{id:null, src:''}]
        this.pictures.kitchen =[{id:null, src:''},{id:null, src:''},{id:null, src:''},{id:null, src:''}]

        this.model.landlordData = {
            name: "",
            identification_type: "",
            identification_number: "",
            identification_proof:{
                front: "",
                back: ""
            }
        }
        this.userInfo.companies = []
        this.userInfo.landlord = {
            data:{
                name: "",
                identification_type: "",
                identification_number: "",
                identification_proof:{
                    front: "",
                    back: ""
                }
            }
        }
        this.model.amenities = []
        this.model.ownersData = []
        this.model.details = {
            size_sqf: null,
            size_sqm: null,
            bedroom: null,
            bathroom: null,
            price: null,
            psqft: null,
            psqm: null,
            available: "",
            furnishing: "",
            description: "",
            type: "",
            sale_date: "",
            property_type: "",
            tenure: "",
            completion_date: "",
            type_of_sale: "",
            purchaser_address_indicator: "",
            planning_region: "",
            planning_area: ""
        }
        this.model.address ={
            floor: '',
            unit: '',
            block_number: '',
            street_name: '',
            postal_code: '',
            coordinates: [],
            country: 'Singapore',
            full_address: '',
        }
        this.model.companyData = {
            name: "",
            registration_number: "",
            documents: [''],
        }
        // this.model.shareholders = [{
        //     name: "",
        //     identification_type: "",
        //     identification_number: "",
        //     identification_proof: {
        //         front: "",
        //         back: ""
        //     }
        // }]


        this.documentsGhost.push({
            id: '',
            src: ''
        })
        this.amenitiesPreview = []

      
        setTimeout(() => this.elementCompanyDocuments[0] ='#document0', 100)

        
        this.idCompanyDocument[0] = 'document0';

        this.getUserInfo();
        this.propertyService.getAllDevelopment()
                    .subscribe(data => {
                        this.developments = data;
                        for(var i = 0; i < this.developments.length ; i++){
                            this.developmentsForSelectInput.push({
                                label: this.developments[i].name,
                                value: this.developments[i]
                            })
                        }
                        this.developmentsForSelectInput
                      })
        this.amenityService.getAll()
                    .subscribe(data =>{
                        this.amenities = data;
                    })

        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
        this.livingReqMessage = this.diningReqMessage = this.bedroomReqMessage = this.bathroomReqMessage = this.kitchenReqMessage = "Drop files here to upload (Required)";
    }
    ngAfterViewInit()
    {
       //  var target = this.document.getElementById('descriptionInput').offsetTop;
       // console.log(target);
    }
    getUserInfo(){
        this.userService.getByToken().subscribe(data=>{
            this.userInfo = data;
            this.ownedTypeTrigger('ngOnInit');
            if(this.userInfo.landlord.data.name){
                this.model.landlordData = {
                    name: this.userInfo.landlord.data.name,
                    identification_type: this.userInfo.landlord.data.identification_type,
                    identification_number: this.userInfo.landlord.data.identification_number,
                    identification_proof:{
                        front: this.userInfo.landlord.data.identification_proof.front,
                    }
                }
                this.model.shareholders = []
                this.model.shareholders[0]= {
                    name:                     this.userInfo.landlord.data.name,
                    identification_type:      this.userInfo.landlord.data.identification_type,
                    identification_number: this.userInfo.landlord.data.identification_number,
                    identification_proof:{
                        front: this.userInfo.landlord.data.identification_proof.front,
                    }
                }
                if(this.userInfo.landlord.data.identification_proof.back){
                    if(this.userInfo.landlord.data.identification_proof.back !== null){
                        this.model.landlordData.identification_proof.back = this.userInfo.landlord.data.identification_proof.back;
                        this.model.shareholders[0].identification_proof.back = this.userInfo.landlord.data.identification_proof.back;
                    }
                }
                this.idFrontShareholders[0] = 'shareholdersFront0'
                this.idBackShareholders[0] = 'shareholdersBack0'
                setTimeout(() => this.elementFrontShareholders[0] ='#shareholdersFront0', 100)
                setTimeout(() => this.elementBackShareholders[0] ='#shareholdersBack0', 100)

                if(this.userInfo.landlord.data.owners.length > 0){
                    for (var i = 0; i < this.userInfo.landlord.data.owners.length; i++) {
                        this.model.ownersData.push({
                            name: this.userInfo.landlord.data.owners[i].name,
                            identification_type: this.userInfo.landlord.data.owners[i].identification_type,
                            identification_number: this.userInfo.landlord.data.owners[i].identification_number,
                            identification_proof: {
                                front: this.userInfo.landlord.data.owners[i].identification_proof.front,
                            }
                        })
                        if(this.userInfo.landlord.data.owners[i].identification_proof.back){
                            this.model.ownersData[i].identification_proof.back = this.userInfo.landlord.data.owners[i].identification_proof.back;
                        }
                        var index = this.model.ownersData.length-1
                        this.idFrontOwners.push('ownersFront'+index);
                        this.idBackOwners.push('ownersBack'+index);
                        setTimeout(() => this.elementFrontOwners.push('#ownersFront'+index), 100)
                        setTimeout(() => this.elementBackOwners.push('#ownersBack'+index), 100)
                    }
                }
            }else{
                this.model.shareholders = []
                this.model.shareholders[0] = {
                    name:                     '',
                    identification_type:      '',
                    identification_number: '',
                    identification_proof:{
                        front: '',
                        back: ''
                    }
                };
                this.idFrontShareholders[0] = 'shareholdersFront0'
                this.idBackShareholders[0] = 'shareholdersBack0'
                setTimeout(() => this.elementFrontShareholders[0] ='#shareholdersFront0', 100)
                setTimeout(() => this.elementBackShareholders[0] ='#shareholdersBack0', 100)
            }
            if(this.id_property != null){
                this.findPropertyById();
                this.editPage=true;
            }else{
                this.checkDraft();
            }
        })
    }
    checkDraft(){
        this.propertyService.checkDraft()
            .subscribe(data=>{
                if(data.length>0){
                    this.id_property=data[0]._id;
                    this.model.status= "draft";
                    if(!this.userInfo.landlord.data.name){
                        if(data[0].temp.owner) {
                            this.model.landlordData = {
                                name: data[0].temp.owner.name,
                                identification_type: data[0].temp.owner.identification_type,
                                identification_number: data[0].temp.owner.identification_number,
                                identification_proof:{
                                    front: data[0].temp.owner.identification_proof.front,
                                    back: data[0].temp.owner.identification_proof.back
                                }
                            }
                            this.elementFrontShareholders = [];
                            this.elementBackShareholders = [];
                            this.idFrontShareholders = [];
                            this.idBackShareholders = [];

                            this.model.shareholders[0]= {
                                name:                     data[0].temp.owner.name,
                                identification_type:      data[0].temp.owner.identification_type,
                                identification_number: data[0].temp.owner.identification_number,
                                identification_proof:{
                                    front: data[0].temp.owner.identification_proof.front,
                                    back: data[0].temp.owner.identification_proof.back
                                }
                            }
                            this.idFrontShareholders.push('shareholdersFront0');
                            this.idBackShareholders.push('shareholdersBack0');
                            setTimeout(() => this.elementFrontShareholders.push('#shareholdersFront0'), 100);
                            setTimeout(() => this.elementBackShareholders.push('#shareholdersBack0'), 100);
                        }

                        if(data[0].temp.shareholders.length > 0){
                            for (var i = 0; i < data[0].temp.shareholders.length; i++) {
                                this.model.ownersData.push({
                                    name: data[0].temp.shareholders[i].name,
                                    identification_type: data[0].temp.shareholders[i].identification_type,
                                    identification_number: data[0].temp.shareholders[i].identification_number,
                                    identification_proof: {
                                        front: data[0].temp.shareholders[i].identification_proof.front,
                                        back: data[0].temp.shareholders[i].identification_proof.back
                                    }
                                })
                                var index = this.model.ownersData.length-1
                                this.idFrontOwners.push('ownersFront'+index);
                                this.idBackOwners.push('ownersBack'+index);
                                setTimeout(() => this.elementFrontOwners.push('#ownersFront'+index), 100)
                                setTimeout(() => this.elementBackOwners.push('#ownersBack'+index), 100)

                                // this.model.shareholders.push({
                                //     name: data[0].temp.shareholders[i].name,
                                //     identification_type: data[0].temp.shareholders[i].identification_type,
                                //     identification_number: data[0].temp.shareholders[i].identification_number,
                                //     identification_proof: {
                                //         front: data[0].temp.shareholders[i].identification_proof.front,
                                //         back: data[0].temp.shareholders[i].identification_proof.back
                                //     }
                                // })
                                // var index = this.model.shareholders.length-1
                                // this.idFrontShareholders.push('shareholdersFront'+index);
                                // this.idBackShareholders.push('shareholdersBack'+index);
                                // setTimeout(() => this.elementFrontShareholders.push('#shareholdersFront'+index), 100)
                                // setTimeout(() => this.elementBackShareholders.push('#shareholdersBack'+index), 100)
                            }
                        }
                    }
                    this.buildModel(data[0]);
                }
            })
    }
    findPropertyById(){
        this.propertyService.getPropertyById(this.id_property)
            .subscribe(data => {
                this.editPage = true;
                this.model.status = data.status;
                this.buildModel(data);
            })
    }
    buildModel(data:any){
        //Step1
        if(data.development){
            this.development          = data.development
            this.model.development    = data.development._id;
            this.selectedDevelopment  = data.development;
        }

        this.model.address = data.address
        this.model.details = data.details
        this.model.details.bedroom = this.model.details.bedroom.toString()
        this.model.details.bathroom = this.model.details.bathroom.toString()
        this.model.details.available = new Date(data.details.available)
        this.model.address.coordinates[0] = this.lng = +data.address.coordinates[0];
        this.model.address.coordinates[1] = this.lat = +data.address.coordinates[1];
        this.filtered = true;
        for (var i = 0; i < data.amenities.length; i++) {
            this.model.amenities.push(data.amenities[i]._id)
            this.amenitiesPreview.push(data.amenities[i])
        }

        //Step 2
        for (var i = 0; i < data.pictures.living.length; i++) {
            this.pictures.living[i].id  = data.pictures.living[i]._id
            this.pictures.living[i].src = data.pictures.living[i].url
            if(i==0){
                this.picturesPreview.living = this.pictures.living[i].src
            }
        }
        for (var i = 0; i < data.pictures.dining.length; i++) {
            this.pictures.dining[i].id  = data.pictures.dining[i]._id
            this.pictures.dining[i].src = data.pictures.dining[i].url
            if(i==0){
                this.picturesPreview.dining = this.pictures.dining[i].src
            }
        }
        for (var i = 0; i < data.pictures.bed.length; i++) {
            this.pictures.bed[i].id  = data.pictures.bed[i]._id
            this.pictures.bed[i].src = data.pictures.bed[i].url
            if(i==0){
                this.picturesPreview.bed = this.pictures.bed[i].src
            }
        }
        for (var i = 0; i < data.pictures.toilet.length; i++) {
            this.pictures.toilet[i].id  = data.pictures.toilet[i]._id
            this.pictures.toilet[i].src = data.pictures.toilet[i].url
            if(i==0){
                this.picturesPreview.toilet = this.pictures.toilet[i].src
            }
        }
        for (var i = 0; i < data.pictures.kitchen.length; i++) {
            this.pictures.kitchen[i].id  = data.pictures.kitchen[i]._id
            this.pictures.kitchen[i].src = data.pictures.kitchen[i].url
            if(i==0){
                this.picturesPreview.kitchen = this.pictures.kitchen[i].src
            }
        }

        //Step3
        if(this.userInfo.username != data.owner.user.username){
            this.model.landlordData = {
                    name: data.owner.user.landlord.data.name,
                    identification_type: data.owner.user.landlord.data.identification_type,
                    identification_number: data.owner.user.landlord.data.identification_number,
                    identification_proof:{
                        front: data.owner.user.landlord.data.identification_proof.front,
                    }
                }
                this.model.shareholders = []
                this.model.shareholders[0]= this.model.landlordData
                if(data.owner.user.landlord.data.identification_proof.back){
                    if(data.owner.user.landlord.data.identification_proof.back !== null){
                        this.model.landlordData.identification_proof.back = data.owner.user.landlord.data.identification_proof.back;
                        this.model.shareholders[0].identification_proof.back = data.owner.user.landlord.data.identification_proof.back;
                    }
                }

                if(data.owner.user.landlord.data.owners.length > 0){
                    for (var i = 0; i < data.owner.user.landlord.data.owners.length; i++) {
                        this.model.ownersData.push({
                            name: data.owner.user.landlord.data.owners[i].name,
                            identification_type: data.owner.user.landlord.data.owners[i].identification_type,
                            identification_number: data.owner.user.landlord.data.owners[i].identification_number,
                            identification_proof: {
                                front: data.owner.user.landlord.data.owners[i].identification_proof.front,
                            }
                        })
                        if(data.owner.user.landlord.data.owners[i].identification_proof.back){
                            this.model.ownersData[i].identification_proof.back = data.owner.user.landlord.data.owners[i].identification_proof.back;
                        }
                    }
                }
        }

        if(data.owned_type == "individual"){
            this.isIndividual = true;
            this.isCompany = false;
        }else if(data.owned_type == "company"){
            this.isCompany = true
            this.isIndividual = false;
            if(data.owner.company.name){
                if(this.model.status == 'draft'){
                    this.selectedCompany = 'new';
                }else{
                    this.selectedCompany = data.owner.company;
                    this.companyChange();
                }
            }
        }


        //Step4
        this.schedules = [];
        var groupName: any = null;
        for (var i = 0; i < data.schedules.length; i++){
            if (data.schedules[i].backup_id!=groupName) {
                this.schedules.push({
                        days        :  [],
                        start_date : new Date(data.schedules[i].start_date),
                        time_slot  : [],
                        backup_id : data.schedules[i].backup_id
                    });
            }
            groupName = data.schedules[i].backup_id;
        }

        for (var i = 0; i < this.schedules.length; i++) {
            var singleSchedule = data.schedules.filter(function(result:any) {
                                return result.backup_id === i.toString();
                              })

            for (var j = 0; j < singleSchedule.length; j++){
                if(this.schedules[i].days.indexOf(singleSchedule[j].day)== -1){
                    this.schedules[i].days.push(singleSchedule[j].day)
                }
                if(this.schedules[i].days.length == 1){
                    this.schedules[i].time_slot.push({
                        time_from_h : singleSchedule[j].time_from.slice(-5,-3),
                        time_from_m : singleSchedule[j].time_from.slice(-2),
                        time_to_h   : singleSchedule[j].time_to.slice(-5,-3),
                        time_to_m   : singleSchedule[j].time_to.slice(-2),
                        error       : 0,
                    })
                }
            }

        }

    }
    searchByPostalCode(postalCode:any){
        let self = this;
        var postalcode = postalCode;
        self.addressReadOnly = false;
        this.model.address.street_name = "";
        this.model.address.block_number=  "";
                var exactAdresLocation: any;
                this.geocoder.geocode({ 'componentRestrictions':
                { 'postalCode': postalcode }, region: "sg" }, function (results:any, status:any) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        // get the LatLng of the postalcode
                        self.filtered = true;
                        var result = results[0].geometry.location;

                        self.geocoder.geocode({ latLng: results[0].geometry.location }, function (results:any, status:any) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var address:any;
                                var model: any= {};
                                model.address = {};
                                model.address.coordinates = [];

                                // get the address:s
                                if (results[0].types[0] == 'street_address' && results[0].address_components[1]) {
                                    address  = results[0].address_components[0].long_name + " " + results[0].address_components[1].long_name;
                                    self.model.address.street_name = results[0].address_components[1].long_name
                                    self.model.address.block_number=  results[0].address_components[0].long_name;
                                }
                                else if (results[0].types[0] == 'route') {
                                    address                    =  results[0].address_components[0].long_name;
                                    self.model.address.street_name =  results[0].address_components[0].long_name;
                                }

                                self.geocoder.geocode({ 'address': address, 'componentRestrictions':
                                { 'postalCode': postalcode }, region: "sg" }, function (results:any, status:any) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        self.lng = results[0].geometry.location.lng();
                                        self.lat = results[0].geometry.location.lat()
                                    }
                                });
                            }
                        });

                    }
                });
            // });
    }
    check(command: string){
        //Step 1

        this.model.address.coordinates[0] = this.lng;
        this.model.address.coordinates[1] = this.lat;
        this.model.address.full_address   = this.model.address.block_number+ ' ' +
                                       this.model.address.street_name + ', ' +
                                       'Singapore'               + ' ' +
                                       this.model.address.postal_code;
        this.model.details.psqft    = parseInt(this.model.details.psqft)
        this.model.details.size_sqm = this.model.details.size_sqf/10.764;
        this.model.details.size_sqm = this.model.details.size_sqm.toFixed(2)
        this.model.details.psqm     = this.model.details.price/this.model.details.size_sqm;
        this.model.details.psqm     = this.model.details.psqm
        //Step 2
        this.model.pictures = {};
        this.model.pictures.living      = [];
        this.model.pictures.dining      = [];
        this.model.pictures.bed         = [];
        this.model.pictures.toilet      = [];
        this.model.pictures.kitchen     = [];
        for (var i = 0; i < 4; i++) {
            if(this.pictures.living[i].id != null){
                this.model.pictures.living.push(this.pictures.living[i].id)
            }
            if(this.pictures.dining[i].id != null){
                this.model.pictures.dining.push(this.pictures.dining[i].id)
            }
            if(this.pictures.bed[i].id != null){
                this.model.pictures.bed.push(this.pictures.bed[i].id)
            }
            if(this.pictures.toilet[i].id != null){
                this.model.pictures.toilet.push(this.pictures.toilet[i].id)
            }
            if(this.pictures.kitchen[i].id != null){
                this.model.pictures.kitchen.push(this.pictures.kitchen[i].id)
            }
        }

        //Step3
        if(this.isIndividual == true){
            this.model.owned_type = "individual"

        }else if(this.isCompany == true){
            this.model.owned_type = "company"

        }

        //Step4
        this.model.schedules = [];

        for (var i = 0; i < this.schedules.length; i++) {
                for (var j = 0; j < this.schedules[i].days.length; j++) {
                    for (var k = 0; k < this.schedules[i].time_slot.length; k++) {
                        this.model.schedules.push({
                            day         : this.schedules[i].days[j],
                            start_date  : this.schedules[i].start_date,
                            time_from   : this.schedules[i].time_slot[k].time_from_h+":"+this.schedules[i].time_slot[k].time_from_m,
                            time_to     : this.schedules[i].time_slot[k].time_to_h+":"+this.schedules[i].time_slot[k].time_to_m,
                            backup_id   : this.schedules[i].backup_id
                        })

                    }
                }
            }
        if(command=='prev'){
            if(this.isPage != 3) {
                this.step3Valid = false;
                this.prev();
            }else{
                this.prev();
            }

        }else if(command=='next'){
            this.next();
            window.scroll(0,0);
        }else if(command=='later'){
            if(this.isPage == 4) {
                if(this.globalError > 0) {
                    this.globalErrorMessage = true;
                }
                else{
                    this.preDraft()
                }
            }
            else{
                this.preDraft()
            }
        }
    }
    preCheck(valid:any, command:string){
        if(valid){
            if(this.isPage == 3) {
              var errorStep3:number = 0
              if(this.isIndividual){
                  if(this.model.landlordData.name&&
                    this.model.landlordData.identification_proof.front&&
                    this.model.landlordData.identification_type&&
                    this.model.landlordData.identification_number
                    ){
                    if(this.model.ownersData.length > 0){
                        for (var i = 0; i < this.model.ownersData.length; ++i) {
                            if(!this.model.ownersData[i].name||
                                !this.model.ownersData[i].identification_type||
                                !this.model.ownersData[i].identification_number||
                                !this.model.ownersData[i].identification_proof.front
                                ){
                                errorStep3 += 1;
                            }
                        }
                    }
                    if(errorStep3==0){
                        this.step3submitted=false;
                        this.check(command)
                    }
                  }
              }else{
                  if(this.model.companyData.name&&
                    this.model.companyData.registration_number&&
                    this.model.shareholders[0].name&&
                    this.model.shareholders[0].identification_type&&
                    this.model.shareholders[0].identification_number&&
                    this.model.shareholders[0].identification_proof.front
                    ){

                    for (var i = 0; i < this.model.companyData.documents.length; ++i) {
                        if(!this.model.companyData.documents[i]){
                            errorStep3 += 1;
                        }
                    }

                    if(this.model.shareholders.length > 1){
                        for (var j = 1; j < this.model.shareholders.length; ++j) {
                            if(!this.model.shareholders[j].name||
                                !this.model.shareholders[j].identification_type||
                                !this.model.shareholders[j].identification_number||
                                !this.model.shareholders[j].identification_proof.front
                                ){
                                errorStep3 += 1;
                            }
                        }
                    }
                    if(errorStep3==0){
                        this.step3submitted =false;
                        this.check(command)
                    }
                  }
              }
            }else if(this.isPage == 4) {
                this.errorStep4 = 0
                for (var i = 0; i < this.schedules.length; ++i) {
                    if(!this.schedules[i].start_date || this.schedules[i].days.length==0){
                        this.errorStep4 += 1;
                    }
                    for (var j = 0; j < this.schedules[i].time_slot.length; j++) {
                        if(!this.schedules[i].time_slot[j].time_to_h||
                            !this.schedules[i].time_slot[j].time_to_m||
                            !this.schedules[i].time_slot[j].time_from_m||
                            !this.schedules[i].time_slot[j].time_from_h
                            ){
                        this.errorStep4 += 1;
                        }
                    }
                }
                if(this.errorStep4==0){
                    this.validateSchedule();
                    if(this.globalError == 0) {
                        if(this.schedules.length == 0) {
                            this._notificationsService.error(
                                    'Error', 'You not have any schedule yet.',
                            )
                        }else{
                            this.step1submitted = this.step2submitted = this.step3submitted =this.step4submitted =false;
                            this.check(command)
                        }                           
                    }else{
                        this._notificationsService.error(
                            'Error',
                            'You have error in this page',
                        )
                    }
                }
            }else if(this.isPage == 1){
                if(this.model.address.postal_code.toString().length > 5 ){
                    this.step1submitted = false;
                    this.check(command);
                }
            }else if(this.isPage == 2){
                var message = '';
                var emptyLivingPicture :number = this.checkEmptyPicture('living');
                var emptyDiningPicture :number = this.checkEmptyPicture('dining');
                var emptyBedPicture    :number = this.checkEmptyPicture('bed');
                var emptyToiletPicture :number = this.checkEmptyPicture('toilet');
                var emptyKitchenPicture:number = this.checkEmptyPicture('kitchen');
                if((emptyLivingPicture==4)&&(emptyDiningPicture==4)&&(emptyBedPicture==4)&&(emptyToiletPicture==4)&&(emptyKitchenPicture==4)){
                    message = 'Please upload property pictures';
                }else if(emptyLivingPicture==4){
                    message = 'Living room is empty';
                }else if(emptyDiningPicture==4){
                    message = 'Dining room is empty';
                }else if(emptyBedPicture==4){
                    message = 'Bed room is empty';
                }else if(emptyToiletPicture==4){
                    message = 'Toilet room is empty';
                }else if(emptyKitchenPicture==4){
                    message = 'Kitchen room is empty';
                }else{
                    this.check(command)
                }

                if(message != ''){
                    this._notificationsService.error(
                                'Error',
                                message,
                    )
                }
            }
            else{
                this.step1submitted = this.step2submitted = this.step3submitted =this.step4submitted =false;
                this.check(command)
            }

        }else{
            if(this.isPage == 1){
                // if(!this.model.development){
                //     this.scrollPage(0);
                // }else if (!this.model.address.postal_code.length || this.model.address.postal_code.length < 6){
                //     this.scrollPage(100);
                // }else if(!this.model.address.floor || !this.model.address.unit ){
                //     this.scrollPage(105);
                // }else if(!this.model.details.size_sqf){
                //     this.scrollPage(201);
                // }else if(!this.model.address.block_number){
                //     this.scrollPage(256);
                // }else if(!this.model.address.street_name){
                //     this.scrollPage(361);
                // }else if(!this.model.details.bedroom){
                //     this.scrollPage(408);
                // }else if(!this.model.details.bathroom){
                //     this.scrollPage(488);
                // }else if(!this.model.details.price){
                //     this.scrollPage(948);
                // }else if(!this.model.details.psqft || this.model.details.psqft<1){
                //     this.scrollPage(1033);
                // }else if(!this.model.details.available){
                //     this.scrollPage(1100);
                // }else if(!this.model.details.furnishing){
                //     this.scrollPage(1239);
                // }else if(this.model.amenities.length == 0){
                //     this.scrollPage(1297);
                // }else if(!this.model.details.description){
                //     this.scrollPage(1325);
                // }else if(!this.model.address.coordinates[0]||!this.model.address.coordinates[1]){
                    this._notificationsService.error(
                            'Error',
                            'You have error in this page',
                    )
               
            }
        }
    }
    developmentChange(development:any){
        this.model.address.street_name = "";
        this.model.address.block_number=  "";
        this.development = development;
        this.model.development = this.development._id;

        if(development.address){
            if(development.address.street_name){
                this.addressReadOnly = true;
                this.model.address.street_name = development.address.street_name;
            }
            if(development.address.coordinates.length > 1){
                this.filtered = true;
                this.lng = +development.address.coordinates[0];
                this.lat = +development.address.coordinates[1];
            }
        }
        this.checkExistingUnit();
    }
    countPsqft(price:number, size:number){
        if(price > 0 && size > 0){
            this.model.details.psqft = (price/size).toFixed(2);
        }
    }
    prev():void{
        this.isPage = this.isPage - 1;
    }
    next():void{
        this.isPage = this.isPage + 1;
        if(this.isPage==5){
            var latlng = new google.maps.LatLng(this.lat, this.lng);
            this.myMap.triggerResize(true).then(() => this.myMap._mapsWrapper.setCenter({lat: this.lat, lng: this.lng}));
            // this.myMap.setCenter(latlng);
        }
    }
    addDocument() {
        this.model.companyData.documents.push(null)
        this.documentsGhost.push({
            id: ''
        })
        var index = this.model.companyData.documents.length-1
        this.idCompanyDocument.push('document'+index);
        setTimeout(() => this.elementCompanyDocuments.push('#document'+index), 100)
    }
    removeDocument(i: number) {
        this.model.companyData.documents.splice(i, 1)
        this.documentsGhost.splice(i, 1)
        this.elementCompanyDocuments.splice(i, 1);
        this.idCompanyDocument.splice(i, 1);
    }
    addOwner() {
        this.model.ownersData.push({
            name: '',
            identification_type: '',
            identification_number: '',
            identification_proof: {
                front: null,
                back: null
            }
        })
        var index = this.model.ownersData.length-1
        this.idFrontOwners.push('ownersFront'+index);
        this.idBackOwners.push('ownersBack'+index);
        setTimeout(() => this.elementFrontOwners.push('#ownersFront'+index), 100)
        setTimeout(() => this.elementBackOwners.push('#ownersBack'+index), 100)
    }
    removeOwner(i: number) {
        this.model.ownersData.splice(i, 1)
        this.elementFrontOwners.splice(i, 1);
        this.elementBackOwners.splice(i, 1);
        this.idFrontOwners.splice(i, 1);
        this.idBackOwners.splice(i, 1);
    }
    addShareholder() {
        this.model.shareholders.push({
            name: '',
            identification_type: '',
            identification_number: '',
            identification_proof: {
                front: null,
                back: null
            }
        })
        var index = this.model.shareholders.length-1
        this.idFrontShareholders.push('shareholdersFront'+index);
        this.idBackShareholders.push('shareholdersBack'+index);
        setTimeout(() => this.elementFrontShareholders.push('#shareholdersFront'+index), 100)
        setTimeout(() => this.elementBackShareholders.push('#shareholdersBack'+index), 100)
    }
    removeShareholder(i: number) {
        this.model.shareholders.splice(i, 1)
        this.elementFrontShareholders.splice(i, 1);
        this.elementBackShareholders.splice(i, 1);
        this.idFrontShareholders.splice(i, 1);
        this.idBackShareholders.splice(i, 1);
    }
    addTimeSlot(i: number) {
        this.schedules[i].time_slot.push({
                time_from_h : '',
                time_from_m : '',
                time_to_h   : '',
                time_to_m   : ''
        });
    }
    removeTimeSlot(i: number, j: number) {
        this.schedules[i].time_slot.splice(j,1);
    }
    addViewingSchedule() {
        var index = this.schedules.length.toString()
        this.schedules.push({
            days        :  [],
            start_date : '',
            time_slot  : [{
                time_from_h : '',
                time_from_m : '',
                time_to_h   : '',
                time_to_m   : '',
                error       : 0
            }],
            backup_id : index
        });
    }
    removeViewingSchedule(i: number) {
        this.schedules.splice(i, 1);
    }
    handleFileSuccess(event:any, type:any, index:number): void {
        if(type == "living") {
            this.pictures.living[index].id  = event.file.uploadedId
            this.attachementService.getById(event.file.uploadedId)
                    .subscribe(data=>{
                        this.picturesPreview.living = data.url;
                    })
            if(index!=0){
                if(!this.pictures.living[0].id && !this.pictures.living[0].src){
                    document.getElementById('living1').innerHTML = this.optPictureInnerHtml;
                }
            }
        }else if(type == "dining") {
            this.pictures.dining[index].id  = event.file.uploadedId
            this.attachementService.getById(event.file.uploadedId)
                    .subscribe(data=>{
                        this.picturesPreview.dining = data.url;
                    })
            if(index!=0){
                if(!this.pictures.dining[0].id && !this.pictures.dining[0].src){
                    document.getElementById('dining1').innerHTML = this.optPictureInnerHtml;
                }
            }
        }else if(type == "bed"){
            this.pictures.bed[index].id  = event.file.uploadedId
            this.attachementService.getById(event.file.uploadedId)
                    .subscribe(data=>{
                        this.picturesPreview.bed = data.url;
                    })
            if(index!=0){
                if(!this.pictures.bed[0].id && !this.pictures.bed[0].src){
                    document.getElementById('bedroom1').innerHTML = this.optPictureInnerHtml;
                }
            }
        }else if(type == "toilet"){
            this.pictures.toilet[index].id  = event.file.uploadedId
            this.attachementService.getById(event.file.uploadedId)
                    .subscribe(data=>{
                        this.picturesPreview.toilet = data.url;
                    })
            if(index!=0){
                if(!this.pictures.toilet[0].id && !this.pictures.toilet[0].src){
                    document.getElementById('bathroom1').innerHTML = this.optPictureInnerHtml;
                }
            }
        }else if(type == "kitchen"){
            this.pictures.kitchen[index].id  = event.file.uploadedId
            this.attachementService.getById(event.file.uploadedId)
                    .subscribe(data=>{
                        this.picturesPreview.kitchen = data.url;
                    })
            if(index!=0){
                if(!this.pictures.kitchen[0].id && !this.pictures.kitchen[0].src){
                    document.getElementById('kitchen1').innerHTML = this.optPictureInnerHtml;
                }
            }
        }

    }
    handleFileFailure(event:any, type:any, index:number): void {
    }
    handleFileSending(event:any, type:any, index:number): void {
    }
    handleFileAdded(event:any): void {
    }
    handleFileRemoved(id:string, type:any, index:number): void {
        this.attachementService.delete(id)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
        var emptyPicture: number = 0;
        if(type == "living") {
            this.pictures.living[index] = {id:null,src:''}
            emptyPicture = this.checkEmptyPicture(type);
            if(emptyPicture == 4){
                document.getElementById('living1').innerHTML = this.reqPictureInnerHtml;
            }else{
                if(this.pictures.living[0].id == null && !this.pictures.living[0].src){
                    document.getElementById('living1').innerHTML = this.optPictureInnerHtml;
                }  
            } 
        }else if(type == "dining") {
            this.pictures.dining[index] = {id:null,src:''}
            emptyPicture = this.checkEmptyPicture(type);
            if(emptyPicture == 4){
               document.getElementById('dining1').innerHTML = this.reqPictureInnerHtml;
            }else{
              if(this.pictures.dining[0].id== null && !this.pictures.dining[0].src){
                    document.getElementById('dining1').innerHTML = this.optPictureInnerHtml;
              }  
            } 
        }else if(type == "bed") {
            this.pictures.bed[index] = {id:null,src:''}
            emptyPicture = this.checkEmptyPicture(type);
            if(emptyPicture == 4){
               document.getElementById('bedroom1').innerHTML  = this.reqPictureInnerHtml;
            }else {
                if(this.pictures.bed[0].id== null && !this.pictures.bed[0].src){
                    document.getElementById('bedroom1').innerHTML = this.optPictureInnerHtml;
                }
            }
        }else if(type == "toilet") {
            this.pictures.toilet[index] = {id:null,src:''}
            emptyPicture = this.checkEmptyPicture(type);
            if(emptyPicture == 4){
               document.getElementById('bathroom1').innerHTML = this.reqPictureInnerHtml;
            }else{
              if(this.pictures.toilet[0].id == null && !this.pictures.toilet[0].src){
                    document.getElementById('bathroom1').innerHTML = this.optPictureInnerHtml;
               }  
            } 
        }else if(type == "kitchen") {
            this.pictures.kitchen[index] = {id:null,src:''}
            emptyPicture = this.checkEmptyPicture(type);
            if(emptyPicture == 4){
               document.getElementById('kitchen1').innerHTML = this.reqPictureInnerHtml;
            }else {
                if(this.pictures.kitchen[0].id == null && !this.pictures.kitchen[0].src){
                    document.getElementById('kitchen1').innerHTML = this.optPictureInnerHtml;
                }
            }
        }
    }
    checkEmptyPicture(type:string){
        var emptyPicture: number = 0;
        for (var i = 0; i <= 3; ++i) {
            if(this.pictures[type][i].id == null && !this.pictures[type][i].src) {
                emptyPicture += 1;
            }                      
        }
        return emptyPicture;
    }
    checkAmenitiesCheckbox(amenity: any){
        var index = this.model.amenities.indexOf(amenity._id);
        if(index >= 0){
            this.model.amenities.splice( index, 1 );
            this.amenitiesPreview.splice( index, 1 );
        }else{
            this.model.amenities.push(amenity._id)
            this.amenitiesPreview.push(amenity)
        }
    }
    handleFileSuccessOwner(event:any, type:any): void {
        if(type == "front") {
            this.model.landlordData.identification_proof.front = event.file.uploadedId
        }else if(type == "back") {
            this.model.landlordData.identification_proof.back = event.file.uploadedId
        }
    }
    handleFileFailureOwner(event:any, type:any): void {
    }
    handleFileSendingOwner(event:any, type:any): void {
    }
    handleFileAddedOwner(event:any): void {
    }
    handleFileRemovedOwner(id:string, type:any): void {
        this.attachementService.delete(id)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
        if(type == "front") {
            this.model.landlordData.identification_proof.front  = null
        }else if(type == "back") {
            this.model.landlordData.identification_proof.back  = null
        }
    }
    //Handle file for additional owners :
    handleFileSuccessOwners(event:any, type:any, index:number): void {
        if(type == "front") {
            this.model.ownersData[index].identification_proof.front = event.file.uploadedId
            this.ownerFront = false;
        }else if(type == "back") {
            this.model.ownersData[index].identification_proof.back  = event.file.uploadedId
        }
    }
    handleFileFailureOwners(event:any, type:any, index:number): void {
    }
    handleFileSendingOwners(event:any, type:any, index:number): void {
    }
    handleFileAddedOwners(event:any): void {
    }
    handleFileRemovedOwners(id:string, type:any, index:number): void {
        this.attachementService.delete(id)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
        if(type == "front") {
            if(this.model.status == 'draft' && !this.userInfo.landlord.data.name){
                this.elementFrontOwners[index] = null
                setTimeout(() => this.elementFrontOwners[index] = '#shareholdersFront'+index, 300)
            }
            this.model.ownersData[index].identification_proof.front  = null
        }else if(type == "back") {
            if(this.model.status == 'draft' && !this.userInfo.landlord.data.name){
                this.elementBackOwners[index] = null
                setTimeout(() => this.elementBackOwners[index] = '#shareholdersFront'+index, 300)
            }
            this.model.ownersData[index].identification_proof.back  = null
        }
    }
    select(index:number, name:string){
        return '#'+name+index;
    }
    selectId(index:number, name:string){
         return name+index;
    }
    checkDayBox(scheduleIndex:number, day:string){
        var index = this.schedules[scheduleIndex].days.indexOf(day);
        if(index >= 0){
            this.schedules[scheduleIndex].days.splice( index, 1 );
        }else{
            this.schedules[scheduleIndex].days.push(day)
        }
    }
    validatestep4(){
        this.days_selected = [];
        this.errorDaysElement = [];
        for(var i = 0; i < this.schedules.length; i++){
            for(var j = 0; j < this.schedules[i].days.length; j++){
                if(this.days_selected.indexOf(this.schedules[i].days[j]) > -1) {
                    this.errorDays = true;
                    if(this.errorDaysElement.indexOf(this.schedules[i].days[j]) == -1) {
                        this.errorDaysElement.push(this.schedules[i].days[j]);
                    }
                }
                else{
                    this.days_selected.push(this.schedules[i].days[j]);
                    if(this.errorDaysElement.length > 0) {
                        this.errorDays = true;
                    }
                    else{
                        this.errorDays = false;
                    }

                }
            }
            this.time_selected = [];
            for(var k = 0; k < this.schedules[i].time_slot.length; k++){
                if(this.schedules[i].time_slot[k].time_from_h > this.schedules[i].time_slot[k].time_to_h) {
                    this.errorTimeTo = true;
                }
                else if(this.schedules[i].time_slot[k].time_from_h == this.schedules[i].time_slot[k].time_to_h) {
                    if(this.schedules[i].time_slot[k].time_from_m > this.schedules[i].time_slot[k].time_to_m) {
                        this.errorTimeTo = true;
                    }
                    else if(this.schedules[i].time_slot[k].time_from_m == this.schedules[i].time_slot[k].time_to_m) {
                        this.errorTimeTo = true;
                    }
                }
                else{
                    if(this.time_selected.length > 0) {
                        var count = 0;
                        for(var l = 0; l < this.time_selected.length; l++){
                            if(this.schedules[i].time_slot[k].time_from_h < this.time_selected[l].time_from_h && this.schedules[i].time_slot[k].time_to_h > this.time_selected[l].time_to_h) {
                                this.errorTime2 = true;
                            }
                            else if(this.schedules[i].time_slot[k].time_from_h > this.time_selected[l].time_from_h && this.schedules[i].time_slot[k].time_from_h < this.time_selected[l].time_to_h) {
                                this.errorTime = true;
                            }
                            else if(this.schedules[i].time_slot[k].time_from_h == this.time_selected[l].time_from_h) {
                                if(this.schedules[i].time_slot[k].time_from_m > this.time_selected[l].time_from_m) {
                                    this.errorTimeFrom = true;
                                }
                            }
                            else if(this.schedules[i].time_slot[k].time_from_h == this.time_selected[l].time_to_h) {
                                if(this.schedules[i].time_slot[k].time_from_m < this.time_selected[l].time_to_m) {
                                    this.errorTimeFrom = true;
                                }
                            }
                            else if(this.schedules[i].time_slot[k].time_to_h > this.time_selected[l].time_from_h && this.schedules[i].time_slot[k].time_to_h < this.time_selected[l].time_to_h) {
                                this.errorTimeTo2 = true;
                            }
                            else if(this.schedules[i].time_slot[k].time_to_h == this.time_selected[l].time_from_h) {
                                if(this.schedules[i].time_slot[k].time_to_m > this.time_selected[l].time_from_m) {
                                    this.errorTimeTo2 = true;
                                }
                            }
                            else if(this.schedules[i].time_slot[k].time_to_h == this.time_selected[l].time_to_h) {
                                this.errorTimeTo2 = true;
                            }
                            else{
                                count += 1;
                            }
                        }
                        if(count == this.time_selected.length) {
                            this.time_selected.push(this.schedules[i].time_slot[k]);
                            this.errorTime2 = false;
                            this.errorTime = false;
                            this.errorTimeFrom = false;
                            this.errorTimeTo = false;
                        }
                    }
                    else{
                        if(this.time_selected.indexOf(this.schedules[i].time_slot[k]) > -1) {
                            this.errorTimeTo = true;
                        }
                        else{
                            this.time_selected.push(this.schedules[i].time_slot[k]);
                            this.errorTime2 = false;
                            this.errorTime = false;
                            this.errorTimeFrom = false;
                            this.errorTimeTo = false;
                        }
                    }
                }
            }
        }
        for(var m = 0; m < this.errorDaysElement.length; m++){
            if(this.days_selected.indexOf(this.errorDaysElement[m]) == -1){
                this.errorDaysElement.pop(this.errorDaysElement[m]);
            }
        }
        if(this.errorDaysElement.length > 0){
            this.globalError += 1;
        }
        else if(this.errorTime2 == true) {
            this.globalError += 1;
        }
        else if(this.errorTime == true) {
            this.globalError += 1;
        }
        else if(this.errorTimeFrom == true) {
            this.globalError += 1;
        }
        else if(this.errorTimeTo == true) {
            this.globalError += 1;
        }
        else if(this.errorTimeTo2 == true) {
            this.globalError += 1;
        }
        else if(this.errorDays == true) {
            this.globalError += 1;
        }
        else{
            this.globalError = 0;
        }
    }
    //Handle file for Company's Document :
    handleFileSuccessDocuments(event:any, index:number): void {
        this.model.companyData.documents[index] = event.file.uploadedId
    }
    handleFileFailureDocuments(event:any, index:number): void {
    }
    handleFileSendingDocuments(event:any, index:number): void {
    }
    handleFileAddedDocuments(event:any): void {
    }
    handleFileRemovedDocuments(event:any, index:number): void {
        this.attachementService.delete(event.uploadedId)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
        this.model.companyData.documents[index] = null
    }
     //Handle file for additional shareholders:
    handleFileSuccessShareholders(event:any, type:any, index:number): void {
        if(type == "front") {
            this.model.shareholders[index].identification_proof.front = event.file.uploadedId
        }else if(type == "back") {
            this.model.shareholders[index].identification_proof.back  = event.file.uploadedId
        }
    }
    handleFileFailureShareholders(event:any, type:any, index:number): void {
    }
    handleFileSendingShareholders(event:any, type:any, index:number): void {
    }
    handleFileAddedShareholders(event:any): void {
    }
    handleFileRemovedShareholders(id:string, type:any, index:number): void {
        this.attachementService.delete(id)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
        if(type == "front") {
            if(this.model.status == 'draft' && !this.userInfo.landlord.data.name){
                this.elementFrontShareholders[index] = null
               setTimeout(() => this.elementFrontShareholders[index] = '#shareholdersFront'+index, 300)
            }
            this.model.shareholders[index].identification_proof.front  = null
        }else if(type == "back") {
            if(this.model.status == 'draft' && !this.userInfo.landlord.data.name){
                this.elementBackShareholders[index] = null
               setTimeout(() => this.elementBackShareholders[index] = '#shareholdersBack'+index, 300)
            }
            this.model.shareholders[index].identification_proof.back  = null
        }
    }
    ownedTypeTrigger(command:string){
        if(!this.editPage){
            if(!this.userInfo.landlord.data.name){
                if(command == 'ngOnInit' || command == 'individual'){
                    this.isCompany=false;
                    this.isIndividual=true;
                }else if(command == 'company'){
                    this.isCompany=true;
                    this.isIndividual=false;
                }
            }else{
                if(this.userInfo.owned_properties[0].owned_type=="individual"){
                    this.isCompany=false;
                    this.isIndividual=true;
                }else{
                    this.isCompany=true;
                    this.isIndividual=false;
                }
            }
        }
    }
    create(){
        if(this.model.status=="draft"){
            this.model.status="pending";
            this.update();
        }else{
            this.createAsNew();
        }
    }
    preDraft(){
        if(this.model.status=="draft"){
            this.updateAsDraft();
        }else{
            this.createAsDraft();
        }
    }
    createAsNew(){

        this.model.details.bedroom  = parseInt(this.model.details.bedroom)
        this.model.details.bathroom = parseInt(this.model.details.bathroom)
        if(this.isIndividual == true){
            delete this.model.companyData
            this.model.shareholders = this.model.ownersData;
            delete this.model.ownersData

            if(!this.model.landlordData.identification_proof.back){
                delete this.model.landlordData.identification_proof.back;
            }
            for (var i = 0; i < this.model.shareholders.length; ++i) {
               if(!this.model.shareholders[i].identification_proof.back){
                   delete this.model.shareholders[i].identification_proof.back
               }
            }

            if(this.userInfo.landlord.data.name){
                delete this.model.shareholders
                delete this.model.landlordData
            }
        }else if(this.isCompany == true){
            for (var i = 0; i < this.model.shareholders.length; ++i) {
               if(!this.model.shareholders[i].identification_proof.back){
                   delete this.model.shareholders[i].identification_proof.back
               }
            }
            this.model.landlordData  = this.model.shareholders[0]
            this.model.shareholders.splice(0, 1)
            delete this.model.ownersData
            if(this.selectedCompany!='new'){
                delete this.model.companyData
                // if(this.model.shareholders.length > this.selectedCompany.shareholders.length){

                // }
                if(this.selectedCompany.shareholders){
                    if(this.model.shareholders.length > this.selectedCompany.shareholders.length){
                        this.model.shareholders.splice(0, this.selectedCompany.shareholders.length)
                    }else{
                        delete this.model.shareholders;
                    }
                }
            }
        }
        this.propertyService.create(this.model)
                .then(
                    response => {
                           this.router.navigate(['admin/property']);
                           this._notificationsService.success(
                            'Success',
                            'Thanks for submitting, your listing will be reviewed by our team of hardworking administrator. If all goes well, it should be up within the next three business days (though it usually takes shorter than that).',
                           )
                         },
                    error => {
                        console.log(error);
                        this._notificationsService.error(
                            'error',
                            'Failed to save, server error',
                           )
                       }
        );
    }
    createAsDraft(){

        this.model.details.bedroom  = parseInt(this.model.details.bedroom)
        this.model.details.bathroom = parseInt(this.model.details.bathroom)
        if(this.isIndividual == true){
            delete this.model.companyData
            this.model.shareholders = this.model.ownersData;
            delete this.model.ownersData
            
            if(!this.model.landlordData.identification_proof.back){
                delete this.model.landlordData.identification_proof.back;
            }
            for (var i = 0; i < this.model.shareholders.length; ++i) {
               if(!this.model.shareholders[i].identification_proof.back){
                   delete this.model.shareholders[i].identification_proof.back
               }
            }

            if(this.userInfo.landlord.data.name){
                delete this.model.shareholders
            }
        }else if(this.isCompany == true){
            for (var i = 0; i < this.model.shareholders.length; ++i) {
               if(!this.model.shareholders[i].identification_proof.back){
                   delete this.model.shareholders[i].identification_proof.back
               }
            }
            this.model.landlordData  = this.model.shareholders[0]
            delete this.model.ownersData
            this.model.shareholders.splice(0, 1)
            if(this.selectedCompany!='new'){
                delete this.model.companyData
                if(this.selectedCompany.shareholders){
                    if(this.model.shareholders.length > this.selectedCompany.shareholders.length){
                        this.model.shareholders.splice(0, this.selectedCompany.shareholders.length)
                    }else{
                        delete this.model.shareholders;
                    }
                }
            }
        }
        this.model.status= "draft";
         this.propertyService.create(this.model)
                .then(
                    response => {
                        this.router.navigate(['admin/property']);
                        this._notificationsService.success(
                            'Success',
                            'Save successful, you can continue later',
                        )

                         },
                    error => {
                        console.log(error);
                         this._notificationsService.error(
                            'Error',
                            'Failed to save, server error',
                        )
                       }
        );
    }
    update(){

        this.model.details.bedroom  = parseInt(this.model.details.bedroom)
        this.model.details.bathroom = parseInt(this.model.details.bathroom)
        if(this.isIndividual == true){
            delete this.model.companyData
            this.model.shareholders = this.model.ownersData;
            delete this.model.ownersData

            if(!this.model.landlordData.identification_proof.back){
                delete this.model.landlordData.identification_proof.back;
            }
            for (var i = 0; i < this.model.shareholders.length; ++i) {
               if(!this.model.shareholders[i].identification_proof.back){
                   delete this.model.shareholders[i].identification_proof.back
               }
            }

            if(this.userInfo.landlord.data.name || this.editPage){
                delete this.model.shareholders
                delete this.model.landlordData
            }
        }else if(this.isCompany == true){
            for (var i = 0; i < this.model.shareholders.length; ++i) {
               if(!this.model.shareholders[i].identification_proof.back){
                   delete this.model.shareholders[i].identification_proof.back
               }
            }
            this.model.landlordData  = this.model.shareholders[0]
            if(this.userInfo.landlord.data.name || this.editPage){
                delete this.model.landlordData
            }
            this.model.shareholders.splice(0, 1)
            delete this.model.ownersData
            if(this.selectedCompany!='new'){
                delete this.model.companyData
                if(this.selectedCompany.shareholders){
                    if(this.model.shareholders.length > this.selectedCompany.shareholders.length){
                        this.model.shareholders.splice(0, this.selectedCompany.shareholders.length)
                    }else{
                        delete this.model.shareholders;
                    }
                }
            }
        }
        this.propertyService.update(this.model, this.id_property)
                .then(
                    response => {
                        this.router.navigate(['admin/property']);
                         this._notificationsService.success(
                            'Success',
                            'Listing saved',
                          )

                         },
                    error => {
                        console.log(error);
                         this._notificationsService.error(
                            'Error',
                            'Failed to update, server error',
                         )
                       }
        );
    }
    updateAsDraft(){

        this.model.details.bedroom  = parseInt(this.model.details.bedroom)
        this.model.details.bathroom = parseInt(this.model.details.bathroom)
        if(this.isIndividual == true){
            delete this.model.companyData
            this.model.shareholders = this.model.ownersData;

            if(!this.model.landlordData.identification_proof.back){
                delete this.model.landlordData.identification_proof.back;
            }
            for (var i = 0; i < this.model.shareholders.length; ++i) {
               if(!this.model.shareholders[i].identification_proof.back){
                   delete this.model.shareholders[i].identification_proof.back
               }
            }
            delete this.model.ownersData
            if(this.userInfo.landlord.data.name){
                delete this.model.shareholders
            }
        }else if(this.isCompany == true){
            for (var i = 0; i < this.model.shareholders.length; ++i) {
               if(!this.model.shareholders[i].identification_proof.back){
                   delete this.model.shareholders[i].identification_proof.back
               }
            }
            this.model.landlordData  = this.model.shareholders[0]
            this.model.shareholders.splice(0, 1)
            delete this.model.ownersData
            if(this.selectedCompany!='new'){
                delete this.model.companyData
            }
        }
        this.model.status= "draft";
        this.propertyService.update(this.model, this.id_property)
                .then(
                    response => {
                        this.router.navigate(['admin/property']);
                        this._notificationsService.success(
                            'Success',
                            'Save successful, you can continue later',
                        )

                         },
                    error => {
                        console.log(error);
                         this._notificationsService.error(
                            'Error',
                            'Failed to save, server error',
                        )
                    }
        );
    }
    companyChange(){
        // this.model.shareholders.slice(0, 1)
        // this.idFrontShareholders.slice(0, 1)
        // this.idBackShareholders.slice(0, 1)
        // this.elementFrontShareholders.slice(0, 1)
        // this.elementBackShareholders.slice(0, 1)

        if(this.selectedCompany=='new'){
            delete this.model.owner
            this.documentsGhost = []

            this.model.companyData = {
                name: "",
                registration_number: "",
                documents: [''],
             }

             this.documentsGhost.push({
                id: '',
                src: ''
            })
            this.model.shareholders        =  this.model.shareholders.slice(0, 1);
            this.elementFrontShareholders  = this.elementFrontShareholders.slice(0, 1);
            this.elementBackShareholders   = this.elementBackShareholders.slice(0, 1);
            this.idFrontShareholders       = this.idFrontShareholders.slice(0, 1);
            this.idBackShareholders        = this.idBackShareholders.slice(0, 1);

            this.elementCompanyDocuments = []
            this.idCompanyDocument= []

            setTimeout(() => this.elementCompanyDocuments.push('#document0'), 100)
            this.idCompanyDocument.push('document0');
        }else{
            this.model.shareholders        =  this.model.shareholders.slice(0, 1);
            this.elementFrontShareholders  = this.elementFrontShareholders.slice(0, 1);
            this.elementBackShareholders   = this.elementBackShareholders.slice(0, 1);
            this.idFrontShareholders       = this.idFrontShareholders.slice(0, 1);
            this.idBackShareholders        = this.idBackShareholders.slice(0, 1);

            this.model.owner = {};
            this.model.owner.company = this.selectedCompany._id;
            this.elementCompanyDocuments= []
            this.idCompanyDocument= []

            this.model.companyData = {
                name: this.selectedCompany.name,
                registration_number: this.selectedCompany.registration_number,
                documents: this.selectedCompany.documents,
            }
            this.documentsGhost = []
            for (var i = 0; i < this.model.companyData.documents.length; i++) {
                this.documentsGhost.push({
                    id: this.model.companyData.documents[i].url,
                })
            }
            if(this.selectedCompany.shareholders != null){
                if(this.selectedCompany.shareholders.length > 0){
                    for (var i = 0; i < this.selectedCompany.shareholders.length; i++){
                        this.model.shareholders.push({
                                name:                     this.selectedCompany.shareholders[i].name,
                                identification_type:      this.selectedCompany.shareholders[i].identification_type,
                                identification_number:    this.selectedCompany.shareholders[i].identification_number,
                                identification_proof:{
                                    front: this.selectedCompany.shareholders[i].identification_proof.front,
                                    back: this.selectedCompany.shareholders[i].identification_proof.back,                          }
                        })
                        var index = this.model.shareholders.length-1
                        this.idFrontShareholders.push('shareholdersFront'+index);
                        this.idBackShareholders.push('shareholdersBack'+index);
                        setTimeout(() => this.elementFrontShareholders.push('#shareholdersFront'+index), 100)
                        setTimeout(() => this.elementBackShareholders.push('#shareholdersBack'+index), 100)
                    }
                }
            }


        }
    }
    findAttachment(id:string){
        this.attachementService.getById(id)
            .subscribe(data=>{
            return data.url;
        })
    }

    validateSchedule(){
        this.globalError = 0;

        for (var i = 0; i < this.schedules.length; ++i) {
            for (var j = 0; j < this.schedules[i].time_slot.length; ++j) {
                if(+(this.schedules[i].time_slot[j].time_from_h+this.schedules[i].time_slot[j].time_from_m) >=
                   +(this.schedules[i].time_slot[j].time_to_h+this.schedules[i].time_slot[j].time_to_m))
                {
                   this.schedules[i].time_slot[j].error = 1;
                   this.globalError += 1;
                }else{
                    this.schedules[i].time_slot[j].error = 0;
                }
                for (var k = j+1; k < this.schedules[i].time_slot.length; ++k) {
                    if((+(this.schedules[i].time_slot[j].time_from_h+this.schedules[i].time_slot[j].time_from_m)>=
                       +(this.schedules[i].time_slot[k].time_from_h+this.schedules[i].time_slot[k].time_from_m))
                       &&
                       (+(this.schedules[i].time_slot[j].time_from_h+this.schedules[i].time_slot[j].time_from_m)<=
                       +(this.schedules[i].time_slot[k].time_to_h+this.schedules[i].time_slot[k].time_to_m))){
                        this.schedules[i].time_slot[j].error = 1;
                        this.globalError += 1;
                    }
                    else if((+(this.schedules[i].time_slot[j].time_to_h+this.schedules[i].time_slot[j].time_to_m)>=
                       +(this.schedules[i].time_slot[k].time_from_h+this.schedules[i].time_slot[k].time_from_m))
                       &&
                       (+(this.schedules[i].time_slot[j].time_to_h+this.schedules[i].time_slot[j].time_to_m)<=
                       +(this.schedules[i].time_slot[k].time_to_h+this.schedules[i].time_slot[k].time_to_m))){
                        this.schedules[i].time_slot[j].error = 1;
                        this.globalError += 1;
                    }else if((+(this.schedules[i].time_slot[k].time_from_h+this.schedules[i].time_slot[k].time_from_m)>=
                       +(this.schedules[i].time_slot[j].time_from_h+this.schedules[i].time_slot[j].time_from_m))
                       &&
                       (+(this.schedules[i].time_slot[k].time_from_h+this.schedules[i].time_slot[k].time_from_m)<=
                       +(this.schedules[i].time_slot[j].time_to_h+this.schedules[i].time_slot[j].time_to_m))){
                        this.schedules[i].time_slot[j].error = 1;
                        this.globalError += 1;
                    }else if((+(this.schedules[i].time_slot[k].time_to_h+this.schedules[i].time_slot[k].time_to_m)>=
                       +(this.schedules[i].time_slot[j].time_from_h+this.schedules[i].time_slot[j].time_from_m))
                       &&
                       (+(this.schedules[i].time_slot[k].time_to_h+this.schedules[i].time_slot[k].time_to_m)<=
                       +(this.schedules[i].time_slot[j].time_to_h+this.schedules[i].time_slot[j].time_to_m))){
                        this.schedules[i].time_slot[j].error = 1;
                        this.globalError += 1;
                    }
                    else{
                        this.schedules[i].time_slot[j].error = 0;
                    }
                }
            }
        }
        if(this.globalError==0 && this.schedules.length > 1){
            this.validateSchedule2();
        }
    }

    validateSchedule2(){
        for (var i = 0; i < this.schedules.length; ++i) {
            for (var j = i+1; j < this.schedules.length; ++j) {
                let totalDayExist: number = 0;
                // Check for Existing  day
                for (var k = 0; k < this.schedules[i].days.length; ++k) {
                    if(this.schedules[j].days.indexOf(this.schedules[i].days[k]) >= 0){
                        totalDayExist += 1;
                    }
                }
                if(totalDayExist > 0){
                    for (var l = 0; l < this.schedules[i].time_slot.length; ++l) {
                        for (var m = 0; m < this.schedules[j].time_slot.length; ++m) {
                            if((+(this.schedules[i].time_slot[l].time_from_h+this.schedules[i].time_slot[l].time_from_m)>=
                               +(this.schedules[j].time_slot[m].time_from_h+this.schedules[j].time_slot[m].time_from_m))
                               &&
                               (+(this.schedules[i].time_slot[l].time_from_h+this.schedules[i].time_slot[l].time_from_m)<=
                               +(this.schedules[j].time_slot[m].time_to_h+this.schedules[j].time_slot[m].time_to_m))){
                                this.schedules[i].time_slot[l].error = 1;
                                this.schedules[j].time_slot[m].error = 1;
                                this.globalError += 1;
                            }
                            else if((+(this.schedules[i].time_slot[l].time_to_h+this.schedules[i].time_slot[l].time_to_m)>=
                               +(this.schedules[j].time_slot[m].time_from_h+this.schedules[j].time_slot[m].time_from_m))
                               &&
                               (+(this.schedules[i].time_slot[l].time_to_h+this.schedules[i].time_slot[l].time_to_m)<=
                               +(this.schedules[j].time_slot[m].time_to_h+this.schedules[j].time_slot[m].time_to_m))){
                                this.schedules[i].time_slot[l].error = 1;
                                this.schedules[j].time_slot[m].error = 1;
                                this.globalError += 1;
                            }else if((+(this.schedules[j].time_slot[m].time_from_h+this.schedules[j].time_slot[m].time_from_m)>=
                               +(this.schedules[i].time_slot[l].time_from_h+this.schedules[i].time_slot[l].time_from_m))
                               &&
                               (+(this.schedules[j].time_slot[m].time_from_h+this.schedules[j].time_slot[m].time_from_m)<=
                               +(this.schedules[i].time_slot[l].time_to_h+this.schedules[i].time_slot[l].time_to_m))){
                                this.schedules[i].time_slot[l].error = 1;
                                this.schedules[j].time_slot[m].error = 1;
                                this.globalError += 1;
                            }else if((+(this.schedules[j].time_slot[m].time_to_h+this.schedules[j].time_slot[m].time_to_m)>=
                               +(this.schedules[i].time_slot[l].time_from_h+this.schedules[i].time_slot[l].time_from_m))
                               &&
                               (+(this.schedules[j].time_slot[m].time_to_h+this.schedules[j].time_slot[m].time_to_m)<=
                               +(this.schedules[i].time_slot[l].time_to_h+this.schedules[i].time_slot[l].time_to_m))){
                                this.schedules[i].time_slot[l].error = 1;
                                this.schedules[j].time_slot[m].error = 1;
                                this.globalError += 1;
                            }
                            else{
                                this.schedules[i].time_slot[l].error = 0;
                                this.schedules[j].time_slot[m].error = 0;
                            }
                        }
                    }
                }
            }
        }
    }

    // public scroll(){
    //     this.scrollPage(0);
    // }

    @HostListener("window:scroll", [])
    onWindowScroll() {
        // this.currentPosition = this.document.body.scrollTop;
        // console.log(this.currentPosition)
    }

    // smoothScroll(targer:number){
    //     var target = document.getElementById(id).offsetTop; //jarak antara atas dan div
        
    //     var scrollAnimate = setTimeout(function(){
    //                                         this.smoothScroll(id);
    //                                     }, 5) // fungsi ,waktu

    //     posY = posY + jarak;

    //     //berhenti pada bagian tertentu
    //     if(posY >= target){
    //         clearTimeout(scrollAnimate);// berhentikan
    //         posY = 500;
    //     }else {
    //      window.scroll(0, posY) // parameter x dan y
    // }

    //     return false;
    // } 

    // public goToHead2(): void {
    //      let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#descriptionInput');
    //      this.pageScrollService.start(pageScrollInstance);
    //  }; 

    scrollPage(target: number){
        var interval: number = 0.000001;
        var scroll = setInterval(()=> {
            if(target >= this.currentPosition){
                clearInterval(scroll);
            }

            this.currentPosition -= 100;
            window.scroll(0, this.currentPosition) ;
         },interval);
    }

    onTimeSLotClick(i:number, j:number){
        if( this.schedules[i].time_slot[j].time_from_h&&
            this.schedules[i].time_slot[j].time_from_m&&
            this.schedules[i].time_slot[j].time_to_h&&
            this.schedules[i].time_slot[j].time_to_m&&
            this.schedules[i].start_date&&
            (this.schedules[i].days.length > 0)&&
            this.step4submitted){
            this.validateSchedule();
        }
    }
    checkExistingUnit(){
        var existProperty:any;
        let self = this;
        if(this.model.address.floor.length == 1){
            this.model.address.floor = '0'+this.model.address.floor.toString();
        }
        if(this.model.address.unit.length == 1){
            this.model.address.unit = '0'+this.model.address.unit.toString();
        }
        if(!this.editPage && this.model.address.unit && this.model.address.floor && this.model.development){
            existProperty = this.development.properties.find(function (data:any){
                    return data.address.floor+'-'+data.address.unit == self.model.address.floor+'-'+self.model.address.unit;
                })
            if(existProperty){
                this.model.address.street_name = existProperty.address.street_name;
                this.model.address.block_number=  existProperty.address.block_number;
                this.model.details.size_sqf   = existProperty.details.size_sqf;
                this.model.address.postal_code = existProperty.address.postal_code
                this.lng = +existProperty.address.coordinates[0];
                this.lat = +existProperty.address.coordinates[1];
                this._notificationsService.error(
                                    'Error', 'The listing you are submitting is already listed on staysmart. Please kindly email to support@staysmart.sg if you have further questions.',
                            )
                this.disableNextButton = true;
            }else{
                this._notificationsService.info(
                                    'Info', 'Data for unit #'+this.model.address.floor+'-'+this.model.address.unit+' not found, please fill manually.' 
                            )
                this.disableNextButton = false;
            }
        }
    }
}
