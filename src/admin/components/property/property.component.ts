import { Component, OnInit, Input, Output } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';

import { Properties } from '../../modelsV2/properties';
import { PropertyService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';

@Component({
  selector: 'my-property',
  templateUrl : './property.component.html',
  
  providers: [PropertyService, AlertService]
})

export class AdminPropertyComponent implements OnInit {
	properties: Properties[];
  propertyId: any;
  url: string;
  contentClass: string = 'content';

  constructor(
    private propertyService: PropertyService, 
    private router: Router, 
    private activateRoute: ActivatedRoute, 
    private alertService: AlertService,
    private sharedService: SharedService
  ) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.url = this.router.url;
    this.getProperties();
    this.activateRoute.params.subscribe((params: Params) => {
      this.propertyId = params['id'];
    });
  }

  getProperties(): void {
    this.propertyService.getAllProperty().subscribe(properties => { 
      this.properties = properties.filter((data:any) => 
              data.status !== "draft"
         )
      console.log(this.properties);
    });
  }

  deleteProperty(value:Properties): void {
    this.propertyService.delete(value._id)
      .then(response => {
        this.alertService.success('Delete property successful', true);
        this.getProperties();
      },
      error => {
        alert(`The property could not be deleted, server Error.`);
      }
    );
  }

  viewProperty(value:Properties): void {
    this.router.navigate(['admin/property/' + value._id + '/detail']);
  }
  editProperty(value:Properties): void {
    this.router.navigate(['admin/property/' + value._id + '/edit']);
  }
}