import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';

import { Blogs } from '../../modelsV2/blogs';
import { BlogService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';

@Component({
  selector: 'my-blog',
  templateUrl : './blog.component.html',
  
  providers: [BlogService, AlertService]
})

export class AdminBlogComponent implements OnInit {
	blogs: Blogs[];
  contentClass: string = 'content';

  constructor(private alertService: AlertService,private blogService: BlogService, private router: Router, private sharedService: SharedService) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.getBlogs();
  }

  getBlogs(): void {
    this.blogService.getBlogs().subscribe(blogs => { 
      this.blogs = blogs;
    });
  }

  deleteBlog(value:Blogs): void {
    this.blogService.delete(value._id)
      .then(response => {
        this.alertService.success('Delete blog successful', true);
        this.getBlogs();
      },
      error => {
        alert(`The blog could not be deleted, server Error.`);
      }
    );
  }

  editBlog(value:Blogs): void {
    this.router.navigate(['admin/blog/' + value._id + '/edit']);
  }

  viewBlog(value:Blogs): void {
    this.router.navigate(['admin/blog/' + value._id + '/detail']);
  }
}