import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Blogs } from '../../modelsV2/blogs';
import { BlogCategories } from '../../modelsV2/blog-categories';
import { BlogService, BlogCategoryService, AlertService, AttachmentService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'my-blog',
  templateUrl : './edit-blog.component.html',
  
  providers: [ BlogService, BlogCategoryService, AlertService, AttachmentService],
})

export class AdminEditBlogComponent {
	myForm : FormGroup;
	blogs: Blogs[];
  categories: BlogCategories[];
  dataBlog: any = {};
  blogId: any;
  model: any = {};
  contentClass: string = 'content';

	constructor(private formbuilder: FormBuilder,private alertService: AlertService, private blogService: BlogService, private attachmentService: AttachmentService,
   private blogCategoryService : BlogCategoryService, private activateRoute: ActivatedRoute, private location: Location, private router: Router, private http: Http, private sharedService: SharedService) 
  { 
    this.location = location; 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

	ngOnInit(): void {
    this.activateRoute.params.subscribe((params: Params) => {
      this.blogId = params['id'];
    });

    this.getBlogCategories();

    if (this.blogId != null) {
      console.log(this.blogId);
      this.blogService.getById(this.blogId)
        .subscribe(blog => {
        this.dataBlog = blog;
      });
    }

		this.myForm = this.formbuilder.group({
      title : ['', Validators.required],
      source : ['', Validators.required],
      content : [],
      category : []
		});
	}

  getBlogCategories(): void {
    this.blogCategoryService.getBlogCategories().subscribe(blogCategories => { 
        this.categories = blogCategories;
        console.log(this.categories);
    });
  }

	createBlog(): void {
    if (this.dataBlog.cover == undefined) {
      alert('Please add cover for photo');
    }
    else {
  		this.blogService.create(this.dataBlog)
      .then(
        data => {
          this.alertService.success('Create blog successful', true);
          this.router.navigate(['/admin/blog']);
        },
        error => {
          console.log(error);
          this.alertService.error(error);
        }
      );
    }
	}

  updateBlog(): void {
    this.blogService.update(this.dataBlog)
    .then(
      response => {
        this.alertService.success('Update blog successful', true);
        this.router.navigate(['/admin/blog']);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  handleFileSuccess(event:any, image:any): void {
    console.log('success', event, image);
    this.attachmentService.getById(event.file.uploadedId)
      .subscribe(data=>{
          this.dataBlog.cover = data;
      })
  }

  handleFileFailure(event:any, image:any): void {
    console.log('fail', event, image);
  }

  handleFileSending(event:any, image:any): void {
    console.log('send', event, image);
  }

  handleFileAdded(event:any): void {
    console.log('add', event);
  }

  handleFileRemoved(event:any): void {
    console.log('remove', event);
    this.attachmentService.delete(event.uploadedId)
    .then(
        response => {
            console.log(response);
        },
        error => {
            console.log(error);
        }
    );
  }
}
