import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';

import { Blogs } from '../../modelsV2/blogs';
import { BlogService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';

@Component({
  selector: 'my-blog-detail',
  templateUrl : './blog-detail.component.html',
  
  providers: [BlogService, AlertService]
})

export class AdminBlogDetailComponent implements OnInit {
	blogs: Blogs[];
  dataBlog: any;
  blogId: string;
  test: {};
  contentClass: string = 'content';

  constructor(private alertService: AlertService,private blogService: BlogService, private router: Router,
              private activateRoute: ActivatedRoute, private sharedService: SharedService) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.activateRoute.params.subscribe((params: Params) => {
      this.blogId = params['id'];
      this.blogService.getById(this.blogId)
      .subscribe(blog => {
          this.dataBlog = blog;
          console.log(this.dataBlog);
      });
      
    });
  }
}