import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HistoryService } from '../../../client/services/history.service';
import "rxjs/add/operator/takeWhile";
import { SharedService } from '../../../client/services/shared.service';

import * as moment from 'moment'

@Component({
    selector: 'my-history',
    templateUrl : './history.component.html',
    
    providers: [HistoryService]
})

export class AdminHistoryComponent implements OnInit {
    history: any;
    subscription: boolean = true;
    contentClass: string = 'content';

    constructor(
        private router: Router, 
        private historyService: HistoryService,
        private sharedService: SharedService
        ) 
    { 
        this.sharedService.collapse$.subscribe((collapse) => {
            if (collapse === true) {
                this.contentClass = 'content resize-margin-left';
            }
            else {
                this.contentClass = 'content'; 
            }
        }); 
    }

    ngOnInit() {
        // this.getAll();
        this.getHistory();
    }

    getHistory() {
        this.historyService.getAll().takeWhile(()=> this.subscription).subscribe((history:any) => { 
            this.history = history;
            for (var i = 0; i < this.history.length; ++i) {
                this.history[i].letter_of_intent.data.end_date = moment(history[i].letter_of_intent.data.date_commencement)
                .add(history[i].letter_of_intent.data.term_lease, 'M');
            }
        });
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
