import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Banks } from '../../modelsV2/banks';
import { BankService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';

@Component({
  selector: 'my-bank',
  templateUrl : './bank.component.html',
  
  providers: [BankService, AlertService]
})

export class AdminBankComponent implements OnInit {
	banks: Banks[];
  contentClass: string = 'content';

  constructor(private bankService: BankService, private router: Router, private alertService: AlertService, private sharedService: SharedService) 
  { 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

  ngOnInit(): void {
    this.getBanks();
  }

  getBanks() {
    this.bankService.getBanks().subscribe(banks => { 
        this.banks = banks;
        console.log(banks) ;
    });
  }

  deleteBank(value:Banks): void {
    this.bankService.delete(value._id)
      .then(response => {
        this.alertService.success('Delete bank successful', true);
        this.getBanks();
      },
      error => {
        alert(`The bank could not be deleted, server Error.`);
      }
    );
  }

  editBank(value:Banks): void {
    this.router.navigate(['admin/bank/' + value._id + '/edit']);
  }
}
