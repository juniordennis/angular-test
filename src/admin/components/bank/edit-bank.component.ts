import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Banks } from '../../modelsV2/banks';
import { BankService, AlertService } from '../../service/index';
import { SharedService } from '../../../client/services/shared.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'my-bank',
  templateUrl : './edit-bank.component.html',
  
  providers: [BankService, AlertService]
})

export class AdminEditBankComponent {
	myForm : FormGroup;
	banks: Banks[];
  dataBank: any = {};
  bankId: any;
  model: any = {};
  contentClass: string = 'content';

	constructor(private formbuilder: FormBuilder, private bankService : BankService, private alertService: AlertService,
              private activateRoute: ActivatedRoute, private location: Location, private router: Router, private sharedService: SharedService) 
  { 
    this.location = location; 
    this.sharedService.collapse$.subscribe((collapse) => {
        if (collapse === true) {
            this.contentClass = 'content resize-margin-left';
        }
        else {
            this.contentClass = 'content'; 
        }
    }); 
  }

	ngOnInit(): void {
    this.activateRoute.params.subscribe((params: Params) => {
      this.bankId = params['id'];
    });

    if (this.bankId != null) {
      this.bankService.getById(this.bankId)
        .subscribe(bank => {
        this.dataBank = bank;
      });
    }

		this.myForm = this.formbuilder.group({
      code : ['', Validators.required],
      name : ['', Validators.required],
      description : ['']
		});
	}

	createBank(): void {
    this.bankService.create(this.dataBank)
    .then(
      data => {
          this.alertService.success('Create Bank successful', true);
          this.router.navigate(['/admin/bank']);
      },
      error => {
          console.log(error);
          alert(`The Bank could not be save, server Error.`);
      }
    );
	}

  updateBank(): void {
    this.bankService.update(this.dataBank)
    .then(
      response => {
        this.alertService.success('Update bank successful', true);
        this.router.navigate(['/admin/bank']);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
}
