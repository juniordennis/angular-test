import { Attachments,Users } from './index'

export class Companies {
	id : string;
	name : string;
	registeration_number : string;
	document : [ Attachments ];
	created_by : [ Users ];
	created_at : string;
}