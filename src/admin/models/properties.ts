import { Amenities,Attachments,Users,Companies,Developments } from './index'

export class Properties {
	id : string;
	development : string;
	address : {
		floor: string,
		unit:  string,
		block_number:  number,
		street_name:  string,
		postal_code:  number,
		coordinates: [ string ],
		country:  string,
		full_address:  string,
		type: string
	};
	details : {
		size_sqf: number,
		size_sqm: number,
		bedroom: number,
		bathroom: number,
		price: number,
		psqft: number,
		psqm: number,
		available: string,
		furnishing: string,
		description: string,
		type: string,
		sale_date: string,
		property_type: string,
		tenure: string,
		completion_date: string,
		type_of_sale: string,
		purchaser_address_indicator: string,
		planning_region: string,
		planning_area: string
	};
	// amenities : [ Amenities ];
	// pictures : {
	// 	living : [ Attachments ],
	// 	dining : [ Attachments ],
	// 	bed : [ Attachments ],
	// 	toilet : [ Attachments ],
	// 	kitchen : [ Attachments ]
	// };
	// owned_type : string;
	// owner : {
	// 	user : Users,
	// 	company : Companies,
	// 	shareholder : [{
	// 		name : string,
	// 		identification_type : string,
	// 		identification_number : string,
	// 		identification_proof : {
	// 			front : Attachments ,
	// 			back : Attachments
	// 		}
	// 	}]
	// };
	// publish : boolean;
	// confirmation : {
	// 	status : string,
	// 	proof : string,
	// 	by : string,
	// 	date : string
	// };
	// status : string;
	// histories : [{
	// 	action : string,
	// 	date : string,
	// 	data : {}
	// }];
	// created_at : string;
} 