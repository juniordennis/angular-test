import { Users, Attachments, BlogCategories, Comments } from './index';

export class Blogs {
	id : string;
	cover : Attachments;
	category : BlogCategories;
	title : string;
	source : string;
	content : string;
	comments : [Comments];
	created_at : string;
	created_by : Users;
}