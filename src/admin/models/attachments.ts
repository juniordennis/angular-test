export class Attachments {
	id : string;
	name : string;
	key : string;
	type : string;
	metadata : {};
	remarks : string;
	uploaded_at : string;
}