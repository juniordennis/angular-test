import { Attachments } from './index'

export class Payments {
	id : string;
	type : string;
	fee : [{
		code : string,
		name : string,
		amount : number,
		status : string,
		refunded : boolean 
	}];
	// attachment: Attachments;
	// payment_confirmation: Attachments;
	fee_payment: string;
	total_payment: string;
	minus_payment: string;
	refund_payment: string;
	refund : boolean;
	remarks : string;
}