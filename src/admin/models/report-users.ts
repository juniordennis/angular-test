import { Users } from './index';

export class ReportUsers {
	id : string;
	username : string;
	user_id : Users;
	message : string;
	report_by : Users;
	created_at : string;
}