import { Attachments,Banks,Companies,Properties,Agreements } from './index';

export class Users {
	id : string;
	username : string;
	email : string;
	password : string;
	salt : string;
	phone : string;
	role : string;
	verification : {
		verified : boolean,
		verified_by : string,
		verified_date : string,
		expires : string,
		code : string
	};
	// tenant : {
	// 	data : {
	// 		name : string,
	// 		identification_type : string,
	// 		identification_number : string,
	// 		identification_proof : {
	// 			front : Attachments,
	// 			back : Attachments
	// 		},
	// 		bank_account : {
	// 			bank : Banks,
	// 			name : string,
	// 			no : number
	// 		}
	// 	},
	// 	histories : [{
	// 		date : string,
	// 		data : {}
	// 	}]
	// };
	// landlord : {
	// 	data : {
	// 		name : string,
	// 		identification_type : string,
	// 		identification_number : string,
	// 		identification_proof : {
	// 			front : Attachments,
	// 			back : Attachments
	// 		},
	// 		company : Companies,
	// 		bank_account : {
	// 			bank : Banks,
	// 			name : string,
	// 			no : number
	// 		}
	// 	},
	// 	histories : [{
	// 		date : string,
	// 		data : {}
	// 	}]
	// };
	// owned_properties : [ Properties ];
	// rented_properties : [{
	// 	until : string,
	// 	property : Properties,
	// 	agreements : Agreements
	// }];
	// agreements : [ Agreements ];
	// dreamtalk : [{
	// 	loginToken : string,
	// 	loginTokenExpires : string
	// }];
	// companies : Companies;
	created_at : string;
	report_user : {
		reported : boolean,
		count_report : number
	};
}