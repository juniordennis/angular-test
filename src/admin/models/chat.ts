import {Users, Properties } from './index';

export class ChatRooms {
	_id : string;
	room_id : string;
	property_id : Properties;
	landlord : Users;
	tenant : Users;
	manager : Users;
	status : string;
	archived : boolean;
	created_at : string;
}