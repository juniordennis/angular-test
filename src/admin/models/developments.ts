import { Properties } from './index'

export class Developments {
	id : string;
	name : string;
	slug : string;
	number_of_units : number;
	properties : [ Properties ];
	tenure : string;
	age : number;
	planning_region : string;
	planning_area : string;
	type_of_area : string;
	postal_district : string; 
}