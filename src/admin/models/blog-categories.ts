import { Users } from './index';

export class BlogCategories {
	id : string;
	name : string;
	description : string;
	created_by : Users;
	created_at : string;
}