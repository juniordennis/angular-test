import { Users } from './index';

export class Faqs {
	id : string;
	question : string;
	answer : string;
	for : string;
	created_at : string;
	created_by : Users;
}