import { Component } from '@angular/core';

@Component({
  selector: 'admin-app',
  templateUrl: './admin.component.html',
})

export class AdminComponent { 
	name = 'Staysmart'; 
	authToken: any;
	public options = {
        position: ["bottom", "right"],
        timeOut: 5000,
        lastOnBottom: true,
        showProgressBar: true,
        pauseOnHover: true,
        clickToClose: true,
        animate: "fromLeft"
    }

	getToken() {
        this.authToken = JSON.parse(localStorage.getItem('authToken' || null));
    }
}
