import { NgModule, Directive, ElementRef, ErrorHandler } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule, Title } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { RoutingModule } from '../../routes/route.module';
import { DataTableModule,SharedModule,ScheduleModule,DialogModule,InputMaskModule,FieldsetModule,CalendarModule,LightboxModule } from 'primeng/primeng';
import { Ng2TelInput } from '../components/ng2-intl-tel-input';

import { CarouselModule, BsDropdownModule, ModalModule, DatepickerModule, CollapseModule, AccordionModule } from 'ng2-bootstrap';
import { CKEditorModule } from 'ng2-ckeditor';
import { DropdownModule as DropdownPrimeng } from 'primeng/primeng';
import { AgmCoreModule, GoogleMapsAPIWrapper, MapsAPILoader } from 'angular2-google-maps/core';
import { PdfViewerComponent } from 'ng2-pdf-viewer';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { PopoverModule } from 'ngx-popover';

import { AdminComponent }  from './admin.component';
import { AdminHeaderComponent } from '../components/header/header.component';
import { AdminSidebarComponent } from '../components/sidebar/sidebar.component';

import { AdminLoginComponent } from '../components/login/login.component';
import { AdminDashboardComponent } from '../components/dashboard/dashboard.component';
import { AdminUserComponent } from '../components/user/user.component';
import { AdminUserDetailComponent } from '../components/user/user-detail.component';
import { AdminEditUserComponent } from '../components/user/edit-user.component';
import { AdminBankComponent } from '../components/bank/bank.component';
import { AdminEditBankComponent } from '../components/bank/edit-bank.component';
import { AdminFaqComponent } from '../components/faq/faq.component';
import { AdminEditFaqComponent } from '../components/faq/edit-faq.component';
import { AdminBlogCategoryComponent } from '../components/blog-category/blog-category.component';
import { AdminEditBlogCategoryComponent } from '../components/blog-category/edit-blog-category.component';
import { AdminAppointmentComponent } from '../components/appointment/appointment.component';
import { AdminAmenityComponent } from '../components/amenity/amenity.component';
import { AdminEditAmenityComponent } from '../components/amenity/edit-amenity.component';
import { AdminBlogComponent } from '../components/blog/blog.component';
import { AdminBlogDetailComponent } from '../components/blog/blog-detail.component';
import { AdminEditBlogComponent } from '../components/blog/edit-blog.component';
import { AdminReportUserComponent } from '../components/report-user/report-user.component';
import { AdminDevelopmentComponent } from '../components/development/development.component';
import { AdminEditDevelopmentComponent } from '../components/development/edit-development.component';
import { AdminLetterOfIntentComponent } from '../components/letter-of-intent/letter-of-intent.component';
import { AdminLetterOfIntentDetailComponent } from '../components/letter-of-intent/detail.component';
import { AdminTenancyAgreementComponent } from '../components/tenancy-agreement/tenancy-agreement.component';
import { AdminTenancyAgreementDetailComponent } from '../components/tenancy-agreement/detail.component';
import { AdminStampCertificateComponent } from '../components/stamp-certificate/stamp-certificate.component';
import { AdminPropertyComponent } from '../components/property/property.component';
import { AdminCreatePropertyComponent } from '../components/property/create-property/create-property.component';
import { AdminViewPropertyComponent } from '../components/property/view-property/view-property.component';
import { AdminHistoryComponent } from '../components/history/history.component';
import { AdminCertificateComponent } from '../components/certificate/certificate.component';
import { DropzoneComponent } from '../components/dropzone/dropzone.component';
import { LoadingComponent }  from '../components/loading.component';

//Directives
import { Collapse } from '../components/collapse/collapse.component';
import { CapitalizeFirstPipe }  from '../components/capitalize.component';
import { SafeHtmlPipe }  from '../components/safehtml.component';
import { AdminEqualValidator } from '../components/user/equal-validator.directive';

 
//Middleware
import { AsAdminMiddleware } from '../../middleware';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    SharedModule,
    ScheduleModule,
    InputMaskModule,
    DialogModule,
    FieldsetModule,
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    DatepickerModule.forRoot(),
    DropdownPrimeng,
    CalendarModule,
    RoutingModule,
    CKEditorModule,
    HttpModule,
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyAdFKpzbZrvYZA5v0e8EhnJ7T6fdp0v0So',
        libraries: ["places"],
        region: 'SG'
    }),
    SimpleNotificationsModule.forRoot(),
    PopoverModule,
    RouterModule, 
    LightboxModule
  ],
  declarations: [
    AdminLoginComponent,
    AdminComponent,
    AdminHeaderComponent,
    AdminSidebarComponent,
    AdminDashboardComponent,
    AdminUserComponent,
    AdminUserDetailComponent,
    AdminEditUserComponent,
    AdminBankComponent,
    AdminEditBankComponent,
    AdminFaqComponent,
    AdminEditFaqComponent,
    AdminBlogCategoryComponent,
    AdminEditBlogCategoryComponent,
    AdminAppointmentComponent,
    AdminAmenityComponent,
    AdminEditAmenityComponent,
    AdminBlogComponent,
    AdminBlogDetailComponent,
    AdminReportUserComponent,
    AdminEditBlogComponent,
    AdminDevelopmentComponent,
    AdminEditDevelopmentComponent,
    AdminLetterOfIntentComponent,
    AdminLetterOfIntentDetailComponent,
    AdminTenancyAgreementComponent,
    AdminTenancyAgreementDetailComponent,
    AdminStampCertificateComponent,
    AdminPropertyComponent,
    AdminCreatePropertyComponent,
    AdminViewPropertyComponent,
    AdminHistoryComponent,
    AdminCertificateComponent,
    DropzoneComponent,
    Collapse,
    PdfViewerComponent,
    Ng2TelInput,
    CapitalizeFirstPipe,
    SafeHtmlPipe,
    LoadingComponent,
    AdminEqualValidator
  ],
  // directives: [ DropzoneComponent ],
  providers: [ 
    AsAdminMiddleware,
  ],
  bootstrap: [ AdminComponent ]
})

export class AdminModule { }
