// MODULE
import { NgModule, Directive, ElementRef, ErrorHandler }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule, BsDropdownModule, ModalModule, DatepickerModule, CollapseModule, AccordionModule, TooltipModule  } from 'ng2-bootstrap';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule as DropdownPrimeng } from 'primeng/primeng';
import { SignaturePadModule } from 'angular2-signaturepad';
import { NouisliderModule } from 'ng2-nouislider';
import { SelectModule } from 'ng2-select';
// import { DDPClient, DDPStorage, DDPCacheEngine } from 'rxjs-ddp-client';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { PopoverModule } from 'ngx-popover';
import { ShareButtonsModule } from "ng2-sharebuttons";

import { AdminModule } from '../../admin/app/admin.module';
import { RoutingModule } from '../../routes/route.module';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { AgmCoreModule, GoogleMapsAPIWrapper, MapsAPILoader } from 'angular2-google-maps/core';
import { ScheduleModule, DialogModule, DataTableModule, CalendarModule, SharedModule, CheckboxModule } from 'primeng/primeng';
// COMPONENT
import { AppComponent }  from './app.component';
import { HomeComponent } from '../components/home/home.component';
import { CommissionComponent } from '../components/commission/commission.component';
import { FooterComponent } from '../components/footer/footer.component';
import { CopyrightComponent } from '../components/copyright/copyright.component';
import { NavbarComponent }  from '../components/navbar/navbar.component';
import { PropertyComponent }  from '../components/property/property.component';
import { ListPropertyComponent }  from '../components/list-property/list-property.component';
import { ListOnePropertyComponent }  from '../components/list-property/list-one.component';
import { ListTwoPropertyComponent }  from '../components/list-property/list-two.component';
import { ListThreePropertyComponent }  from '../components/list-property/list-three.component';
import { PropertyDetailComponent }  from '../components/property-detail/property-detail.component';
import { LoiInitiateComponent }  from '../components/loi-initiate/loi-initiate.component';
import { LoiViewComponent }  from '../components/loi-view/loi-view.component';
import { LoiReuploadComponent }  from '../components/loi-reupload/loi-reupload.component';
import { LoiConfirmComponent }  from '../components/loi-confirm/loi-confirm.component';
import { TaInitiateComponent }  from '../components/ta-initiate/ta-initiate.component';
import { TaViewComponent }  from '../components/ta-view/ta-view.component';
import { TaPaymentComponent }  from '../components/ta-payment/ta-payment.component';
import { IlViewComponent }  from '../components/il-view/il-view.component';
import { ChatComponent }  from '../components/chat/chat.component';
import { ScheduleViewingComponent }  from '../components/schedule-viewing/schedule-viewing.component';
import { VerificationComponent }  from '../components/verification/verification.component';
import { ForgetPasswordEmailComponent }  from '../components/forget-password-email/forget-password-email.component';
import { SettingComponent }  from '../components/setting/setting.component';
import { AboutUsComponent }  from '../components/about-us/about-us.component';
import { PrivacyComponent }  from '../components/privacy/privacy.component';
import { TermsConditionComponent }  from '../components/terms-condition/terms-condition.component';
import { FeesChargesComponent }  from '../components/fees-charges/fees-charges.component';
import { FaqComponent }  from '../components/faq/faq.component';
import { BlogComponent }  from '../components/blog/blog.component';
import { BlogViewComponent }  from '../components/blog-view/blog-view.component';
import { SavingsComponent }  from '../components/savings/savings.component';
import { MemberSectionComponent }  from '../components/member-section/member-section.component';
import { ListingComponent }  from '../components/member-section/listing/listing.component';
import { FavouriteComponent }  from '../components/member-section/favourite/favourite.component';
import { ProposedComponent }  from '../components/member-section/proposed/proposed.component';
import { UpcomingTemplateComponent }  from '../components/member-section/upcoming/upcoming-template.component';
import { UpcomingComponent }  from '../components/member-section/upcoming/upcoming.component';
import { UpcomingDetailComponent }  from '../components/member-section/upcoming/upcoming-detail.component';
import { ArchivedComponent }  from '../components/member-section/archived/archived.component';
import { LetterOfIntentComponent }  from '../components/member-section/letter-of-intent/letter-of-intent.component';
import { TenancyAgreementComponent }  from '../components/member-section/tenancy-agreement/tenancy-agreement.component';
import { InventoryComponent }  from '../components/member-section/inventory/inventory.component';
import { SignatureComponent }  from '../components/signature.component';
import { LoadingComponent }  from '../components/loading.component';
import { SafeHtmlPipe }  from '../components/safehtml.component';
import { CapitalizeFirstPipe }  from '../components/capitalize.component';
import { CapitalizeFirstIfAllPipe }  from '../components/capitalize-all.component';
import { NotFoundComponent }  from '../components/404/404.component';
import { ControlMessagesComponent } from '../components/control-message/control-message.component';
// SERVICE
import { MyErrorHandler } from '../error-handler';
import { AttachmentService } from '../services/attachment.service';
import { AppointmentService }  from '../services/appointment.service';
import { BankService } from '../services/bank.service';
import { ChatService } from '../services/chat.service';
import { ReportService } from '../services/report.service';
import { UserService } from '../services/user.service';
import { AgreementService } from '../services/agreement.service';
import { AuthenticationService } from '../services/authentication.service';
import { ManagerService } from '../services/manager.service';
import { DropzoneComponent } from '../components/dropzone.component';
import { PropertyService } from '../services/property.service';
import { SharedService } from '../services/shared.service';
import { AmenityService } from '../services/amenity.service';
import { FacebookService } from 'ng2-facebook-sdk';
import { StorageService } from '../services/storage.service';
import { ValidationService } from '../services/validation.service';
import { BlogService } from '../services/blog.service';
import { FaqService } from '../services/faq.service';
import { NotificationUserService } from '../services/notification-user.service';
import { OdometerService } from '../services/odometer.service';
// DIRECTIVE
import { EqualValidator } from '../equal-validator.directive';
import { FocusDirective } from '../autofocus';
import { DirectionsMapDirective } from '../google-map-direction.directive';
import { ScrollDirective } from '../scroll-directive';
import { OrderBy } from '../components/orderBy'

// import { MyDDPClient } from '../my-ddp-client';
import { VerifiedMiddleware, LoggedInMiddleware } from '../../middleware';

// import { TestDirective, CustomMarkerDirective } from '../directives/test.directive';

@NgModule({
  imports: [
    CheckboxModule,
    AccordionModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    CarouselModule.forRoot(),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    RouterModule,
    ModalModule.forRoot(),
    AdminModule,
    RoutingModule,
    ScheduleModule,
    DialogModule,
    DataTableModule,
    DropdownPrimeng,
    SignaturePadModule,
    CollapseModule.forRoot(),
    DatepickerModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBM2rfc8PllYP7eboUbDg9_Aielo4h7J-g',
      libraries: ["places"],
      region: 'SG'
    }),
    CalendarModule,
    SharedModule,
    SimpleNotificationsModule.forRoot(),
    PopoverModule,
    NouisliderModule,
    ShareButtonsModule.forRoot(),
    SelectModule,
    TooltipModule.forRoot(),

  ],
  declarations: [
    EqualValidator,
    FocusDirective,
    DirectionsMapDirective,
    AppComponent,
    HomeComponent,
    CommissionComponent,
    FooterComponent,
    CopyrightComponent,
    NavbarComponent,
    PropertyComponent,
    ListPropertyComponent,
    ListOnePropertyComponent,
    ListTwoPropertyComponent,
    ListThreePropertyComponent,
    PropertyDetailComponent,
    LoiInitiateComponent,
    LoiViewComponent,
    LoiReuploadComponent,
    LoiConfirmComponent,
    TaInitiateComponent,
    TaViewComponent,
    TaPaymentComponent,
    IlViewComponent,
    ChatComponent,
    ScheduleViewingComponent,
    VerificationComponent,
    ForgetPasswordEmailComponent,
    SettingComponent,
    AboutUsComponent,
    PrivacyComponent,
    TermsConditionComponent,
    FeesChargesComponent,
    FaqComponent,
    BlogComponent,
    BlogViewComponent,
    SavingsComponent,
    MemberSectionComponent,
    ListingComponent,
    FavouriteComponent,
    ProposedComponent,
    ArchivedComponent,
    LetterOfIntentComponent,
    TenancyAgreementComponent,
    InventoryComponent,
    UpcomingTemplateComponent,
    UpcomingComponent,
    UpcomingDetailComponent,
    SignatureComponent,
    LoadingComponent,
    SafeHtmlPipe,
    CapitalizeFirstPipe,
    CapitalizeFirstIfAllPipe,
    OrderBy,
    NotFoundComponent,
    DropzoneComponent,
    ControlMessagesComponent,
    // TestDirective,
    // CustomMarkerDirective,
    ScrollDirective
  ],
  providers: [
    { provide: ErrorHandler, useClass: MyErrorHandler },
    AttachmentService,
    AgreementService,
    AppointmentService,
    BankService,
    ChatService,
    UserService,
    AuthenticationService,
    GoogleMapsAPIWrapper,
    AgreementService,
    PropertyService,
    SharedService,
    AmenityService,
    FacebookService,
    StorageService,
    AppComponent,
    NavbarComponent,
    LoggedInMiddleware,
    VerifiedMiddleware,
    ManagerService,
    ValidationService,
    BlogService,
    FaqService,
    NotificationUserService,
    OdometerService,
    ReportService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
