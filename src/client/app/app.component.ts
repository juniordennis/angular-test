import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { SharedService } from '../services/shared.service';
import { StorageService } from '../services/storage.service';
import { DDPCacheEngine } from 'rxjs-ddp-client';
import { MyDDPClient } from '../my-ddp-client';
import { NotificationsService } from 'angular2-notifications';
import {
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError,
    Router
} from '@angular/router';
declare var ga:Function;

// const myDDPClient = new MyDDPClient();

@Component({
  selector: 'my-app',
  templateUrl: `./../templates/app.html`,
  providers: []
})
export class AppComponent implements OnInit {
  _storageService: DDPCacheEngine = this.storageService;
  name: any;
  // ddpClient: MyDDPClient;
  private showFooter: Boolean = true;
  private showHeader: Boolean = true;
  public options = {
        position: ["bottom", "right"],
        timeOut: 5000,
        lastOnBottom: true,
        showProgressBar: true,
        pauseOnHover: true,
        clickToClose: true,
        animate: "fromLeft"
    }

	constructor(
    private router: Router,
    private userService: UserService,
    private sharedService: SharedService,
    // private ddpClient: MyDDPClient,
    private storageService: StorageService,
    private notificationsService: NotificationsService
  ) {
    this.router.events.subscribe((route) => {
      if (route instanceof NavigationEnd) {
        if (!(route.urlAfterRedirects.includes('/admin'))) {
          ga('set', 'page', route.urlAfterRedirects);
          ga('send', 'pageview');
        }
      }
    })
  }
	ngOnInit(): void {
    // connect to DDP
    // this.ddpClient = new MyDDPClient();
    // this.ddpClient.initCacheStorage(this._storageService);
    // this.ddpClient.connect();
    // console.log(this.ddpClient);

    this.sharedService.changeEmitted$.subscribe(change => {
      this.showFooter = change.showFooter !== undefined ? change.showFooter : true;
      this.showHeader = change.showHeader !== undefined ? change.showHeader : true;
    });

    this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
    });
  }
}
