export class Listing {
	id: number;
	name: string;
	place: string;
	monthly: number;
	image: string;
}