import { Manager } from './manager'

export class ChatRoom {
	_id: string;
	room_id: string;
	property_id: string;
	landlord: string;
	tenant: string;
	manager: Manager;
	created_at: Date;
}
