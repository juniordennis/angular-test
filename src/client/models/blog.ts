import { BlogCategory } from './blog-category'
import { Comment } from './comment'

export class Blog {
	id: number;
	title: string;
	cover: string;
	category: BlogCategory[];
	content: string;
	comments: Comment[];
	slug: string;
	source: string;
	created_at: Date;
	created_by: string;
}