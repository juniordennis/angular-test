export class Payment {
    id: string;
    type: string;
    fee: { 
        code: string,
        name: string,
        amount: number,
        status: string,
        refunded: boolean
    };
    attachment: string;
    refund: boolean;
    remarks: string;
}