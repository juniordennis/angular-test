export class Amenity {
	id: string;
	name: string;
	description: string;
	icon: string;
	created_at: Date;
}