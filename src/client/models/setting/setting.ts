export class Setting {
	id: number;
	username: string;
	identification: string;
	email: string;
	phone: number;
}