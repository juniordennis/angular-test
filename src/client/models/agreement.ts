import { Attachment } from './attachment'
import { Property } from './property'

export class Agreement {
    room_id: string;
    landlord: string;
    tenant: string;
    property: Property;
    appointment: string;
    letter_of_intent: {
        data: {
            monthly_rental: number,
            term_lease: number,
            date_commencement: Date,
            requirements: string,
            populate_tenant: string,
            landlord: {
                full_name: string,
                identification_type: string,
                identification_number: string,
                identification_proof: {
                    front: Attachment[],
                    back: Attachment[]
                };
            };
            tenant: {
                name: string,
                identification_type: string,
                identification_number: string,
                identification_proof: {
                    front: Attachment[],
                    back: Attachment[]
                };
            };
            occupiers: {
                name: string,
                identification_type: string,
                identification_number: string
                identification_proof: {
                    front: Attachment[],
                    back: Attachment[]
        	    };
            };
            gfd_amount: number,
            sd_amount: number,
            security_deposit: number,
            term_payment: number,
            minor_repair_cost: number,
            lapse_offer: number,
            term_lease_extend: number,
            appointment: string,
            property: string,
            confirmation: {
                tenant: {
                    sign: string,
                    date: Date,
                    remarks: string
                };
                landlord: {
                    sign: string,
                    date: Date,
                    remarks: string
                };
            };
            payment: string,
            status: string,
            created_at: Date
        };
    };
    tenancy_agreement: {
        data: {
            confirmation: {
                tenant: {
                    sign: string,
                    date: Date,
                    remarks: string
                };
                landlord: {
                    sign: string,
                    date: Date,
                    remarks: string
                };
            };
            payment: string,
            status: string,
            created_at: Date
        };
    };
    inventory_list: {
        data: {
            id: string,
            confirmation: {
                tenant: {
                    sign: string,
                    date: Date,
                    remarks: string
                };
                landlord: {
                    sign: string,
                    date: Date,
                    remarks: string
                };
            };
            status: string,
            property: string,
            created_at: Date
            list: {
                name: string,
                items: {
                    name: string,
                    quantity: number,
                    remark: string,
                    attachments: Attachment[],
                    landlord_check: boolean,
                    tenant_check: boolean
                };
            };
        };
    };
}