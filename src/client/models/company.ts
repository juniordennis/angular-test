import { Attachment } from './attachment'

export class Company {
    id: string;
    name: string;
    registration_number: number;
    documents: string;
    shareholders: {
        name: string,
        identification_type: string,
        identification_number: string,
        identification_proof: {
            front: Attachment[],
            back: Attachment[]
		};
	};
    created_by: string;
    created_at: Date;
}