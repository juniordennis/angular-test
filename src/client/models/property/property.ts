
export class Property {
  _id  : string;
  development : string;
  address: {
    floor: string;
    unit: string;
    block_number: string;
    street_name: string;
    postal_code: string;
    coordinates: any;
    country: string;
    full_address: string;
    type: string;
  };
  details: {
    size_sqf: number;
    size_sqm: number,
    bedroom: number;
    bathroom: number;
    price: number;
    psqft: number;
    psqm: number;
    available: Date;
    furnishing: string;
    description: string,
    type: string;
    sale_date: string;
    property_type: string;
    tenure: string;
    completion_date: 2011,
    type_of_sale: string;
    purchaser_address_indicator: string;
    planning_region: string;
    planning_area: string;
  };
  schedules: [
    {
      _id : string;
      day : string;
      start_date : Date;
      time_from: string;
      time_to : string;
    }
  ];
  amenities: any[];
  pictures: {
    living: string[];
    dining: string[];
    bed: string[];
    toilet: string[];
    kitchen: string[];
  };
  owned_type: string;
  owner: {
    user: string;
    company: string;
  };
  publish: boolean;
  manager: string;
  confirmation: {
    status: string;
    proof: string;
    by: string;
    date: Date;
  };
  status: string;
  histories: [
    { 
      action: string; 
      date: string;
      data: any;
    }
  ];
  created_at: Date;
}
