import { Property } from './property'

export class Development {
    _id: string;
    name: string;
    slug: string;
    number_of_units: number;
    properties: Property[];
    tenure: string;
    description: string;
    age: number;
    planing_region: string;
    planing_area: string;
    type_of_area: string;
    postal_district: number;
    address: {
        block_number: number,
        street_name: string,
        postal_code: number,
        coordinates: string,
        country: string,
        full_address: string,
        type: string
    };
}