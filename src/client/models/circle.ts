export class Circle {
	lat: number;
	lng: number;
	radius: number;
	draggable:boolean;
	visible:boolean;
}