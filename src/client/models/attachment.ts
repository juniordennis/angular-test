export class Attachment {
	_id  : string;
	name : string;
	key: string;
	size: string;
	type: string;
	remarks: string;
	uploaded_at: Date;
	url: string;
}
