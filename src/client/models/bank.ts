export class Bank {
	id: string;
	code: number;
	name: string;
	description: string;
	created_at: Date;
}