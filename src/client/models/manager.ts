export class Manager {
    property: string;
    data: {
        manager: string,
        owner: string,
        status: string,
        created_at: Date
    };
}