export class BlogCategory {
	id: string;
	name: string;
	description: string;
	created_by: string;
	created_at: Date;
}