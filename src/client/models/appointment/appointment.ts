export class Appointment {
    _id: string;
    room_id: string;
    landlord: string;
    tenant: string;
    property: string;
    schedule: string;
    chosen_time: {
        date: Date,
        from: string,
        to: string
    };
    status: string;
}

