import { Attachment } from './attachment'
import { ChatRoom } from './chat-room'
import { Property } from './property'
import { Company } from './company'
import { Agreement } from './agreement'

export class User {
	_id: number;
	username: string;
	email: string;
	password: string;
	phone: number;
	user: string;
	role: string;
	verification: {
		verified: boolean,
    	verified_date: Date,
    	expires: Date,
    	code: string
	};
	tenant: {
    	data: {
	    	name: string,
	    	identification_type: string,
	    	identification_number: string,
	    	identification_proof: {
		    	front: Attachment[],
		    	back: Attachment[]
	     	};
		    bank_account: {
		    	bank: string,
		        name: string,
		        no: number
		    };
    	};
	};
	landlord: {
		data: {
			name: string,
	    	identification_type: string,
	    	identification_number: string,
	    	identification_proof: {
	    		front: string,
	    		back: string
	    	};
			owners: {
			    name: string,
			    identification_type: string,
			    identification_number: string,
			    identification_proof: {
			    	front: string,
			    	back: string
			      	};
			};
		    bank_account: {
		    	bank: string,
		        name: string,
		        no: number
		    };
		};
  	};
	owned_properties: Property[];
	rented_properties: Property[];
	shortlisted_properties: Property[];
  	agreements: Agreement[];
	dreamtalk: {
    	_id: string,
    	loginToken: string,
    	loginTokenExpires: Date,
    };
	managed_poperty: Property[];
	chat_rooms: ChatRoom[];
	shortlisted_property: Property[];
	blocked_users: string;
	companies: Company[];
	services: {
		facebook: {
			facebookId: string,
			facebookToken: string
		};
    };
	reset_password: {
		token: string,
		created_at: Date,
		expired_at: Date
  	};
	created_at: Date;
}
