import { Attachment } from './attachment'
import { Manager} from './manager'
import { Amenity } from './amenity'

export class Property {
    _id: string;
    development: string;
    address: {
        floor: number,
        unit: number,
        block_number: number,
        street_name: string,
        postal_code: number,
        coordinates: string,
        country: string,
        full_address: string,
        type: string
    };
    details: {
        size_sqf: number,
        size_sqm: number,
        bedroom: number,
        bathroom: number,
        price: number,
        psqft: number,
        psqm: number,
        vailable: Date,
        furnishing: string,
        description: string,
        type: string,
        sale_date: Date,
        property_type: string,
        tenure: string,
        completion_date: Date,
        type_of_sale: string,
        purchaser_address_indicator: string,
        planning_region: string,
        planning_area: string
    };
    schedules: {
      _id: string,
      day: string,
      start_date: Date,
      time_from: Date,
      time_to: Date
    };
    amenities: Amenity[];
    pictures: {
        living: Attachment[],
        dining: Attachment[],
        bed: Attachment[],
        toilet: Attachment[],
        kitchen: Attachment[]
    };
    owned_type: string;
    owner: {
        user: string,
        company: string
    };
    manager: Manager[];
    confirmation: {
        status: string,
        proof: string,
        by: string,
        date: Date
    };
    temp: {
        owners: {
            name: string,
            identification_type: string,
            identification_number: string,
            dentification_proof: {
                front: Attachment[],
                back: Attachment[]
            };
        };
        shareholders: {
            name: string,
            identification_type: string,
            identification_number: string,
            identification_proof: {
                front: Attachment[],
                back: Attachment[]
            };
        }
    };
    status: string;
    created_at: Date;
}