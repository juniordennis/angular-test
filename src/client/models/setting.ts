export class Settings {
	id: number;
	username: string;
	fullname: string;
	password: string;
	identification: string;
	email: string;
	phone: number;
	user: string;
}