export class Faq {
	id: string;
	question: string;
	answer: string;
	for: string;
	created_at: Date;
	created_by: string;
}