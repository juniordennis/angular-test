export class Comment {
	id: string;
	name: string;
	email: string;
	user: string;
	content: string;
	blog: string;
	type: string;
	created_at: Date;
	replies: string;
}