export class Report {
	_id: string;
	reported: string;
	reporter: string;
	reason: string;
	created_at: Date;
}