
export var Mockappointment: any[] = [
  {
  	room_id: "2039832fj023jf320fj",
  	landlord: "1",
  	tenant: "2",
  	property: "583r3ddd97c971ddd3",
  	schedule: "239520952u09aefa",
  	choosen_time: {
    	date: "2016-09-23T06:38:28.483Z",
    	from: "16:30",
    	to: "17:00"
  	},
  	status: "pending"
  },{
    room_id: "2039832fj023jf320fj",
    landlord: "2",
    tenant: "1",
    property: "583r3ddd97c971ddd3",
    schedule: "239520952u09aefa",
    choosen_time: {
      date: "2016-09-23T06:38:28.483Z",
      from: "16:30",
      to: "17:00"
    },
    status: "pending"
  },{
    room_id: "2039832fj023jf320fj",
    landlord: "1",
    tenant: "2",
    property: "583r3ddd97c971ddd3",
    schedule: "239520952u09aefa",
    choosen_time: {
      date: "2016-09-23T06:38:28.483Z",
      from: "17:00",
      to: "17:30"
    },
    status: "pending"
  },{
    room_id: "2039832fj023jf320fj",
    landlord: "2",
    tenant: "1",
    property: "583r3ddd97c971ddd3",
    schedule: "239520952u09aefa",
    choosen_time: {
      date: "2016-09-23T06:38:28.483Z",
      from: "17:00",
      to: "17:30"
    },
    status: "pending"
  }]