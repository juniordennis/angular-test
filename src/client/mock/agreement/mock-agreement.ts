export var Mockagreement: any[] = [{
  "room_id": "2039832fj023jf320fj",
  "landlord": "1",
  "tenant": "2",
  "property": "583r3ddd97c971ddd3",
  "appointment": "3r3ddd97c971ddd3",
  "letter_of_intent": {
    "data": {
      "monthly_rental": 5500,
      "term_lease": 6,
      "date_commencement": "2017-01-21T00:00:00.000Z",
      "requirements": ["Nothing"],
      "populate_tenant": true,
      "landlord": {
        "full_name": "Junior",
        "type": "passport",
        "identification_number": "23052-3A0-B",
        "identity_front": "ZZoENjte4Bwy6uZ7G",
        "identity_back": "5ewjSnDKvXjjQuQR7"
      },
      "tenant": {
        "name": "Dennis",
        "type": "passport",
        "identification_number": "23052-3A0-B",
        "identity_front": "ZZoENjte4Bwy6uZ7G",
        "identity_back": "5ewjSnDKvXjjQuQR7"
      },
      "gfd_amount": 5500,
      "sd_amount": 132,
      "security_deposit": 5500,
      "term_payment": 1,
      "minor_repair_cost": 200,
      "lapse_offer": 7,
      "term_lease_extend": 6,
      "appointment": "M2JC6xiywrFdED9q2",
      "property": "nNtGhPeQTvKRxHiaH",
      "confirmation": {
        "tenant": {
          "sign": "data:image/png;base64",
          "date": "2017-01-20T04:13:00.179Z",
          "remarks": ""
        },
        "landlord": {
          "sign": "data:image/png;base64",
          "date": "2017-01-20T04:13:00.179Z",
          "remarks": ""
        }
      },
      "payment": "9S49fke93WfkeDDeafe",
      "status": "accepted",
      "created_at": "2017-01-20T04:13:00.179Z"
    },
    "histories": [
      {
        "date": "2016-09-23T06:38:28.483Z",
        "data": {}
      }
    ]
  },
  "tenancy_agreement": {
    "data": {
      "confirmation": {
        "tenant": {
          "sign": "data:image/png;base64",
          "date": "2017-01-20T04:13:00.179Z",
          "remarks": ""
        },
        "landlord": {
          "sign": "data:image/png;base64",
          "date": "2017-01-20T04:13:00.179Z",
          "remarks": ""
        }
      },
      "stamp_certificate": "foto/pdf",
      "payment": "9S49fke93WfkeDDeafe",
      "status": "pending/accepted/expired/tenant-confirmation/admin-confirmation",
      "created_at": "2017-01-20T04:13:00.179Z"
    },
    "histories": [
      {
        "date": "2016-09-23T06:38:28.483Z",
        "data": {}
      }
    ]
  },
  "inventory_list": {
    "data": {
      "id": "GhaPLqXNcu9sLaaPy",
      "confirmation": {
        "tenant": {
          "sign": "data:image/png;base64",
          "date": "2017-01-20T04:13:00.179Z",
          "remarks": ""
        },
        "landlord": {
          "sign": "data:image/png;base64",
          "date": "2017-01-20T04:13:00.179Z",
          "remarks": ""
        }
      },
      "status": "pending/completed",
      "property": "5ewjSnDKvXjjQuQR7",
      "created_at": "2017-02-14T07:44:39.818Z",
      "list": [
        {
          "name": "Kitchen (Wet)",
          "items": [
            {
              "name": "item1",
              "quantity": "1",
              "remark": "asdasd",
              "attachments": ["a3q09fjq0", "qfqj3fiqfj"],
              "landlord_check": true,
              "tenant_check": true
            }
          ]
        }
      ]
    },
    "histories": [
      {
        "date": "2016-09-23T06:38:28.483Z",
        "data": {}
      }
    ]
  },
}]