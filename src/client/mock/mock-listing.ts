export const LISTING: any[] = [
    { id: 11, name: 'Mr. Nice', place: 'DownTown', monthly: 5134, image: '' },
    { id: 12, name: 'Mr. Bad', place: 'DownHell', monthly: 2312, image: '' },
    { id: 13, name: 'Mr. Killer', place: 'DownHeaven', monthly: 1223, image: '' },
    { id: 14, name: 'Mr. Optimist', place: 'DownFountain', monthly: 9568, image: '' },
  ];
