import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Report } from '../models/report';
import { url } from '../../global';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ReportService {
  constructor(private http : Http) {}
	data: any;

  getAll() {
    return this.http.get(url + 'user_reports', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getById(id:any) {
    return this.http.get(url + 'user_reports/' + id, this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  getDetailUserReported(id:any) {
    return this.http.get(url + 'user_reports/reported/' + id, this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  countGroupReportUser() {
    return this.http.get(url + 'user_reports/count', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  create(body:any): Promise<any> {
    return this.http.post(url +  'user_reports', body, this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  delete(idUser:any): Promise<void> {
    return this.http.delete(url + 'user_reports/reported/' + idUser, this.jwt())
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  reportsUser(id:string) {
    return this.http.post(url + 'user_reports/reported/' + id, '', this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  private jwt() {
    let currentUser = JSON.parse(localStorage.getItem('authToken'));
    if (currentUser && currentUser.token) {
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
        return new RequestOptions({ headers: headers });
    }
  }
}