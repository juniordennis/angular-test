import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Settings } from '../models/setting';
import { SETTINGS } from '../mock/mock-settings';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class SettingService {
	data: any;

	getSettings(): Promise<Settings[]> {
		return Promise.resolve(SETTINGS);
	}

	getSetting(id:any): Promise<Settings[]> {
  		for (var i = 0; i < SETTINGS.length; i++) {
	  		if (SETTINGS[i].id == id) {
	  			this.data = SETTINGS[i];
	  		}
  		}
  		return Promise.resolve(this.data);
  	}

  getCurrentPassword(id:any): Promise<Settings[]> {
      for (var i = 0; i < SETTINGS.length; i++) {
        if (SETTINGS[i].id == id) {
          this.data = SETTINGS[i].password;
        }
      }
      return Promise.resolve(this.data);
    }

  	update(id:any,value:any): Promise<Settings[]> {
   	// return this.http.post(url + '' + id,value, this.jwt())
    //   .toPromise()
    //   .then(res => res.json().data)
    //   .catch(this.handleError);
    return Promise.resolve(SETTINGS);
	}
}