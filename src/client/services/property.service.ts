import { Injectable } from '@angular/core';
import { url } from '../../global';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import { Marker } from '../models/marker';


@Injectable()
export class PropertyService {
  public headers: Headers;
  public token: string;
  constructor(private http: Http, private router: Router) { }

  getMapsDevelopments(latlng: any, pricemin: any, pricemax: any, bedroom: any, bathroom: any, available: any, sizemin: any, sizemax: any, location: any, radius: any) {
    return this.http.get(url + 'developments/browse/' + latlng + '/' + pricemin + '/' + pricemax + '/' + bedroom + '/' + bathroom + '/' + available + '/' + sizemin + '/' + sizemax + '/' + location + '/' + radius, this.jwt())
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().message || 'Server error'));
  }

  getAllDevelopments() {
    return this.http.get(url + 'developments/', this.jwt())
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().message || 'Server error'));
  }

  getAllProperty(latlng: any, pricemin: any, pricemax: any, bedroom: any, bathroom: any, available: any, sizemin: any, sizemax: any, location: any, radius: any) {
    return this.http.get(url + 'properties/browse/' + latlng + '/' + pricemin + '/' + pricemax + '/' + bedroom + '/' + bathroom + '/' + available + '/' + sizemin + '/' + sizemax + '/' + location + '/' + radius, this.jwt())
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().message || 'Server error'));
  }

  getPropertyById(id: any) {
    return this.http.get(url + 'properties/' + id, this.jwt())
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().message || 'Server error'));
  }

  create(body: any): Promise<any> {
    return this.http.post(url + 'properties/', JSON.stringify(body), this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  update(body: any, id: string): Promise<any> {
    return this.http.put(url + 'properties/update/' + id, JSON.stringify(body), this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  checkDraft() {
    return this.http.get(url + 'properties/draft', this.jwt())
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().message || 'Server error'));
  }


  shortlist(id: any): Promise<any> {
    return this.http.post(url + 'properties/shortlist_property/' + id, '', this.jwt())
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  unshortlist(id: any): Promise<any> {
    return this.http.delete(url + 'properties/shortlist_property/' + id, this.jwt())
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('authToken'));
    if (currentUser && currentUser.token) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + currentUser.token });
      return new RequestOptions({ headers: headers });
    }
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
