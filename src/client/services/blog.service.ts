import { Injectable } from '@angular/core';

import { url } from '../../global';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Blog } from '../models/blog'
import { BLOG } from '../mock/mock-blog'

@Injectable()
export class BlogService {
	public headers: Headers;
    public data: any;
    constructor(private http: Http,private router: Router) {}

	getAll() {
        return this.http.get(url + 'blogs', this.jwt())
        	.map((res:Response) => res.json())
        	.catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

	getBySlug(slug:string) {
        return this.http.get(url + 'blogs/slug/' + slug, this.jwt())
        	.map((res:Response) => res.json())
        	.catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    delete(id: string): Promise<void> {
        return this.http.delete(url + 'comments/' + id, this.auth())
            .toPromise()
            .then(() => null)
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    loadComments() {
        return this.http.get(url + 'comments/', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    createComments(body: any): Promise<any> {
        return this.http.post(url +  'comments/', JSON.stringify(body), this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    subscribe(body: any): Promise<any> {
        return this.http.post(url +  'subscribes/', JSON.stringify(body))
            .toPromise()
            .then(res => res.json().data)
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }
    
    getUsers() {
        return this.http.get(url + 'users', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    private auth() {
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }

    private jwt() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return new RequestOptions({ headers: headers });
    }
}