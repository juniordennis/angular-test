import { Injectable } from '@angular/core';
import { DDPClient } from 'rxjs-ddp-client';
import { MyDDPClient } from '../my-ddp-client';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { url, googleMapStyles } from '../../global';
import 'rxjs/add/operator/toPromise';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SharedService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  private navbarDdp = new BehaviorSubject<any>(null);
  // Observable string streams
  ddp$ = this.navbarDdp.asObservable();
  changeEmitted$ = this.emitChangeSource.asObservable();

  collapse:boolean;
  collapse$:Subject<boolean> = new Subject();
  // Service message commands
  emitChange(change: any) {
    this.emitChangeSource.next(change);
  }

  updateDdp(ddp: MyDDPClient) {
    this.navbarDdp.next(ddp);
  }

  getDdp() {
    return this.navbarDdp.asObservable();
  }

  getCustomStyles() {
    return Promise.resolve(googleMapStyles);
  }

  changeAdminSidebarClass() {
    this.collapse = !this.collapse;
    this.collapse$.next(this.collapse);
  }

}
