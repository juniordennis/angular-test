import { Injectable } from '@angular/core';
import { url } from '../../global';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';

@Injectable()
export class AmenityService {
	public headers: Headers;
    public token: string;
    constructor(private http: Http,private router: Router) {}

	getAll(){
        return this.http.get(url + 'amenities', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    private jwt() {
            let headers = new Headers({ 'Content-Type': 'application/json' });
            return new RequestOptions({ headers: headers });
    }
}