import { Injectable } from '@angular/core';

import { Listing } from '../models/listing'
import { LISTING } from '../mock/mock-listing'

@Injectable()
export class ListingService {
	getListings(): Promise<Listing[]> {
		return Promise.resolve(LISTING);
	}
}