import { Injectable } from '@angular/core';
import { url } from '../../global';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';


@Injectable()
export class FaqService {
	public headers: Headers;
    public token: string;
    constructor(private http: Http) {}

	getAll(){
        return this.http.get(url + 'faqs')
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }
    getFilter(filter:string){
        return this.http.get(url + 'faqs/filter/' + filter)
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }
    
    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}