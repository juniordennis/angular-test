import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { url } from '../../global';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HistoryService {
    constructor(private http : Http) {}
    data: any;

    getAll() {    
        return this.http.get(url + 'agreements/history', this.jwt())
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    private jwt() {
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}