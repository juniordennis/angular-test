import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { url } from '../../global';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ChatService {
    constructor(private http: Http) {}

    getAll(){
        return this.http.get(url + 'chats', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    getByUser(){
        return this.http.get(url + 'chats/users', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    getById(id:string){
        return this.http.get(url + 'chats/' + id, this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    createRoom(body:any): Promise<any> {
        return this.http.post(url +  'chats/room/create', body, this.jwt())
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    update(body:any): Promise<any> {
        return this.http.post(url + 'developments/update/' + body._id,body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    delete(id: string): Promise<void> {
        return this.http.delete(url + 'chats/rooms/delete/' + id, this.jwt())
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }

}
