import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Appointment } from '../../../models/appointment/appointment'
import { Mockappointment } from '../../../mock/appointment/mock-appointment'
// import { url } from '../global';
import 'rxjs/add/operator/toPromise';
 
@Injectable()
export class AppointmentService {
    constructor(private http: Http) {}

    getAppointments(): Promise<Appointment[]> {
        return Promise.resolve(Mockappointment);
    }

    getAppointment(id: string): Promise<Appointment> {
        return this.getAppointments()
            .then(data => data.find(data => data._id === id));
    }

    // getAll(){
    //     return this.http.get(url + 'facilities', this.jwt())
    //         .map((res:Response) => res.json())
    //         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    // }

    // getById(id:string){
    //     return this.http.get(url + 'facilities/' + id, this.jwt())
    //         .map((res:Response) => res.json())
    //         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    // }

    // create(body:any): Promise<Facility> {
    //     return this.http.post(url +  'facilities', body, this.jwt())
    //         .toPromise()
    //         .then(res => res.json().data)
    //         .catch(this.handleError);
    // }

    // update(body:Facility): Promise<Facility> {
    //     return this.http.post(url + 'facilities/update/' + body._id,body, this.jwt())
    //         .toPromise()
    //         .then(res => res.json().data)
    //         .catch(this.handleError);
    // }

    // delete(id: string): Promise<void> {
    // return this.http.delete(url + 'facilities/' + id, this.jwt())
    //   .toPromise()
    //   .then(() => null)
    //   .catch(this.handleError);
    // }

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}