import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AuthenticationService } from './authentication.service';
import { url } from '../../global';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {

	constructor(private http: Http, private authenticationService: AuthenticationService) { }

	getAll(){
		return this.http.get(url + 'users', this.jwt())
			.map((res:Response) => res.json())
			.catch((error:any) => Observable.throw(error.json().message || 'Server error'));
	}

	getByToken() {
		return this.http.get(url + 'me', this.jwt())
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().message || 'Server error'));
	}

	getById(id:string) {
		return this.http.get(url + 'users/' + id, this.jwt())
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().message || 'Server error'));
	}

	getCode(id: any) {
		return this.http.post(url + 'users/verification_code/' + id, '', this.jwt())
            .toPromise()
            .then(res => res)
            .catch(this.handleError);
	}

	Activate(id: any, code: any) {
		console.log(code);
		return this.http.post(url + 'users/activate/' + id, code, this.jwt())
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().message || 'Server error'));
	}

	updateData(userID: string, body: any) {
		console.log(body);
		return this.http.put(url + 'users/update/' + userID, body, this.jwt())
			.toPromise()
			.then(res => res.json().data)
			.catch(this.handleError);
	}

	searchUser(name: any) {
		return this.http.get(url + 'users/search/' + name, this.jwt())
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().message || 'Server error'));
	}

	getProperty() {
		return this.http.get(url + 'users/property/nonmanager', this.jwt())
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().message || 'Server error'));
	}

	requestToken() {
		return this.http.get(url + 'chats/request_token', this.jwt())
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().message || 'Server error'));
	}

	getValid(name:any) {
		return this.http.get(url + 'users/check/'+ name)
			.map((res: Response) => res.json())
			.catch((error: any) => Observable.throw(error.json().message || 'Server error'));
	}

	block(id:string){
		return this.http.put(url + 'users/block/' + id,'', this.jwt())
			.toPromise()
			.then(res => res.json().data)
			.catch(this.handleError);
	}

	unblock(id:string){
		return this.http.put(url + 'users/unblock/' + id,'', this.jwt())
			.toPromise()
			.then(res => res.json().data)
			.catch(this.handleError);
	}

	private handleError(error: any): Promise<any> {
		console.error('An error occurred', error);
		return Promise.reject(error.message || error);
	}

	private jwt() {
		// create authorization header with jwt token
		let currentUser = JSON.parse(localStorage.getItem('authToken'));
		if (currentUser && currentUser.token) {
			let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
			return new RequestOptions({ headers: headers });
		}
	}
}
