import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable, Subscriber } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AuthenticationService } from './authentication.service';
import { url } from '../../global';
import 'rxjs/add/operator/toPromise';
import * as io from 'socket.io-client';

@Injectable()
export class NotificationUserService {
  // private url: string = 'http://128.199.85.243:3000/';
  private url: string = 'https://192.168.4.118:5000/';
  private socket: any;

  constructor(private http: Http, private authenticationService: AuthenticationService) { }

  getAll(){
    return this.http.get(url + 'notifications/user', this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  update(id:string){
    return this.http.post(url + 'notifications/update/' + id, '', this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  getOwn(userId: string) {
    let observable = new Observable((observer: Subscriber<any>) => {
      this.socket = io(this.url);
      this.socket.on(`notification_` + userId, (data: any) => { 
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    })     
    return observable;
  }

  generateLink(notification:any){

    var link:string = '';
    this.update(notification._id);
    switch (notification.type) {
      case 'received_LOI' :
        link = '/loi/' + notification.ref_id + '/view';
      break;
      case 'accepted_LOI' :
        link = '/loi/' + notification.ref_id + '/view';
      break;
      case 'rejected_LOI' :
        link = '/loi/' + notification.ref_id + '/view';
      break;
      case 'initiated_LOI' :
        link = '/loi/' + notification.ref_id + '/view';
      break;
      case 'initiated_TA' :
        link = '/ta/' + notification.ref_id + '/view';
      break;
      case 'rejected_TA' :
        link = '/ta/' + notification.ref_id + '/view';
      break;
      case 'accepted_TA' :
        link = '/ta/' + notification.ref_id + '/view';
      break;
      case 'initiated_IL' :
        link = '/inventory/' + notification.ref_id + '/view';
      break;
      case 'confirmed_IL' :
        link = '/inventory/' + notification.ref_id + '/view';
      break;
      case 'received_Inventory' :
        link = '/inventory/' + notification.ref_id + '/view';
      break;
      case 'approved_property' :
        link = '/property/' + notification.ref_id + '/view';        
      break;
      case 'rejected_property' :
        link = '/property/' + notification.ref_id + '/view';        
      break;
      case 'appointment_proposed':
        link = '/dashboard/proposed';
      break;
    }
    return link;
  }

  getByLimit(limit: number){
    return this.http.get(url + 'notifications/limit/' + limit, this.jwt())
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
  }

  read(){
    return this.http.post(url + 'notifications/read', '', this.jwt())
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('authToken'));
    if (currentUser && currentUser.token) {
      let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
      return new RequestOptions({ headers: headers });
    }
  }
}
