import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { url } from '../../global';
import { DDPCacheEngine } from 'rxjs-ddp-client';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class StorageService {
  public data: any = {}
  get(key: string) {
    return this.data[key] ? this.data[key].asObservable() : this.data[key] = new BehaviorSubject<any>(null);
  }
  set(key: string, value: any) {
    this.data[key] ? this.data[key].next(value) : this.data[key] = new BehaviorSubject<any>(value);
  }
  remove(key: string) {
    this.data[key] = undefined;
  }
}
