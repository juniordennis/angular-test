import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { url } from '../../global';
import 'rxjs/add/operator/toPromise';
 
@Injectable()
export class BankService {
    constructor(private http: Http) {}

    getAll(){
        return this.http.get(url + 'banks', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    getById(id:string){
        return this.http.get(url + 'banks/' + id, this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    create(body:any){
        return this.http.post(url +  'banks', body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    update(body:any){
        return this.http.post(url + 'banks/update/' + body._id,body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    delete(id: string): Promise<void> {
        return this.http.delete(url + 'banks/' + id, this.jwt())
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }

}