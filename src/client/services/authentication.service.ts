import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import { URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/catch';
import { url, urlAuth } from '../../global';

@Injectable()
export class AuthenticationService {
  public headers: Headers;
  public token: string;
  private authToken: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private http: Http, private _router: Router) { }

  getAuthToken(): Observable<string> {
    // this.authToken.next(localStorage.getItem('authToken'));
    this.setAuthToken(localStorage.getItem('authToken'))
    return this.authToken.asObservable();
  }

  setAuthToken(token: string): void {
    this.authToken.next(token);
  }

  login(username: string, password: string): Observable<void> {
    let self = this;
    return this.http.post(urlAuth + 'auth/local/', { username: username, password: password })
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let user = response.json();
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('authToken', JSON.stringify(user));
          self.setAuthToken(localStorage.getItem('authToken'));
        }
        return response.json();
      }).catch((error: any) => Observable.throw(error.json().message || 'Server error'));
  }

  loginFacebook(id: string, accessToken: string): Observable<any> {
    let self = this;
    return this.http.post(urlAuth + 'auth/facebook/', { id: id, accessToken: accessToken })
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let user = response.json();
        var authtoken = { "token": user.token };
        var authid = { "token": user.userId };
        var authstatus = { "token": user.data };
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('authToken', JSON.stringify(authtoken));
          localStorage.setItem('authId', JSON.stringify(authid));
          localStorage.setItem('authData', JSON.stringify(authstatus));
          self.setAuthToken(localStorage.getItem('authToken'));
        }
      });
  }

  logout() {
    // clear token remove user from local storage to log user out
    localStorage.removeItem('authToken');
    localStorage.removeItem('Dreamtalk.id');
    localStorage.removeItem('Dreamtalk.token');
    localStorage.removeItem('Dreamtalk.tokenExpires');
    this.setAuthToken(localStorage.getItem('authToken'));
    // this._router.navigate(['/']);
  }

  create(body: any): Observable<void> {
    let self = this;
    return this.http.post(url + 'signup', body)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let user = response.json();
        var authtoken = { "token": user.token };
        var authid = { "token": user.userId };
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('authToken', JSON.stringify(authtoken));
          localStorage.setItem('authId', JSON.stringify(authid));
          self.setAuthToken(localStorage.getItem('authToken'));
        }
      });
  }

  sentMail(email: any) {
    return this.http.post(url + 'send_reset_password', email)
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  resetPass(id:any , body:any) {
    return this.http.post(url + 'reset_password/' + id, body)
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
