import { Injectable } from '@angular/core';

import { Setting } from '../../models/setting/setting'
import { SETTING } from '../../mock/setting/mock-setting'

@Injectable()
export class SettingService {
	getSettings(): Promise<Setting[]> {
		return Promise.resolve(SETTING);
	}
}