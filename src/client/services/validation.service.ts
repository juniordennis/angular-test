import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'; 
import { Injectable, ReflectiveInjector, Injector } from '@angular/core';
import { Http, Headers, RequestOptions, Response, HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { url } from '../../global';
import 'rxjs/add/operator/toPromise';

interface ValidationResult {
       [key: string]: boolean;
  }

@Injectable()
export class ValidationService {

    constructor(private http: Http) {}

    static getValidatorErrorMessage(title: string, validatorName: string, validatorValue?: any) {
        let config = {
            'required': `${title} is required`,
            'pattern' : `${title} can contain letters (a-z), numbers (0-9), dashes (-), underscores (_)`,
            'invalidEmailAddress': 'Email Address must be a valid e-mail address',
            'invalidUniqueEmail' : `This email already taken`,
            'invalidUniqueUsername' : `This Username already taken`,
            'invalidUniquePhone' : `This Mobile Number already taken`,
            'passwordMatch' : `New passwords doesn't match`,
            'minlength': `${title} must be at least ${validatorValue.requiredLength} characters`
        };

        return config[validatorName];
    }

    static emailValidator(control:any) {
        // RFC 2822 compliant regex
        if (control._value){
            if (control._value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
                return null;
            } else {
                return { 'invalidEmailAddress': true };
            }
        }
    }
    // static confirmPassword(control:FormGroup) {000000
        //console.log(control);
        // return control.get('password').value === control.get('passwordConfirmation').value
      // ? null : {'invalidMatchPassword': true};
        // RFC 2822 compliant regex
        // if (control.get('password').value){
        //         console.log(control.get('password').value);
        //     if (control._parent._value.password === control._parent._value.passwordConfirmation) {
        //         console.log(control['_parent']._value);
        //         console.log(control.parent.value.password);
        //         console.log(control._parent._value.passwordConfirmation);
                 // return null;
        //     } else {
        //         return { 'invalidMatchPassword': true };
        //     }
        // }
    // }
    static emailUnique(control:any): Promise<ValidationResult>{
        const providers = (<any>HttpModule).decorators[0].args[0].providers;
        const injector = ReflectiveInjector.resolveAndCreate(providers);
        const http = injector.get(Http);

        return new Promise(resolve => {
            http.get(url +'users/check/' +control._value)
                .map((res:Response) => res.json())
                .subscribe((data:any) => {
                    console.log(data);
                    if (data.message== true) {
                        resolve({ invalidUniqueEmail: true });
                    }
                    else { 
                        resolve({ invalidUniqueEmail: null}); 

                    }
                })
        });
    }

    static usernameUnique(http:Http, control:any){
        
        return {'invalidUniqueUsername' : false };
    }

    static phoneUnique(http:Http, control:any){    
       
        return {'invalidUniquePhone' : false };
    }
}
