import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { url } from '../../global';
import 'rxjs/add/operator/toPromise';
import { Agreements } from '../../admin/models/agreements'
import { Mockagreement } from '../mock/agreement/mock-agreement'

@Injectable()
export class AgreementService {
    constructor(private http: Http) {}

    getAgreements(): Promise<Agreements[]> {
        return Promise.resolve(Mockagreement);
    }

    getAgreement(id: string): Promise<Agreements> {
        return this.getAgreements()
            .then(data => data.find(data => data.id === id));
    }

    getAll(){
        return this.http.get(url + 'agreements', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    getAllAdmin(){
        return this.http.get(url + 'agreements/all', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    getById(id:string){
        return this.http.get(url + 'agreements/' + id, this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    getByUser(){
        return this.http.get(url + 'agreements/user', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    getCertificateList(){
        return this.http.get(url + 'list_certificate', this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    uploadProofLandlord(id:string, body:any){
        return this.http.post(url + 'transfer/landlord/' + id,body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    getLoiReport(id:string){
        return this.http.get(url + 'report/loi/' + id, this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    getTaReport(id:string){
        return this.http.get(url + 'report/ta/' + id, this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    create(body:any){
        return this.http.post(url +  'agreements', body, this.jwt())
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    update(id:string, body:any){
        return this.http.post(url + 'agreements/update/' + id,body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    delete(id: string): Promise<void> {
        return this.http.delete(url + 'developments/' + id, this.jwt())
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    postAgreementLoi(appointment_id:string){
        return this.http.post(url + 'loi/appointment/'+ appointment_id,'', this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    getLOI(id:string){
        return this.http.get(url + 'loi/' + id, this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    createLOI(id:string, body:any){
        return this.http.post(url +  'loi/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    updateLOI(body:any){
        return this.http.post(url + 'developments/update/' + body._id,body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    deleteLOI(id: string): Promise<void> {
        return this.http.delete(url + 'developments/' + id, this.jwt())
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    sendLOI(id:string){
        return this.http.post(url +  'loi/send/' + id, '', this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    acceptLOI(id:string, body:any){
        return this.http.post(url +  'loi/status/accepted/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    rejectLOI(id:string, body:any){
        return this.http.post(url +  'loi/status/rejected/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().message)
            .catch(this.handleError);
    }

    getTA(id:string){
        return this.http.get(url + 'ta/' + id, this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    createTA(id:string, body:any){
        console.log(body)
        return this.http.post(url +  'ta/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    updateTA(body:any){
        return this.http.post(url + 'ta/update/' + body._id,body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    deleteTA(id: string): Promise<void> {
        return this.http.delete(url + 'ta/' + id, this.jwt())
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    sign(id:string, body:any){
        return this.http.post(url +  'agreement/confirmation/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    payment(id:string, body:any){
        return this.http.post(url +  'agreement/payment/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    acceptPayment(id:string, body:any) {
        return this.http.post(url + 'agreement/payment/accepted/' +  id, body, this.jwt())
        .toPromise()
        .then(res => res.json().data)
        .catch(this.handleError);
    }
    
    rejectPayment(id:string, body:any) {
        return this.http.post(url + 'agreement/payment/rejected/' +  id, body, this.jwt())
        .toPromise()
        .then(res => res.json().data)
        .catch(this.handleError);
    }

    sendTA(id:string, body:any){
        return this.http.post(url +  'ta/send/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    acceptTA(id:string, body:any){
        return this.http.post(url +  'ta/status/acccepted/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    rejectTA(id:string, body:any){
        return this.http.post(url +  'ta/status/rejected/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    uploadStampCertificate(id:string, body:any){
        return this.http.post(url +  'ta/stamp_certificate/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    getIL(id:string){
        return this.http.get(url + 'inventorylist/' + id, this.jwt())
            .map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().message || 'Server error'));
    }

    createIL(id:string, body:any){
        console.log(body)
        return this.http.post(url +  'inventorylist/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    confirmIL(id:string, body:any){
        return this.http.post(url + 'inventorylist/tenant_checked/' + id, body, this.jwt())
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    uploadRefundProof(id:string, body:any) {
        return this.http.post(url + 'agreement/payment/refund/' +  id, body, this.jwt())
        .toPromise()
        .then(res => res.json().data)
        .catch(this.handleError);
    }

    print(id:string, body:any) {
        return this.http.post(url + 'report/print/' +  id, body, this.jwt())
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }

}
