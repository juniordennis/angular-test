import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DDPClient } from 'rxjs-ddp-client';
import { DDPCacheEngine } from 'rxjs-ddp-client';
import { StorageService } from './services/storage.service'

export type MY_DDP_COLLECTIONS = 'users' | 'chats' | 'rooms' | 'roomMembers';
export const MY_DDP_COLLECTIONS = {
  USERS: 'users' as MY_DDP_COLLECTIONS,
  CHATS: 'chats' as MY_DDP_COLLECTIONS,
  ROOMS: 'rooms' as MY_DDP_COLLECTIONS,
  ROOMMEMBERS: 'roomMembers' as MY_DDP_COLLECTIONS,

};

export interface IUser {
  _id: string;
  username: string;
  email: string;
}


export class MyDDPClient extends DDPClient {
  public ddpStatus: {
    isConnected: boolean;
    isDisconnected: boolean;
  };

  public status: BehaviorSubject<any> = new BehaviorSubject<any>(this.ddpStatus);
  public storageService: StorageService;

  constructor() {
    super();
    this.ddpStatus = {
      isConnected: false,
      isDisconnected: true
    };
  }

  initCacheStorage(cacheEngine: DDPCacheEngine) {
    this.ddpStorage.setCacheEngine(cacheEngine);
    this.ddpStorage.persistToStore([MY_DDP_COLLECTIONS.CHATS]);
    this.ddpStorage.loadFromCache([MY_DDP_COLLECTIONS.CHATS]);
  }

  connect() {
    const ddpServerUrl = 'wss://dt.shrimpventures.com/websocket';
    super.connect(ddpServerUrl);
  }

  login(token: string) {
    return this.callWithPromise('login', [{ resume: token }]);
  }

  logout() {
    this.ddpStorage.clearCache([MY_DDP_COLLECTIONS.CHATS]);
    super.close();
  }

  sendChat(params:any){
    return this.callWithPromise('chats.insert', [ params ]);
  }

  refreshUnseen(roomId:any){
    return this.callWithPromise('rooms.seen', [{ id: roomId }]);
  }

  // Events called by DDPClient
  onConnected() {
    // DDP connected, now we can login and subscribe to the publications on the server
    let self = this;
    self.ddpStatus.isConnected = true;
    self.ddpStatus.isDisconnected = false;
    self.status.next(self.ddpStatus);
    // let loggingIn = function() {
    //   self.login()
    //   .then((result:any) => {
    //     // setChatLocalStorage('Dreamtalk', result.id, result.token, result.tokenExpires);
    //     self.subscribePublications();
    //     self.observeCollections();
    //   }).catch((err:any) => {
    //     console.log(err);
    //     if (err.error == 403 && err.reason == "You've been logged out by the server. Please log in again.") {
    //       // call get token API and re login
    //     } else if (err.error == 'too-many-requests') {
    //       setTimeout(loggingIn, 10000);
    //     } else {
    //       console.log(err);
    //     }
    //   });
    // }
    // loggingIn();
  }

  getStatus() {
    return this.status.asObservable();
  }

  onDisconnected() {
    // DDP disconnected, notify user

    this.ddpStatus.isConnected = true;
    this.ddpStatus.isDisconnected = false;
    this.status.next(this.ddpStatus);
  }

  onSocketError(error: any) {
    // Custom code on Socket error
  }

  onSocketClosed() {
    console.log('this socket closed');
    // Custom code on Socket closed
  }

  onMessage(data: any) {
    // Custom code for handle special DDP server messages
  }

  // Custom utility methos
  subscribePublications() {
    const since = this.ddpStorage.lastSyncTime;
    this.subscribe('rooms.all', [since]);
    this.subscribe('roomMembers', [since]);
  }

  subscribeChatPublications(roomId:any) {
    this.subscribe('chats.inRoom', [{ roomId: roomId }]);
  }

  observeCollections() {
    let self = this;
    this.observeCollection<IUser[]>(MY_DDP_COLLECTIONS.USERS)
      .subscribe(users => {
        self.storageService.set('users', users);
      });
  }

  observeRoomsCollections() {
    let self = this;
    this.observeCollection(MY_DDP_COLLECTIONS.ROOMS)
      .subscribe(rooms => {
        self.storageService.set('rooms', rooms);
      });
  }

  observeChatsCollections() {
    let self = this;
    this.observeCollection(MY_DDP_COLLECTIONS.CHATS)
      .subscribe(chats => { console.log('chats:', chats)});
  }

  observeRoomMembersCollections() {
    let self = this;
    this.observeCollection(MY_DDP_COLLECTIONS.ROOMMEMBERS)
      .subscribe(roomMembers => {
        self.storageService.set('roomMembers:', roomMembers);
      });
  }

  // collections(name:string){
  //   return this.storageService.get(name);
  // }

  getAllCollectionData$(collectionName: MY_DDP_COLLECTIONS) {
        // To access data direcly from the collection you can use the ddpStorage methods
        return this.ddpStorage.getObservable(collectionName);
    }
}
