import {Directive, ElementRef, Input, HostListener} from '@angular/core';
import * as $ from 'jquery';

@Directive({
  selector: '[scroll]'
})
export class ScrollDirective {
  private element: any;
  private errorField: any;
  @Input() scroll: boolean;
  @Input('formIdScroll') formIdScroll: string;
  // private hasScrolled = false;
  constructor ($element: ElementRef) {
    this.element = $element;
  }

  @HostListener('click', ['$event.target']) 
    elementClicked(event: MouseEvent) {
        this.scrollToELement();
    }

  scrollToELement () {
    // if(this.scroll){
      var idForm = '#'+this.formIdScroll
      this.errorField = $(idForm+' .ng-invalid')
      for (let z = 0; z < this.errorField.length; ++z) {
        if (this.errorField[z].localName === "input" ||this.errorField[z].localName === "textarea") {
          var eTop = $('#'+this.errorField[z].id).offset().top; //get the offset top of the element
          $("html, body").animate({ scrollTop: eTop-100 }, 600);
          if (this.errorField[z].type === "text" || this.errorField[z].type === "textarea" || this.errorField[z].type === "number") {
            $('#'+this.errorField[z].id).focus();
          }
          // this.hasScrolled = true;
          break
        }
      }  
    // }
  }
}