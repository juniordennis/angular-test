import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { BlogService } from '../../services/blog.service';
declare var jQuery:any;
import 'slick-carousel';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'blog',
  templateUrl: './blog.html',
//   template:`<carousel>
//   <slide>
//     <img src="assets/images/nature/1.jpg" alt="First slide">
//   </slide>
//   <slide>
//     <img src="assets/images/nature/2.jpg" alt="Second slide">
//   </slide>
//   <slide>
//     <img src="assets/images/nature/3.jpg" alt="Third slide">
//   </slide>
// </carousel>`
})

export class BlogComponent implements OnInit{
    subscription: boolean = true;
    public blogs: any[] = [];
    public blog: any[] = [];
    public datas: any[] = [];
    public loop: number = 0;
    public slide: number = 0;
    public total: number = 0;
    public totalSlide:any[] = [];

    constructor(private router: Router, private blogService: BlogService) { }
	ngOnInit():any {
        // jQuery('.single-item').slick({
        //     infinite: true,
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //     arrows: true,
        //     autoplay:true,
        //     autoplaySpeed: 5000,
        //     cssEase: 'linear'
        // });
        this.loadAllBlogs();
    }

   private loadAllBlogs() {
        this.blogService.getAll()
        .takeWhile(()=> this.subscription)
        .subscribe(data => { 
            this.blogs = data;
            for (var i = 0; i < this.blogs.length; ++i) {
                let content = this.blogs[i].content;
                let contentNew = content.replace(/<img[^>]*>/g,"");
                this.blogs[i].content = this.smart_substr(contentNew,225) + ''
            }
            for (let b = 0; b < 2; ++b) {
                this.datas[b] = []
            }
            for(let i = 0; i < this.datas.length; i++){
                this.totalSlide.push(i);
                this.total = this.total + 3;
                    for (let a = this.loop; a < this.total; ++a) {
                        this.datas[i].push({ 
                            cover: this.blogs[a].cover.url,
                            slug: this.blogs[a].slug, 
                            title: this.blogs[a].title, 
                            desc: this.blogs[a].content, 
                        })
                    }
                this.loop = this.loop + 3
            } 
        });
    }

    add(slug: any){
        this.blogService.getBySlug(slug)
        .takeWhile(()=> this.subscription)
        .subscribe(blog => { 
            let title = blog.title;
            let cover = blog.cover.url;
            let content = blog.content;
            let contentNew = content.replace(/(<([^>]+)>)/ig,"");
            if (contentNew.length > 225) {
                contentNew = contentNew.substr(0, 225)+'';
            }
            let desc = contentNew;
            localStorage.setItem('blog_title', title);
            localStorage.setItem('blog_cover', cover);
            localStorage.setItem('blog_desc', desc);
            this.router.navigate(['/blog-view/' + slug]);
        });
    }

    smart_substr(str:string, len:number) {
        var temp = str.substr(0, len);
        if(temp.lastIndexOf('<') > temp.lastIndexOf('>')) {
            temp = str.substr(0, 1 + str.indexOf('>', temp.lastIndexOf('<')));
        }
        return temp;
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}