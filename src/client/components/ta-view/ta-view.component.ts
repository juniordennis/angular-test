import { Component, ViewChild, OnInit } from '@angular/core';
import { SignatureComponent } from '../signature.component'
import { AgreementService } from '../../services/agreement.service';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { StorageService } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import { ChatService } from '../../services/chat.service';
import { NavbarComponent } from '../navbar/navbar.component'
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'ta-view',
  templateUrl: './ta-view.html',
})

export class TaViewComponent implements OnInit{
    subscription: boolean = true;
	id: string;
	date: any;
	user_id: any;
	report: any;
	agreement: any;
	landlord: any;
	landlord_id: any;
	tenant: any;
	tenant_id: any;
	landlord_sign: any;
	tenant_sign: any;
	ta: any;
	ta_payment: any;
	status: any;
	stamp: any;
	inventory_list: any;
	model:any = {};
    public ddp: MyDDPClient;
    public loadingReject: boolean = false;
    public loadingAccept: boolean = false;
    public dreamtalkReady: boolean = false;
	@ViewChild(SignatureComponent) signatureComponent: SignatureComponent;

	constructor(
		private userService: UserService,
		private agreementService: AgreementService,
		private router: Router,
		private route: ActivatedRoute,
		private notificationsService: NotificationsService,
        private sharedService: SharedService,
        private storageService: StorageService,
        private chatService: ChatService,
        private navbarComponent: NavbarComponent,
		){}

	ngOnInit():any{
        this.storageService.get('dreamtalk.ready').takeWhile(()=> this.subscription).subscribe((ready: boolean) => { this.dreamtalkReady = ready });
        this.sharedService.ddp$.takeWhile(()=> this.subscription).subscribe((ddp: MyDDPClient) => {
            this.ddp = ddp;
        }, (error: any) => {
            console.log('Error ddp', error);
        })
        this.route.params.takeWhile(()=> this.subscription).subscribe(params => {
            this.id = params['id'];
        });
		this.date = Date.now();
		this.userService.getByToken().takeWhile(()=> this.subscription).subscribe(user => {
            this.user_id = user._id;
        });
		this.agreementService.getTaReport(this.id).takeWhile(()=> this.subscription).subscribe(report => {
            this.report = report;
        });
		this.agreementService.getById(this.id).takeWhile(()=> this.subscription).subscribe(agreement => {
			this.agreement = agreement;
			console.log(agreement);
		});
		this.agreementService.getTA(this.id).takeWhile(()=> this.subscription).subscribe(ta => {
            this.ta = ta;
            console.log(ta);
        });
	}
	send(){
		this.model.sign = this.signatureComponent.data;
		if(this.model.sign) {
			this.model.status = 'landlord';
			this.model.type = 'tenancy_agreement';
			this.agreementService.sendTA(this.id, this.model)
			.then(
				response => {
					this.router.navigate(['/dashboard/tenancy-agreement']);
					console.log(response)
				},
				error => {
					console.log(error)
				}
			);
		} else {
	        this.notificationsService.error('', 'Please fill the sign')
		}
	}

	accept(){
		if(!this.ta.confirmation.tenant.sign) {
			this.model.sign = this.signatureComponent.data;
			if(this.model.sign) {
			    this.loadingAccept = true
				this.model.status = 'tenant';
				this.model.type = 'tenancy_agreement';
				this.agreementService.acceptTA(this.id, this.model)
				.then(
					response => {
						this.router.navigate(['/ta/'+ this.id +'/payment']);
						if (localStorage.getItem('authToken')) {
			                if (this.dreamtalkReady) {
			                    if (this.ddp) {
			                        if (!this.agreement.room_id) {
			                            let roomParams = { property_id: this.agreement.property._id };
			                            this.chatService.createRoom(roomParams)
			                            .then((res: any) => {
			                                let roomId = res && res.room_id ? res.room_id : undefined;
			                                let chatParams: any = {
			                                    roomId: roomId,
			                                    message: '-',
			                                    extra: { 
			                                        ref : 'ta_accept',
			                                        ref_id: this.id,
			                                    }
			                                  };
			                                this.ddp.sendChat(chatParams).then((res: any) => {
			                                    this.loadingAccept = false
			                                }).catch((error: any) => {
			                                    console.log(error);;
			                                })
			                            }).catch((err: any) => {
			                                console.log(err);
			                            })
			                        }else{
			                            let chatParams: any = {
			                                roomId: this.agreement.room_id.room_id,
			                                message: '-',
			                                extra: { 
			                                    ref : 'ta_accept',
			                                    ref_id: this.id,
			                                }
			                              };
			                            this.ddp.sendChat(chatParams).then((res: any) => {
			                                this.loadingAccept = false
			                            }).catch((error: any) => {
			                                console.log(error);
			                                this.ddp.sendChat(chatParams).then((res: any) => {
			                                    this.loadingAccept = false
			                                }).catch((error: any) => {
			                                    console.log(error);
			                                })
			                            })
			                        }
			                    } else {
			                        // no ddp
			                    }
			                } else {
			                    // dreamtalk not ready
			                }
			            } else {
			                this.navbarComponent.modalLogin.show()
			            }
					},
					error => {
						console.log(error)
					}
				);
			} else {
	            this.notificationsService.error('', 'Please fill the sign')
			}
		} else {
			this.router.navigate(['/ta/'+ this.id +'/payment']);
		}
	}

	reject(){
	    this.loadingReject = true
	    if(this.model.reason) {
	    	console.log(this.model)
			this.agreementService.rejectTA(this.id,this.model)
			.then(
				response => {
					this.notificationsService.success('Success','TA rejected')
					this.router.navigate(['/dashboard/tenancy-agreement']);
					if (localStorage.getItem('authToken')) {
		                if (this.dreamtalkReady) {
		                    if (this.ddp) {
		                        if (!this.agreement.room_id) {
		                            let roomParams = { property_id: this.agreement.property._id };
		                            this.chatService.createRoom(roomParams)
		                            .then((res: any) => {
		                                let roomId = res && res.room_id ? res.room_id : undefined;
		                                let chatParams: any = {
		                                    roomId: roomId,
		                                    message: '-',
		                                    extra: { 
		                                        ref : 'ta_reject',
		                                        ref_id: this.id,
		                                    }
		                                  };
		                                this.ddp.sendChat(chatParams).then((res: any) => {
		                                    this.loadingReject = false
		                                }).catch((error: any) => {
		                                    console.log(error);
		                                })
		                            }).catch((err: any) => {
		                                console.log(err);
		                            })
		                        }else{
		                            let chatParams: any = {
		                                roomId: this.agreement.room_id.room_id,
		                                message: '-',
		                                extra: { 
		                                    ref : 'ta_reject',
		                                    ref_id: this.id,
		                                }
		                              };
		                            this.ddp.sendChat(chatParams).then((res: any) => {
		                                this.loadingReject = false
		                            }).catch((error: any) => {
		                                console.log(error);
		                                this.ddp.sendChat(chatParams).then((res: any) => {
		                                    this.loadingReject = false
		                                }).catch((error: any) => {
		                                    console.log(error);
		                                })
		                            })
		                        }
		                    } else {
		                        // no ddp
		                    }
		                } else {
		                    // dreamtalk not ready');
		                }
		            } else {
		                this.navbarComponent.modalLogin.show()
		            }
				},
				error => {
					console.log(error)
				}
			);
	    } else {
	        this.notificationsService.error('', 'Please write the remark')
	    }
	}

	printDownload() {
		this.model.type = 'ta';
		this.agreementService.print(this.id, this.model)
		.then(response => {
			console.log(response,'response')
		}).catch(error => {
			console.log(error,'error')
		});
	}

	gotoInventory(){
		this.router.navigate(['/inventory/'+ this.id]);
	}

	back(){
		this.router.navigate(['/dashboard/tenancy-agreement']);
	}

	ngOnDestroy() {
        this.subscription = false;
    }
}