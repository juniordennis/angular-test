import { Component, OnInit, Directive, Input, NgModule, NgZone, ViewChild, ElementRef } from '@angular/core';
// import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { googleMapStyles } from '../../../global';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { UserService } from '../../services/user.service';
import { PropertyService } from '../../services/property.service';
import { AgreementService } from '../../services/agreement.service';
import { AmenityService } from '../../services/amenity.service';
import { ChatService } from '../../services/chat.service';
import { StorageService } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import { NotificationsService } from 'angular2-notifications';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Marker } from '../../models/marker';
import { User } from '../../models/user';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from '../navbar/navbar.component'
import "rxjs/add/operator/takeWhile";

import { DirectionsMapDirective } from '../../google-map-direction.directive';
import { AgmCoreModule, MapsAPILoader, GoogleMapsAPIWrapper } from 'angular2-google-maps/core';

declare var google: any;

@Component({
	selector: 'property-detail',
	templateUrl: './property-detail.html',
	providers : [ GoogleMapsAPIWrapper ]
})

export class PropertyDetailComponent implements OnInit {
	subscription: boolean = true;
	public location = {};
	public id: any;
	public property: any;
	public model: any = {};
	public user: User;
	public ddp: MyDDPClient;
	public customStyle = googleMapStyles;
	public markers: any = [];
	public lat: any;
	public lng: any;
	public zoom: number = 13;
	public minZoom: number = 11;
	public maxZoom: number = 15;
	public message: string;
	public dreamtalkReady: boolean = false;
	public loading: boolean = false;
	public origin: any;
  	public destination: any;
  	public direct: boolean = false;
  	public marker: boolean = true;
  	public submitted: boolean = false;
	public loadingView: boolean = false;

	constructor(
		// private ddpClient: MyDDPClient,
		private userService: UserService,
		private propertyService: PropertyService,
		private agreementService: AgreementService,
		private amenityService: AmenityService,
		private chatService: ChatService,
		private storageService: StorageService,
		private sharedService: SharedService,
		private notificationsService: NotificationsService,
		private navbarComponent: NavbarComponent,
		private router: Router,
		private route: ActivatedRoute,
		private gmapsApi: GoogleMapsAPIWrapper,
		private mapsAPILoader: MapsAPILoader,
      	private ngZone: NgZone,
      	private _elementRef : ElementRef
	) { }

	ngOnInit(): void {
		this.userService.getByToken()
		.takeWhile(()=> this.subscription)
		.subscribe((user: User) => {
			this.user = user;
		}, (error: any) => {
			this.user = undefined;
		});
		this.sharedService.ddp$
		.takeWhile(()=> this.subscription)
		.subscribe((ddp: MyDDPClient) => {
			this.ddp = ddp;
		}, (error: any) => {
			console.log('Error ddp', error);
		})
		this.route.params
		.takeWhile(()=> this.subscription)
		.subscribe(params => {
			this.id = params['id'];
		});
		this.propertyService.getPropertyById(this.id)
			.takeWhile(()=> this.subscription)
			.subscribe(property => {
				this.property = property;
				this.lat = +property.address.coordinates[1];
				this.lng = +property.address.coordinates[0];
				this.markers = [{
					lat: +property.address.coordinates[1],
					lng: +property.address.coordinates[0],
					label: '1',
					draggable: false
				}];
			});
		this.storageService.get('dreamtalk.ready').subscribe((ready: boolean) => { this.dreamtalkReady = ready });

		this.storageService.get('dreamtalk.ready')
		.takeWhile(()=> this.subscription)
		.subscribe((ready: boolean) => { this.dreamtalkReady = ready });
		
		if(navigator.geolocation){
			navigator.geolocation.getCurrentPosition(this.setPosition.bind(this));
		};
    }

	setPosition(position: any){
	  	this.location = position.coords;
	  	this.origin = this.location;
	  	this.destination = {latitude: this.lat, longitude: this.lng};
    }

    getLocation(){
    	this.submitted = true;
	    this.direct = true;
	    this.marker = false;
		this.submitted = false;
	}

	cancel(){
		this.submitted = true;
		this.direct = false;
	    this.marker = true;
		this.submitted = false;
	}

	viewSchedule(id: any) {
		this.loadingView = true
		this.router.navigate(['/appointment/' + id + '/initiate-view'])
		this.model.property = id 
		if (localStorage.getItem('authToken')) {
			this.agreementService.create(this.model) 
				.then(res => {})
				.catch(err => {})
		}
	}

	messageLandlord(propertyId: string) {
		let roomParams = { property_id: propertyId };
		if (this.user) {
			if (this.dreamtalkReady) {
				if (this.ddp) {
					if (this.message) {
						this.loading = true
						this.chatService.createRoom(roomParams)
						.then((res: any) => {
							let roomId = res && res.room_id ? res.room_id : undefined;
							let chatParams: any = {
								message: this.message,
								roomId: roomId,
							};
							this.ddp.sendChat(chatParams).then((res: any) => {
								this.loading = false
								this.message = '';
								this.notificationsService.success('Success', 'Message has been delivered.');
							}).catch((error: any) => {
								console.log(error);
								this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
							})
						}).catch((err: any) => {
							console.log(err);
							this.notificationsService.error('Error', 'Failed to create room');
						})
					}
				} else {
					// no ddp
				}
			} else {
				// dreamtalk not ready
				this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
			}
		} else {
			this.navbarComponent.modalLogin.show()
		}
	}

	ngOnDestroy() {
		this.subscription = false;
	}
}
