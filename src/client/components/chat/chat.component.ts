import { Component, OnInit, ViewChild } from '@angular/core';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { SharedService } from '../../services/shared.service';
import { ReportService } from '../../services/report.service';
import { UserService } from '../../services/user.service';
import { PropertyService } from '../../services/property.service';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { AgreementService } from '../../services/agreement.service';
import { NotificationsService } from 'angular2-notifications';
import { StorageService } from '../../services/storage.service';
import { ChatService } from '../../services/chat.service';
import { generateChatTemplate } from './generator.component';
import { ModalDirective } from 'ng2-bootstrap';
import { OrderBy } from '../orderBy';
import  * as _  from "underscore";

@Component({
  selector: 'chat',
  templateUrl: './chat.html',
  styleUrls: ['./chat.css'],
})

export class ChatComponent implements OnInit {
	@ViewChild('modal') public modal: ModalDirective;
	subscription: boolean = true;
	model:any = {};
	isTenant: boolean;
	rooms: any[] = [];
	landlords: any[] = [];
	tenants: any[] = [];
	dreamtalkReady: boolean = false;
	roomId: string;
	chats: any[] = [];
	roomData: any;
	userId: string;
	datas: any[] = [];
	blockedId: any[] =[];
	blocked:boolean = false;
	me: any;
	id: string;
	type: string;
	agreement: any;
	loading: boolean = false;
	ready: boolean = false
	// blocked:any;
	public ddp: MyDDPClient;
	public message: string;
	totalUnseenLandlord : number = 0;
	totalUnseenTenant : number = 0;
	totalUnseenLandlordEnquiries : number = 0;
	totalUnseenLandlordPending : number = 0;
	totalUnseenLandlordRented : number = 0;
	totalUnseenTenantEnquiries : number = 0;
	totalUnseenTenantPending : number = 0;
	totalUnseenTenantRented : number = 0;
	unseenLandlord: number = 0;
	unseenLandlordEnquiries: number = 0;
	unseenLandlordPending: number = 0;
	unseenLandlordRented: number = 0;
	unseenTenant: number = 0;
	unseenTenantEnquiries: number = 0;
	unseenTenantPending: number = 0;
	unseenTenantRented: number = 0;
    landlordEnquiries: any[] = [];
    landlordPending: any[] = [];
    landlordRented: any[] = [];
    tenantEnquiries: any[] = [];
    tenantPending: any[] = [];
    tenantRented: any[] = [];
    landlordEnquiriesCollapse: boolean = false;
    landlordPendingCollapse: boolean = false;
    landlordRentedCollapse: boolean = false;
    tenantEnquiriesCollapse: boolean = false;
    tenantPendingCollapse: boolean = false;
    tenantRentedCollapse: boolean = false;
    landlordEnquiriesClass: string = "btn-plus-icon-chat chat-cursor icon-hide-chat";
    landlordPendingClass: string = "btn-plus-icon-chat chat-cursor icon-hide-chat";
    landlordRentedClass: string = "btn-plus-icon-chat chat-cursor icon-hide-chat";
    tenantEnquiriesClass: string = "btn-plus-icon-chat chat-cursor icon-hide-chat";
    tenantPendingClass: string = "btn-plus-icon-chat chat-cursor icon-hide-chat";
    tenantRentedClass: string = "btn-plus-icon-chat chat-cursor icon-hide-chat";


  	constructor(
  		private reportService: ReportService,
		private userService: UserService,
		private chatService: ChatService,
		private propertyService: PropertyService,
		private router: Router,
		private route: ActivatedRoute,
		private sharedService: SharedService,
		private agreementService: AgreementService,
		private notificationsService:NotificationsService,
		private storageService: StorageService,
  	) { }

  	ngOnInit(): void {
  		this.userService.getByToken()
    	.takeWhile(()=> this.subscription)
    	.subscribe(data => {
    		this.me = data;
    		for (let z = 0; z < this.me.blocked_users.length; ++z) {
    			this.blockedId.push(this.me.blocked_users[z]._id)
    		}
    		this.userId = data._id;
    	})
  		let self = this

  		this.route.params.takeWhile(()=> self.subscription).subscribe(params => {
            this.id = params['id'];
            var interval = setInterval(function(){  
  				self.storageService.get('dreamtalk.ready').takeWhile(()=> self.subscription).subscribe((ready: boolean) => { self.dreamtalkReady = ready ;})
            	if (self.ready) {
            		clearInterval(interval)
            	}else{
            		if (self.dreamtalkReady) {
	            		if (self.id) {
		            		if (localStorage.getItem('landlord') && localStorage.getItem('tenant')) {
		            			self.sharedService.ddp$.takeWhile(()=> self.subscription).subscribe((ddp: MyDDPClient) => {
								  	self.ddp = ddp;
									self.landlords = JSON.parse(localStorage.getItem('landlord'))
									self.tenants = JSON.parse(localStorage.getItem('tenant'))
									self.landlordEnquiries = self.landlords.filter((data:any) => data.status == "enquiries")
						    		self.landlordPending = self.landlords.filter((data:any) => data.status == "pending")
						    		self.landlordRented = self.landlords.filter((data:any) => data.status == "rented")
						    		self.tenantEnquiries = self.tenants.filter((data:any) => data.status == "enquiries")
						    		self.tenantPending = self.tenants.filter((data:any) => data.status == "pending")
						    		self.tenantRented = self.tenants.filter((data:any) => data.status == "rented")
									for (let z = 0; z < self.landlords.length; ++z) {
										if (self.landlords[z].status == "enquiries") {
											self.unseenLandlordEnquiries = self.unseenLandlordEnquiries + + self.landlords[z].unseenChats
											self.totalUnseenLandlordEnquiries = self.unseenLandlordEnquiries
										}else if (self.landlords[z].status == "pending") {
											self.unseenLandlordPending = self.unseenLandlordPending + + self.landlords[z].unseenChats
											self.totalUnseenLandlordPending = self.unseenLandlordPending
										}else {
											self.unseenLandlordRented = self.unseenLandlordRented + + self.landlords[z].unseenChats
											self.totalUnseenLandlordRented = self.unseenLandlordRented
										}
										self.unseenLandlord = self.unseenLandlord + self.landlords[z].unseenChats;
										self.totalUnseenLandlord = self.unseenLandlord;
									}
									for (let z = 0; z < self.tenants.length; ++z) {
										if (self.tenants[z].status == "enquiries") {
											self.unseenTenantEnquiries = self.unseenTenantEnquiries + + self.tenants[z].unseenChats
											self.totalUnseenTenantEnquiries = self.unseenTenantEnquiries
										}else if (self.tenants[z].status == "pending") {
											self.unseenTenantPending = self.unseenTenantPending + + self.tenants[z].unseenChats
											self.totalUnseenTenantPending = self.unseenTenantPending
										}else {
											self.unseenTenantRented = self.unseenTenantRented + + self.tenants[z].unseenChats
											self.totalUnseenTenantRented = self.unseenTenantRented
										}
										self.unseenTenant = self.unseenTenant + self.tenants[z].unseenChats;
										self.totalUnseenTenant = self.unseenTenant;
									}
									self.unseenLandlord = 0;
									self.unseenTenant = 0;
									self.unseenLandlordEnquiries = 0;
									self.unseenLandlordPending = 0;
									self.unseenLandlordRented = 0;
									self.unseenTenantEnquiries = 0;
									self.unseenTenantPending = 0;
									self.unseenTenantRented = 0;
									self.getChat(self.id,'')
									self.ready = true
								}, (error: any) => {
								  	console.log('Error ddp', error);
								});
		            		}else{
		            			self.sharedService.ddp$.takeWhile(()=> self.subscription).subscribe((ddp: MyDDPClient) => {
								  	self.ddp = ddp;
									self.getData();
									if (self.ready) {
										self.getChat(self.id,'')
									}
								}, (error: any) => {
								  	console.log('Error ddp', error);
								});
		            		}
		            	}else{
		            		self.sharedService.ddp$.takeWhile(()=> self.subscription).subscribe((ddp: MyDDPClient) => {
							  	self.ddp = ddp;
								self.getData();
							}, (error: any) => {
							  	console.log('Error ddp', error);
							});
							this.isTenant = true;
		            	}
	            	}
            	}
            }, 1000);
        });
		  		
  	}

  	scrollDown(element_id:string, speed:number) {
		var element = document.getElementById(element_id);
		if (element) {
		   	// element.scrollTop = element.scrollHeight;
		    // $("#" + element_id).scrollTop($("#" + element_id).get(0).scrollHeight);
		    $("#" + element_id).animate({ scrollTop: $('#' + element_id).prop("scrollHeight")}, speed || 0);
		}
	};

  	getChat(id: string, type:string) {
  		this.router.navigate(['/chat/' , id]);
		this.roomId = id;
		this.type = type;
		this.ddp.subscribeChatPublications(id);
		this.ddp.getAllCollectionData$(MY_DDP_COLLECTIONS.CHATS)
		.subscribe(chats => {
			this.chats = chats.filter((data:any)=> data.roomId === id)
			this.chats = _.sortBy(this.chats, 'createdAt')
			for (let z = 0; z < this.chats.length; ++z) {
				if(this.chats[z] && typeof this.chats[z].extra !== 'undefined') {
					console.log('test')
			      	let type = this.chats[z].extra.ref;
			      	let createdAt = this.chats[z].createdAt;
			      	let message = this.chats[z].message;
			      	if (this.chats[z].uid == this.userId) {
			      		console.log('test1')
			      		var user = this.me;
			      		if(user && user != undefined  && typeof this.chats[z].extra.ref_id !== 'undefined') {
			      			console.log('test2')
				        	let generate = generateChatTemplate({
				          		createdAt : createdAt,
				          		username : user.username,
				          		message: message,
				          		ref_id: this.chats[z].extra.ref_id
				        	}, type);
				        	this.chats[z].template = generate;
				      	}else{
				      		let generate = generateChatTemplate({
				          		createdAt : createdAt,
				          		username : user.username,
				          		message: message,
				        	}, type);
				        	this.chats[z].template = generate;
				      	}
			      	}else{
			      		this.userService.getById(this.chats[z].uid)
			      		.takeWhile(()=> this.subscription)
			      		.subscribe(data => {
			      			var user = data;
			      			if(user && user != undefined  && typeof this.chats[z].extra !== 'undefined') {
					        	let generate = generateChatTemplate({
					          		createdAt : createdAt,
					          		username : user.username,
					          		message: message,
					          		ref_id: this.chats[z].extra.ref_id
					        	}, type);
					        	this.chats[z].template = generate;
					      	}else{
					      		let generate = generateChatTemplate({
					          		createdAt : createdAt,
					          		username : user.username,
					          		message: message,
					        	}, type);
					        	this.chats[z].template = generate;
					      	}
			      		})
			      	}
			    }
			}
			if (type == 'landlord') {
				for (var z = 0; z < this.landlords.length; ++z) {
					if (this.landlords[z].room_id == id) {
						this.roomData = this.landlords[z]
						this.landlords[z].cls = 'col-md-12 col-sm-12 col-xs-12 contact-details active-chat'
					}else{
						this.landlords[z].cls = 'col-md-12 col-sm-12 col-xs-12 contact-details'
					}
				}
				let i = this.blockedId.indexOf(this.roomData.tenant._id)
				if (i != -1) {
					this.roomData.blocked = true
				}else{
					this.roomData.blocked = false
				}
			}else if (type == 'tenant') {
				for (var z = 0; z < this.tenants.length; ++z) {
					if (this.tenants[z].room_id == id) {
						this.roomData = this.tenants[z]
						this.tenants[z].cls = 'col-md-12 col-sm-12 col-xs-12 contact-details active-chat'
					}else{
						this.tenants[z].cls = 'col-md-12 col-sm-12 col-xs-12 contact-details'
					}
				}
				let i = this.blockedId.indexOf(this.roomData.landlord._id)
				if (i != -1) {
					this.roomData.blocked = true
				}else{
					this.roomData.blocked = false
				}
			}else{
				for (var z = 0; z < this.landlords.length; ++z) {
					if (this.landlords[z].room_id == id) {
						this.roomData = this.landlords[z]
						this.isTenant = false;
						this.landlords[z].cls = 'col-md-12 col-sm-12 col-xs-12 contact-details active-chat'
					}else{
						this.landlords[z].cls = 'col-md-12 col-sm-12 col-xs-12 contact-details'
					}
				}
				if (this.roomData) {
					let i = this.blockedId.indexOf(this.roomData.tenant._id)
					if (i != -1) {
						this.roomData.blocked = true
					}else{
						this.roomData.blocked = false
					}
				}else{
					for (var z = 0; z < this.tenants.length; ++z) {
						if (this.tenants[z].room_id == id) {
							this.roomData = this.tenants[z];
							this.tenants[z].cls = 'col-md-12 col-sm-12 col-xs-12 contact-details active-chat';
							this.isTenant = true;
						}else{
							this.tenants[z].cls = 'col-md-12 col-sm-12 col-xs-12 contact-details'
						}
					}
					if (this.roomData) {
						let i = this.blockedId.indexOf(this.roomData.landlord._id)
						if (i != -1) {
							this.roomData.blocked = true
						}else{
							this.roomData.blocked = false
						}
					}
				}
				
			}
			this.ddp.refreshUnseen(id)
			.then((res: any) => {
			}).catch(err => console.log(err , 'unseen err'))
		})
  	}

  	sendChat() {
  		this.loading = true
  		if (this.roomData.landlord._id === this.userId) {
  			this.userService.getById(this.roomData.tenant._id)
  			.takeWhile(()=> this.subscription)
	    	.subscribe(data => {
	    		for (let z = 0; z < data.blocked_users.length; ++z) {
	    			if (this.userId === data.blocked_users[z]._id) {
	    				this.blocked = true
	    			}
	    		}
	    		if (this.blocked === true) {
		    		this.notificationsService.error('',data.username+' has blocked you',)
					this.message = '';
		    	}else{
		    		if (this.message && this.roomId) {
					  	var params: any = {
							message: this.message,
							roomId: this.roomId,
					  	};
					  	this.ddp.sendChat(params)
						.then((res: any) => {
						  	this.message = '';
		    			this.loading = false
						  	this.scrollDown('chatBox', 500);
						}).catch(err => console.log(err, 'error'))
					}
		    	}
	    	})
  		}else{
  			this.userService.getById(this.roomData.landlord._id)
  			.takeWhile(()=> this.subscription)
	    	.subscribe(data => {
	    		for (let z = 0; z < data.blocked_users.length; ++z) {
	    			if (this.userId === data.blocked_users[z]._id) {
	    				this.blocked = true
	    			}
	    		}
	    		if (this.blocked === true) {
		    		this.notificationsService.error('',data.username+' has blocked you',)
		    		this.message = '';
		    	}else{
		    		if (this.message && this.roomId) {
					  	var params: any = {
							message: this.message,
							roomId: this.roomId,
					  	};
					  	this.ddp.sendChat(params)
						.then((res: any) => {
						  	this.message = '';
		    				this.loading = false
						  	this.scrollDown('chatBox', 500);
						}).catch(err => console.log(err, 'error'))
					}
		    	}
	    	})
  		}
  	}

  	viewSchedule(id: any) {
  		var landlord = this.me.owned_properties.filter((data:any) => data._id === id)
  		if (landlord != null && landlord != [] && landlord && landlord.length > 0) {
  			this.notificationsService.alert(' ',"You're here as landlord.")
  		}else{
  			this.router.navigate(['/appointment/' + id + '/initiate-view'])
  		}
	}

	initiateLOI(id:string){
		this.agreementService.getById(id)
		.takeWhile(()=> this.subscription)
		.subscribe(data => {
			if (data.letter_of_intent.data) {
				if (data.letter_of_intent.data.confirmation) {
					if (data.letter_of_intent.data.confirmation.tenant) {
						if (data.letter_of_intent.data.confirmation.tenant.sign) {
							this.router.navigate(['/loi/'+ id +'/view']);
						}else{
							this.notificationsService.alert('','You need to wait tenant to Initiate Letter of Intent.')
						}
					}else{
						this.notificationsService.alert('','You need to wait tenant to Initiate Letter of Intent.')
					}
				}else{
					this.notificationsService.alert('','You need to wait tenant to Initiate Letter of Intent.')
				}
			}else{
				if (data.tenant) {
					if (data.tenant._id === this.me._id) {
						this.router.navigate(['/loi/'+ id +'/initiate']);
					}else{
						this.notificationsService.alert('','You need to wait tenant to Initiate Letter of Intent.')
					}
				}
			}
		})
	}

	initiateTA(id: any){
		this.agreementService.getById(id)
		.takeWhile(()=> this.subscription)
		.subscribe(data => {
			if (data.tenancy_agreement.data) {
				if (data.tenancy_agreement.data.confirmation.landlord.sign) {
					this.router.navigate(['/ta/'+ id +'/view']);
				}else{
					this.notificationsService.alert('','You need to wait landlord to initiate Tenancy Agreement.')
				}
			}else if (data.letter_of_intent.data.status) {
				if (data.letter_of_intent.data.status == 'accepted') {
					if (data.landlord._id === this.me._id) {
						this.router.navigate(['/ta/'+ id +'/initiate']);
					}else{
						this.notificationsService.alert('','You need to wait landlord to initiate Tenancy Agreement.')
					}
				}else{
					this.notificationsService.alert('','Letter of intent needs to be accepted before initiating Tenancy Agreement.')
				}
			}else{
				this.notificationsService.alert('','Letter of intent needs to be accepted before initiating Tenancy Agreement.')
			}
		})
    }

    viewInventory(id:any){
    	this.agreementService.getById(id)
    	.takeWhile(()=> this.subscription)
		.subscribe(data => {
			if (data.inventory_list.data) {
				if (data.inventory_list.data.confirmation) {
					if (data.inventory_list.data.confirmation.landlord) {
						if (data.inventory_list.data.confirmation.landlord.sign) {
							this.router.navigate(['/inventory/'+ id]);
						}else{
							this.notificationsService.alert('','You need to wait landlord to create Inventory List.')
						}
					}else{
						if (data.tenancy_agreement.data.status == 'accepted') {
							if (data.landlord._id === this.me._id) {
								this.router.navigate(['/inventory/'+ id]);
							}else{
								this.notificationsService.alert('','You need to wait landlord to create Inventory List.')
							}
						}else{
							this.notificationsService.alert('','Inventory can be created after Letter of Intent and Tenancy Agreement is accepted.')
						}
					}
				}else{
					if (data.tenancy_agreement.data.status == 'accepted') {
						if (data.landlord._id === this.me._id) {
							this.router.navigate(['/inventory/'+ id]);
						}else{
							this.notificationsService.alert('','You need to wait landlord to create Inventory List.')
						}
					}else{
						this.notificationsService.alert('','Inventory can be created after Letter of Intent and Tenancy Agreement is accepted.')
					}
				}	
			}else if(data.tenancy_agreement.data.status){
				if (data.tenancy_agreement.data.status == 'accepted') {
					if (data.landlord._id === this.me._id) {
						this.router.navigate(['/inventory/'+ id]);
					}else{
						this.notificationsService.alert('','You need to wait landlord to create Inventory List.')
					}
				}else{
					this.notificationsService.alert('','Inventory can be created after Tenancy Agreement is accepted.')
				}
			}else{
				this.notificationsService.alert('','Inventory can be created after Letter of Intent and Tenancy Agreement is accepted.')
			}
		})
    }

    deleteChat(id:string){
    	this.chatService.delete(id)
    	.then(
			response => {
				this.notificationsService.success(
					'Success',
					'Delete conversation success',
					)
				this.router.navigate(['chat']);
			},
			error => {
				this.notificationsService.error(
					'Error',
					'Delete conversation failed',
					)
				console.log(error)
			}
		);
    }

    getData(){
		this.chatService.getByUser()
		.takeWhile(()=> this.subscription)
    	.subscribe(datas => {
    		for (var z = 0; z < datas.length; ++z) {
    			datas[z].cls = 'col-md-12 col-sm-12 col-xs-12 contact-details'
    		}
    		this.landlords = datas.filter((data:any) => data.landlord._id === this.userId)
    		this.tenants = datas.filter((data:any) => data.tenant._id === this.userId)
    		this.ddp.getAllCollectionData$(MY_DDP_COLLECTIONS.ROOMMEMBERS)
	  		.subscribe(datas => {
				this.datas = datas ;
				for (let z = 0; z < this.datas.length; ++z) {
					if (this.datas[z].uid === this.userId) {
						for (let x = 0; x < this.landlords.length; ++x) {
							let unseen = this.datas.find((data: any) => data ? data.roomId == this.landlords[x].room_id : false);
							if (!unseen) {
								this.landlords[x].unseenChats = 0 
							}else{
								this.landlords[x].unseenChats = unseen.unseenChats
							}
						}
						for (let x = 0; x < this.tenants.length; ++x) {
							let unseen = this.datas.find((data: any) => data ? data.roomId == this.tenants[x].room_id : false);
							if (!unseen || !unseen.unseenChats) {
								this.tenants[x].unseenChats = 0 
							}else{
								this.tenants[x].unseenChats = unseen.unseenChats
							}
						}
					}
				}
	    		this.landlordEnquiries = this.landlords.filter((data:any) => data.status == "enquiries")
	    		this.landlordPending = this.landlords.filter((data:any) => data.status == "pending")
	    		this.landlordRented = this.landlords.filter((data:any) => data.status == "rented")
	    		this.tenantEnquiries = this.tenants.filter((data:any) => data.status == "enquiries")
	    		this.tenantPending = this.tenants.filter((data:any) => data.status == "pending")
	    		this.tenantRented = this.tenants.filter((data:any) => data.status == "rented")
				for (let z = 0; z < this.landlords.length; ++z) {
					if (this.landlords[z].status == "enquiries") {
						this.unseenLandlordEnquiries = this.unseenLandlordEnquiries + + this.landlords[z].unseenChats
						this.totalUnseenLandlordEnquiries = this.unseenLandlordEnquiries
					}else if (this.landlords[z].status == "pending") {
						this.unseenLandlordPending = this.unseenLandlordPending + + this.landlords[z].unseenChats
						this.totalUnseenLandlordPending = this.unseenLandlordPending
					}else {
						this.unseenLandlordRented = this.unseenLandlordRented + + this.landlords[z].unseenChats
						this.totalUnseenLandlordRented = this.unseenLandlordRented
					}
					this.unseenLandlord = this.unseenLandlord + this.landlords[z].unseenChats;
					this.totalUnseenLandlord = this.unseenLandlord;
				}
				for (let z = 0; z < this.tenants.length; ++z) {
					if (this.tenants[z].status == "enquiries") {
						this.unseenTenantEnquiries = this.unseenTenantEnquiries + + this.tenants[z].unseenChats
						this.totalUnseenTenantEnquiries = this.unseenTenantEnquiries
					}else if (this.tenants[z].status == "pending") {
						this.unseenTenantPending = this.unseenTenantPending + + this.tenants[z].unseenChats
						this.totalUnseenTenantPending = this.unseenTenantPending
					}else {
						this.unseenTenantRented = this.unseenTenantRented + + this.tenants[z].unseenChats
						this.totalUnseenTenantRented = this.unseenTenantRented
					}
					this.unseenTenant = this.unseenTenant + this.tenants[z].unseenChats;
					this.totalUnseenTenant = this.unseenTenant;
				}
				this.unseenLandlord = 0;
				this.unseenTenant = 0;
				this.unseenLandlordEnquiries = 0;
				this.unseenLandlordPending = 0;
				this.unseenLandlordRented = 0;
				this.unseenTenantEnquiries = 0;
				this.unseenTenantPending = 0;
				this.unseenTenantRented = 0;
				localStorage.setItem('landlord', JSON.stringify(this.landlords));
				localStorage.setItem('tenant', JSON.stringify(this.tenants));
				this.ready = true
	  		});
    	}, error => { console.log('error retrieving rooms', error) });
    	
    }

    showModal(id:string){
    	this.model.id = id
    	this.modal.show()
    }

    block(id:string){
    	this.userService.block(id)
    	.then(
			response => {
                this.notificationsService.success('Success','This user has been successfully blocked.',)
                let i = this.blockedId.indexOf(id)
                if (i != -1) {
                	this.blockedId.push(id);
                }
                this.roomData.blocked = true
            },
        ).catch(
        	error => {
        		console.log(error)
            	this.notificationsService.error('Error',error,)
            }
        )
    }

    unblock(id:string){
    	this.userService.unblock(id)
    	.then(
			response => {
                this.notificationsService.success('Success','This user has been successfully unblocked.',)
                let i = this.blockedId.indexOf(id)
                if (i != -1) {
                	this.blockedId.splice(i, 1);
                }
                this.roomData.blocked = false
            },
        ).catch(
        	error => {
        		console.log(error)
            	this.notificationsService.error('Error',error,)
            }
       )
    }

    reportUser(){
    	this.reportService.create(this.model)
    	.then(res => {
    		this.notificationsService.success('Success','Your report has been successfully submitted.')
    	}).catch(err => this.notificationsService.error('Error',err))
    }

    openCollapse(type:string, type2:string){
    	if (type === "landlord" ) {
    		if (type2 === "enquiries") {
    			this.landlordEnquiriesCollapse = !this.landlordEnquiriesCollapse
    			if (this.landlordEnquiriesCollapse === true) {
    				this.landlordEnquiriesClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat collapsed'
    			}else {
    				this.landlordEnquiriesClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat'
    			}
    		}else if (type2 === "pending") {
    			this.landlordPendingCollapse = !this.landlordPendingCollapse
    			if (this.landlordPendingCollapse === true) {
    				this.landlordPendingClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat collapsed'
    			}else {
    				this.landlordPendingClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat'
    			}
    		}else {
    			this.landlordRentedCollapse = !this.landlordRentedCollapse
    			if (this.landlordRentedCollapse === true) {
    				this.landlordRentedClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat collapsed'
    			}else {
    				this.landlordRentedClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat'
    			}
    		}
    	}else{
    		if (type2 === "enquiries") {
    			this.tenantEnquiriesCollapse = !this.tenantEnquiriesCollapse
    			if (this.tenantEnquiriesCollapse === true) {
    				this.tenantEnquiriesClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat collapsed'
    			}else {
    				this.tenantEnquiriesClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat'
    			}
    		}else if (type2 === "pending") {
    			this.tenantPendingCollapse = !this.tenantPendingCollapse
    			if (this.tenantPendingCollapse === true) {
    				this.tenantPendingClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat collapsed'
    			}else {
    				this.tenantPendingClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat'
    			}
    		}else {
    			this.tenantRentedCollapse = !this.tenantRentedCollapse
    			if (this.tenantRentedCollapse === true) {
    				this.tenantRentedClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat collapsed'
    			}else {
    				this.tenantRentedClass = 'btn-plus-icon-chat chat-cursor icon-hide-chat'
    			}
    		}
    	}
    }

    ngOnDestroy() {
    	this.subscription = false;
    }
}
