import * as moment from 'moment'
var chatTemplate = function(data:any) {
	let message = '';
	if(data.message !== '-') {
		message = '<span class="">"'+data.message+'"</span><br>'
	}
	return '<div class="col-md-3 col-sm-3 col-xs-3 bg-for-logo">'
	+       '<div class="chat-box-pd request">'
	+        '<img src="/assets/images/'+data.logo+'" class="img-submited-schedule-logo">'
	+       '</div>'
	+    '</div>'
	+    '<div class="col-md-9 col-sm-9 col-xs-9 bg-for-content">'
	+       '<div class="chat-box-test request">'
	+        '<span class="request-title">STAYSMART</span>'
	+        '<br>'
	+        '<span class="request-content-title">'+data.title+'</span>'
	+     '</div>'
	+  '</div>'
	+  '<div class="col-md-12 col-sm-12 col-xs-12 behind-request">'
	+    '<span class="">'+data.request+'<span class="color-in-status-blue no-bold">'+data.date+'.</span></span><br>'
	+    message
	+     '<span class=""><a href="'+data.url+'" id="chatNotifylink" ref="'+data.ref+'" ref_id="'+data.ref_id+'" class="color-in-status-blue">Click here to see '+data.click_message+'</a></span>'
	+  '</div>';
};

export function generateChatTemplate(field:any, options:any) {
	if(field && field.username && field.createdAt && options) {
		let date = moment(field.createdAt).format('DD/MM/YYYY');
		let ref_id = field.ref_id;
		let message = field.message;
		let username = field.username;

		if(options == 'appt_proposed') {
			let data = {
				logo: 'calendar.png',
				title : 'Appointments Proposed Request',
				username : username,
				message: message,
				request : username+' sent a proposed request on ',
				date : date,
				ref : 'appt_proposed',
				ref_id: '',
				url: 'dashboard/proposed',
				click_message : 'proposed appointments',
			}
			return chatTemplate(data);
		}
		else if(options == 'appt_confirm') {
			let data = {
				logo: 'calendar.png',
				title : 'Appointments Accepted',
				username : username,
				message: message,
				request : username+' accepted your proposed request on ',
				date : date,
				ref : 'appt_confirm',
				ref_id : ref_id,
				url: 'dashboard/upcoming/'+ref_id,
				click_message : 'accepted appointments',
			}
			return chatTemplate(data);
		}
		else if(options == 'appt_reject') {
			let data = {
				logo: 'calendar.png',
				title : 'Appointments Rejected',
				username : username,
				message: message,
				request : username+' rejected your proposed request on ',
				date : date,
				ref : 'appt_reject',
				ref_id : '',
				url: 'dashboard/archived/',
				click_message : 'rejected appointments',
			}
			return chatTemplate(data);
		}
		else if(options == 'appt_cancel') {
			let data = {
				logo: 'calendar.png',
				title : 'Appointments Cancelled',
				username : username,
				message: message,
				request : username+' cancelled proposed request on ',
				date : date,
				ref : 'appt_cancel',
				ref_id : '',
				url: 'dashboard/proposed',
				click_message : ' appointments',
			}
			return chatTemplate(data);
		}

		else if(options == 'loi_amendByLandlord') {
			let data = {
				logo: 'envelope.png',
				title : 'Letter of Intent Amended',
				username : username,
				message: message,
				request : username+' amended letter of intent on ',
				date : date,
				ref : 'loi_amend',
				ref_id : ref_id,
				url: 'loi/' + ref_id +'/view',
				click_message : ' letter of intent',
			}
			return chatTemplate(data);
		}
		else if(options == 'loi_amendByTenant') {
			let data = {
				logo: 'envelope.png',
				title : 'Letter of Intent Amended',
				username : username,
				message: message,
				request : username+' amended letter of intent on ',
				date : date,
				ref : 'loi_amend',
				ref_id : ref_id,
				url: 'loi/' + ref_id +'/view',
				click_message : ' letter of intent',
			}
			return chatTemplate(data);
		}
		else if(options == 'loi_create') {
			let data = {
				logo: 'envelope.png',
				title : 'Letter of Intent Created',
				username : username,
				message: message,
				request : username+' created letter of intent on ',
				date : date,
				ref : 'loi_create',
				ref_id : ref_id,
				url: 'loi/' + ref_id +'/view',
				click_message : ' letter of intent',
			}
			return chatTemplate(data);
		}
		else if(options == 'loi_accept') {
			let data = {
				logo: 'envelope.png',
				title : 'Letter of Intent Accepted',
				username : username,
				message: message,
				request : username+' accepted letter of intent on ',
				date : date,
				ref : 'loi_accept',
				ref_id : ref_id,
				url: 'loi/' + ref_id +'/view',
				click_message : ' letter of intent',
			}
			return chatTemplate(data);
		}
		else if(options == 'loi_reject') {
			let data = {
				logo: 'envelope.png',
				title : 'Letter of Intent Rejected',
				username : username,
				message: message,
				request : username+' rejected letter of intent on ',
				date : date,
				ref : 'loi_reject',
				ref_id : ref_id,
				url: 'loi/' + ref_id +'/view',
				click_message : ' letter of intent',
			}
			return chatTemplate(data);
		}

		else if(options == 'ta_amend') {
			let data = {
				logo: 'greenshake.png',
				title : 'Tenancy Agreement Amended',
				username : username,
				message: message,
				request : username+' amended tenancy agreement on ',
				date : date,
				ref : 'ta_amend',
				ref_id : ref_id,
				url: 'ta/' + ref_id +'/view',
				click_message : ' tenancy agreement',
			}
			return chatTemplate(data);
		}
		else if(options == 'ta_create') {
			let data = {
				logo: 'greenshake.png',
				title : 'Tenancy Agreement Created',
				username : username,
				message: message,
				request : username+' created tenancy agreement on ',
				date : date,
				ref : 'ta_create',
				ref_id : ref_id,
				url: 'ta/' + ref_id +'/view',
				click_message : ' tenancy agreement',
			}
			return chatTemplate(data);
		}
		else if(options == 'ta_accept') {
			let data = {
				logo: 'greenshake.png',
				title : 'Tenancy Agreement Accepted',
				username : username,
				message: message,
				request : username+' accepted tenancy agreement on ',
				date : date,
				ref : 'ta_accept',
				ref_id : ref_id,
				url: 'ta/' + ref_id +'/view',
				click_message : ' tenancy agreement',
			}
			return chatTemplate(data);
		}
		else if(options == 'ta_reject') {
			let data = {
				logo: 'greenshake.png',
				title : 'Tenancy Agreement Rejected',
				username : username,
				message: message,
				request : username+' rejected tenancy agreement on ',
				date : date,
				ref : 'ta_reject',
				ref_id : ref_id,
				url: 'ta/' + ref_id +'/view',
				click_message : ' tenancy agreement',
			}
			return chatTemplate(data);
		}

		else if(options == 'inventory_create') {
			let data = {
				logo: 'file.png',
				title : 'Inventory Created',
				username : username,
				message: message,
				request : username+' created inventory list on ',
				date : date,
				ref : 'inventory_create',
				ref_id : ref_id,
				url: 'inventory/' + ref_id ,
				click_message : ' inventory list',
			}
			return chatTemplate(data);
		}
		else if(options == 'inventory_update') {
			let data = {
				logo: 'file.png',
				title : 'Inventory Updated',
				username : username,
				message: message,
				request : username+' updated inventory list on ',
				date : date,
				ref : 'inventory_update',
				ref_id : ref_id,
				url: 'inventory/' + ref_id ,
				click_message : ' inventory list',
			}
			return chatTemplate(data);
		}
		else if(options == 'inventory_confirm') {
			let data = {
				logo: 'file.png',
				title : 'Inventory Confirmed',
				username : username,
				message: message,
				request : username+' confirmed inventory list on ',
				date : date,
				ref : 'inventory_confirm',
				ref_id : ref_id,
				url: 'inventory/' + ref_id ,
				click_message : ' inventory list',
			}
			return chatTemplate(data);
		}

	}
}