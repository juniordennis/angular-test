import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { NotificationsService } from 'angular2-notifications';
import { ValidationService } from '../../services/validation.service'; 
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'forget-password-email',
  templateUrl: './forget-password-email.html',
})
export class ForgetPasswordEmailComponent implements OnInit{ 
  subscription: boolean = true;
	model: any = {};
	public id: any;
  myFormForgot: FormGroup;
  myFormReset: FormGroup;
  resetsuccess:boolean = false;

	constructor(private router: Router,
        private authenticationService:AuthenticationService,
        private route: ActivatedRoute,
        private ValidationService: ValidationService,
        private formbuilder: FormBuilder,
        private _notificationsService:NotificationsService) {}

	ngOnInit():void{
		this.route.params
    .takeWhile(()=> this.subscription)
    .subscribe(params => {
            this.id = params['id'];
        });
      this.myFormForgot = this.formbuilder.group({ 
      email: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(70)])]
      });
      this.myFormReset = this.formbuilder.group({ 
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        repassword: ['', [Validators.required, Validators.minLength(6)]]  
      });   
	}

	sentMail() {
        this.authenticationService.sentMail(this.model)
        .then(
            response => {
                this.router.navigate(['']);
                this._notificationsService.success(
                            'Success',
                            'Email sent. Check your mailbox.',
                    )
            },
            error => {
                this._notificationsService.error(
                            'Error',
                            'Email not send. Check your email.',
                    )
            }
        );
    }

    resetPass() {
        this.authenticationService.resetPass(this.id, this.model)
        .then(
             response => {
               this.router.navigate(['']);
               this._notificationsService.success(
                            'Success',
                            'Your password has been changed..',
                    )
             },
             error => {
                  this._notificationsService.error(
                            'Error',
                            'Update error. Please try again',
                    )
             }
         );
    }

    ngOnDestroy() {
      this.subscription = false;
    }
}
