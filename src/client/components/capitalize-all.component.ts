import { Component, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizeFirstIfAll'
})
export class CapitalizeFirstIfAllPipe implements PipeTransform {
  transform(value: string, args: any[]): string {
    if (value === null) return 'Not assigned';
    if (value == 'all') {
    	return 'All';
    }
    else {
    	return value;
    }
  }
}