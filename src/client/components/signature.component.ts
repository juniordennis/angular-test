import { Component, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
 
@Component({
  selector: 'signature',
  template: `
    <div style="width: 350px; height: 135px; border: 1px dashed grey; touch-action: none; margin:auto;">
      <signature-pad [options]="signaturePadOptions" (onEndEvent)="drawComplete()"></signature-pad>
    </div>
    <div class="col-md-12">
      <button (click)="drawClear()" class="clear-sign-pad btn btn-sm btn-danger">Clear</button>
    </div>
  `
})
 
export class SignatureComponent{
  data:any;

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  private signaturePadOptions: Object = { 
    'minWidth': 1,
    'maxWidth': 2,
    'canvasWidth': 350,
    'canvasHeight': 135,
  };

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.set('maxWidth', 2);
    this.signaturePad.clear();
  }

  drawComplete() {
    this.data= this.signaturePad.toDataURL('image/png');
  }

  drawClear(){
    this.signaturePad.clear();
  }
}