import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { AgreementService } from '../../services/agreement.service';
import { BankService } from '../../services/bank.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { StorageService } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import { ChatService } from '../../services/chat.service';
import { NavbarComponent } from '../navbar/navbar.component'
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'ta-initiate',
  templateUrl: './ta-initiate.html',
})

export class TaInitiateComponent implements OnInit{
	subscription: boolean = true;
	id:string;
	banks: SelectItem[];
	selectedBank: any;
	model: any = {};
	bank:any;
	landlord:any;
	isValid: boolean;
	bank_valid: boolean;
	agreement: any
	private value:any = {};
	public ddp: MyDDPClient;
	public loading: boolean = false;
	public dreamtalkReady: boolean = false;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private agreementService: AgreementService,
		private bankService: BankService,
		private storageService: StorageService,
		private sharedService: SharedService,
		private chatService: ChatService,
		private navbarComponent: NavbarComponent,
		) {}

	ngOnInit(): void{
		this.storageService.get('dreamtalk.ready').takeWhile(()=> this.subscription).subscribe((ready: boolean) => { this.dreamtalkReady = ready });
		this.sharedService.ddp$.takeWhile(()=> this.subscription).subscribe((ddp: MyDDPClient) => {
			this.ddp = ddp;
		}, (error: any) => {
			console.log('Error ddp', error);
		})
		this.storageService.get('dreamtalk.ready').takeWhile(()=> this.subscription).subscribe((ready: boolean) => { this.dreamtalkReady = ready });
		this.bank_valid = false;
		this.route.params.subscribe(params => {
			this.id = params['id'];
		});
		this.sharedService.ddp$.takeWhile(()=> this.subscription).subscribe((ddp: MyDDPClient) => {
			this.ddp = ddp;
		}, (error: any) => {
			console.log('Error ddp', error);
		})
		this.agreementService.getById(this.id).takeWhile(()=> this.subscription).subscribe(data => console.log(data))
		this.banks = [];
		this.bankService.getAll()
		.subscribe(bank => { 
			this.bank = bank;
			for (let a = 0; a < this.bank.length; ++a) {
				this.banks.push(this.bank[a].name);
			}
		});
	}

	createTA(valid:boolean) {
		this.isValid = true
		if (valid) {
			this.bankService.getAll()
			.takeWhile(()=> this.subscription)
			.subscribe(bank => { 
				var a = bank.find((data:any) => data.name == this.selectedBank[0].id);
				this.model.bank = a._id
				this.bank_valid = true
				this.agreementService.createTA(this.id, this.model)
				.then(
					response => {
						this.router.navigate([ '/ta/'+ this.id +'/view']);
						if (localStorage.getItem('authToken')) {
							if (this.dreamtalkReady) {
								if (this.ddp) {
									if (!this.agreement.room_id) {
										let roomParams = { property_id: this.agreement.property._id };
										this.loading = true
										this.chatService.createRoom(roomParams)
										.then((res: any) => {
											let roomId = res && res.room_id ? res.room_id : undefined;
											let chatParams: any = {
												roomId: roomId,
												message: '-',
												extra: { 
													ref : 'ta_create',
													ref_id: this.id,
												}
											  };
											this.ddp.sendChat(chatParams).then((res: any) => {
												this.loading = false
											}).catch((error: any) => {
												console.log(error);
											})
										}).catch((err: any) => {
											console.log(err);
										})
									}else{
										this.loading = true
										let chatParams: any = {
											roomId: this.agreement.room_id.room_id,
											message: '-',
											extra: { 
												ref : 'ta_create',
												ref_id: this.id,
											}
										  };
										this.ddp.sendChat(chatParams).then((res: any) => {
											this.loading = false
										}).catch((error: any) => {
											console.log(error);
											this.ddp.sendChat(chatParams).then((res: any) => {
												this.loading = false
											}).catch((error: any) => {
												console.log(error);
											})
										})
									}
								} else {
									// no ddp
								}
							} else {
								// dreamtalk not ready
								// this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
							}
						} else {
							this.navbarComponent.modalLogin.show()
						}
					},
					error => {
						console.log(error)
					}
				);
			})
		}
	}

	number(event: any) {
        const pattern = /[0-9\+\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    text(event: any) {
        const pattern = /[a-z\A-Z\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

	public selected(value:any):void {
		console.log('Selected value is: ', value);
	}
 
	public removed(value:any):void {
		console.log('Removed value is: ', value);
	}
 
	public typed(value:any):void {
		console.log('New search input: ', value);
	}
 
	public refreshValue(value:any):void {
		this.value = value;
	}

	ngOnDestroy() {
		this.subscription = false;
	}
}