import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { NotificationsService } from 'angular2-notifications';
import "rxjs/add/operator/takeWhile";
import * as moment from 'moment'

@Component({
	selector: 'verification',
	templateUrl: './verification.html',
})

export class VerificationComponent {
	subscription: boolean = true;
	model:any = {};
	id: any;
	loading:boolean = false
	disable:boolean = false
	countdown: any;
	milisec: number;
	time:any;

	constructor(private router: Router,private userService:UserService,private route: ActivatedRoute, private notificationsService: NotificationsService ) {}

	ngOnInit():void{
		let self = this;
		this.route.params.takeWhile(()=> this.subscription).subscribe(params => {
			this.id = params['id'];
		});
		this.time = setInterval( timer, 100 );
		function timer() {
		    if (localStorage.getItem('time')) {
				self.milisec = +Date.now() - +localStorage.getItem('time')
				self.countdown = self.milisec / 1000
				self.countdown = parseInt(self.countdown)
				self.countdown = 300 - self.countdown
				if (self.countdown < 1) {
					self.disable = false
					localStorage.removeItem('time')
					clearInterval(self.time)
				}else{
					self.disable = true
				}
			}else{
				self.disable = false
			}
		}
	}

	sendCode(){
		this.userService.getCode(this.id)
		.then(response => {
			this.notificationsService.success('Success','Verification Code sent')
			localStorage.setItem('time', Date.now().toString())
			this.ngOnInit()
		}).catch(error => {
			this.notificationsService.error('Error','Error, please try again later')
		})
	}

	submitCode(){
		this.loading = true
		this.userService.Activate(this.id, this.model)
		.takeWhile(()=> this.subscription)
		.subscribe(
			response => {
				this.router.navigate(['']);
				this.notificationsService.success('Success','Your account Verified')
			},
			error => {
				this.notificationsService.error('Error','Error, please try again later')
			}
		);
	}

	ngOnDestroy() {
		this.subscription = false;
	}
}
