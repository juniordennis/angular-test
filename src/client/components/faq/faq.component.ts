import { Component, OnInit, OnDestroy } from '@angular/core';
import { FaqService } from '../../services/faq.service';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'faq',
  templateUrl: './faq.html',
})

export class FaqComponent implements OnInit{
	subscription: boolean = true;
	isFaqLandlord: any;
	isFaqTenant: any;
	landlordFaqs: any;
	tenantFaqs: any;

	constructor(private faqService: FaqService){}

	ngOnInit():any{
		this.faqService.getFilter('landlord').takeWhile(()=> this.subscription).subscribe(landlordFaqs => {
			this.landlordFaqs = landlordFaqs;
		});
        this.faqService.getFilter('tenant').takeWhile(()=> this.subscription).subscribe(tenantFaqs => {
			this.tenantFaqs = tenantFaqs;
        });
	}

	ngOnDestroy() {
		this.subscription = false
	}
}
