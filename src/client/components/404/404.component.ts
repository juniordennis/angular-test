import { Component } from '@angular/core';

@Component({
  selector: 'not-found',
  templateUrl: `client/components/404/404.html`,
})
export class NotFoundComponent { 
	
}
