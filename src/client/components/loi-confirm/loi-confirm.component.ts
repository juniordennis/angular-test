import { Component, ViewChild, OnInit } from '@angular/core';
import { SignatureComponent } from '../signature.component'
import { AgreementService } from '../../services/agreement.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { StorageService } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import { ChatService } from '../../services/chat.service';
import { NavbarComponent } from '../navbar/navbar.component'
import { NotificationsService } from 'angular2-notifications';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'loi-confirm',
  templateUrl: './loi-confirm.html',
})

export class LoiConfirmComponent implements OnInit {
    subscription: boolean = true;
    id:string;
    model:any = {};
    agreements_id: any;
    landlord: any;
    date: any;
    public ddp: MyDDPClient;
    public loading: boolean = false;
    public dreamtalkReady: boolean = false;
    agreement:any;
	@ViewChild(SignatureComponent) signatureComponent: SignatureComponent;

	constructor(
		private agreementService: AgreementService,
		private router: Router,
		private route: ActivatedRoute,
		private sharedService: SharedService,
        private storageService: StorageService,
        private chatService: ChatService,
        private navbarComponent: NavbarComponent,
        private notificationsService: NotificationsService,
		){}

	ngOnInit():any{
        this.storageService.get('dreamtalk.ready').takeWhile(()=> this.subscription).subscribe((ready: boolean) => { this.dreamtalkReady = ready });
        this.sharedService.ddp$.takeWhile(()=> this.subscription).subscribe((ddp: MyDDPClient) => {
            this.ddp = ddp;
        }, (error: any) => {
            console.log('Error ddp', error);
        })
		this.route.params
        .takeWhile(()=> this.subscription)
        .subscribe(params => {
			this.id = params['id'];
		});
		this.date = Date.now();
    	// this.sharedService.emitChange({ showFooter: false, showHeader: true });
		this.agreementService.getById(this.id)
        .takeWhile(()=> this.subscription)
        .subscribe(agreement => {
			this.agreement = agreement
        });
	}

	accept(){
		this.model.sign = this.signatureComponent.data;
        if(this.model.sign) {
    		this.model.status = 'landlord';
    		this.model.type = 'letter_of_intent';
    		this.agreementService.acceptLOI(this.id, this.model)
    		.then(
    			response => {
                    this.router.navigate([ '/loi/'+ this.id +'/view']);
    				if (localStorage.getItem('authToken')) {
                        if (this.dreamtalkReady) {
                            if (this.ddp) {
                                if (!this.agreement.room_id) {
                                    let roomParams = { property_id: this.agreement.property._id };
                                    this.loading = true
                                    this.chatService.createRoom(roomParams)
                                    .then((res: any) => {
                                        let roomId = res && res.room_id ? res.room_id : undefined;
                                        let chatParams: any = {
                                            roomId: roomId,
                                            message: '-',
                                            extra: { 
                                                ref : 'loi_accept',
                                                ref_id: this.id,
                                            }
                                          };
                                        this.ddp.sendChat(chatParams).then((res: any) => {
                                            this.loading = false
                                            // this.notificationsService.success('Success', 'Message has been delivered.');
                                        }).catch((error: any) => {
                                            console.log(error);
                                            // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                        })
                                    }).catch((err: any) => {
                                        console.log(err);
                                        // this.notificationsService.error('Error', 'Failed to create room');
                                    })
                                }else{
                                    this.loading = true
                                    let chatParams: any = {
                                        roomId: this.agreement.room_id.room_id,
                                        message: '-',
                                        extra: { 
                                            ref : 'loi_accept',
                                            ref_id: this.id,
                                        }
                                      };
                                    this.ddp.sendChat(chatParams).then((res: any) => {
                                        this.loading = false;
                                        // this.notificationsService.success('Success', 'Message has been delivered.');
                                    }).catch((error: any) => {
                                        console.log(error);
                                        this.ddp.sendChat(chatParams).then((res: any) => {
                                            this.loading = false
                                            // this.notificationsService.success('Success', 'Message has been delivered.');
                                        }).catch((error: any) => {
                                            console.log(error);
                                            // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                        })
                                        // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                    })
                                }
                            } else {
                                // no ddp
                            }
                        } else {
                            // dreamtalk not ready
                            // this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
                        }
                    } else {
                        this.navbarComponent.modalLogin.show()
                    }
                },
                error => {
                    console.log(error)
                }
    		);
        } else {
            this.notificationsService.error('', 'Please fill the sign');
        }
	}

	back(){
		this.router.navigate([ '/loi/'+ this.id +'/view']);
	}

    ngOnDestroy() {
        this.subscription = false;
    }
}
