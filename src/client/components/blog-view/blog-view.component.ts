import { Component, OnInit, Directive, AfterViewInit, ElementRef, Input, Inject  } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { BlogService } from '../../services/blog.service';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { NgForm, FormsModule } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { AuthenticationService } from '../../services/authentication.service';
import { NotificationsService } from 'angular2-notifications';
import "rxjs/add/operator/takeWhile";

@Directive({
    selector: '[focus]'
})

class FocusDirective {
    @Input()
    focus:boolean;
    constructor(@Inject(ElementRef) private element: ElementRef) {}
    protected ngOnChanges() {
        this.element.nativeElement.focus();
    }
}

@Component({
  selector: 'blog-view',
  templateUrl: './blog-view.html',
  providers:[BlogService],
})

export class BlogViewComponent implements OnInit {
    subscription: boolean = true;
    public blog: any ={};
    public blogSlug: string;
    public blogs: any;
    public data: any;
    public submitted: boolean;
    public user: any;
    public model: any = {};
    public comments: any[] = []; 
    public currentUser: any;
    public commentForm: boolean = false;
    public replyForm: boolean = true;
    public confirmation: boolean = false;
    public validateEmail: boolean = false;
    public inputFocused: boolean = false;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formbuilder: FormBuilder,
        private notificationsService: NotificationsService,    
        private blogService: BlogService,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        @Inject(ElementRef) private element: ElementRef 
    ) { }    


    ngOnInit(): void {
        var lower_count = 0;
        var high_count = 4;
        let old_blog_title = localStorage.getItem('blog_title');
        let old_blog_cover = localStorage.getItem('blog_cover');
        let old_blog_desc = localStorage.getItem('blog_desc');
        if (old_blog_title != '') {
            let refresh = localStorage.getItem('refresh');
            if (refresh != old_blog_title) {
                localStorage.setItem('refresh', old_blog_title);
                localStorage.setItem('blog_url', location.href);
            }
        }
        this.route.params
        .takeWhile(()=> this.subscription)
        .subscribe(params => {
            this.blogSlug = params['slug'];
            this.blogService.getBySlug(this.blogSlug)
            .takeWhile(()=> this.subscription)
            .subscribe(data => { 
                this.blog = data;
                let title = data.title;
                let cover = data.cover.url;
                let content = data.content;
                let contentNew = content.replace(/(<([^>]+)>)/ig,"");
                if (contentNew.length > 225) {
                    contentNew = contentNew.substr(0, 225)+' ...';
                }
                let desc = contentNew;
                setTimeout(() => {
                    document.title = title;
                    document.getElementById("meta-title").setAttribute("content", title);
                    document.getElementById("meta-title:og").setAttribute("content", title);
                    document.getElementById("meta-description").setAttribute("content", desc);
                    document.getElementById("meta-description:og").setAttribute("content", desc);
                    document.getElementById("meta-img").setAttribute("content", cover);
                    document.getElementById("meta-url").setAttribute("content", location.href);
                }, 700);
                localStorage.setItem('blog_title', title);
                localStorage.setItem('blog_cover', cover);
                localStorage.setItem('blog_desc', desc);
                if (old_blog_title != title) {
                    localStorage.setItem('refresh', title);
                    localStorage.setItem('blog_url', location.href),
                    this.ngOnInit();
                }
                this.blogService.getAll()
                .takeWhile(()=> this.subscription)
                .subscribe( blogData => { 
                    this.blogs = blogData;
                    let length_blog = blogData.length;
                    if(length_blog > 4) {
                       lower_count = length_blog - 4;
                       high_count = length_blog; 
                    }

                    var similar_blog = 0;
                    var similar: any [] = [];
                    for(var i = 0; i < length_blog; i++) {
                        if(blogData[i].category != null && blogData[i].category.name == data.category.name
                         && blogData[i].slug != data.slug && similar_blog < 4) {
                            similar.push({ 
                                cover: blogData[i].cover.url, 
                                title: blogData[i].title, 
                                desc: blogData[i].content,
                                link: blogData[i].slug, 
                                created_at: blogData[i].created_at 
                            })
                            similar_blog += 1;
                        }
                    }
                    
                    if(similar_blog == 0) {
                        for(var i = lower_count; i < high_count; i++) {
                            if(blogData[i].slug != data.slug) {
                                similar.push({
                                    cover: blogData[i].cover.url, 
                                    title: blogData[i].title, 
                                    desc: blogData[i].content,
                                    link: blogData[i].slug, 
                                    created_at: blogData[i].created_at 
                                })
                            }
                        }
                    }  
                    this.data = similar;
                });            
            });
        });
        this.authenticationService.getAuthToken().takeWhile(()=> this.subscription).subscribe((token:string) => {
        this.userService.getByToken().takeWhile(()=> this.subscription).subscribe((user:any) => {
        this.user = user;
        this.model.name = this.user.username;
        this.model.email = this.user.email;
        })
        }, (error:any) => {
        this.user = undefined;
        })
    }

    smartSubstr(str:string, len:number) {
        var temp = str.substr(0, len);
        if(temp.lastIndexOf('<') > temp.lastIndexOf('>')) {
            temp = str.substr(0, 1 + str.indexOf('>', temp.lastIndexOf('<')));
        }
        return temp;
    }

    createComment(myForm: NgForm) {
        if (this.model.name != null && this.model.email != null && this.model.content != null 
            && this.model.name != "" && this.model.email != "" && this.model.content != "" && this.model.content != " ") {
            if (this.model.email.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
                this.submitted = true;
                this.model.blog = this.blog._id;
                this.blogService.createComments(this.model)
                .then(
                    data => {
                    this.submitted = false;
                    this.notificationsService.success(
                        'Success',
                        'Create Comments successful',
                    )
                    this.model.content = " ";
                    this.ngOnInit();
                    },
                    error => {
                    this.submitted = false;
                    this.notificationsService.error(
                        'Error',
                        'The Comment could not be save, server Error.',
                    )
                    }
                );
                this.confirmation = false;
            }
            else {
                this.confirmation = true;
            }
        }
    }

    subscribe(email: any) {
        email = this.model.email;
        if (email) {
            if (email.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
                this.blogService.subscribe(email)
                .then(
                    data => {
                    this.notificationsService.success(
                        'Success',
                        'Thanks For Subscribe',
                    )
                    },
                    error => {
                    this.notificationsService.error(
                        'Error',
                        'Cannot Subscribe, server Error.',
                    )
                    }
                );
                this.confirmation = false;
            }
            else {
                this.confirmation = true;
            }
        }
        else {
            this.validateEmail = true;
        }
    }

    replyButton() {
        this.confirmation = false;
        this.commentForm = true;
        this.replyForm = false;
            this.model.content = "";
            this.inputFocused = true;
    }

    cancelButton() {
        this.confirmation = false;
        this.commentForm = false;
        this.replyForm = true;
            this.model.content = "";
            this.model.commentID = "";
    }

    checkCommentId(commentID: any) {
        this.model.blog = this.blog._id;
        this.model.commentID = commentID;
    }

    createReply(myForm: NgForm) {
        if (this.model.name != null && this.model.email != null && this.model.content != null 
            && this.model.name != "" && this.model.email != "" && this.model.content != "" && this.model.content != " ") {
            if (this.model.email.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
                this.submitted = true;
                this.model.blog = this.blog._id;
                this.model.commentID;
                this.blogService.createComments(this.model)
                .then(
                    data => {
                    this.submitted = false;
                    this.notificationsService.success(
                        'Success',
                        'Create Comment successful',
                    )
                    this.ngOnInit();
                    this.model.content = " ";
                    },
                    error => {
                    this.submitted = false;
                    this.notificationsService.error(
                        'Error',
                        'The Comment could not be save, server Error.',
                    )
                    }
                );
                this.confirmation = false;
            }
            else {
                this.confirmation = true;
            }
        }
    }

    deleteComment(id :string) {       
        this.blogService.delete(id)
        .then(
            data => {
                this.ngOnInit()
                this.notificationsService.success(
                    'Success',
                    'Delete comment successful',
                )
            },
            error => {
                this.notificationsService.error(
                    'Error',
                    'Failed to delete, server error',
                )
            }
        );
    }    

    ngOnDestroy() {
        this.subscription = false;
    }
}