import { Component, OnInit, Directive, AfterViewInit, ElementRef, Input, Inject, ViewChild, Renderer  } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';	
import { UserService } from '../../services/user.service';	
import { AttachmentService } from '../../services/attachment.service';
import { url } from '../../../global';
import { ManagerService } from '../../services/manager.service';
import { NotificationsService } from 'angular2-notifications';
import { ValidationService } from '../../services/validation.service'; 
import { ModalDirective } from 'ng2-bootstrap';
import { NavbarComponent } from '../navbar/navbar.component';
import * as $ from 'jquery';
import "rxjs/add/operator/takeWhile";

function matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
	return (group: FormGroup): {[key: string]: any} => {
			let password = group.controls[passwordKey];
			let confirmPassword = group.controls[confirmPasswordKey];

			if (password.value !== confirmPassword.value) {
				return {
					mismatchedPasswords: true
				};
			}
		}
}
function matchingOldPasswords(passwordKey: string, confirmPasswordKey: string) {
	return (group: FormGroup): {[key: string]: any} => {
			let password = group.controls[passwordKey];
			if (password.value !== confirmPasswordKey) {
				return {
					mismatchedOldPasswords: true
				};
			}
		}
}


@Component({
	selector: 'setting',
	templateUrl: './setting.html',
})

export class SettingComponent implements OnInit{ 
	@ViewChild('modalNotif') public modalNotif: ModalDirective;
	subscription: boolean = true;
	focus: boolean = true;
	myForm : FormGroup;
	public myFormPassword : FormGroup;
	public link: any;
	public dataProfile: any = {};
	public dataEmail: string;
	public dataUsername: string;
	public settingId: any;
	public currentPassword: any;
	public data: any = {};
	model: any = {};
	user: any = {};
	public displaypassword = false;
	public status = false;
	public profilePhotoID : any;
	public header : any;
	public isNotSocial: boolean = true;
	public picture: string;
	properties: any
	classes:any[] = [];
	add:boolean = false;
	public password = new FormControl('', [Validators.required, Validators.minLength(6)]);
	appointedProperties: any[] = [];
	ownedProperties: any[] = [];
	managedProperties: any[] = [];
	private id:string;
	public shownedOwnedProperties: any[] = [];
	public hiddenOwnedProperties: any[] = [];
	public seeAllOwnedProperties: boolean = false;
	public showHiddenOwnedProperties: boolean = false;
	public clickedOwnedProperties: boolean = true;

	public shownedManagedProperties: any[] = [];
	public hiddenManagedProperties: any[] = [];
	public seeAllManagedProperties: boolean = false;
	public showHiddenManagedProperties: boolean = false;
	public clickedManagedProperties: boolean = true;

	public shownedAppointedProperties: any[] = [];
	public hiddenAppointedProperties: any[] = [];
	public seeAllAppointedProperties: boolean = false;
	public showHiddenAppointedProperties: boolean = false;
	public clickedAppointedProperties: boolean = true;

	constructor(
		private formbuilder: FormBuilder, 
		private userService: UserService,
		private managerService: ManagerService,
		private activateRoute: ActivatedRoute, 
		private location: Location, 
		private attachementService: AttachmentService,
    	private ValidationService: ValidationService,
		private notificationsService: NotificationsService,
		private navbarComponent: NavbarComponent,
		private router: Router) { this.location = location; }

	ngOnInit(): void {
		this.add = false;
		this.clickedOwnedProperties = true;
		this.model.property = [];
		this.model.chat = true;
		this.managerService.getOwn()
		.takeWhile(()=> this.subscription)
		.subscribe(data => {
			this.appointedProperties = data.filter((data:any) => data.status == 'pending');
			if(this.appointedProperties.length > 4){
				this.shownedAppointedProperties = data.filter((data:any) => data.status == 'pending').slice(0, 4);
				this.hiddenAppointedProperties = data.filter((data:any) => data.status == 'pending').slice(4, this.appointedProperties.length);
				this.seeAllAppointedProperties = true;
			}
			else {
				this.shownedAppointedProperties = this.appointedProperties;
			}

		})
		this.userService.getByToken()
		.takeWhile(()=> this.subscription)
		.subscribe(dataUser => {
			this.ownedProperties = dataUser.owned_properties.filter((data:any) => data.status != 'draft');
			if(this.ownedProperties.length > 4){
				this.shownedOwnedProperties = dataUser.owned_properties.filter((data:any) => data.status != 'draft').slice(0, 4);
				this.hiddenOwnedProperties = dataUser.owned_properties.filter((data:any) => data.status != 'draft').slice(4, this.ownedProperties.length);
				this.seeAllOwnedProperties = true;
			}
			else {
				this.shownedOwnedProperties = this.ownedProperties;
			}
			this.managedProperties = dataUser.managed_properties.filter((data:any) => data.status != 'draft');
			if(this.managedProperties.length > 4){
				this.shownedManagedProperties = dataUser.managed_properties.filter((data:any) => data.status != 'draft').slice(0, 4);
				this.hiddenManagedProperties = dataUser.managed_properties.filter((data:any) => data.status != 'draft').slice(4, this.managedProperties.length);
				this.seeAllManagedProperties = true;
			}
			else {
				this.shownedManagedProperties = this.managedProperties;
			}

			this.dataProfile = dataUser;
			this.dataEmail = dataUser.email;
			this.dataUsername = dataUser.username;
			this.status = true;
			if (this.dataProfile.service){
				this.isNotSocial = false;
			}
		});
		this.myFormPassword = this.formbuilder.group({
			oldpassword : ['', Validators.required],
			newpassword: this.password,
			confirmpassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
		});
		this.link = url+'attachments';
		let currentUser = JSON.parse(localStorage.getItem('authToken'));
		if (currentUser && currentUser.token) {
			this.header = {'Authorization': 'Bearer ' + currentUser.token}
		}
		this.myForm = this.formbuilder.group({ 
	      username: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(70), Validators.pattern('^[a-zA-Z0-9_.-]*$')]), this.usernameUnique.bind(this)], 
	      email: ['', Validators.compose([Validators.required, ValidationService.emailValidator]), this.emailUnique.bind(this)],
	      fullname : [''],
			identification : [''],
			phone : ['']
	    }); 
	}

	clickSeeAllOwnedProperties() {
		if (this.seeAllOwnedProperties === true) {
			this.showHiddenOwnedProperties = true;
			this.clickedOwnedProperties = false;
		}
		else {
			this.notificationsService.info(
		          'Info',
		          'All portfolio owned displayed',
		        )
		}
	}

	clickSeeAllManagedProperties() {
		if (this.seeAllManagedProperties === true) {
			this.showHiddenManagedProperties = true;
			this.clickedManagedProperties = false;
		}
		else {
			this.notificationsService.info(
		          'Info',
		          'All portfolio managed displayed',
		        )
		}
	}

	clickSeeAllAppointedProperties() {
		if (this.seeAllAppointedProperties === true) {
			this.showHiddenAppointedProperties = true;
			this.clickedAppointedProperties = false;
		}
		else {
			this.notificationsService.info(
		          'Info',
		          'All portfolio appointed displayed',
		        )
		}
	}

	showPassword(){
		this.displaypassword = true;
	}
	
	passwordMatch(control:any) {
        let that = this;
        return {'passwordMatch': false};
    }

	emailUnique(control:any){
	    return new Promise(resolve => {
	      this.userService.getValid(control._value).subscribe(data => {
	          if (data.message == true && control._value != this.dataEmail){
	            resolve ({ 'invalidUniqueEmail': true });
	          }
	          else{
	            resolve(null);
	          }
	      });
	    })
	}

	usernameUnique(control:any){
	    return new Promise(resolve => {
	      this.userService.getValid(control._value).subscribe(data => {
	          if (data.message == true && control._value != this.dataUsername){
	            resolve ({ 'invalidUniqueUsername': true });
	          }
	          else{
	            resolve(null);
	          }
	      });
	    })
	}

	hidePassword(){
		this.displaypassword = false;
	}

	updateSetting(){
		this.userService.updateData(this.dataProfile._id, this.dataProfile)
		.then(
			data => {
				this.notificationsService.success(
		          'Success',
		          'Account Updated',
		        )
		        this.router.navigate(['']);			
			},
			error => {
				this.notificationsService.error(
		          'Error',
		          'Account Not Updated',
		        )			 
			}
		);
	 }

	updatePassword(){
		this.userService.updateData(this.dataProfile._id, this.data)
		.then(
			data => {
				this.notificationsService.success(
		          'Success',
		          'Password successfully updated',
		        )
		        this.displaypassword = false;		
			},
			error => {
				console.log(error._body);
				this.notificationsService.error(
		          'Error',
		          'Incorrect password',
		        )			 
			}
		);
	}

	search(){
		this.userService.searchUser(this.model.name)
		.takeWhile(()=> this.subscription)
		.subscribe(
			data => {
				this.model.manager = data._id
			},
			error => {
				console.log(error)				 
			}
		);
	}

	getProperty(){
		this.userService.getProperty()
		.takeWhile(()=> this.subscription)
		.subscribe(properties => {
			this.properties = properties.filter((data:any) => data.status != 'draft' && data.manager == null);
			for (let z = 0; z < this.properties.length; ++z) {
				this.classes.push("list-image")
			}
		});
	}

	clicked(id:any , no:any){
		var i = this.model.property.indexOf(id);
		if(i == -1) {
			this.model.property.push(id)
			let a = this.model.property.indexOf(id);
			this.classes[no] = "list-image property-selected"
		}else{
			this.model.property.splice(i, 1);
			this.classes[no] = "list-image"
		}
		
	}

	confirm(){
		this.managerService.create(this.model)
		.then(
			response => {
				this.modalNotif.show()		
			},
			error => {
				console.log(error)				 
			}
		);
	}

	getId(id:string){
		this.id = id
	}

	view(id:string){
		this.router.navigate(['property/' + id + '/view'])
	}

	accept(){
		this.managerService.accept(this.id)
		.then(
			data => {
				this.notificationsService.success(
		        	'Success',
		            'Appointment of Manager accepted',
		        )		
		        this.ngOnInit()	
			},
			error => {
				console.log(error)
				this.notificationsService.error(
		        	'Error',
		            'Error accept appointment of manager',
		        )				 
			}
		);
	}

	 handleFileSuccess(event:any): void {
        // this.myForm.controls['lists']).controls[no]['controls']['items'].controls[no]['controls']['attachment'].value
        this.picture = event.file.uploadedId;
        console.log(this.picture)
        this.add = true;
    }

    handleFileFailure(event:any): void {
    }

    handleFileSending(event:any): void {
    }

    handleFileAdded(event:any): void {
    }

    handleFileRemoved(event:any): void {
        this.attachementService.delete(this.model.picture)
        .then(
            response => {
                this.picture = null
            },
            error => {
                console.log(error);
            }
        );
    }

    upload(dropzone:any){
    	this.user.picture = this.picture
        this.userService.updateData(this.dataProfile._id, this.user)
        .then(
			(response:any) => {
                this.notificationsService.success('Success','Upload Success.')
        		dropzone._dropzone.removeAllFiles(true);
                this.ngOnInit()
                this.navbarComponent.ngOnInit()
            },
            (error:any) => {
            	this.notificationsService.error('Error','Upload failed.')
            }
        );
    }

    setFocus(id:any){
    	setTimeout(function(){id.focus();}, 100);
    }

    ngOnDestroy() {
    	this.subscription = false;
    }
}
