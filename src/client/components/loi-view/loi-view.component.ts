import { Component, ViewChild, OnInit } from '@angular/core';
import { SignatureComponent } from '../signature.component'
import { AgreementService } from '../../services/agreement.service';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Agreement } from '../../models/agreement';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { StorageService } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import { ChatService } from '../../services/chat.service';
import { NavbarComponent } from '../navbar/navbar.component'
import "rxjs/add/operator/takeWhile";

@Component({
    selector: 'loi-view',
    templateUrl: './loi-view.html',
    providers: [SignatureComponent],
})

export class LoiViewComponent implements OnInit {
    subscription: boolean = true;
    id: string;
    agreement: any;
    date: any;
    model: any = {};
    dataReport: any = [];
    loi: any;
    report: any;
    userId: any;
    agreementId: any;
    landlordId: any;
    name: any;
    isReject: boolean;
    public ddp: MyDDPClient;
    public loading: boolean = false;
    public dreamtalkReady: boolean = false;
    @ViewChild(SignatureComponent) signatureComponent: SignatureComponent;

    constructor(
        private agreementService: AgreementService,
        private userService: UserService,
        private router: Router,
        private route: ActivatedRoute,
        private notificationsService: NotificationsService,
        private sharedService: SharedService,
        private storageService: StorageService,
        private chatService: ChatService,
        private navbarComponent: NavbarComponent,
        ) { }

    ngOnInit(): any {
        this.storageService.get('dreamtalk.ready').takeWhile(()=> this.subscription).subscribe((ready: boolean) => { this.dreamtalkReady = ready });
        this.sharedService.ddp$.takeWhile(()=> this.subscription).subscribe((ddp: MyDDPClient) => {
            this.ddp = ddp;
        }, (error: any) => {
            console.log('Error ddp', error);
        })
        this.date = Date.now();

        this.route.params.takeWhile(()=> this.subscription).subscribe(params => {
            this.id = params['id'];
        });
        this.userService.getByToken().takeWhile(()=> this.subscription).subscribe(user => {
            this.userId = user._id;
        });
        this.agreementService.getLoiReport(this.id).takeWhile(()=> this.subscription).subscribe(report => {
            this.report = report;
        });
        this.agreementService.getById(this.id).takeWhile(()=> this.subscription).subscribe(agreement => {
            this.agreement = agreement;
        });
        this.agreementService.getLOI(this.id).takeWhile(()=> this.subscription).subscribe(loi => {
            this.loi = loi;
        });
        // this.viewPayment = true;
    }

    uploadPayment() {
        if(!this.loi.confirmation.tenant.sign) {
            this.model.sign = this.signatureComponent.data;
            this.model.status = 'tenant';
            this.model.type = 'letter_of_intent';
            if(this.model.sign) {
                this.agreementService.sign(this.id, this.model)
                .then(response => {
                    this.router.navigate(['/loi/' + this.id + '/reupload']);
                }).catch(error => {
                    // todo:
                    console.log(error);
                });
            } else {
                this.notificationsService.error('','Please fill the sign')
            }
        } else {
            this.router.navigate(['/loi/' + this.id + '/reupload']);
        }
    }

    send() {
        this.agreementService.sendLOI(this.id)
        .then(response => {
            // todo: toast notification
            this.router.navigate(['/dashboard/letter-of-intent']);
        }).catch(error => {
            console.log(error);
        });
    }

    gotoConfirm() {
        this.router.navigate(['/loi/' + this.id + '/confirm']);
    }

    reject() {
        this.loading = true
        if(this.model.reason) {
            this.agreementService.rejectLOI(this.id, this.model)
            .then(response => {
                this.notificationsService.success('Success', 'LOI Rejected');
                this.router.navigate(['/dashboard/letter-of-intent']);;
                if (localStorage.getItem('authToken')) {
                    if (this.dreamtalkReady) {
                        if (this.ddp) {
                            if (!this.agreement.room_id) {
                                let roomParams = { property_id: this.agreement.property._id };
                                this.chatService.createRoom(roomParams)
                                .then((res: any) => {
                                    let roomId = res && res.room_id ? res.room_id : undefined;
                                    let chatParams: any = {
                                        roomId: roomId,
                                        message: '-',
                                        extra: { 
                                            ref : 'loi_reject',
                                            ref_id: this.id,
                                        }
                                      };
                                    this.ddp.sendChat(chatParams).then((res: any) => {
                                        this.loading = false
                                        // this.notificationsService.success('Success', 'Message has been delivered.');
                                    }).catch((error: any) => {
                                        console.log(error);
                                        // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                    })
                                }).catch((err: any) => {
                                    console.log(err);
                                    // this.notificationsService.error('Error', 'Failed to create room');
                                })
                            }else{
                                let chatParams: any = {
                                    roomId: this.agreement.room_id.room_id,
                                    message: '-',
                                    extra: { 
                                        ref : 'loi_reject',
                                        ref_id: this.id,
                                    }
                                  };
                                this.ddp.sendChat(chatParams).then((res: any) => {
                                    this.loading = false
                                    // this.notificationsService.success('Success', 'Message has been delivered.');
                                }).catch((error: any) => {
                                    console.log(error);
                                    this.ddp.sendChat(chatParams).then((res: any) => {
                                        this.loading = false
                                        // this.notificationsService.success('Success', 'Message has been delivered.');
                                    }).catch((error: any) => {
                                        console.log(error);
                                        // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                    })
                                    // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                })
                            }
                        } else {
                            // no ddp
                        }
                    } else {
                        // dreamtalk not ready
                        // this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
                    }
                } else {
                    this.navbarComponent.modalLogin.show()
                }
            }).catch(error => {
                console.log(error)
            });
        } else {
            this.notificationsService.error('','Please write the remark')
        }
    }

    printDownload() {
        this.model.type = 'loi';
        this.agreementService.print(this.id, this.model)
        .then(response => {
            console.log(response,'response')
        }).catch(error => {
            console.log(error,'error')
        });
    }

    back() {
        this.router.navigate(['/dashboard/letter-of-intent']);
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
