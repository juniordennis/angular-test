 import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AgreementService } from '../../services/agreement.service';
import { UserService } from '../../services/user.service';
import { AttachmentService } from '../../services/attachment.service';
import { BankService } from '../../services/bank.service';
import { Router, ActivatedRoute } from '@angular/router';
import { url } from '../../../global';
import { NotificationsService } from 'angular2-notifications';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { StorageService } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import { ChatService } from '../../services/chat.service';
import { NavbarComponent } from '../navbar/navbar.component'
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'loi-initiate',
  templateUrl: './loi-initiate.html',
})

export class LoiInitiateComponent implements OnInit {
	subscription: boolean = true;
	isFirst: boolean = false;
	isSecond: boolean = false;
	isValid: boolean;
	isValid2: boolean;
	id: string;
	agreement: any
	user: any;
	banks: any[] = [];
	bank: any
	selectedBank: string;
	public types:Array<string> = ['NRIC', 'PASSPORT', 'FIN','UEN','Other'];
	selectedTypes: string;
	myForm: FormGroup;
	requirements: any[];
	elementFront: string[] = [];
    elementBack: string[] = [];
 	idFront: string[] = [];
    idBack: string[] = [];
    index: number = -1;
	header: any;
	link: any;
	front: any = null;
	back: any = null;
	bankId: any;
	model: any = {};
	data: any[] = []
	private value:any = {};
	public ddp: MyDDPClient;
	public loading: boolean = false;
	public dreamtalkReady: boolean = false;

	constructor(
		private formbuilder: FormBuilder,
		private router: Router,
		private route: ActivatedRoute,
		private agreementService: AgreementService,
		private userService: UserService,
		private attachementService: AttachmentService,
		private bankService: BankService,
		private notificationsService: NotificationsService,
		private storageService: StorageService,
		private sharedService: SharedService,
		private chatService: ChatService,
		private navbarComponent: NavbarComponent,
	) {}

	ngOnInit(): void{
		this.storageService.get('dreamtalk.ready').takeWhile(()=> this.subscription).subscribe((ready: boolean) => { this.dreamtalkReady = ready });
		this.route.params.takeWhile(()=> this.subscription).subscribe(params => {
			this.id = params['id'];
		});
		this.sharedService.ddp$.takeWhile(()=> this.subscription).subscribe((ddp: MyDDPClient) => {
			this.ddp = ddp;
		}, (error: any) => {
			console.log('Error ddp', error);
		})
		this.bankService.getAll()
		.takeWhile(()=> this.subscription)
		.subscribe(bank => { 
			for (let a = 0; a < bank.length; ++a) {
				this.banks.push(bank[a].name)
			}
		});
		this.agreementService.getById(this.id).takeWhile(()=> this.subscription).subscribe(data => {this.agreement = data; console.log(this.agreement)})
		this.link = url+'attachments';
		let currentUser = JSON.parse(localStorage.getItem('authToken'));
		if (currentUser && currentUser.token) {
			this.header = {'Authorization': 'Bearer ' + currentUser.token}
		}
		this.isFirst = true;
		this.isSecond = false;
		this.myForm = this.formbuilder.group({
			monthly_rental: ['', Validators.required],
			term_lease: ['', Validators.required],
			date_commencement: ['', Validators.required],
			requirements: this.formbuilder.array([this.initOtherRequirement()]),
			tenant: this.formbuilder.group({
				name: ['', Validators.required],
				identification_type: ['', Validators.required],
				identification_number: ['', Validators.required],
				identification_proof: this.formbuilder.group({
					front: ['', Validators.required],
					back: [undefined],
				}),
				bank_account: this.formbuilder.group({
					bank: ['', Validators.required],
					name: ['', Validators.required],
					no: ['', Validators.required],
				}),
			}),
			occupiers: this.formbuilder.array([]),
		});
		this.getUser();
	}
 
  	public selected(value:any):void {
    	console.log('Selected value is: ', value);
  	}
 
  	public removed(value:any):void {
    	console.log('Removed value is: ', value);
  	}
 
  	public typed(value:any):void {
    	console.log('New search input: ', value);
  	}
 
  	public refreshValue(value:any):void {
    	this.value = value;
  	}

	getUser() {
		this.userService.getByToken()
		.takeWhile(()=> this.subscription)
		.subscribe(user => {
			this.user = user;
			if(this.user.tenant.data) {
				if(this.user.tenant.data.name && this.user.tenant.data.identification_type && this.user.tenant.data.identification_number && this.user.tenant.data.identification_proof.front && this.user.tenant.data.identification_proof.back) {
					this.data.push(this.user.tenant.data.identification_type)
					this.myForm.patchValue({
						tenant: {
							name: this.user.tenant.data.name,
							identification_type: this.data,
							identification_number: this.user.tenant.data.identification_number,
							identification_proof: {
								front: this.user.tenant.data.identification_proof.front,
								back: this.user.tenant.data.identification_proof.back
							}
						}
					});
				}
				if(this.user.tenant.data.name && this.user.tenant.data.identification_type && this.user.tenant.data.identification_number && this.user.tenant.data.identification_proof.front && !this.user.tenant.data.identification_proof.back) {
					this.data.push(this.user.tenant.data.identification_type)
					this.myForm.patchValue({
						tenant: {
							name: this.user.tenant.data.name,
							identification_type: this.data,
							identification_number: this.user.tenant.data.identification_number,
							identification_proof: {
								front: this.user.tenant.data.identification_proof.front
							}
						}
					});
				}
			}
		})
		
	}

	initOtherRequirement() {
		return this.formbuilder.group({
			require: [null],
		});
	}

	addOtherRequirement() {
		const control = <FormArray>this.myForm.controls['requirements'];
		control.push(this.initOtherRequirement());
	}

	removeOtherRequirement(i: number) {
		const control = <FormArray>this.myForm.controls['requirements'];
		control.removeAt(i);
	}

	initOccupier() {
		return this.formbuilder.group({
			name: ['', Validators.required],
			identification_type: ['', Validators.required],
			identification_number: ['', Validators.required],
			identification_proof: this.formbuilder.group({
				front: ['', Validators.required],
				back: [],
			}),
		});
	}

	addOccupier() {
		const control = <FormArray>this.myForm.controls['occupiers'];
		control.push(this.initOccupier());
		var index = control.length-1
        this.idFront.push('ownersFront'+index);
        this.idBack.push('ownersBack'+index);
        setTimeout(() => this.elementFront.push('#ownersFront'+index), 100)
        setTimeout(() => this.elementBack.push('#ownersBack'+index), 100)
	}

	removeOccupier(i: number) {
		const control = <FormArray>this.myForm.controls['occupiers'];
		control.removeAt(i);
		this.idFront.splice(i, 1);
        this.idBack.splice(i, 1);
        this.elementFront.splice(i, 1);
        this.elementBack.splice(i, 1);
	}

	number(event: any) {
        const pattern = /[0-9\+\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    text(event: any) {
        const pattern = /[a-z\A-Z\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

	createLOI(model:any, valid:any) {
		this.isValid2 = true;
		if(valid) {
			this.bankService.getAll()
			.takeWhile(()=> this.subscription)
			.subscribe(bank => { 
				this.bankId = bank.find((data:any) => data.name == model.tenant.bank_account.bank[0].id);
				this.requirements = [];
				if(model.requirements.length > 0) {
					for (let a = 0; a < model.requirements.length; ++a) {
						if (model.requirements[a].require != null) {
							this.requirements.push(model.requirements[a].require)
						}else{
							this.requirements.push(null)
						}
					}
					model.requirements = this.requirements
				}
				model.tenant.identification_type = model.tenant.identification_type[0].id
				model.tenant.bank_account.bank = this.bankId._id;
				if (model.tenant.identification_proof.back == undefined) {
					delete model.tenant.identification_proof['back']
				}
				this.agreementService.createLOI(this.id, model)
				.then(
					response => {
						this.router.navigate([ '/loi/'+ this.id +'/view']);
						this.notificationsService.success('Success','LOI initiated')
						if (localStorage.getItem('authToken')) {
							if (this.dreamtalkReady) {
								if (this.ddp) {
									if (!this.agreement.room_id) {
										let roomParams = { property_id: this.agreement.property._id };
										this.loading = true
										this.chatService.createRoom(roomParams)
										.then((res: any) => {
											let roomId = res && res.room_id ? res.room_id : undefined;
											let chatParams: any = {
								                roomId: roomId,
								                message: '-',
								                extra: { 
												    ref : 'loi_create',
												    ref_id: this.id,
												}
					              			};
											this.ddp.sendChat(chatParams).then((res: any) => {
												this.loading = false
												// this.notificationsService.success('Success', 'Message has been delivered.');
											}).catch((error: any) => {
												console.log(error);
												// this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
											})
										}).catch((err: any) => {
											console.log(err);
											// this.notificationsService.error('Error', 'Failed to create room');
										})
									}else{
										this.loading = true
										let chatParams: any = {
							                roomId: this.agreement.room_id.room_id,
							                message: '-',
							                extra: { 
											    ref : 'loi_create',
											    ref_id: this.id,
											}
				              			};
										this.ddp.sendChat(chatParams).then((res: any) => {
											console.log(res)
											this.loading = false
											// this.router.navigate([ '/loi/'+ this.id +'/view']);
											// this.notificationsService.success('Success', 'Message has been delivered.');
										}).catch((error: any) => {
											console.log(error);
											this.ddp.sendChat(chatParams).then((res: any) => {
												this.loading = false
												// this.router.navigate([ '/loi/'+ this.id +'/view']);
												// this.notificationsService.success('Success', 'Message has been delivered.');
											}).catch((error: any) => {
												console.log(error);
												// this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
											})
											// this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
										})
									}
								} else {
									// no ddp
								}
							} else {
								// dreamtalk not ready
								// this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
							}
						} else {
							this.navbarComponent.modalLogin.show()
						}
            			this.router.navigate(['/loi/' + this.id + '/view']);
					},
					error => {
						this.notificationsService.error(
							'Error',
							'Initiate LOI failed',
							)
						console.log(error)
					}
				);
			});
		}
	}

	handleFileSuccess(event:any, type:any): void {
		if(type == "front") {
			(<FormArray>this.myForm.controls['tenant']).controls['identification_proof']['controls']['front'].patchValue(event.file.uploadedId)
		}else{
			(<FormArray>this.myForm.controls['tenant']).controls['identification_proof']['controls']['back'].patchValue(event.file.uploadedId)
		}
	}

	handleFileFailure(event:any, type:any): void {
		console.log('fail', event, type);
	}

	handleFileSending(event:any, type:any): void {
		console.log('send', event, type);
	}

	handleFileAdded(event:any): void {
		console.log('add', event);
	}

	handleFileRemoved(event:any, type:any): void {
		if(type == "front") {
			this.front = null
		}else{
			this.back = null
		}
		this.attachementService.delete(event.uploadedId)
		.then(
			response => {
				console.log(response);
			},
			error => {
				console.log(error);
			}
		);
	}

	handleFileSuccessOwners(event:any, type:any, index:number): void {
        if(type == "front") {
			(<FormArray>this.myForm.controls['occupiers']).controls[index]['controls']['identification_proof']['controls']['front'].patchValue(event.file.uploadedId)
        }else if(type == "back") {
           (<FormArray>this.myForm.controls['occupiers']).controls[index]['controls']['identification_proof']['controls']['back'].patchValue(event.file.uploadedId)
        }
    }
    handleFileFailureOwners(event:any, type:any, index:number): void {
    }
    handleFileSendingOwners(event:any, type:any, index:number): void {
    }
    handleFileAddedOwners(event:any): void {
    }
    handleFileRemovedOwners(id:string, type:any, index:number): void {
        this.attachementService.delete(id)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
        if(type == "front") {
        	(<FormArray>this.myForm.controls['occupiers']).controls[index]['controls']['identification_proof']['controls']['front'].patchValue(null)
        }else if(type == "back") {
            (<FormArray>this.myForm.controls['occupiers']).controls[index]['controls']['identification_proof']['controls']['back'].patchValue(null)
        }
    }

	next(monthly_rental:boolean,
		term_lease:boolean,
		date_commencement:boolean,
		bank:boolean,
		name:boolean,
		no:boolean) {
		console.log(monthly_rental,term_lease,date_commencement,bank,name,no)
		this.isValid = true;
		if(monthly_rental && term_lease && date_commencement && bank && name && no) {
			this.isFirst = false
			this.isSecond = true
		}else{
			this.isFirst = true;
			this.isSecond = false;
		}
	}

	ngOnDestroy() {
		this.subscription = false;
	}
}
