import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatePipe } from '@angular/common';
import { AppComponent } from '../../app/app.component';
import { FormBuilder, FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { ValidationService } from '../../services/validation.service';
import { UserService } from '../../services/user.service';
import { ChatService } from '../../services/chat.service';
import { NotificationUserService } from '../../services/notification-user.service';
import { StorageService } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import { FacebookService, FacebookLoginResponse, FacebookInitParams} from 'ng2-facebook-sdk';
import { ModalDirective } from 'ng2-bootstrap';
import { MyDDPClient } from '../../my-ddp-client';
import { NotificationsService } from 'angular2-notifications';
import { FocusDirective } from '../../autofocus';
import { MapsAPILoader} from 'angular2-google-maps/core';
import { NouisliderComponent } from 'ng2-nouislider';
import { Observable } from 'rxjs/Observable';
// import { Ng2SliderComponent } from 'ng2-slider-component';
import "rxjs/add/operator/takeWhile";
import * as $ from 'jquery';

declare var google: any;

@Component({
	selector: 'my-navbar',
	templateUrl: './navbar.html',
	styles:[`.pointer{cursor:pointer}`],
	
	providers: [AuthenticationService, FacebookService],
})

export class NavbarComponent implements OnInit {
	@ViewChild('modalLogin') public modalLogin: ModalDirective;
	@ViewChild('modalConfirmation') public modalConfirmation: ModalDirective;
	@ViewChild('modalSignup') public modalSignup: ModalDirective;
	subscription: boolean = true;
	private user: any;
	public errorMsg = '';
	public test : string = 'any';
	public loginErrorMessage: string;
	public searchNavbarControl: FormControl;
	@ViewChild("searchNavbar")
	public searcNavbarhElementRef: ElementRef;
	navbarFilter : boolean =false;
	ddpClient: MyDDPClient;
	model: any = {};
	register: any = {};
	registerData: any = {};
	confirmation: any = {};
	error = '';
	authToken: any;
	myFormLogin: FormGroup;
	myFormRegister: FormGroup;
	myFormConfirmation: FormGroup;
	myFormNavbar: FormGroup;
	@ViewChild('someKeyboardSlider2') someKeyboardSlider2: NouisliderComponent;
	public someKeyboard2: number[] = [1, 3];
	id: any;
	userID: any;
	userData: any;
	accessToken: any;
	token: any;
	dtToken: any;
	data:any = {};
	ready: boolean = false;
	connecting: boolean = false;
	loginSpinner : boolean = false;
	registerSpinner : boolean = false;
	usernameError: boolean = false;
	usernameErrorMessage: string = 'any';
	emailError: boolean = false;
	emailErrorMessage: string = 'any';
	phoneError: boolean = false;
	phoneErrorMessage: string = 'any';
	passwordError: boolean = false;
	passwordErrorMessage: string = 'any';
	passwordConfirmationError: boolean = false;
	passwordConfirmationErrorMessage: string = 'any';
	public numberBedroom: any = ['all'];
	public numberBathroom: any = ['all'];
	public somevalue : any = [ 0, 50000 ];
	private someKeyboard2EventHandler = (e: KeyboardEvent) => {

		// determine which handle triggered the event
		let index = parseInt((<HTMLElement>e.target).getAttribute('data-handle'));

		let multiplier: number = 0;
		let stepSize = 0.1;

		switch ( e.which ) {
			case 40:  // ArrowDown
			case 37:  // ArrowLeft
				multiplier = -2;
				e.preventDefault();
				break;

			case 38:  // ArrowUp
			case 39:  // ArrowRight
				multiplier = 3;
				e.preventDefault();
				break;

			default:
				break;
		}

		let delta = multiplier * stepSize;
		let newValue = [].concat(this.someKeyboard2);
		newValue[index] += delta;
		this.someKeyboard2 = newValue;
	};
	public someKeyboardConfig2: any = {
		behaviour: 'drag',
		connect: true,
		start: [0, 50000],
		keyboard: true,  // same as [keyboard]="true"
		cells: new FormControl([0,50000]),
		range: {
			min: 0,
			max: 50000
		},
		pips: {
			mode: 'values',
			density: 10,
			values: [0, 50000]
		},
		onKeydown: this.someKeyboard2EventHandler
	};
	notifications: any = [];
	notificationsToShow: any = [];
	unclickedNotificationTotal: number = 0;
	lastNotificationIndex: number = 5;
	timerSubscription: any;
	notificationsSubscription: any;
	showValid: boolean;
	address: string;
	availableDate: string;
	priceRange: string;
	dateRent: string = 'all';
	oldPhone: string = '';
	latlng: any;
	constructor(
		private router: Router,
		private fb: FacebookService,
		private appComponent: AppComponent,
		private authService: AuthenticationService,
		private formbuilder: FormBuilder,
		// private ddpClient: MyDDPClient,
		private userService: UserService,
		private chatService: ChatService,
		private storageService: StorageService,
		private sharedService: SharedService,
		private route: ActivatedRoute,
		private ValidationService: ValidationService,
		private mapsapiloader: MapsAPILoader,
		private notificationsService: NotificationsService,
		private notificationUserService: NotificationUserService,
	) {
		let fbParams: FacebookInitParams = {
			appId: '1875682209317089',
			xfbml: true,
			version: 'v2.8'
		};
		this.fb.init(fbParams);
	}

	ngOnInit(): void {
		$("#phone").intlTelInput({
			autoPlaceholder: true,
			autoHideDialCode: false,
			initialCountry: "sg",
			utilsScript: "public/assets/intl-tel-input/js/utils.js"
		});
		$("#phoneMobile").intlTelInput({
			autoPlaceholder: true,
			autoHideDialCode: false,
			initialCountry: "sg",
			utilsScript: "public/assets/intl-tel-input/js/utils.js"
		});
	 
		this.ddpClient = new MyDDPClient();
		this.ddpClient.initCacheStorage(this.storageService);
		this.ddpClient.connect();
		this.sharedService.updateDdp(this.ddpClient);
		this.storageService.set('dreamtalk.ready', this.ready);
		this.connecting = true;
		let self = this;
		this.getTokenAndLogin();
		this.myFormLogin = this.formbuilder.group({
			username: ['', Validators.required],
			password: ['', Validators.required]
		});

		this.myFormRegister = this.formbuilder.group({
			username: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(70), Validators.pattern('^[a-zA-Z0-9_.-]*$')])],
			email: ['', [Validators.required, ValidationService.emailValidator]],
			phone: ['', Validators.required],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
			passwordConfirmation: ['', [Validators.required, Validators.minLength(6)]]
		});
		this.myFormConfirmation = this.formbuilder.group({
			username: ['', Validators.required],
			email: ['', Validators.required],
			phone: ['', Validators.required],
		});
		this.myFormNavbar = this.formbuilder.group({
			range: [''],
		});
		this.authService.getAuthToken().takeWhile(()=> this.subscription).subscribe((token:string) => {
			if (token !== null && token !== undefined) {
				this.userService.getByToken().takeWhile(()=> this.subscription).subscribe((user:any) => {
					this.user = user;
					this.subscribeNotification(user._id);
					this.getNotifications();
				}, (error:any) => {
					this.user = undefined;
				});
			} else {
				this.user = undefined;
			}
		});
		this.searchNavbarControl = new FormControl()
		this.mapsapiloader.load().then(() => {
			let autocomplete = new google.maps.places.Autocomplete(this.searcNavbarhElementRef.nativeElement, { types: ["address"], componentRestrictions: { country: 'SG' } });
			google.maps.event.addListener(autocomplete, 'place_changed', () => {
				let place = autocomplete.getPlace();
				this.address = place.name;
				let lat = place.geometry.location.lat();
				let lng = place.geometry.location.lng();
				this.latlng = lat+','+lng;
				if (place) {
					 this.navbarFilter = true;
				}
			});
		});

		// jQuery("#phone").intlTelInput();
	}

	showDropdown() {
		if (this.navbarFilter == true) {
			this.navbarFilter = false;
		}
	}
	changeRequest() {
		this.router.navigate(['/property/browse'], { queryParams: { latlng: this.latlng, pricemin: this.priceRange[0], pricemax: this.priceRange[1], bedroom: this.numberBedroom, bathroom: this.numberBathroom, available: this.dateRent, sizemin: 'all', sizemax: 'all', location: this.address, radius: 'all' } });
	}
	handleClickAvailable() {
		var datePipe = new DatePipe('en-SG');
        this.dateRent = datePipe.transform(this.availableDate, 'yyyy-MM-dd');
	}

	get employeeDetailLink() {
		return ['/property/browse', 'all', 'all', 'all', 'all', 'all', 'all', 'all', 'all', 'all', 'all'];
	}

	checkRoute(url:string) {
		if (url == '/') {
			return false;
		}
		else if (url.includes('/browse') == true) {
			return false;
		}
		else {
			return true;
		}
	}

	getTokenAndLogin() {
		this.ddpClient.getStatus().takeWhile(()=> this.subscription).subscribe((status: any) => {
			if (status == undefined) {
				this.connecting = false
			}else{
				this.connecting = true
			}
			// console.log('status', status);
			if (status && status.isConnected && localStorage.getItem('authToken')) {
				this.connecting = true
				this.ddpClient.subscribePublications();
				// console.log('logging in dreamtalk');
				if (localStorage.getItem('Dreamtalk.token')) {
					this.loginDreamtalk(localStorage.getItem('Dreamtalk.token'));
				} else {
					this.userService.requestToken()
					.takeWhile(()=> this.subscription)
					.subscribe(res => {
						this.connecting = true
						this.loginDreamtalk(res.token);
					})
				}
			}
		}, (error: any) => {
			this.ready = false;
			this.connecting = false;
			this.storageService.set('dreamtalk.ready', this.ready);
			console.log('2', error);
		});
		// jQuery("#phone").intlTelInput();
		this.showValid = true;
	}

	subscribeNotification(userId: string) {
		this.notificationUserService.getOwn(userId).takeWhile(() => this.subscription).subscribe((data: any) => {
			this.getNotifications();
		});
	}

	emailUnique(control:any){
		return new Promise(resolve => {
			this.userService.getValid(control._value).takeWhile(()=> this.subscription).subscribe(data => {
					if (data.message == true){
						resolve ({ 'invalidUniqueEmail': true });
					}
					else{
						resolve(null);
					}
			});
		})
	}

	phoneUnique(control:any){
		return new Promise(resolve => {
			this.userService.getValid(control._value).takeWhile(()=> this.subscription).subscribe(data => {
					if (data.message == true){
						resolve ({ 'invalidUniquePhone': true });
					}
					else{
						resolve(null);
					}
			});
		})
	}

	usernameUnique(control:any){
		return new Promise(resolve => {
			this.userService.getValid(control._value).takeWhile(()=> this.subscription).subscribe(data => {
					if (data.message == true){
						resolve ({ 'invalidUniqueUsername': true });
					}
					else{
						resolve(null);
					}
			});
		})
	}

	loginDreamtalk(token: string) {
		this.dtToken = token
		this.ddpClient.login(token).then((res: any) => {
			localStorage.setItem('Dreamtalk.id', res.id);
			localStorage.setItem('Dreamtalk.token', res.token);
			localStorage.setItem('Dreamtalk.tokenExpires', res.tokenExpires);
			this.ready = true;
			this.storageService.set('dreamtalk.ready', this.ready);
			// this.ddpClient.getStatus().subscribe((status: any) => {
			//   if (status && status.isConnected && localStorage.getItem('authToken')) {
			//     this.ready = true;
			//   }
			// })
		}).catch((err: any) => {
			if (err.error == 403 && err.reason == "You've been logged out by the server. Please log in again.") {
				localStorage.removeItem('Dreamtalk.id');
				localStorage.removeItem('Dreamtalk.token');
				localStorage.removeItem('Dreamtalk.tokenExpires');
				this.connecting = false
				this.getTokenAndLogin();
			} else if (err.error == 'too-many-requests') {
				this.connecting = true
				setTimeout(this.loginDreamtalk(this.dtToken), 10000);
			} else {
				this.connecting = false
				console.log(err);
			}
			this.ready = false;
			this.storageService.set('dreamtalk.ready', this.ready);
		})
	}

	someFunction(): void {
		this.fb.login().then((response: FacebookLoginResponse) => {
			this.id = response.authResponse.userID;
			this.accessToken = response.authResponse.accessToken;
			this.authService.loginFacebook(this.id, this.accessToken).takeWhile(()=> this.subscription).subscribe(data => {
				let idtemp = JSON.parse(localStorage.getItem('authId'));
				let datatemp = JSON.parse(localStorage.getItem('authData'));
				this.userID = idtemp.token;
				this.userData = datatemp.token;
				localStorage.removeItem('authId');
				localStorage.removeItem('authData');
				this.modalLogin.hide();
				if (this.userData == false) {
					this.modalConfirmation.show();
					this.clearModal();
				}
			});
		}, (error: any) => { console.error(error) });
	}

	goToForget(){
		this.modalLogin.hide();
		// this.router.navigate(['/foo'], { queryParams: { bar: 1, baz: 2 }});
		this.router.navigate(['/forget-password-email']);
	}
	goToList(){
		this.router.navigate(['/property/browse'], { queryParams: { latlng: 'all', pricemin: 'all', pricemax : 'all', bedroom:'all', bathroom:'all', available:'all', sizemin:'all', sizemax:'all', location:'all', radius:'all' }});
	}
	goCreateListing(){
		var token = localStorage.getItem('authToken');
		if(token) {
			this.router.navigate(['/property/new']);
		}
		else{
			this.modalLogin.show();
		}
	}

	createRoom(){
		this.data.property_id = "58db6ec24b67841df0d7443f";
		this.chatService.createRoom(this.data)
		.then(
			response => {
				console.log(response)
				this.notificationsService.success(
					'Success',
					'Create room success',
				)
				this.router.navigate([ '/chat']);
			},
			error => {
				console.log(error)
			}
		);
	}

	login() {
		this.loginSpinner = true;
		this.authService.login(this.model.username, this.model.password).takeWhile(()=> this.subscription).subscribe((data:any) => {
			this.loginSpinner = false;
			this.modalLogin.hide();
			this.userService.requestToken()
			.takeWhile(()=> this.subscription)
				.subscribe(token => {
					this.loginDreamtalk(token.token);
					this.connecting = true
				})
			this.getNotifications();
		}, (err:any) => {
			this.loginSpinner = false;
			this.loginErrorMessage = err;
		});
		this.showValid = true;
	}

	logout() {
		this.authService.logout();
		this.ddpClient.logout();
		this.showValid = false;
		this.router.navigate(['']);
		this.lastNotificationIndex =5;
	}

	changeValue(event: any, name: string) {
		if (name == 'phone') {
			let value = event.target.value;
			let pattern = /^(\+[0-9]{1,5})?([1-9][0-9]{9})?[0-9]*$/;
			let validate = pattern.test(value);
			if (validate == false) {
				this.phoneError = true;
				this.phoneErrorMessage = 'Phone can contain letters (a-z), numbers (0-9), dashes (-), underscores (_)';
			}
			else {
				let phone = $("#phone").intlTelInput("getNumber");
				this.userService.getValid(phone).takeWhile(()=> this.subscription).subscribe((data:any) => {
					if (data.message == true) {
						this.phoneError = true;
						this.phoneErrorMessage = 'The specified phone number is already in use.';
					}
					else {
						this.phoneError = false;
					}
				});
			}
		}
		else {
			this.userService.getValid(event.target.value).takeWhile(()=> this.subscription).subscribe((data:any) => {
				if (data.message == true) {
					if (name == 'username') {
						this.usernameError = true;
						this.usernameErrorMessage = 'The specified username is already in use.';
					}
					if (name == 'email') {
						this.emailError = true;
						this.emailErrorMessage = 'The specified email address is already in use.';
					}
				}
				else {
					if (name == 'username') {
						this.usernameError = false;
					}
					if (name == 'email') {
						this.emailError = false;
					}
				}
			});
		}
	}

	validatePassword(event: any) {
		if (event.target.value.length < 6) {
			this.passwordConfirmationError = true;
			this.passwordConfirmationErrorMessage = "Passwords at least 6 characters.";
		}
		else {
			if (event.target.value != this.register.password) {
				this.passwordConfirmationError = true;
				this.passwordConfirmationErrorMessage = "Passwords doesn't match";
			}
			else {
				this.passwordConfirmationError = false;
			}
		}
	}

	registerThis() {
		this.registerSpinner = true;
		if (this.register.password != this.register.passwordConfirmation) {
			this.registerSpinner = false;
			if (this.register.passwordConfirmation.length >= 6) {
				this.passwordConfirmationError = true;
				this.passwordConfirmationErrorMessage = "Passwords doesn't match";	
			}
			else {
				this.passwordConfirmationError = true;
				this.passwordConfirmationErrorMessage = "Passwords at least 6 characters.";
			}
		}
		else {
			this.registerData = this.register;
			let phone = $("#phone").intlTelInput("getNumber");
			this.registerData.phone = phone;
			this.authService.create(this.registerData).takeWhile(()=> this.subscription).subscribe(data => {
				let idtemp = JSON.parse(localStorage.getItem('authId'));
				this.userID = idtemp.token;
				localStorage.removeItem('authId');
				this.registerSpinner = false;
				this.modalSignup.hide();
				this.notificationsService.success(
						'Success',
						'Your account has been added',
					)
				this.router.navigate(['/verification/' + this.userID]);
			}, (error:any) => {
				this.registerSpinner = false;
				this.passwordConfirmationError = false;
				var lists = JSON.parse(error._body);
				if(lists.errors.username) {
					this.usernameError = true;
					this.usernameErrorMessage = lists.errors.username.message;
				}
				else {
					this.usernameError = false;
				}
				if(lists.errors.email) {
					this.emailError = true;
					this.emailErrorMessage = lists.errors.email.message;
				}
				else {
					this.emailError = false;
				}
				if(lists.errors.phone) {
					this.phoneError = true;
					this.phoneErrorMessage = lists.errors.phone.message;
				}
				else {
					this.phoneError = false;
				}
				if(lists.errors.password) {
					this.passwordError = true;
					this.passwordErrorMessage = lists.errors.password.message;
				}
				else {
					this.passwordError = false;
				}
			});
		}
	}

	confirmThis() {
		let phone = $("#phoneMobile").intlTelInput("getNumber");
		this.confirmation.phone = phone;
		this.userService.updateData(this.userID, this.confirmation).then((data:any) => {
			this.modalConfirmation.hide();
		}).catch((error:any) => {
			this.modalLogin.show();
		})
	}

	clearModal() {
		this.model = {};
		this.myFormRegister.reset();
		document.getElementById("username").focus();
	}

	setFocus(id:any){
    	setTimeout(function(){id.focus();}, 100);
    }

	handleClick(event: any, part: string, number: string) {
		if (part == 'bedroom') {
			if (event.target.checked) {
				if (number == 'all') {
					this.numberBedroom = [];
					this.numberBedroom.push(number);
				} else {
					if (this.numberBedroom.length == 1 && this.numberBedroom.indexOf('all') > -1) {
						this.numberBedroom = [];
					}
					this.numberBedroom.push(+number);
				}
			} else {
				this.numberBedroom.splice(this.numberBedroom.indexOf(+number), 1);
				if (this.numberBedroom.length == 0) {
					this.numberBedroom.push('all');
				}
			}
		} else if (part == "bathroom") {
			if (event.target.checked) {
				if (number == 'all') {
					this.numberBathroom = [];
					this.numberBathroom.push(number);
				} else {
					if (this.numberBathroom.length > 0 && this.numberBathroom.indexOf('all') > -1) {
						this.numberBathroom = [];
					}
					this.numberBathroom.push(+number);
				}
			} else {
				this.numberBathroom.splice(this.numberBathroom.indexOf(+number), 1);
				if (this.numberBathroom.length == 0) {
					this.numberBathroom.push('all');
				}
			}
		}
	}


	getNotifications(): void{
		this.notificationUserService.getByLimit(this.lastNotificationIndex).takeWhile(()=> this.subscription).subscribe((notifications:any)=>{
			this.notifications = notifications.data;
			this.unclickedNotificationTotal = notifications.total_unread;
			this.subscribeToData();
		});
	}

	gotopage(notification:any){
		var link = this.notificationUserService.generateLink(notification);
		this.router.navigate([link]);
	}

	loadNotificationLazy() {
		this.lastNotificationIndex = this.lastNotificationIndex+5;
	}

	private subscribeToData(): void {
		this.timerSubscription = Observable.timer(5000).first().takeWhile(()=> this.subscription).subscribe(() => this.getNotifications());
	}

	public ngOnDestroy(): void {
		this.subscription = false;
	}

	readAllNotification(){
		this.notificationUserService.read();		
	}
}
