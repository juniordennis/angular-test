import { Component, OnInit } from '@angular/core';
import { url } from '../../../global';
import { AttachmentService } from '../../services/attachment.service';
import { AgreementService } from '../../services/agreement.service';
import { NotificationsService } from 'angular2-notifications';
// import { SharedService } from '../../services/shared.service';
import { Router, ActivatedRoute } from '@angular/router';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'loi-reupload',
  templateUrl: './loi-reupload.html',
})

export class LoiReuploadComponent {
	subscription: boolean = true;
	id:string;
	model:any = {}
	header:any;
	link :any;
	loi :any;
	total :any;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private attachementService: AttachmentService,
		private agreementService: AgreementService,
		private notificationsService: NotificationsService,
		// private sharedService: SharedService
	) {}

	ngOnInit(): void{
		this.route.params
		.takeWhile(()=> this.subscription)
		.subscribe(params => {
			this.id = params['id'];
		});
    	// this.sharedService.emitChange({ showFooter: false, showHeader: true });
		this.link = url+'attachments';
		let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
        this.agreementService.getLOI(this.id)
        .takeWhile(()=> this.subscription)
        .subscribe(loi => {
        	this.loi = loi;
        	this.total = this.loi.gfd_amount+this.loi.sd_amount;
        });
	}

	handleFileSuccess(event:any): void {
		this.model.attachment = event.file.uploadedId
  	}

	handleFileFailure(event:any): void {
	    console.log('fail', event);
	}

	handleFileSending(event:any): void {
	    console.log('send', event);
	}

	handleFileAdded(event:any): void {
	    console.log('add', event);
	}

	handleFileRemoved(event:any, type:any): void {
		this.attachementService.delete(event.uploadedId)
        .then(
            response => {},
            error => {
                console.log(error);
            }
        );
	}

	savePayment() {
		if(this.model.attachment) {
			this.model.type = 'letter_of_intent';
			console.log(this.model);
			this.agreementService.payment(this.id, this.model)
			.then(
				response => {
					this.notificationsService.success(
						'Success',
						'Payment uploaded',
						)
					this.router.navigate([ '/loi/'+ this.id +'/view']);
				},
				error => {
					this.notificationsService.error(
						'Error',
						'Failed to upload payment',
						)
					console.log(error,'error')
				}
			);
		} else {
			this.notificationsService.error('', 'Please upload the payment first')
		}
    }

    back(){
    	this.router.navigate([ '/loi/'+ this.id +'/view']);
    }
	
    ngOnDestroy() {
    	this.subscription = false;
    }
}