import { Component, OnInit, OnDestroy, ViewChild, NgZone, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BlogService } from '../../services/blog.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PropertyService } from '../../services/property.service';
import { ListingService } from '../../services/listing.service';
import { SharedService } from '../../services/shared.service';
import { OdometerService } from '../../services/odometer.service';
import { FormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MapsAPILoader } from 'angular2-google-maps/core';
import "rxjs/add/operator/takeWhile";
import * as moment from 'moment';

import * as $ from 'jquery';

declare var google: any;

@Component({
    selector: 'home',
    templateUrl: './home.html',
    styles: [`
    .calendaraa
    {
        padding: 5px 10px 0px 10px;height: auto;line-height: 24px;background-color: #fff;
    }
    .slider {
        width: 100%;
        margin: 100px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide:focus {
      outline: none;
    }

    .slick-slide img {
      width: 220px;
    }

    .slick-prev:before,
    .slick-next:before {
        color: black;
    }
    `],
    
    providers: [ListingService, BlogService],
})

export class HomeComponent implements OnInit {
    subscription: boolean = true;
    blogs: any[];
    properties: any[];
    propertyOne: any;
    propertyTwo: any;
    propertyThree: any;
    listings: any[];
    public isCollapsed: boolean = false;
    public dt: Date = new Date();
    public minDate: Date = void 0;
    public formats: string[] = ['DD-MM-YYYY', 'YYYY/MM/DD', 'DD.MM.YYYY', 'shortDate'];
    public format: string = this.formats[0];
    public dateOptions: any = {
        formatYear: 'YY',
        startingDay: 1
    };
    @ViewChild("search")
    public searchElementRef: ElementRef;
    private opened: boolean = false;
    public prices: any = []
    public searchControl: FormControl;
    public numberBedroom: any = ['all'];
    public numberBathroom: any = ['all'];
    public pricemin: any = 'all';
    public pricemax: any = 'all';
    public date: any = 'all';
    public rangePriceMin: any = [];
    public rangePriceMax: any = [];
    public pricestat: string = 'pricemin';
    public streetName: string ='all';
    public odometer: number = 0;
    public availableDate: string;
    public latlng: any;

    constructor(
        private blogService: BlogService,
        private listingService: ListingService,
        private sharedService: SharedService,
        private propertyService: PropertyService,
        private mapsapiloader: MapsAPILoader,
        private router: Router,
        private odometerService: OdometerService
       ) {
    }

    public collapsed(event: any): void {
        // console.log(event);
    }
    public getDate(): number {
        return this.dt && this.dt.getTime() || new Date().getTime();
    }
    public open(): void {
        this.opened = !this.opened;
    }
    public today(): void {
        this.dt = new Date();
    }
    // public expanded(event:any):void {
        //     console.log(event);
        // }

    handleClick(event: any, part: string, number: string) {
        if (part == 'bedroom') {
            if (event.target.checked) {
                if (number == 'all') {
                    this.numberBedroom = [];
                    this.numberBedroom.push(number);
                } else {
                    if (this.numberBedroom.length == 1 && this.numberBedroom.indexOf('all') > -1) {
                        this.numberBedroom = [];
                    }
                    this.numberBedroom.push(+number);
                }
            } else {
                this.numberBedroom.splice(this.numberBedroom.indexOf(+number), 1);
                if (this.numberBedroom.length == 0) {
                    this.numberBedroom.push('all');
                }
            }
        } else if (part == "bathroom") {
            if (event.target.checked) {
                if (number == 'all') {
                    this.numberBathroom = [];
                    this.numberBathroom.push(number);
                } else {
                    if (this.numberBathroom.length > 0 && this.numberBathroom.indexOf('all') > -1) {
                        this.numberBathroom = [];
                    }
                    this.numberBathroom.push(+number);
                }
            } else {
                this.numberBathroom.splice(this.numberBathroom.indexOf(+number), 1);
                if (this.numberBathroom.length == 0) {
                    this.numberBathroom.push('all');
                }
            }
        }
    }
    handleClickPrice(event: any, part: string, number: string) {
        if (part == 'pricemin') {
          if(number == 'all') {
            this.pricemin = number.toLowerCase();
          }
          else{
            this.pricemin = number;
          }
        } else {
          if(number == 'all') {
            this.pricemax = number.toLowerCase();
          }
          else{
            this.pricemax = number;
          }
        }
    }
    handleClickDate(event: any) {
        var datePipe = new DatePipe('en-SG');
        this.date = datePipe.transform(this.availableDate, 'yyyy-MM-dd');
    }
    listing() {
        this.router.navigate(['/property/browse'], { queryParams: { latlng: this.latlng, pricemin: this.pricemin, pricemax: this.pricemax, bedroom: this.numberBedroom, bathroom: this.numberBathroom, available: this.date, sizemin: 'all', sizemax: 'all', location: this.streetName, radius: 'all' } });
    }
    add(slug: any){
        this.blogService.getBySlug(slug)
        .takeWhile(()=> this.subscription)
        .subscribe(blog => {
            let title = blog.title;
            let cover = blog.cover.url;
            let content = blog.content;
            let contentNew = content.replace(/(<([^>]+)>)/ig,"");
            if (contentNew.length > 225) {
                contentNew = contentNew.substr(0, 225)+' ...';
            }
            let desc = contentNew;
            var old_title = localStorage.getItem('blog_title');
            localStorage.setItem('blog_title', title);
            localStorage.setItem('blog_cover', cover);
            localStorage.setItem('blog_desc', desc);
            var new_title = localStorage.getItem('blog_title');
            if (old_title != new_title) {
                this.add(slug);
            }
            else {
                var cur_title = localStorage.getItem('blog_title');
                if (cur_title == old_title && cur_title == new_title) {
                    this.router.navigate(['/blog-view/' + slug]);
                }
                else {
                    this.add(slug);
                }
            }
            // console.log(localStorage);
            // this.router.navigate(['/blog-view/' + slug]);
        });
    }
    smart_substr(str:string, len:number) {
        var temp = str.substr(0, len);
        if(temp.lastIndexOf('<') > temp.lastIndexOf('>')) {
            temp = str.substr(0, 1 + str.indexOf('>', temp.lastIndexOf('<')));
        }
        return temp;
    }

    toTop() {
        // document.body.scrollTop = 0;
        $("html, body").animate({ scrollTop: 0 }, 600);
    }
    ngOnInit(): void {
        $('.regular').slick({
            dots: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
        });

        // this.sharedService.emitChange({ showFooter: false, showHeader: false });
        this.blogService.getAll()
        .takeWhile(()=> this.subscription)
        .subscribe(blogs => {
            if(blogs.length > 0 && blogs.length < 3) {
                this.blogs = blogs.slice(0,blogs.length);
            } else if (blogs.length == 0) {
                this.blogs = blogs;
            } else {
                this.blogs = blogs.slice((blogs.length - 3),blogs.length);
            }
        });
        this.propertyService.getAllProperty('all','all','all','all','all','all','all','all','all','all')
        .takeWhile(()=> this.subscription)
        .subscribe(properties => {
            if(properties.length > 0 && properties.length < 3) {
                this.properties = properties.slice(0,properties.length);
            } else if (properties.length == 0) {
                this.properties = properties;
            } else {
                this.properties = properties.slice((properties.length - 3),properties.length);
            }
            this.propertyOne = this.properties[0];
            this.propertyTwo = this.properties[1];
            this.propertyThree = this.properties[2];
        });
        for (var i = 0; i <= 49; i++) {
            var object: any = {
                iterate: +i,
                price: +i * 1000,
                state: this.pricestat,
            };
            this.rangePriceMin.push(object);
        }
        for (var i = 1; i <= 50; i++) {
            var object: any = {
                iterate: +i,
                price: +i * 1000,
                state: this.pricestat,
            };
            this.rangePriceMax.push(object);
        }
        this.searchControl = new FormControl()
        this.mapsapiloader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {types: ["address"], componentRestrictions: {country: 'SG'}}  );
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                let place = autocomplete.getPlace();
                this.streetName=place.name;
                let lat = place.geometry.location.lat();
                let lng = place.geometry.location.lng();
                this.latlng = lat+','+lng;
            });
        });
        this.odometerService.get().takeWhile(()=> this.subscription).subscribe((data:any) =>{
            setTimeout( () => {this.animateOdometer(data.odometer)}, 1000);
        })

    }

    animateOdometer(value: number){
        var interval: number = 0.001;
        var odometer = setInterval(()=> {
            if(value - this.odometer == 1){
                clearInterval(odometer);
            }
            if(value - this.odometer <= 101){
                this.odometer += 1;
            }else{
                this.odometer += 111;
            }
         },interval);
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
