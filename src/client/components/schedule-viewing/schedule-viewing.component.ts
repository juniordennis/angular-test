import { Component, OnInit, ChangeDetectorRef, EventEmitter, ViewChild } from '@angular/core';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { PropertyService } from '../../services/property.service';
import { AppointmentService } from '../../services/appointment.service';
import { AmenityService } from '../../services/amenity.service';
import { ChatService } from '../../services/chat.service';
import { StorageService } from '../../services/storage.service';
import { NotificationsService } from 'angular2-notifications';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { ModalDirective } from 'ng2-bootstrap';
import * as $ from 'jquery';
import { Options } from "fullcalendar";
import { Location } from '@angular/common';
import  * as _  from "underscore";
import { User } from '../../models/user';
import { SharedService } from '../../services/shared.service';
import { NavbarComponent } from '../navbar/navbar.component'
import "rxjs/add/operator/takeWhile";

import * as moment from 'moment'

@Component({
  selector: 'schedule-viewing',
  templateUrl: './schedule-viewing.html',
  
})

export class ScheduleViewingComponent {
	@ViewChild('modal') public modal: ModalDirective;
	month:any
	subscription: boolean = true;
	appointments: any[] = [];
	appointment: any;
	events: any[] = [];
	header: any;
	dialogVisible: boolean = false;
	idGen: number = 100;
	isCalenderView:boolean;
	event: any = {};
	today: any = Date.now();
	id: any;
	property: any;
	date:any;
	schedule: any[] = [];
	color: any;
	time_to: any = [];
	time_from: any = [];
	time_to_minute: any = [];
	time_from_minute: any = [];
	timings:any[] = [];
	timing:any[] = [];
	selectedTiming:any[] = [];
	selected:boolean = false;
	model:any = {};
	day:any[] = [];
	user:User;
	loading:boolean = false;
	pointerClass:any;
	selectedValue: any[] = [];
	isSelected:boolean = false;
    public message: string;
    public dreamtalkReady: boolean = false;
    public ddp: MyDDPClient;

	constructor(
		private _location: Location,
    	private userService: UserService,
		private propertyService: PropertyService,
		private amenityService: AmenityService,
		private appointmentService: AppointmentService,
		private router: Router,
      	private chatService: ChatService,
      	private storageService: StorageService,
     	private notificationsService: NotificationsService,
		private navbarComponent: NavbarComponent,
		private route: ActivatedRoute,
    	private sharedService: SharedService,
		private cd: ChangeDetectorRef) {}

	ngOnInit():void{
		this.month = moment().format('MMMM YYYY')
		this.userService.getByToken()
		.takeWhile(()=> this.subscription)
		.subscribe((user: User) => {
	      	this.user = user;
	    }, (error: any) => {
	      	this.user = undefined;
	    });
	    this.sharedService.ddp$
	    .takeWhile(()=> this.subscription)
	    .subscribe((ddp: MyDDPClient) => {
	      	this.ddp = ddp;
	    }, (error: any) => {
	      	console.log('Error ddp', error);
	    })
		this.storageService.get('dreamtalk.ready').takeWhile(()=> this.subscription).subscribe((ready:boolean) => { this.dreamtalkReady = ready });
		var weekday = new Array(7);
		weekday[0] =  "Sunday";
		weekday[1] = "Monday";
		weekday[2] = "Tuesday";
		weekday[3] = "Wednesday";
		weekday[4] = "Thursday";
		weekday[5] = "Friday";
		weekday[6] = "Saturday";
		this.route.params
		.takeWhile(()=> this.subscription)
		.subscribe(params => {
			this.id = params['id'];
			this.appointmentService.getByProperty(this.id)
		    .subscribe(data => {
		    	this.appointments = data
		    })
		});
		this.propertyService.getPropertyById(this.id)
		.takeWhile(()=> this.subscription)
		.subscribe(property => { 
			this.property = property;
			this.model.property = property._id;
			this.model.landlord = property.owner.user._id
			this.schedule = property.schedules
			this.today = moment().format('YYYY-MM-DD');
			for (var a = 0; a < this.schedule.length; ++a) {
				var z = weekday.indexOf(this.schedule[a].day);
				var x = this.day.indexOf(z)
				if (x == -1) {
					this.day.push(z)
					var time_to = moment(this.schedule[a].time_to, 'HH:mm').format('HH')
					var time_from = moment(this.schedule[a].time_from, 'HH:mm').format('HH')
					var minute_to = moment(this.schedule[a].time_to, 'HH:mm').format('mm')
					var minute_from = moment(this.schedule[a].time_from, 'HH:mm').format('mm')
					var to = moment(this.schedule[a].time_to, 'HH:mm').format('HH:mm')
					var from = moment(this.schedule[a].time_from, 'HH:mm').format('HH:mm')
					if ((+time_to - +time_from) > 1) {
						this.color = "#68CF68"
					}else if((+time_to - +time_from) < 2) {
						if ((+time_to[a] - +time_from) === 1) {
							if ((+minute_to - +minute_from) > 20) {
								this.color = "#68CF68"
							}else if ((+minute_to - +minute_from) < 20){
								this.color = "#F6E900"
							}
						}else{
							this.color = "#F6E900"
						}
					}else{
						this.color = "#ff93a6"
					}
					this.events.push({
						id:this.schedule[a]._id,
						title: " ",
						// "start": this.today+'T00:00:00',
						dow: [ z ],
						day: this.schedule[a].day,
						editable: false,
						color: this.color,
						className: 'green-calender-color',
						allDay: true,
						start: '10:00', // a start time (10am in this example)
	    				end: '14:00',
						time_from: [from],
						time_to: [to],
						ranges: [{
	                      start: moment(this.today,'YYYY-MM-DD'),
	                      end: moment(this.today,'YYYY-MM-DD').endOf('year'),
	                    }],
					});
					this.isCalenderView = true;
				}else{
					var to = moment(this.schedule[a].time_to, 'HH:mm').format('HH:mm')
					var from = moment(this.schedule[a].time_from, 'HH:mm').format('HH:mm')
					for (let i = 0; i < this.events.length; ++i) {
						if (this.events[i].day == this.schedule[a].day) {
							this.events[i].time_from.push(from)
							this.events[i].time_to.push(to)
						}
					}
				}
			}
			var weekno = [0,1,2,3,4,5,6];
			let unavailable = _.difference(weekno, this.day);
			if (unavailable !== []) {
				this.events.push({
					id: '',
					dow: unavailable,
					editable: false,
					allDay: true,
					color: '#ff93a6',
					className: 'pink-calender-color',
					start: '10:00', // a start time (10am in this example)
	    			end: '14:00',
					time_from: 0,
					time_to: 0,
					ranges: [{
	                  start: moment(this.today,'YYYY-MM-DD'),
	                  end: moment(this.today,'YYYY-MM-DD').endOf('year'),
	                }],
				});
			}
		});
		this.header = {
			left: '',
			center: '',
			right: '',
		};
	}

	pushTime(status:any, id:any, time:any, time2:any, array:any) {
	  	if(status) {
	    	if(status == 'pending') {
	      		array.push({id: id, time: time, time2: time2, status:"Pending Confirmation", cls:"green-box-viewing yellow"});
	    	} else if(status == 'accepted' || status == 'archived' || status == 'unavailable') {
	      		array.push({id: id, time: time, time2: time2, status:"Not Available", cls:"green-box-viewing pink"});
	    	} else if(status == 'rejected') {
	      		array.push({id: id, time: time, time2: time2, status:"Available", cls:"green-box-viewing"});
	    	}
	  	} else {
	    	array.push({id: id, time: time, time2: time2, status:"Available", cls:"green-box-viewing"});
	  	}
	}

	eventRender(event:any, element:any, view:any) {
		return (event.ranges.filter(function(range:any){ // test event against all the ranges
	        return (event.start.isBefore(range.end) &&
	                event.end.isAfter(range.start));
	    }).length)>0; //if it isn't in one of the ranges, don't render it (by returning false)
	}

	handleDayClick(event:any) {
		//trigger detection manually as somehow only moving the mouse quickly after click triggers the automatic detection
		this.cd.detectChanges();
	}

	prev(fc:any) {
		fc.prev();
		this.month = moment(fc.getDate()).format('MMMM YYYY')
	}

	next(fc:any) {
		fc.next();
		this.month = moment(fc.getDate()).format('MMMM YYYY')
	}

	getSelected(timing:any){
		var i = this.timing.indexOf(timing);
		if(i != -1) {
			this.timing.splice(i, 1);
		}else{
			this.timing.push(timing)
		}
	}

	removeSelected(){
		this.timing = []
	}

	select(){
		if (this.selectedValue.length > 0) {
			this.model.time = []
			this.model.time2 = []
			this.selected = true
			for (let z = 0; z < this.timing.length; ++z) {
				this.model.time.push(this.timing[z].time)
				this.model.time2.push(this.timing[z].time2)
				this.model.schedule = this.timing[z].id
			}
			this.model.date = this.date;
			this.timing = []
		}
	}

	confirm(id:string){
		this.loading = true;
		if (this.selected) {
			console.log(this.model)
			this.appointmentService.create(this.model) 
			.then(
	            res => {
	            	let roomParams = { property_id: id };
					this.router.navigate(['/dashboard/proposed']);
				    if (this.user) {
				    	if (this.dreamtalkReady) {
							if (this.ddp) {
								if (!res.room_id) {
									if (this.message) {
					            		this.chatService.createRoom(roomParams)
					            		.then((res: any) => {
					              			let roomId = res && res.room_id ? res.room_id : undefined;
					              			let chatParams: any = {
								                message: this.message,
								                roomId: roomId,
								                extra: { 
												    ref : 'appt_proposed',
												    ref_id: res.appointment_id,
												}
					              			};
					              			this.ddp.sendChat(chatParams).then((res: any) => {
							                // todo: do use the response
								                this.message = '';
						              		}).catch((error: any) => {
						                		console.log(error);;
						              		})
					            		}).catch((err: any) => {
						              		console.log(err);
						            	})
					          		}else{
					            		this.chatService.createRoom(roomParams)
					            		.then((res: any) => {
					              			let roomId = res && res.room_id ? res.room_id : undefined;
					              			let chatParams: any = {
								                message: '-',
								                roomId: roomId,
								                extra: { 
												    ref : 'appt_proposed',
												    ref_id: res.appointment_id,
												}
					              			};
					              			this.ddp.sendChat(chatParams).then((res: any) => {
							                // todo: do use the response
						              		}).catch((error: any) => {
						                		console.log(error);
						              		})
					            		}).catch((err: any) => {
						              		console.log(err);;
						            	})
					          		}
								}else{
									this.chatService.getById(res.room_id)
									.takeWhile(()=> this.subscription)
							        .subscribe(data => { 
							        	if (data.room_id) {
							        		if (this.message) {
							            		this.chatService.createRoom(roomParams)
							            		.then((res: any) => {
							              			let roomId = res && res.room_id ? res.room_id : undefined;
							              			let chatParams: any = {
										                message: this.message,
										                roomId: roomId,
										                extra: { 
														    ref : 'appt_proposed',
														    ref_id: res.appointment_id,
														}
							              			};
							              			this.ddp.sendChat(chatParams).then((res: any) => {
									                // todo: do use the response
									                	this.loading = false
										                this.message = '';
										            	this.loading = false;
								              		}).catch((error: any) => {
								                		console.log(error);
								              		})
							            		}).catch((err: any) => {
								              		console.log(err);
								            	})
							          		}else{
							            		this.chatService.createRoom(roomParams)
							            		.then((res: any) => {
							              			let roomId = res && res.room_id ? res.room_id : undefined;
							              			let chatParams: any = {
										                message: '-',
										                roomId: roomId,
										                extra: { 
														    ref : 'appt_proposed',
														    ref_id: res.appointment_id,
														}
							              			};
							              			this.ddp.sendChat(chatParams).then((res: any) => {
									                // todo: do use the response
									                console.log(res,'berhasil')
								              		}).catch((error: any) => {
								                		console.log(error);
								              		})
							            		}).catch((err: any) => {
								              		console.log(err);;
								            	})
							          		}
							        	}
							        }, err => {
							        	console.log(err, 'err')
							        });
									
								}
							} else {
								// no ddp
							}
						} else {
							// dreamtalk not ready
							// this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
						}
				    } else {
						this.navbarComponent.modalLogin.show()
					    // login modal showing
					    // todo: replace notification with login modal
				    }
	            }).catch(err => console.log(err, 'ini error'))
		}else{
			this.notificationsService.error('Error', 'Please select the schedule.');
			this.loading = false
		}
			
	}

	handleEventClick(e:any) {
		this.timings = []
		this.selectedValue = []
		let start = e.calEvent.start;
		let end = e.calEvent.end;
		if(e.view.name === 'month') {
			start.stripTime();
		}
		if(end) {
			end.stripTime();
			this.event.end = end.format();
		}
		this.event.id = e.calEvent.id;
		this.event.start = start.format();
		this.event.allDay = e.calEvent.allDay;
		this.event.time_to = e.calEvent.time_to;
		this.event.time_from = e.calEvent.time_from;
		this.date = moment(start).format("dddd, DD-MM-YYYY");
		this.model.showDate = moment(start).format("DD-MM-YYYY");
		if (this.event.id !== null && this.event.id !== '' && this.event.id) {
			for (let z = 0; z < this.event.time_to.length; ++z) {
				let hours = this.durationMinutes(this.event.time_from[z], this.event.time_to[z])
				if(hours < 0) {
		    		hours = Math.abs(hours);
				}
				for(let i = 0; i < hours; i++) {
				    let add30Min = i * 30;
				    let add60Min = (i+1) * 30;
				    let timeAdded = moment(this.event.time_from[z], 'HH:mm').add(add30Min, 'minutes').format('HH:mm');
				    let timeAdded2 = moment(this.event.time_from[z], 'HH:mm').add(add60Min, 'minutes').format('HH:mm');
				    this.appointment = this.appointments.filter(data => data.chosen_time.date == this.date && data.chosen_time.from == timeAdded && data.chosen_time.to == timeAdded2)
				    let time_hour = moment().format('HH')
				    let time_minute = moment().format('mm')
				    let date = moment().format("dddd, DD-MM-YYYY")
				    let timeAdded_hour = moment(timeAdded, 'HH:mm').format('HH')
				    let timeAdded_minute = moment(timeAdded, 'HH:mm').format('mm')
				    if (this.appointment !== null && this.appointment !== [] && this.appointment.length > 0) {
				    	this.pushTime(this.appointment[0].status, this.event.id, timeAdded, timeAdded2, this.timings);
				    }else {
				    	if (date == this.date) {
				    		if (+time_hour > +timeAdded_hour) {
					    		this.pushTime('unavailable', this.event.id, timeAdded, timeAdded2, this.timings);
					    	}else if (+time_hour == +timeAdded_hour) {
					    		if (+time_minute > +timeAdded_minute) {
					    			this.pushTime('unavailable', this.event.id, timeAdded, timeAdded2, this.timings);
					    		}else{
					    			this.pushTime(null, this.event.id, timeAdded, timeAdded2, this.timings);
					    		}
					    	}else{
					    		this.pushTime(null, this.event.id, timeAdded, timeAdded2, this.timings);
					    	}
				    	}else{
				    		this.pushTime(null, this.event.id, timeAdded, timeAdded2, this.timings);
				    	}
				    	
				    }
				}	
			}
			this.modal.show()
		}
	}

	back(){
		this._location.back();
	}

	durationMinutes(time_from:any, time_to:any) {
		let mFrom = moment(time_from, 'HH:mm');
		let mTo = moment(time_to, 'HH:mm');
		let diff = mTo.diff(mFrom, 'minutes');
		return diff / 30;
	}

	ngOnDestroy() {
		this.subscription = false;
	}
}

export class MyEvent {
	id: string;
	dow: number[];
	editable: boolean;
	color: string;
	start: string;
    end: string;
	allDay: boolean = true;
	className: string;
	time_to: number;
	time_from: number;
	time_to_minute: number;
	time_from_minute: number;
	ranges: [{
		start: Date;
		end : Date;
	}]
}