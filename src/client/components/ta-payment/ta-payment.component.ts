import { Component, OnInit } from '@angular/core';
import { url } from '../../../global';
import { AttachmentService } from '../../services/attachment.service';
import { AgreementService } from '../../services/agreement.service';
// import { SharedService } from '../../services/shared.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import "rxjs/add/operator/takeWhile";


@Component({
  selector: 'ta-payment',
  templateUrl: './ta-payment.html',
})

export class TaPaymentComponent {
    subscription: boolean = true;
	id:string;
	model:any = {}
	header:any;
	link :any;
	loi :any;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private attachementService: AttachmentService,
        private agreementService: AgreementService,
		private notificationsService: NotificationsService,
		// private sharedService: SharedService
	) {}

	ngOnInit(): void{
        this.route.params.takeWhile(()=> this.subscription).subscribe(params => {
            this.id = params['id'];
        });
        this.agreementService.getLOI(this.id).takeWhile(()=> this.subscription).subscribe(loi => {
        	this.loi = loi;
        });
    	// this.sharedService.emitChange({ showFooter: false, showHeader: true });
		this.link = url+'attachments';
		let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
	}

	handleFileSuccess(event:any): void {
		this.model.attachment = event.file.uploadedId
  	}

	handleFileFailure(event:any): void {
	    console.log('fail', event);
	}

	handleFileSending(event:any): void {
	    console.log('send', event);
	}

	handleFileAdded(event:any): void {
	    console.log('add', event);
	}

	handleFileRemoved(event:any, type:any): void {
		this.attachementService.delete(event.uploadedId)
        .then(
            response => {
                console.log(response);
            },
            error => {
                console.log(error);
            }
        );
	}

	savePayment() {
		this.model.type = 'tenancy_agreement';
        if(this.model.attachment) {
            this.agreementService.payment(this.id, this.model)
            .then(
                response => {
                    this.router.navigate([ '/ta/'+ this.id +'/view']);
                },
                error => {
                    console.log(error)
                }
            );
        } else {
            this.notificationsService.error('', 'Please upload the payment first')
        }
    }

    back(){
    	this.router.navigate([ '/ta/'+ this.id +'/view']);
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}