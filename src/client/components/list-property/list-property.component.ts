import { Component, OnInit, ViewChild, NgZone, ElementRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { PropertyService } from '../../services/property.service';
import { UserService } from '../../services/user.service';
import { SharedService } from '../../services/shared.service';
import { Marker } from '../../models/marker';
import { Circle } from '../../models/circle';
import { BrowserModule } from "@angular/platform-browser";
import { FormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MapsAPILoader} from 'angular2-google-maps/core';
import "rxjs/add/operator/takeWhile";

declare var google: any;

@Component({
	selector: 'my-listproperty',
	templateUrl: './list-property.html',
	styles: [`.pricemin{text-align:-webkit-left}.pricemax{text-align:-webkit-right}.sqftmin{text-align:-webkit-left}.sqftmax{text-align:-webkit-right}`],
})

export class ListPropertyComponent implements OnInit {
	subscription: boolean = true;
	public customStyle: any;
	public active: number = 1;
	public incrementNumber: number = 6;
	public show: number = 6;
	//for maps
	public zoom: number = 11;
	public minZoom: number = 11;
	public maxZoom: number = 15;
	public datas: any = {};
	public dataProperties: any = [];
	public showProperties: any = [];
	public markers: any = [];
	public circles: any;
	public showcircles: boolean = false;
	public lat: number = 1.290270;
	public lng: number = 103.851959;
	public latlng: any;

	//for form filter
	public numberBedroom: any = ['all'];
	public numberBathroom: any = ['all'];
	public pricemin: any = 0;
	public pricemax: any = 0;
	public sqftmin: any = 0;
	public sqftmax: any = 0;
	public location: string = 'all';
	public queryLocation: string = 'all';
	public radius: any = 'all';
	public queryRadius: number = 1500;
	public rangePrice: any = [];
	public rangeSqft: any = [];
	public dateRent: any;
	public pricestat: any = 'pricemin';
	public sqftstat: any = 'sqftmin';
	private parameter: any;
	private sub: any;
	private temp: any;
	@ViewChild("searchLocation")
	public searchLocationElementRef: ElementRef;
	public searchLocationControl: FormControl;
	public isCollapsed: boolean = false;
	public shortlistedProperty: any[];
	public up: boolean = false;
	public down: boolean = true;
	public propertySearchDropdown: boolean = true;
	public hideLocationSearch: string = 'hidden';
	public showCalendar: boolean = false;
	public selectedDate: string = 'all';
	public availableDate: string;
	public dateText: string = 'all';
	public mirrorRangePrice: any = [];
	public mirrorRangeSqft: any = [];
	public priceRangeText: string = 'all';
	public sqftRangeText: string = 'all';

	constructor(
		private propertyService: PropertyService,
		private sharedService: SharedService,
		private mapsapiloader: MapsAPILoader,
		private route: ActivatedRoute,
		private router: Router,
		private userService: UserService,
	) {
		this.sharedService.getCustomStyles().then((styles) => {
			this.customStyle = styles;
		});
	}


	// initial center position for the map
	ngOnInit(): void {
		this.shortlistedProperty = []
		this.sub = this.route.queryParams
		.takeWhile(()=> this.subscription)
		.subscribe((params: Params) => {
			this.parameter = params;
			this.latlng = this.parameter.latlng;
			this.numberBedroom = [];
			if (this.parameter.bedroom == "all") {
				this.numberBedroom.push(this.parameter.bedroom);
			} else {
				this.temp = this.parameter.bedroom.split(',');
				for (var i = 0; i < this.temp.length; i++) {
					var x = this.temp[i];
					this.numberBedroom.push(+x);
				}
			}
			this.numberBathroom = [];
			if (this.parameter.bathroom == "all") {
				this.numberBathroom.push(this.parameter.bathroom);
			} else {
				this.numberBathroom = [];
				this.temp = this.parameter.bathroom.split(',');
				for (var i = 0; i < this.temp.length; i++) {
					var x = this.temp[i];
					this.numberBathroom.push(+x);
				}
			}
			this.pricemin = this.parameter.pricemin;
			this.pricemax = this.parameter.pricemax;
			this.sqftmin = this.parameter.sizemin;
			this.sqftmax = this.parameter.sizemax;
			if (this.parameter.radius == 'all') {
				this.queryRadius = 1500;
				this.radius = 'all';
			} else {
				let temp = this.parameter.radius;
				this.queryRadius = +temp;
				this.radius = +temp;
			}
			this.dateRent = this.parameter.available;
			if (this.parameter.location == 'all') {
				this.location = 'all';
				this.queryLocation = 'all';
			} else {
				this.location = this.parameter.location;
				this.queryLocation = this.parameter.location;
			}
			this.getProperties();
			this.getMaps();
		});
		
		this.searchLocationControl = new FormControl()
		this.mapsapiloader.load().then(() => {
			let autocomplete = new google.maps.places.Autocomplete(this.searchLocationElementRef.nativeElement, { types: ["address"], componentRestrictions: { country: 'SG' } });
			google.maps.event.addListener(autocomplete, 'place_changed', () => {
				let place = autocomplete.getPlace();
				if (place) {
					this.circles = place;
					this.showcircles = true;
					this.lat = place.geometry.location.lat();
					this.lng = place.geometry.location.lng();
					this.latlng = this.lat+','+this.lng;
					this.location = place.name;
					this.queryLocation = place.name;
					this.changeQuery();
					this.hideLocationSearch = 'hidden';
				} else {
					this.location = 'all';
					this.latlng = 'all';
					this.showcircles = false;
					this.queryLocation = "all";
					this.changeQuery();
					this.hideLocationSearch = 'hidden';
				}
			});
		});
		for (var i = 1; i <= 50; i++) {
			var object: any = {
				iterate: +i,
				price: +i * 1000,
				state: this.pricestat,
			};
			this.rangePrice.push(object);
			this.mirrorRangePrice = this.rangePrice;
		}
		for (var i = 2; i <= 40; i += 2) {
			var object: any = {
				iterate: +i,
				sqft: +i * 100,
				state: this.sqftstat,
			};
			this.rangeSqft.push(object);
			this.mirrorRangeSqft = this.rangeSqft;
		}
	}

	public getProperties() {
		this.propertyService.getAllProperty(this.latlng, this.pricemin, this.pricemax, this.numberBedroom, this.numberBathroom, this.dateRent, this.sqftmin, this.sqftmax, this.queryLocation, this.radius)
			.takeWhile(()=> this.subscription)
			.subscribe(dataProperties => {
				let properties = dataProperties;
				if(localStorage.getItem('authToken')) {
					this.userService.getByToken()
					.takeWhile(()=> this.subscription)
					.subscribe(dataUser => {
						properties = properties.filter((data:any) => data.owner.user._id !== dataUser._id);
						for (let x = 0; x < dataUser.shortlisted_properties.length; ++x) {
							this.shortlistedProperty.push(dataUser.shortlisted_properties[x]._id)
						}
						for (let z = 0; z < properties.length; ++z) {
							var i = this.shortlistedProperty.indexOf(properties[z]._id)
							if(i == -1) {
								properties[z].shortlisted = false;
							}else{
								properties[z].shortlisted = true;
							}
						}
					})
				}
				this.dataProperties = properties;
			});
	}

	public getMaps() {
		this.propertyService.getMapsDevelopments(this.latlng, this.pricemin, this.pricemax, this.numberBedroom, this.numberBathroom, this.dateRent, this.sqftmin, this.sqftmax, this.queryLocation, this.radius)
			.takeWhile(()=> this.subscription)
			.subscribe(datas => {
				this.datas = datas;
				let marker:any = [];
				for (var i = 0; i < this.datas.length; i++) {
					var object: any = {
						lat: datas[i] && datas[i].development && datas[i].development.address ? +datas[i].development.address.coordinates[1] : undefined,
						lng: datas[i] && datas[i].development && datas[i].development.address ? +datas[i].development.address.coordinates[0] : undefined,
						label: '' + datas[i].count,
						draggable: false,
						content: datas[i].development.name,
						iconUrl: 'assets/images/custom-marker6.png'
					};
					marker.push(object);
				}
				this.markers = marker;
			});
	}

	public collapsed(event: any): void {
		console.log(event);
	}

	public expanded(event: any): void {
		console.log(event);
	}

	propertySearch() {
		if (this.propertySearchDropdown == false) {
			this.propertySearchDropdown = true;
			this.up = true;
			this.down = false;
		}
		else {
			this.propertySearchDropdown = false;
			this.up = false;
			this.down = true;	
		}
	}

	hideLocation() {
		if (this.hideLocationSearch == 'show') {
			this.hideLocationSearch = 'hidden';
		}
	}

	showLocation() {
		this.hideCalender();
		if (this.hideLocationSearch == 'hidden') {
			this.hideLocationSearch = 'show';
		}
		else {
			this.hideLocationSearch = 'hidden';	
		}
	}

	searchByDate() {
		this.hideLocation();
		if (this.showCalendar == false) {
			this.showCalendar = true;
		}
		else {
			this.showCalendar = false;
		}
	}

	hideCalender() {
		if (this.showCalendar == true) {
			this.showCalendar = false;
		}
	}

	clearDate() {
		this.availableDate = '';
		this.dateText = 'all';
		this.router.navigate(['/property/browse'], { queryParams: { latlng: this.latlng, pricemin: this.pricemin, pricemax: this.pricemax, bedroom: this.numberBedroom, bathroom: this.numberBathroom, available: 'all', sizemin: this.sqftmin, sizemax: this.sqftmax, location: this.queryLocation, radius: this.radius } });
	}

	changeSelectedDate(event:any) {
		this.selectedDate = event.target.value;
	}

	handleClick(event: any, part: string, number: string) {
		if (part == 'bedroom') {
			if (event.target.checked) {
				if (number == 'all') {
					this.numberBedroom = [];
					this.numberBedroom.push(number);
				} else {
					if (this.numberBedroom.length == 1 && this.numberBedroom.indexOf('all') > -1) {
						this.numberBedroom = [];
					}
					this.numberBedroom.push(+number);
				}
			} else {
				this.numberBedroom.splice(this.numberBedroom.indexOf(+number), 1);
				if (this.numberBedroom.length == 0) {
					this.numberBedroom.push('all');
				}
			}
		} else if (part == "bathroom") {
			if (event.target.checked) {
				if (number == 'all') {
					this.numberBathroom = [];
					this.numberBathroom.push(number);
				} else {
					if (this.numberBathroom.length > 0 && this.numberBathroom.indexOf('all') > -1) {
						this.numberBathroom = [];
					}
					this.numberBathroom.push(+number);
				}
			} else {
				this.numberBathroom.splice(this.numberBathroom.indexOf(+number), 1);
				if (this.numberBathroom.length == 0) {
					this.numberBathroom.push('all');
				}
			}
		}
	}

	priceFunction(event: any, price: any, state: any) {
		event.stopPropagation();
		if (state == 'pricemin') {
			this.pricemin = price;
			this.changeQuery();
			let indexPrice = 0;
			if (price !== 'all') {
				for(var i = 0; i < this.rangePrice.length; i++ ) {
					if (this.rangePrice[i].price === price) {
						indexPrice = i;
					}
				}
				this.mirrorRangePrice = this.rangePrice.slice(indexPrice+1, this.rangePrice.length);	
			}
			else {
				this.mirrorRangePrice = this.rangePrice;		
			}
			this.pricestat = 'pricemax';
		}
		else {
			this.pricemax = price;
			this.changeQuery();
			let indexPrice = 0;
			if (price !== 'all') {
				for(var i = 0; i < this.rangePrice.length; i++ ) {
					if (this.rangePrice[i].price === price) {
						indexPrice = i;
					}
				}
				this.mirrorRangePrice = this.rangePrice.slice(0, indexPrice);	
			}
			else {
				this.mirrorRangePrice = this.rangePrice;		
			}
			this.pricestat = 'pricemin';
		}
		let pricemin = this.pricemin;
		if (this.pricemin == 'all') {
			pricemin = 0;
		}
		let pricemax = this.pricemax;
		if (this.pricemax == 'all' && this.pricemin == 'all') {
			pricemax = 0;
		}
		this.priceRangeText = pricemin+' to '+pricemax;
	}

	sqftFunction(event: any, sqft: any, state: any) {
		event.stopPropagation();
		if (state == 'sqftmin') {
			this.sqftmin = sqft;
			this.changeQuery();
			let indexSqft = 0;
			if (sqft !== 'all') {
				for(var i = 0; i < this.rangeSqft.length; i++ ) {
					if (this.rangeSqft[i].sqft === sqft) {
						indexSqft = i;
					}
				}
				this.mirrorRangeSqft = this.rangeSqft.slice(indexSqft+1, this.rangeSqft.length);	
			}
			else {
				this.mirrorRangeSqft = this.rangeSqft;		
			}
			this.sqftstat = 'sqftmax';
		}
		else {
			this.sqftmax = sqft;
			this.changeQuery();
			let indexSqft = 0;
			if (sqft !== 'all') {
				for(var i = 0; i < this.rangeSqft.length; i++ ) {
					if (this.rangeSqft[i].sqft === sqft) {
						indexSqft = i;
					}
				}
				this.mirrorRangeSqft = this.rangeSqft.slice(0, indexSqft);
			}
			else {
				this.mirrorRangeSqft = this.rangeSqft;		
			}
			this.sqftstat = 'sqftmin';

		}
		let sqftmin = this.sqftmin;
		if (this.sqftmin == 'all') {
			sqftmin = 0;
		}
		let sqftmax = this.sqftmax;
		if (this.sqftmax == 'all' && this.sqftmin == 'all') {
			sqftmax = 0;
		}
		this.sqftRangeText = sqftmin+' to '+sqftmax;
	}

	clearPrice() {
		this.pricemin = 'all';
		this.pricemax = 'all';
		this.changeQuery();
		this.mirrorRangePrice = this.rangePrice;
		this.priceRangeText = 'all';
	}

	clearSqft() {
		this.sqftmin = 'all';
		this.sqftmax = 'all';
		this.changeQuery();
		this.mirrorRangeSqft = this.rangeSqft;
		this.sqftRangeText = 'all';
	}

	emptyLocation() {
		this.location = 'all';
		this.queryLocation = 'all';
		this.radius = 'all';
		this.latlng = 'all';
		this.queryRadius = 1500;
		this.showcircles = false;
		this.changeQuery();
	}

	onRadiusNew(e: any): void {
		let temp = Math.round(e);
		this.radius = +temp;
		this.queryRadius = +temp;
		this.changeQuery();
	}

	incShow(inc: number): void {
		this.show = this.show + inc;
	}

	setFocus(id:any){
    	setTimeout(function(){id.focus();}, 100);
    }

	handleClickDate(event: any) {
        var datePipe = new DatePipe('en-SG');
        let dateText = datePipe.transform(this.availableDate, 'dd/MM/yyyy');
        this.dateText = dateText;
        this.dateRent = datePipe.transform(this.availableDate, 'yyyy-MM-dd');
        this.changeQuery();
    }

	changeQuery() {
		this.router.navigate(['/property/browse'], { queryParams: { latlng: this.latlng, pricemin: this.pricemin, pricemax: this.pricemax, bedroom: this.numberBedroom, bathroom: this.numberBathroom, available: this.dateRent, sizemin: this.sqftmin, sizemax: this.sqftmax, location: this.queryLocation, radius: this.radius } });
	}

	changeLatlng(latlng: any) {
		this.router.navigate(['/property/browse'], { queryParams: { latlng: latlng, pricemin: this.pricemin, pricemax: this.pricemax, bedroom: this.numberBedroom, bathroom: this.numberBathroom, available: this.dateRent, sizemin: this.sqftmin, sizemax: this.sqftmax, location: 'all', radius: this.radius } });
	}

	changeRadius(radius: any) {
		this.router.navigate(['/property/browse'], { queryParams: { latlng: this.latlng, pricemin: this.pricemin, pricemax: this.pricemax, bedroom: this.numberBedroom, bathroom: this.numberBathroom, available: this.dateRent, sizemin: this.sqftmin, sizemax: this.sqftmax, location: 'all', radius: radius } });
	}

	ngOnDestroy() {
		this.subscription = false;
		localStorage.removeItem('dataProperties');
	}
}
