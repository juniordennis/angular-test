import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MapsAPILoader } from 'angular2-google-maps/core';
import { PropertyService } from '../../services/property.service';
import { UserService } from '../../services/user.service';
import { NotificationsService } from 'angular2-notifications';
import { ListPropertyComponent } from './list-property.component';

declare var google: any;

@Component({
	selector: 'my-one-listproperty',
	templateUrl: './list-one.html',
})

export class ListOnePropertyComponent {
	subscription: boolean = true;
	@Input() dataProperties: any = [];
	@Input() datas: any = {};
	@Input('lng') lng: number;
	@Input('lat') lat: number;
	@Input('latlng') latlng: any;
	@Input('show') show: number;
	@Input('incrementNumber') incrementNumber: number;
	@Input('zoom') zoom: number;
	@Input('queryRadius') queryRadius: number;
	@Input('minZoom') minZoom: number;
	@Input('maxZoom') maxZoom: number;
	@Input('circles') circles: any;
	@Input('showcircles') showcircles: boolean;
	@Input() markers: any = [];
	@Input() customStyle: any;
	@Output() radiusNew: EventEmitter<any> = new EventEmitter<any>();
	@Output() incShow: EventEmitter<any> = new EventEmitter<any>();
	public availableDevelopment: any = 'all';
	public aaaradius: any = 1;
	public temporary : any = '';
	public checkShow : number = 1 ;
	infoWindowOpened : any = null;
  	public shortlistedProperty: any[] = [];

	constructor(
		private propertyService:PropertyService,
    	private userService: UserService,
    	private router: Router,
    	private activatedRoute: ActivatedRoute,
    	private listPropertyComponent: ListPropertyComponent) { }

  ngOnInit() {
  	//
  }

	onInfoWindowClose(index: number) {
		if (this.temporary == '') {
			this.availableDevelopment = 'all';
			this.markers[index].iconUrl = 'assets/images/custom-marker6.png';
			this.listPropertyComponent.getProperties();
		}
		else{
			if (this.checkShow == 1) {
				this.availableDevelopment = 'all';
				this.listPropertyComponent.getProperties();
			}
			else {
				this.availableDevelopment = this.temporary;
			}
			this.markers[index].iconUrl = 'assets/images/custom-marker6.png';
			this.checkShow=1;
		}
	}

	shortlistProperty(id:string){
        if(localStorage.getItem('authToken')) {
			this.propertyService.shortlist(id)
			.then(
				data => {
					for (let z = 0; z < this.dataProperties.length; ++z) {
						if(this.dataProperties[z]._id == id) {
							this.dataProperties[z].shortlisted = true
						}
					}
				},
				error => {
					 console.log(error)
				}
			);
		}else{

		}
	}

	unshortlistProperty(id:string){
		this.propertyService.unshortlist(id)
		.then(
			data => {
				for (let z = 0; z < this.dataProperties.length; ++z) {
					if(this.dataProperties[z]._id == id) {
						this.dataProperties[z].shortlisted = false
					}
				}
			},
			error => {
				 console.log(error)
			}
		);
	}

	clickedMarker(label: string, infoWindow: any, index: number) {
		this.listPropertyComponent.getProperties();
		if (label !== 'all') {
			this.dataProperties = this.dataProperties.filter((data:any) => data.development.name == label);
		}
		else {
			this.dataProperties = this.dataProperties;
		}
		if(this.infoWindowOpened !== null){
			if (this.infoWindowOpened != infoWindow) {
				this.infoWindowOpened.close();
				this.temporary = label;
				this.checkShow = 2;
			}
		}

		this.infoWindowOpened = infoWindow;
		this.markers[index].iconUrl = 'assets/images/custom-marker8.png';
		this.availableDevelopment = label;
	}

	getProperties(label:string) {
		this.activatedRoute.queryParams.subscribe((params: Params) => {
			let latlng = params['latlng'];
	        let pricemin = params['pricemin'];
	        let pricemax = params['pricemax'];
	        let bedroom = params['bedroom'];
	        let bathroom = params['bathroom'];
	        let available = params['available'];
			let sizemin = params['sizemin'];
			let sizemax = params['sizemax'];
			let location = params['location'];
			let radius = params['radius'];
	    	this.propertyService.getAllProperty(latlng, pricemin, pricemax, bedroom, bathroom, available, sizemin, sizemax, location, radius)
			.takeWhile(()=> this.subscription)
			.subscribe(dataProperties => {
				let properties = dataProperties;
				if(localStorage.getItem('authToken')) {
					this.userService.getByToken()
					.takeWhile(()=> this.subscription)
					.subscribe(dataUser => {
						properties = properties.filter((data:any) => data.owner.user._id !== dataUser._id);
						for (let x = 0; x < dataUser.shortlisted_properties.length; ++x) {
							this.shortlistedProperty.push(dataUser.shortlisted_properties[x]._id)
						}
						for (let z = 0; z < properties.length; ++z) {
							var i = this.shortlistedProperty.indexOf(properties[z]._id)
							if(i == -1) {
								properties[z].shortlisted = false;
							}else{
								properties[z].shortlisted = true;
							}
						}
					})
				}
				if (label !== 'all') {
					this.dataProperties = properties.filter((data:any) => data.development.name == label);
					
				}
				else {
					this.dataProperties = properties;
				}
			});
	    });	
	}

	showMore() {
		this.incShow.emit(this.incrementNumber);
	}

	centerChanged(event:any) {
		let lat = event.coords.lat;
		let lng = event.coords.lng;
		let latlng = lat+','+lng;
		this.listPropertyComponent.changeLatlng(latlng);
	}
	radiusChanged(event:any) {
		let radius = event;
		this.listPropertyComponent.changeRadius(radius);
	}
}
