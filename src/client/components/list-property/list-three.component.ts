import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute  } from '@angular/router';
import { PropertyService } from '../../services/property.service';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
	selector: 'my-three-listproperty',
	templateUrl: './list-three.html',
})

export class ListThreePropertyComponent {
	@Input() dataProperties:any =[];
	@Input('show') show:number;
	@Input('incrementNumber') incrementNumber: number;
	@Output() incShow: EventEmitter<any> = new EventEmitter<any>();
  	public shortlistedProperty: any[] = [];
  	public id:string;

	constructor(
		private propertyService:PropertyService,
        private router: Router,
    	private userService: UserService,) { }

	showMore() {
		this.incShow.emit(this.incrementNumber);
	}

	shortlistProperty(id:string){
		this.id = id;
        if(localStorage.getItem('authToken')) {
			this.propertyService.shortlist(id)
			.then(
				data => {
					// this.userService.getByToken().subscribe(dataUser => {
			  //           for (let x = 0; x < dataUser.shortlisted_properties.length; ++x) {
			  //             	this.shortlistedProperty.push(dataUser.shortlisted_properties[x]._id)
			  //           }
			  //           for (let z = 0; z < this.dataProperties.length; ++z) {
				 //            var i = this.shortlistedProperty.indexOf(this.dataProperties[z]._id)
				 //            if(i == -1) {
				 //              	this.dataProperties[z].shortlisted = false;
				 //            }else{
				 //              	this.dataProperties[z].shortlisted = true;
				 //            }
			  //           }
			  //       })
					for (let z = 0; z < this.dataProperties.length; ++z) {
						if(this.dataProperties[z]._id == id) {
							this.dataProperties[z].shortlisted = true
						}
					}
				},
				error => {
					 console.log(error)
				}
			);
		}
	}

	unshortlistProperty(id:string){
		this.id = id;
		this.propertyService.unshortlist(id)
		.then(
			data => {
				// this.userService.getByToken().subscribe(dataUser => {
			 //        for (let x = 0; x < dataUser.shortlisted_properties.length; ++x) {
			 //          	this.shortlistedProperty.push(dataUser.shortlisted_properties[x]._id)
			 //        }
			 //        for (let z = 0; z < this.dataProperties.length; ++z) {
				//         var i = this.shortlistedProperty.indexOf(this.dataProperties[z]._id)
				//         if(i == -1) {
				//           	this.dataProperties[z].shortlisted = false;
				//         }else{
				//           	this.dataProperties[z].shortlisted = true;
				//         }
			 //        }
			 //    })
				for (let z = 0; z < this.dataProperties.length; ++z) {
					if(this.dataProperties[z]._id == id) {
						this.dataProperties[z].shortlisted = false
					}
				}
			},
			error => {
				 console.log(error)
			}
		);
	}

	view(id:string){
        if (!this.id && this.id === null && this.id === '') {
            this.router.navigate(['property/' + id + '/view'])
        }
        if (this.id) {
            this.id = null
        }
    }
}
