import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AgreementService } from '../../../services/agreement.service';
import "rxjs/add/operator/takeWhile";
import * as moment from 'moment'

@Component({
	selector: 'inventory',
	templateUrl: `client/components/member-section/inventory/inventory.html`,
})
export class InventoryComponent implements OnInit { 
	subscription: boolean = true;
	isAll:boolean;
	isAction:boolean;
	isPending:boolean;
	isArchived:boolean;
	agreements: any[] = [];
	agreement: any;
	action: any[] = [];
	pending: any[] = [];
	archived: any[] = [];
	userID = '1'

	constructor(
		private agreementService:AgreementService,
		private router:Router,
		){}

	ngOnInit(): void {
		this.isAll = true;
		this.loadAllAgreements()
	}

	private loadAllAgreements() {
		this.agreementService.getByUser()
		.takeWhile(()=> this.subscription)
		.subscribe(agreements => { 
			this.agreements = agreements.filter((agreements:any) => agreements.inventory_list.data.status != null);
			for (let z = 0; z < this.agreements.length; ++z) {
                var a = new Date(this.agreements[z].inventory_list.data.created_at)
                this.agreements[z].inventory_list.data.created_at = moment(a).format('DD/MM/YYYY HH:mm')
            }
			this.action = this.agreements.filter(agreements => agreements.inventory_list.data.status == 'pending');
			this.pending = this.agreements.filter(agreements =>  agreements.inventory_list.data.status == 'pending');
			this.archived = this.agreements.filter(agreements => agreements.inventory_list.data.status == 'completed');
		});
	}

	view(agreement:any){
		this.router.navigate(['/inventory/'+ agreement._id]);
	}

	ngOnDestroy() {
		this.subscription = false;
	}
}
