import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppointmentService } from '../../../services/appointment.service';
import { AgreementService } from '../../../services/agreement.service';
import { UserService } from '../../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ChatService } from '../../../services/chat.service';
import { NotificationsService } from 'angular2-notifications';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'archived',
  templateUrl: `client/components/member-section/archived/archived.html`,
})
export class ArchivedComponent implements OnInit { 
	subscription: boolean = true;
	isLandlord:boolean;
	isRequest:boolean;
	userID: string;
	appointment_id: string;
	property_id: string;
	appointments: any[] = [];
	appointment: any;
	landlords: any[] = [];
	tenants: any[] = [];
	agreements: any[] = [];
	countLandlord: number;
	countTenant: number;
	test : any[];
	test1 : any[] = [];

	constructor(
		private appointmentService:AppointmentService,
		private agreementService:AgreementService,
		private userService:UserService,
        private chatService: ChatService,
		private router:Router,
		private route:ActivatedRoute,
		private notificationsService:NotificationsService,
	){}

	ngOnInit():void{
		this.isLandlord = true;
		this.userService.getByToken()
		.takeWhile(()=> this.subscription)
        .subscribe(user => { 
        	this.userID = user._id;
			this.appointmentService.getOwn()
			.takeWhile(()=> this.subscription)
	        .subscribe(appointment => { 
	        	this.landlords = appointment.filter((data:any) => data.landlord._id == this.userID && (data.status == 'archived' || data.status == 'rejected'))
	        	this.tenants = appointment.filter((data:any) => data.tenant._id == this.userID && (data.status == 'archived' || data.status == 'rejected'))
		        // this.agreementService.getAgreementLoi(this.userID, this.userID)
			       //  .subscribe(agreement => { 
			       //  	console.log(agreement)
			       //  	// this.landlords = appointment.filter((data:any) => data.landlord._id == this.userID && (data.status == 'archived' || data.status == 'rejected'))
			       //  	// this.tenants = appointment.filter((data:any) => data.tenant._id == this.userID && (data.status == 'archived' || data.status == 'rejected'))
			       //  	// console.log(this.landlords)
			       //  	// console.log(this.tenants)
			       //  });
	        });

        });
	}

	delete(id:string){
		this.appointmentService.delete(id)
		.then(
			response => {
				this.router.navigate(['/dashboard/'+ id +'archived']);
			},
			error => {
				console.log(error)
			}
		);
	}

	proposeNew(id:string){
		this.router.navigate(['/appointment/'+ id +'/initiate-view']);
	}

	initiateLOI(agreement_id:string, appointment_id:string){
		this.agreementService.postAgreementLoi(appointment_id)
		.then(
			response => {
				this.router.navigate(['/loi/'+ agreement_id +'/initiate']);
			},
			error => {
				this.notificationsService.error(
					'Error',
					'Create agreement failed'
				)
				console.log(error)
			}
		);
	}

	ngOnDestroy() {
		this.subscription = false;
	}

	messageUser(id:string){
		this.appointmentService.getById(id)
		.takeWhile(()=> this.subscription)
        .subscribe(data => {
        	if (typeof data.room_id !== 'undefined') {
                let roomParams = { property_id: data.property._id };
                this.chatService.createRoom(roomParams)
                .then((res: any) => {
                    this.router.navigate([ '/chat/' + res.room_id ]);
                }).catch((err: any) => {
                    console.log(err);
                    // this.notificationsService.error('Error', 'Failed to create room');
                })
            }else{
                this.router.navigate([ '/chat/' + data.room_id.room_id ]);
            }
       })
    }
}
