import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { PropertyService } from '../../../services/property.service';
import { UserService } from '../../../services/user.service';
import "rxjs/add/operator/takeWhile";

@Component({
    selector: 'proposed',
    templateUrl: `client/components/member-section/listing/listing.html`,
})

export class ListingComponent implements OnInit {
    subscription: boolean = true;
    isRequest: boolean;
    appointments: any[] = [];
    property: any;
    landlords: any[] = [];
    tenants: any[] = [];
    user: any

    constructor(
        private propertyService: PropertyService,
        private userService: UserService,
        private router: Router,
        ) { }

    ngOnInit(): void {
        this.loadAllProperties()
        this.isRequest = true;

    }

    private loadAllProperties() {
        this.userService.getByToken()
        .takeWhile(()=> this.subscription)
        .subscribe((user: any) => {
            this.user = user;
            this.landlords = this.user.owned_properties.filter((data: any) => data.status != 'draft');
            this.tenants = this.user.rented_properties.filter((data: any) => data.status != 'draft');
        })
    }

    edit(id: any) {
        this.router.navigate(['/property', id, 'edit']);
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
