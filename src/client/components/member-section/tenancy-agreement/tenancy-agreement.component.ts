import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AgreementService } from '../../../services/agreement.service';
import { UserService } from '../../../services/user.service';
import "rxjs/add/operator/takeWhile";
import * as moment from 'moment'

@Component({
  selector: 'tenancy-agreement',
  templateUrl: `client/components/member-section/tenancy-agreement/tenancy-agreement.html`,
})
export class TenancyAgreementComponent implements OnInit{ 
    subscription: boolean = true;
	isAll:boolean;
	isAction:boolean;
	isPending:boolean;
	isArchived:boolean;
	agreements: any[] = [];
	agreement: any;
	action: any[] = [];
	pending: any[] = [];
    archived: any[] = [];
	userID: string;

	constructor(
        private agreementService:AgreementService,
		private userService:UserService,
		private router: Router,
		){}

	ngOnInit():void{
		this.isAll = true;
		this.loadAllAgreements()
	}

	private loadAllAgreements() {
        this.userService.getByToken()
        .takeWhile(()=> this.subscription)
        .subscribe(user => { 
            this.userID = user._id;
        });
        this.agreementService.getByUser()
        .takeWhile(()=> this.subscription)
        .subscribe(agreements => { 
        	for (let z = 0; z < agreements.length; ++z) {
                if(agreements[z].tenancy_agreement.data) {
                    console.log(agreements[z].tenancy_agreement.data)
                    var a = new Date(agreements[z].tenancy_agreement.data.created_at)
                    agreements[z].tenancy_agreement.data.created_at = moment(a).format('DD/MM/YYYY HH:mm')
                    this.agreements.push(agreements[z])
                    this.action = this.agreements.filter(agreements => agreements.tenancy_agreement.data.status == 'pending');
		            this.pending = this.agreements.filter(agreements =>  agreements.tenancy_agreement.data.status == 'admin-confirmation');
		            this.archived = this.agreements.filter(agreements => agreements.tenancy_agreement.data.status == 'accepted')
                }
            }
        });
    }

    view(agreement: any){
        this.router.navigate(['/ta/'+ agreement._id +'/view']);
    }

    createIL(agreement: any){
        this.router.navigate(['/inventory/', agreement._id]);
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
