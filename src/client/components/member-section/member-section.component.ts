import { Component, OnDestroy, trigger, transition, style, animate } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'member-section',
  templateUrl: `./member-section.html`,
  animations: [
    trigger(
      'enterAnimation', [
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(-100%)', opacity: 1}))
        ]),
        transition(':enter', [
          style({transform: 'translateX(-100%)', opacity: 1}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 0}))
        ])
      ]
    )
  ],
})
export class MemberSectionComponent { 
	subscription: boolean = true;
	id: string;
	show: boolean = true;
	constructor(
		private router: Router,
		private route: ActivatedRoute,
	){}

	ngOnInit(): void{
		this.route.params.takeWhile(()=>this.subscription).subscribe(params => {
			this.id = params['id'];
		});
	}
	toogle() {
		if (this.show == false) {
			this.show = true;
		}
		else {
			this.show = false;
		}
	}

	ngOnDestroy() {
		this.subscription = false;
	}
}
