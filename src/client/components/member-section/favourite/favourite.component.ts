import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { PropertyService } from '../../../services/property.service';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'favourite',
  templateUrl: `client/components/member-section/favourite/favourite.html`,
})
export class FavouriteComponent implements OnInit{ 
    subscription: boolean = true;
    isRequest:boolean;
    appointments: any[] = [];
    property: any;
    favProperties: any[] = [];
    user:any;
    id:string;

    constructor(
        private userService:UserService,
        private propertyService:PropertyService,
        private router: Router,
        ){}

    ngOnInit():void{
        this.userService.getByToken()
        .takeWhile(()=> this.subscription)
        .subscribe((user:any) => {
            this.user = user;
            this.favProperties = this.user.shortlisted_properties
        }) 
    }

	unshortlistProperty(id:string){
        this.id = id
        this.propertyService.unshortlist(id)
        .then(
            response => {
                this.ngOnInit()
            },
            error => {
                this.ngOnInit()
                console.log(error, 'error')
            }
        );
    }

    view(id:string){
        if (!this.id && this.id === null && this.id === '') {
            this.router.navigate(['property/' + id + '/view'])
        }
        if (this.id) {
            this.id = null
        }
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
