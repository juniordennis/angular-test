import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AgreementService } from '../../../services/agreement.service';
import { UserService } from '../../../services/user.service';
import "rxjs/add/operator/takeWhile";
import * as moment from 'moment'

@Component({
  selector: 'letter-of-intent',
  templateUrl: `client/components/member-section/letter-of-intent/letter-of-intent.html`,
})
export class LetterOfIntentComponent implements OnInit{ 
    subscription: boolean = true;
	isAll:boolean;
	isAction:boolean;
	isPending:boolean;
	isArchived:boolean;
	agreements: any[] = [];
	agreement: any;
	action: any[] = [];
	pending: any[] = [];
	archived: any[] = [];
	userID: string;

	constructor(
        private agreementService:AgreementService,
        private router: Router,
        private route: ActivatedRoute,
		private userService: UserService,
		){}

	ngOnInit():void{
        this.userService.getByToken()
        .takeWhile(()=> this.subscription)
        .subscribe(user => { 
            this.userID = user._id;
        });
		this.isAll = true;
		this.loadAllAgreements()
	}

	private loadAllAgreements() {
        this.agreementService.getByUser()
        .takeWhile(()=> this.subscription)
        .subscribe(agreements => { 
            for (let z = 0; z < agreements.length; ++z) {
                if(agreements[z].letter_of_intent.data.status && agreements[z].letter_of_intent.data.status != 'draft') {
                    var a = new Date(agreements[z].letter_of_intent.data.created_at)
                    agreements[z].letter_of_intent.data.created_at = moment(a).format('DD/MM/YYYY HH:mm')
                    this.agreements.push(agreements[z])
                    this.action = this.agreements.filter(agreements => agreements.letter_of_intent.data.status == 'payment-confirmed' && 
                                                                        agreements.landlord._id == this.userID);
                    this.pending = this.agreements.filter(agreements =>  agreements.letter_of_intent.data.status == 'pending' || 
                                                                    (agreements.letter_of_intent.data.status == 'payment-confirmed'));
                    this.archived = this.agreements.filter(agreements => agreements.letter_of_intent.data.status == 'accepted');
                }
            }
        });
    }

    view(agreement: any){
        this.router.navigate(['/loi/'+ agreement._id +'/view']);
    }

    initiateLOI(agreement: any){
        this.router.navigate(['/loi/'+ agreement._id +'/initiate']);
    }

    initiateTA(agreement: any){
        this.router.navigate(['/ta/'+ agreement._id +'/initiate']);
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}
