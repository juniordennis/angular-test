import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { AppointmentService } from '../../../services/appointment.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { NotificationsService } from 'angular2-notifications';
import { TooltipDirective } from 'ng2-bootstrap';
import * as $ from 'jquery';
import 'fullcalendar';
import {Options} from "fullcalendar";
import "rxjs/add/operator/takeWhile";
import * as moment from 'moment'

@Component({
  selector: 'upcoming',
  templateUrl: `client/components/member-section/upcoming/upcoming.html`,
})
export class UpcomingComponent implements OnInit{ 
    @ViewChild('pop') public pop: TooltipDirective;
    subscription: boolean = true;
	events: any[] = [];
    header: any;
    dialogVisible: boolean = false;
    idGen: number = 100;
	isCalenderView:boolean;
	event: MyEvent;
    today = Date.now();
    user:any;
    id:any;
    appointments:any
	constructor( 
        private cd: ChangeDetectorRef,
        private router: Router,
        private appointmentService:AppointmentService,
        private notificationsService: NotificationsService,
        private userService:UserService) { }

	ngOnInit():void{
        this.loadAppointment();
		this.isCalenderView = true;
        this.header = {
			right: 'prev,next today',
			left: 'title',
			center: ''
		};
	}

	handleDayClick(event:any) {
        this.cd.detectChanges();
    }

    handleEventClick(e:any) {
        this.event = new MyEvent();
        this.event.title = e.calEvent.title;
        let start = e.calEvent.start;
        if(e.view.name === 'month') {
            start.stripTime();
        }
        this.event.data = e.calEvent.data;
        this.event.start = start.format();
        this.event.allDay = e.calEvent.allDay;
        // this.dialogVisible = true;
        this.pop.show()
    }

    // eventRender(event:any, element:any) {
    //     console.log(element)
    //       $(element).tooltip({title: event.data});             
    // }

    loadAppointment(){
        this.userService.getByToken()
        .takeWhile(()=> this.subscription)
        .subscribe((user:any) => {
            this.user = user;
            this.appointmentService.getOwn()
            .takeWhile(()=> this.subscription)
            .subscribe(appointments => { 
                this.appointments = appointments.filter((data:any) => data.status == 'accepted')
                for (let a = 0; a < this.appointments.length; ++a) {
                    if (this.appointments[a].tenant._id == this.user._id) {
                        this.events.push({
                            title: this.appointments[a].chosen_time.from + '-' + this.appointments[a].chosen_time.to,
                            start: moment(this.appointments[a].chosen_time.date, "DD-MM-YYYY").format('YYYY-MM-DD'),
                            color: '#e98b39',
                            data: this.appointments[a],
                            allDay: true
                        });
                    }else{
                        this.events.push({
                            title: this.appointments[a].chosen_time.from + '-' + this.appointments[a].chosen_time.to,
                            start: moment(this.appointments[a].chosen_time.date, "DD-MM-YYYY").format('YYYY-MM-DD'),
                            data: this.appointments[a],
                            allDay: true
                        });
                    }
                }
            });
        }) 
    }

    view(id:string){
        this.router.navigate([ '/dashboard/upcoming/'+ id]);
    }

    archive(id:string){
        this.appointmentService.archive(id)
        .then(
               res => {
                this.notificationsService.success(
                    'Success',
                    'Appointment archived',
                )
                this.ngOnInit()
            },
            err => {
                this.notificationsService.error(
                    'Error',
                    'Archive appointment fail',
                )
                console.log(err)
            }
        );
    }

    delete(id:string){
        this.appointmentService.delete(id)
        .then(
               res => {
                this.notificationsService.success(
                    'Success',
                    'Appointment deleted',
                )
                this.ngOnInit()
            },
            err => {
                this.notificationsService.error(
                    'Error',
                    'Delete appointment fail',
                )
                console.log(err)
            }
        );
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}

export class MyEvent {
    title: string;
    start: string;
    data: any;
    allDay: boolean = true;
}
