import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { AppointmentService } from '../../../services/appointment.service';
import { UserService } from '../../../services/user.service';
import { NotificationsService } from 'angular2-notifications';
import "rxjs/add/operator/takeWhile";
import * as moment from 'moment'

@Component({
  selector: 'upcoming-detail',
  templateUrl: `client/components/member-section/upcoming/upcoming-detail.html`,
})

export class UpcomingDetailComponent { 
	subscription: boolean = true;
	id:string;
	appointment:any;
	user:any;

	constructor( 
        private cd: ChangeDetectorRef,
        private router: Router,
        private route: ActivatedRoute,
        private notificationsService: NotificationsService,
        private appointmentService:AppointmentService,
        private userService:UserService) { }

	ngOnInit():void{
        this.loadAppointment();
	}

	loadAppointment(){
		this.route.params.subscribe(params => {
			this.id = params['id'];
			this.userService.getByToken()
			.subscribe(data => {
				this.user = data
				this.appointmentService.getById(this.id)
				.takeWhile(()=> this.subscription)
		        .subscribe(appointment => { 
		            this.appointment = appointment;
		            //  for (var i = 0; i < this.appointment.length; ++i) {
		            // 	this.appointment[i].chosen_time.date = moment(this.appointment[i].chosen_time.date).format('dddd, DD/MM/YYYY')
		            // }
		        }, err => {
		        	console.log(err, 'err')
		        });
			})
		});
    }

    back(){
    	this.router.navigate([ '/dashboard/upcoming' ]);
    }

    archive(id:string){
		this.appointmentService.archive(id)
	    .then(
	       	res => {
				this.notificationsService.success(
		        	'Success',
		        	'Appointment archived',
		        )
		        this.router.navigate([ '/dashboard/upcoming' ]);
			},
			err => {
				this.notificationsService.error(
			        'Error',
			        'Archive appointment fail',
			    )
				console.log(err)
			}
	    );
    }

    delete(id:string){
		this.appointmentService.delete(id)
	    .then(
	       	res => {
				this.notificationsService.success(
		        	'Success',
		        	'Appointment deleted',
		        )
		        this.router.navigate([ '/dashboard/upcoming' ]);
			},
			err => {
				this.notificationsService.error(
			        'Error',
			        'Delete appointment fail',
			    )
				console.log(err)
			}
	    );
    }

    ngOnDestroy() {
    	this.subscription = false;
    }
}