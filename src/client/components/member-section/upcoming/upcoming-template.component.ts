import { Component } from '@angular/core';

@Component({
  selector: 'upcoming-template',
  template: 
  `
  <div class="dashboard-appointments">
      <h3 class="align-member-section">Upcoming Appointments</h3>

      <router-outlet></router-outlet>
  </div>
  `,
})
export class UpcomingTemplateComponent { 

}