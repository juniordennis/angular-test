import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AppointmentService } from '../../../services/appointment.service';
import { UserService } from '../../../services/user.service';
import { NotificationsService } from 'angular2-notifications';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../../my-ddp-client';
import { StorageService } from '../../../services/storage.service';
import { SharedService } from '../../../services/shared.service';
import { ChatService } from '../../../services/chat.service';
import { NavbarComponent } from '../../navbar/navbar.component'
import "rxjs/add/operator/takeWhile";
import * as moment from 'moment'

@Component({
  selector: 'proposed',
  templateUrl: `client/components/member-section/proposed/proposed.html`,
})

export class ProposedComponent implements OnInit{ 
	subscription: boolean = true;
	isRequest:boolean;
	appointments: any[] = [];
	appointment: any;
	requests: any[] = [];
	proposes: any[] = [];
	user:any;
    public ddp: MyDDPClient;
    public loading: boolean[] = [];
    public loadingAccept: boolean[] = [];
    public loadingReject: boolean[] = [];
    public dreamtalkReady: boolean = false;

	constructor(
		private appointmentService:AppointmentService,
		private userService:UserService,
		private router: Router,
		private notificationsService: NotificationsService,
        private sharedService: SharedService,
        private storageService: StorageService,
        private chatService: ChatService,
        private navbarComponent: NavbarComponent,
		){}

	ngOnInit():void{
		this.loadAllAppointments()
		this.isRequest = true;

	}

	private loadAllAppointments() {
		this.userService.getByToken()
		.takeWhile(()=> this.subscription)
		.subscribe((user:any) => {
			this.appointmentService.getOwn()
			.takeWhile(()=> this.subscription)
	        .subscribe(appointments => { 
	            this.appointments = appointments;
	            this.requests = this.appointments.filter(data => data.landlord._id == user._id && data.status == "pending");
	            for (var i = 0; i < this.requests.length; ++i) {
	            	this.loadingAccept.push(false)
	            	this.loadingReject.push(false)
	            }
	            this.proposes = this.appointments.filter(data => data.tenant._id == user._id && data.status == "pending");
	            for (var i = 0; i < this.proposes.length; ++i) {
	            	this.loading.push(false)
	            }
	            this.sharedService.ddp$.subscribe((ddp: MyDDPClient) => {
					this.ddp = ddp;
				}, (error: any) => {
					console.log('Error ddp', error);
				})
				this.storageService.get('dreamtalk.ready').subscribe((ready: boolean) => { this.dreamtalkReady = ready });
	        });
		})
    }

    accept(id:string, index:number){
    	this.loadingAccept[index] = true
		this.appointmentService.accept(id)
	    .then(
	       	res => {
				this.notificationsService.success(
		        	'Success',
		        	'Appointment accepted',
		        )
		        this.appointmentService.getById(id)
				.takeWhile(()=> this.subscription)
		        .subscribe(data => {
	                if (this.dreamtalkReady) {
	                    if (this.ddp) {
	                        if (!data.room_id) {
	                            let roomParams = { property_id: data.property._id };
	                            this.chatService.createRoom(roomParams)
	                            .then((res: any) => {
	                                let roomId = res.data && res.data.room_id ? res.data.room_id : undefined;
	                                let chatParams: any = {
	                                    roomId: roomId,
	                                    message: '-',
	                                    extra: { 
	                                        ref : 'appt_confirm',
	                                        ref_id: id,
	                                    }
	                                 };
	                                this.ddp.sendChat(chatParams).then((res: any) => {
	                                    this.loadingAccept[index] = false
	                                    // this.notificationsService.success('Success', 'Message has been delivered.');
	                                }).catch((error: any) => {
	                                    console.log(error);
	                                    this.loadingAccept[index] = false
	                                    // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
	                                })
	                            }).catch((err: any) => {
	                                console.log(err);
	                                // this.notificationsService.error('Error', 'Failed to create room');
	                            })
	                        }else{
	                            let chatParams: any = {
	                                roomId: data.room_id.room_id,
	                                message: '-',
	                                extra: { 
	                                    ref : 'appt_confirm',
	                                    ref_id: id,
	                                }
	                            };
	                            this.ddp.sendChat(chatParams).then((res: any) => {
	                                this.loadingAccept[index] = false
	                                // this.notificationsService.success('Success', 'Message has been delivered.');
	                            }).catch((error: any) => {
	                                console.log(error);
	                                this.ddp.sendChat(chatParams).then((res: any) => {
	                                    this.loadingAccept[index] = false
	                                    // this.notificationsService.success('Success', 'Message has been delivered.');
	                                }).catch((error: any) => {
	                                    console.log(error);
	                                    this.loadingAccept[index] = false
	                                    // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
	                                })
	                                // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
	                            })
	                        }
	                    } else {
	                        // no ddp
	                        this.loadingAccept[index] = false
	                    }
	                } else {
	                    // dreamtalk not ready
	                    this.loadingAccept[index] = false
	                    // this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
	                }
	            })
		        this.ngOnInit()
			},
			err => {
				this.notificationsService.error(
			        'Error',
			        'Accept appointment fail',
			    )
				console.log(err)
			}
	    );
    }

    reject(id:string, index:number){
	    this.loadingReject[index] = true
		this.appointmentService.accept(id)
	    .then(
	        res => {
				this.notificationsService.success('Success','Appointment rejected')
			    this.appointmentService.getById(id)
				.takeWhile(()=> this.subscription)
		        .subscribe(data => {
	                if (this.dreamtalkReady) {
	                    if (this.ddp) {
	                        if (!data.room_id) {
	                            let roomParams = { property_id: data.property._id };
	                            this.chatService.createRoom(roomParams)
	                            .then((res: any) => {
	                                let roomId = res.data && res.data.room_id ? res.data.room_id : undefined;
	                                let chatParams: any = {
	                                    roomId: roomId,
	                                    message: '-',
	                                    extra: { 
	                                        ref : 'appt_reject',
	                                        ref_id: id,
	                                    }
	                                  };
	                                this.ddp.sendChat(chatParams).then((res: any) => {
	                                    this.loadingReject[index] = false
	                                    // this.notificationsService.success('Success', 'Message has been delivered.');
	                                }).catch((error: any) => {
	                                    console.log(error);
	                                    this.loadingReject[index] = false
	                                    // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
	                                })
	                            }).catch((err: any) => {
	                                console.log(err);
	                                this.loadingReject[index] = false
	                                // this.notificationsService.error('Error', 'Failed to create room');
	                            })
	                        }else{
	                            let chatParams: any = {
	                                roomId: data.room_id.room_id,
	                                message: '-',
	                                extra: { 
	                                    ref : 'appt_reject',
	                                    ref_id: id,
	                                }
	                              };
	                            this.ddp.sendChat(chatParams).then((res: any) => {
	                                this.loadingReject[index] = false
	                                // this.notificationsService.success('Success', 'Message has been delivered.');
	                            }).catch((error: any) => {
	                                console.log(error);
	                                this.ddp.sendChat(chatParams).then((res: any) => {
	                                    this.loadingReject[index] = false
	                                    // this.notificationsService.success('Success', 'Message has been delivered.');
	                                }).catch((error: any) => {
	                                    console.log(error);
	                                    this.loadingReject[index] = false
	                                    // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
	                                })
	                                // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
	                            })
	                        }
	                    } else {
	                        // no ddp
	                    }
	                } else {
	                    // dreamtalk not ready
	                    // this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
	                }
		        	this.ngOnInit()
	            })
			},
			err => {
				this.notificationsService.error(
			        'Error',
			        'Reject appointment fail',
			    )
				console.log(err)
			}
	    );
    }

    cancel(id:string, index:number){
	    this.loading[index] = true
		this.appointmentService.delete(id)
	    .then(
	        res => {
				this.notificationsService.success('Success','Appointment request canceled')
			    this.appointmentService.getById(id)
				.takeWhile(()=> this.subscription)
		        .subscribe(data => {
	                if (this.dreamtalkReady) {
	                    if (this.ddp) {
	                        if (!data.room_id) {
	                            let roomParams = { property_id: data.property._id };
	                            this.chatService.createRoom(roomParams)
	                            .then((res: any) => {
	                                let roomId = res.data && res.data.room_id ? res.data.room_id : undefined;
	                                let chatParams: any = {
	                                    roomId: roomId,
	                                    message: '-',
	                                    extra: { 
	                                        ref : 'appt_cancel',
	                                        ref_id: id,
	                                    }
	                                  };
	                                this.ddp.sendChat(chatParams).then((res: any) => {
	                                    this.loading[index] = false
	                                    // this.notificationsService.success('Success', 'Message has been delivered.');
	                                }).catch((error: any) => {
	                                    console.log(error);
	                                    // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
	                                })
	                            }).catch((err: any) => {
	                                console.log(err);
	                                // this.notificationsService.error('Error', 'Failed to create room');
	                            })
	                        }else{
	                            let chatParams: any = {
	                                roomId: data.room_id.room_id,
	                                message: '-',
	                                extra: { 
	                                    ref : 'appt_cancel',
	                                    ref_id: id,
	                                }
	                              };
	                            this.ddp.sendChat(chatParams).then((res: any) => {
	                                this.loading[index] = false
	                                // this.notificationsService.success('Success', 'Message has been delivered.');
	                            }).catch((error: any) => {
	                                console.log(error);
	                                this.ddp.sendChat(chatParams).then((res: any) => {
	                                    this.loading[index] = false
	                                    // this.notificationsService.success('Success', 'Message has been delivered.');
	                                }).catch((error: any) => {
	                                    console.log(error);
	                                    // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
	                                })
	                                // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
	                            })
	                        }
	                    } else {
	                        // no ddp
	                    }
	                } else {
	                    // dreamtalk not ready
	                    // this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
	                }
			        this.ngOnInit()
	            })
			},
			err => {
				this.notificationsService.error(
			        'Error',
			        'Cancel appointment request fail',
			    )
				console.log(err)
			}
	    );
    }

    messageUser(id:string){
		this.appointmentService.getById(id)
		.takeWhile(()=> this.subscription)
        .subscribe(data => {
        	if (typeof data.room_id !== 'undefined') {
                let roomParams = { property_id: data.property._id };
                this.chatService.createRoom(roomParams)
                .then((res: any) => {
                    this.router.navigate([ '/chat/'+res.data.room_id]);
                }).catch((err: any) => {
                    console.log(err);
                    // this.notificationsService.error('Error', 'Failed to create room');
                })
            }else{
                this.router.navigate(['/chat/'+data.room_id.room_id]);
            }
       })
    }

    ngOnDestroy() {
    	this.subscription = false;
    }
}
