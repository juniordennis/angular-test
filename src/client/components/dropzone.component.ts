import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Http } from '@angular/http';
import * as Dropzone from 'dropzone';


@Component({
    selector: 'dropzone',
    template: ``,
    styles: ['']
})

/*Dropzone for file uploads*/
export class DropzoneComponent implements OnInit{

    private _dropzone:Dropzone;

    @Input()
    public class:string = ".dropzoneArea";

    @Input()
    public id:string = "dropzone";

    @Input()
    element:string = ".dropzoneArea";

    @Input()
    url:string = "file/post";

    @Input()
    headers:string = "";

    @Input()
    uploadMultiple:boolean = false;

    @Input()
    maxFileSize:number = 2048;

    @Input()
    clickable:string = ".dropzoneArea";

    @Input()
    autoProcessQueue:boolean = true;

    @Input()
    addRemoveLinks:boolean = true;

    @Input()
    createImageThumbnails:boolean = true;

    @Input()
    previewTemplate:string = "<div class='dz-preview dz-file-preview'><div class='dz-details'><img data-dz-thumbnail/></div></div>";

    @Input()
    acceptedFiles:string = "*";

    @Input()
    message:string = "Drop files here to upload";

    @Output()
    sending:EventEmitter<boolean>;

    @Output()
    uploadprogress:EventEmitter<number>;

    @Output()
    addedfile:EventEmitter<any>;

    @Output()
    removedfile:EventEmitter<any>;

    @Output()
    success:EventEmitter<any>;

    @Output()
    error:EventEmitter<any>;

    constructor(private _eleRef:ElementRef, private _http:Http){
        this.sending = new EventEmitter<boolean>();
        this.uploadprogress = new EventEmitter<number>();
        this.success = new EventEmitter<any>();
        this.error = new EventEmitter<any>();
        this.addedfile = new EventEmitter<any>();
        this.removedfile = new EventEmitter<any>();
    }

    initDropzone() {
      let self:DropzoneComponent = this;
      this._dropzone = new Dropzone(this.element, {
          url: this.url,
          maxFiles: 1,
          uploadMultiple: Boolean(this.uploadMultiple),
          maxFilesize: Number(this.maxFileSize),
          clickable: this.clickable,
          headers: this.headers,
          autoProcessQueue: Boolean(this.autoProcessQueue),
          addRemoveLinks: Boolean(this.addRemoveLinks),
          createImageThumbnails: this.createImageThumbnails,
          // previewTemplate: this.previewTemplate,
          acceptedFiles: this.acceptedFiles,
          dictDefaultMessage: this.message,
          // params: { _csrf: body._csrf }
      });
      this._dropzone.on("addedfile", function(file:any) {
        self.addedfile.emit(file);
      });

      this._dropzone.on("removedfile", function(file:any) {
        self.removedfile.emit(file);
      });

      this._dropzone.on("sending", (file:any, xhr:any, formatData:any)=> {
        this.sending.emit(true);
      });

      this._dropzone.on("uploadprogress", (file:any, progress:any, bytesSent:any)=> {
        this.uploadprogress.emit(progress);
      });

      this._dropzone.on("success", (file:any, response:any)=> {
        file.uploadedId = response.idAtt[0];
        file.uploadedUrl = response.urlAtt[0];
        let event = { file: file, res: response };
        this.success.emit(event);
      });

      this._dropzone.on("error", (file:any, message:any)=> {
        this.error.emit({message : 'Failed uploading image.'});
      });
    }

    ngOnInit(){
      this.initDropzone();
    }
}
