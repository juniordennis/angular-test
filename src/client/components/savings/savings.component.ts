import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'savings',
  templateUrl: './savings.html',
})

export class SavingsComponent implements OnInit {
	model:any = {};
	marketPractice: any;
	staysmart: any;
	savings: any;
	valid: boolean;
	output:any = {};

	constructor(
		private notificationsService: NotificationsService,
		){
		this.model = {
            for: "landlord",
            lease: "6"
        };
	}

	ngOnInit(){
		
	}

	changeCalculate(){
		if(this.model.for && this.model.lease && this.model.rental) {
			this.calculate();
		}
	}

	calculate(){
		this.valid = !isNaN(this.model.rental);
		if(this.model.rental > 0  && this.model.lease && this.valid && this.model.for) {
			if(this.model.for == 'landlord') {
				if(this.model.lease == 6) {
					this.marketPractice = this.model.rental / 2;
					this.staysmart = (this.model.rental / 4) * 10 / 100;
				} else if(this.model.lease == 12) {
					this.marketPractice = this.model.rental / 2;
					this.staysmart = (this.model.rental / 2) * 10 / 100;
				} else if(this.model.lease == 24) {
					this.marketPractice = this.model.rental;
					this.staysmart = this.model.rental * 10 / 100;
				}
				this.savings = this.marketPractice - this.staysmart;

			} else if (this.model.for == 'tenant') {
				if(this.model.lease == 6 || this.model.lease == 12) {
					this.marketPractice = this.model.rental / 2;
				} else if(this.model.lease == 24) {
					if(this.model.rental >= 4000) {
						this.marketPractice = 0;
					} else {
						this.marketPractice = this.model.rental;
					}
				}
				this.staysmart = 0;
				this.savings = this.marketPractice - this.staysmart;
			}
		} else {
			this.notificationsService.error(
				'',
				'Monthly Rental value is wrong',
				)
		}
	}
}
