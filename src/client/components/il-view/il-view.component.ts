import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { AgreementService } from '../../services/agreement.service';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SignatureComponent } from '../signature.component';
import { NotificationsService } from 'angular2-notifications';
import { url } from '../../../global';
import { AttachmentService } from '../../services/attachment.service';
import { MyDDPClient, MY_DDP_COLLECTIONS } from '../../my-ddp-client';
import { StorageService } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import { ChatService } from '../../services/chat.service';
import { NavbarComponent } from '../navbar/navbar.component';
import { ModalDirective } from 'ng2-bootstrap';
import * as Dropzone from 'dropzone';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'inventory-add',
  templateUrl: './il-view.html',
})

export class IlViewComponent implements OnInit {
    @ViewChild('modalAttachment') public modalAttachment: ModalDirective;
    @ViewChild(SignatureComponent) signatureComponent: SignatureComponent;
    @ViewChild('customDropzone') customDropzone: Dropzone;
    subscription: boolean = true;
    id: string;
    add: boolean = false;
	isStep:number;
    model:any = {};
    modelTenant:any = {}
    user_id:any;
    agreement:any = [];
    tenant:any;
    landlord:any;
    date = Date.now();
	public myForm: FormGroup;
    isCollapsed:boolean = false;
    name: any[] = [];
    valid: any[] = [];
    selectedValue: any[] = [];
    idList: any[] = [];
    status: any;
    header: any;
    link: any;
    attachmentId: any[] = [];
    attachmentUrl: any[] = [];
    public list: number;
    public item: number;
    modelUpdate: any[] = []
    public ddp: MyDDPClient;
    public loading: boolean = false;
    public dreamtalkReady: boolean = false;

    constructor(
        private _fb: FormBuilder, 
        private agreementService:AgreementService, 
        private attachementService: AttachmentService,
        private userService:UserService,
        private router:Router,
        private route:ActivatedRoute,
        private notificationsService: NotificationsService,
        private storageService: StorageService,
        private sharedService: SharedService,
        private chatService: ChatService,
        private navbarComponent: NavbarComponent,
        ) { }

    ngOnInit() {
        this.storageService.get('dreamtalk.ready').takeWhile(()=> this.subscription).subscribe((ready: boolean) => { this.dreamtalkReady = ready });
        this.sharedService.ddp$.takeWhile(()=> this.subscription).subscribe((ddp: MyDDPClient) => {
            this.ddp = ddp;
        }, (error: any) => {
            console.log('Error ddp', error);
        })
        this.link = url+'attachments';
        let currentUser = JSON.parse(localStorage.getItem('authToken'));
        if (currentUser && currentUser.token) {
            this.header = {'Authorization': 'Bearer ' + currentUser.token}
        }
        this.modelTenant.lists = []
        this.myForm = this._fb.group({
            lists: this._fb.array([]),
        });
        this.route.params
        .takeWhile(()=> this.subscription)
        .subscribe(params => {
            this.id = params['id'];
            this.userService.getByToken()
            .takeWhile(()=> this.subscription)
            .subscribe(user => {
                this.user_id = user._id;
                this.agreementService.getById(this.id)
                .takeWhile(()=> this.subscription)
                .subscribe(agreement => {
                    this.agreement = agreement
                    if(agreement.inventory_list.data.status == 'completed' ) {
                        this.status = agreement.inventory_list.data
                        console.log(this.status)
                    }else{
                        if(agreement.landlord._id == user._id) {
                            this.landlord = agreement
                            if(agreement.inventory_list.data.lists.length > 0) {
                                this.model = agreement.inventory_list.data;
                                this.myForm = this._fb.group({
                                    lists: this._fb.array([]),
                                });
                                for (let z = 0; z < agreement.inventory_list.data.lists.length; ++z) {
                                    this.name.push(agreement.inventory_list.data.lists[z].name)
                                    this.addLists()
                                    for (let x = 0; x < agreement.inventory_list.data.lists[z].items.length; ++x) {
                                        this.addItem(z)
                                        if (agreement.inventory_list.data.lists[z].items[x].attachments != null) {
                                            for (let c = 0; c < agreement.inventory_list.data.lists[z].items[x].attachments.length; ++c) {
                                                this.attachmentUrl[z][x].push(agreement.inventory_list.data.lists[z].items[x].attachments[c].url)
                                            }
                                        }
                                    }
                                }
                                this.myForm.controls['lists'].patchValue(agreement.inventory_list.data.lists, {onlySelf: true});
                            }
                        }else{
                            this.tenant = agreement
                            if(agreement.inventory_list.data.lists.length > 0) {
                                this.model = agreement.inventory_list.data
                            };
                        }
                    } 
                    this.isStep = 1;
                    this.isCollapsed = !this.isCollapsed
                })
            });
        });
    }

    initlist() {
        return this._fb.group({
            name: ['', Validators.required],
            items: this._fb.array([]),
        });
    }

    initItem() {
        return this._fb.group({
            name: ['', Validators.required],
            quantity: ['', Validators.required],
            remark: ['', Validators.required],
            attachments: [],
            landlord_check: [''],
        });
    }

    addLists() {
        const control = <FormArray>this.myForm.controls['lists'];
        control.push(this.initlist());
        for (let z = 0; z < control.length; ++z) {
            this.attachmentId.push([]);
            this.attachmentUrl.push([]);
        }
    }

    addItem(no:number) {
        const control = (<FormArray>this.myForm.controls['lists']).controls[no]['controls']['items'];
        control.push(this.initItem());
        for (let x = 0; x < control.length; ++x) {
            this.attachmentId[no].push([]);
            this.attachmentUrl[no].push([]);
        }
    }

    removeLists(i: number, name: string) {
        var i = this.name.indexOf(name)
        if(i != -1) {
            this.name.splice(i, 1);
        }
        const control = <FormArray>this.myForm.controls['lists'];
        control.removeAt(i);
        this.attachmentId.splice(i ,1)
        this.attachmentUrl.splice(i ,1)
    }

    removeItem(list: number, i: number) {
        const control = (<FormArray>this.myForm.controls['lists']).controls[list]['controls']['items'];
        control.removeAt(i);
        this.attachmentId[list].splice(i ,1)
        this.attachmentUrl[list].splice(i ,1)
    }

    getName(name:string){
        if(name == null || name=='') {
            this.notificationsService.error(
                '         ',
                'Please select area name!',
            )
        }else{
           this.isCollapsed = !this.isCollapsed
           this.name.push(name)

           this.addLists() 
        }
        
    }

    handleFileSuccess(event:any): void {
        // this.myForm.controls['lists']).controls[no]['controls']['items'].controls[no]['controls']['attachment'].value
        this.model.attachmentId = event.file.uploadedId;
        this.model.attachmentUrl = event.file.uploadedUrl;
        this.add = true;
    }

    handleFileFailure(event:any): void {
    }

    handleFileSending(event:any): void {
    }

    handleFileAdded(event:any): void {
    }

    handleFileRemoved(event:any): void {
    }

    next(value:any){
        if(this.model.created_at != null) {
            this.model.lists = value.lists
            this.modelUpdate = value
        }else{
            this.model = value
        }
        for (let z = 0; z < this.model.lists.length; ++z) {
            for (let x = 0; x < this.model.lists[z].items.length; ++x) {
                this.model.lists[z].items[x].attachments = this.attachmentId[z][x];
            }
        }
        for (let z = 0; z < this.model.lists.length; ++z) {
            if(this.model.lists[z].items.length > 0) {
                for (let x = 0; x < this.model.lists[z].items.length; ++x) {
                   if(this.model.lists[z].items[x].name == '' || this.model.lists[z].items[x].quantity == '') {
                        this.notificationsService.error(
                            ' ',
                            'Item name / quantity is empty!',
                        )
                    }else if(this.model.lists[z].items[x].landlord_check == false) {
                        this.notificationsService.alert(
                            ' ',
                            ('Please check item '+ this.model.lists[z].items[x].name +' confirmation!'),
                        )
                    }else{
                        this.valid.push(true)
                    }
                }
            }else{
                this.notificationsService.error(
                    '      ',
                    'Item is Empty!',
                )
            }
        }
        if(this.model.lists.length < 1) {
            this.notificationsService.error(
                '      ',
                "Can't Proceed, Inventory is Empty!",
            )
        }
        if(this.valid.length > 0) {
            var i = this.valid.indexOf(false)
            if(i == -1) {
                for (let b = 0; b < this.model.lists.length; ++b) {
                    this.model.lists[b].name = this.name[b]
                }
                this.isStep = this.isStep+1;
            }
        }
    }

    prev(){
        this.isStep = this.isStep-1;
    }

    create(){
        if(this.model.confirmation == null) {
            this.model.sign = this.signatureComponent.data;
            this.agreementService.createIL(this.id , this.model)
            .then(
                response => {
                    this.notificationsService.success('Success','Inventory list created')
                    this.router.navigate([ '/dashboard/inventory']);;
                    if (localStorage.getItem('authToken')) {
                        if (this.dreamtalkReady) {
                            if (this.ddp) {
                                if (!this.agreement.room_id) {
                                    let roomParams = { property_id: this.agreement.property._id };
                                    this.loading = true
                                    this.chatService.createRoom(roomParams)
                                    .then((res: any) => {
                                        let roomId = res && res.room_id ? res.room_id : undefined;
                                        let chatParams: any = {
                                            roomId: roomId,
                                            message: '-',
                                            extra: { 
                                                ref : 'inventory_create',
                                                ref_id: this.id,
                                            }
                                          };
                                        this.ddp.sendChat(chatParams).then((res: any) => {
                                            this.loading = false
                                            // this.notificationsService.success('Success', 'Message has been delivered.');
                                        }).catch((error: any) => {
                                            console.log(error);
                                            // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                        })
                                    }).catch((err: any) => {
                                        console.log(err);
                                        // this.notificationsService.error('Error', 'Failed to create room');
                                    })
                                }else{
                                    this.loading = true
                                    let chatParams: any = {
                                        roomId: this.agreement.room_id.room_id,
                                        message: '-',
                                        extra: { 
                                            ref : 'inventory_create',
                                            ref_id: this.id,
                                        }
                                      };
                                    this.ddp.sendChat(chatParams).then((res: any) => {
                                        this.loading = false
                                        // this.notificationsService.success('Success', 'Message has been delivered.');
                                    }).catch((error: any) => {
                                        console.log(error);
                                        this.ddp.sendChat(chatParams).then((res: any) => {
                                            this.loading = false
                                            // this.notificationsService.success('Success', 'Message has been delivered.');
                                        }).catch((error: any) => {
                                            console.log(error);
                                            // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                        })
                                        // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                    })
                                }
                            } else {
                                // no ddp
                            }
                        } else {
                            // dreamtalk not ready
                            // this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
                        }
                    } else {
                        this.navbarComponent.modalLogin.show()
                    }
                },
                error => {
                    this.notificationsService.error(
                        'Error',
                        'Create inventory list failed',
                    )
                    console.log(error)
                }
            );
        }else{
            this.agreementService.createIL(this.id , this.modelUpdate)
            .then(
                response => {
                    this.notificationsService.success(
                        'Success',
                        'Inventory list created',
                    )
                    this.router.navigate([ '/dashboard/inventory']);
                     if (localStorage.getItem('authToken')) {
                        if (this.dreamtalkReady) {
                            if (this.ddp) {
                                if (!this.agreement.room_id) {
                                    let roomParams = { property_id: this.agreement.property._id };
                                    this.loading = true
                                    this.chatService.createRoom(roomParams)
                                    .then((res: any) => {
                                        let roomId = res && res.room_id ? res.room_id : undefined;
                                        let chatParams: any = {
                                            roomId: roomId,
                                            message: '-',
                                            extra: { 
                                                ref : 'inventory_update',
                                                ref_id: this.id,
                                            }
                                          };
                                        this.ddp.sendChat(chatParams).then((res: any) => {
                                            this.loading = false
                                            // this.notificationsService.success('Success', 'Message has been delivered.');
                                        }).catch((error: any) => {
                                            console.log(error);
                                            // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                        })
                                    }).catch((err: any) => {
                                        console.log(err);
                                        // this.notificationsService.error('Error', 'Failed to create room');
                                    })
                                }else{
                                    this.loading = true
                                    let chatParams: any = {
                                        roomId: this.agreement.room_id.room_id,
                                        message: '-',
                                        extra: { 
                                            ref : 'inventory_update',
                                            ref_id: this.id,
                                        }
                                      };
                                    this.ddp.sendChat(chatParams).then((res: any) => {
                                        this.loading = false
                                        // this.notificationsService.success('Success', 'Message has been delivered.');
                                    }).catch((error: any) => {
                                        console.log(error);
                                        this.ddp.sendChat(chatParams).then((res: any) => {
                                            this.loading = false
                                            // this.notificationsService.success('Success', 'Message has been delivered.');
                                        }).catch((error: any) => {
                                            console.log(error);
                                            // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                        })
                                        // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                    })
                                }
                            } else {
                                // no ddp
                            }
                        } else {
                            // dreamtalk not ready
                            // this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
                        }
                    } else {
                        this.navbarComponent.modalLogin.show()
                    }
                },
                error => {
                    this.notificationsService.error(
                        'Error',
                        'Create inventory list failed',
                    )
                    console.log(error)
                }
            );
        }
        
        
    }

    getSelected(listId:string, itemId:string){
        var i = this.idList.indexOf(listId)
        if(i != -1) {
            for (let z = 0; z < this.modelTenant.lists.length; ++z) {
                if(this.modelTenant.lists[z].idList == listId) {
                    if(this.modelTenant.lists[z].idItems.length > 1) {
                        let same = this.modelTenant.lists[z].idItems.indexOf(itemId)
                        if(same != -1) {
                            this.modelTenant.lists[z].idItems.splice(same, 1);
                        }else{
                            this.modelTenant.lists[z].idItems.push(itemId)
                        }
                    }else{
                        let same = this.modelTenant.lists[z].idItems.indexOf(itemId)
                        if(same != -1) {
                            this.modelTenant.lists.splice(z, 1);
                            this.idList.splice(z, 1)
                        }else{
                            this.modelTenant.lists[z].idItems.push(itemId)
                        }
                    }
                }
            }
        }else{
            this.idList.push(listId)
            this.modelTenant.lists.push({
                'idList': listId,
                'idItems': [itemId]
            })
        }
    }

    createTenant(){
        this.loading = true
        this.modelTenant.sign = this.signatureComponent.data;
        this.agreementService.confirmIL(this.id , this.modelTenant)
        .then(
            response => {
                this.router.navigate([ '/dashboard/inventory']);
                this.notificationsService.success(
                    'Success',
                    'Inventory list checked',
                )
                if (localStorage.getItem('authToken')) {
                    if (this.dreamtalkReady) {
                        if (this.ddp) {
                            if (!this.agreement.room_id) {
                                let roomParams = { property_id: this.agreement.property._id };
                                this.loading = true
                                this.chatService.createRoom(roomParams)
                                .then((res: any) => {
                                    let roomId = res && res.room_id ? res.room_id : undefined;
                                    let chatParams: any = {
                                        roomId: roomId,
                                        message: '-',
                                        extra: { 
                                            ref : 'inventory_confirm',
                                            ref_id: this.id,
                                        }
                                      };
                                    this.ddp.sendChat(chatParams).then((res: any) => {
                                        this.loading = false
                                        // this.notificationsService.success('Success', 'Message has been delivered.');
                                    }).catch((error: any) => {
                                        console.log(error);
                                        // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                    })
                                }).catch((err: any) => {
                                    console.log(err);
                                    // this.notificationsService.error('Error', 'Failed to create room');
                                })
                            }else{
                                this.loading = true
                                let chatParams: any = {
                                    roomId: this.agreement.room_id.room_id,
                                    message: '-',
                                    extra: { 
                                        ref : 'inventory_confirm',
                                        ref_id: this.id,
                                    }
                                  };
                                this.ddp.sendChat(chatParams).then((res: any) => {
                                    this.loading = false
                                    // this.notificationsService.success('Success', 'Message has been delivered.');
                                }).catch((error: any) => {
                                    console.log(error);
                                    this.ddp.sendChat(chatParams).then((res: any) => {
                                        this.loading = false
                                        // this.notificationsService.success('Success', 'Message has been delivered.');
                                    }).catch((error: any) => {
                                        console.log(error);
                                        // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                    })
                                    // this.notificationsService.error('Error', 'Something went wrong while sending message. Please try again.');
                                })
                            }
                        } else {
                            // no ddp
                        }
                    } else {
                        // dreamtalk not ready
                        // this.notificationsService.error('Error', 'Can not send message yet, wait for the loading is done.');
                    }
                } else {
                    this.navbarComponent.modalLogin.show()
                }
            },
            error => {
                this.notificationsService.error(
                    'Error',
                    'Checked inventory list failed'
                )
                this.loading = false
                console.log(error)
            }
        );
    }

    addAttachment(list:number, item:number){
        this.attachmentId[list][item].push(this.model.attachmentId);
        this.attachmentUrl[list][item].push(this.model.attachmentUrl);
        this.add = false;
    }

    removeAttachment(list:number, item:number){
        this.attachementService.delete(this.attachmentId[list][item])
        .then(
            response => {
                this.attachmentUrl[list][item] = []
            },
            error => {
                console.log(error);
            }
        );
    }

    removeDropzone(dropzone:any){
        // console.log(dropzone)
        // Dropzone.forElement("#dropzone").removeAllFiles(true);
        dropzone._dropzone.removeAllFiles(true);

    }

    showModal(list:number,item:number){
        this.list = list;
        this.item = item;
        setTimeout(() => this.modalAttachment.show() , 100); 
    }

    clear(){
        this.list = null;
        this.item = null;
    }

    ngOnDestroy() {
        this.subscription = false;
    }
}