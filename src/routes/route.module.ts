import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from '../admin/app/admin.component';
import { AdminHeaderComponent } from '../admin/components/header/header.component';
import { AdminSidebarComponent } from '../admin/components/sidebar/sidebar.component';

//Admin Side
import { AdminLoginComponent } from '../admin/components/login/login.component';
import { AdminDashboardComponent } from '../admin/components/dashboard/dashboard.component';
import { AdminUserComponent } from '../admin/components/user/user.component';
import { AdminUserDetailComponent } from '../admin/components/user/user-detail.component';
import { AdminEditUserComponent } from '../admin/components/user/edit-user.component';
import { AdminBankComponent } from '../admin/components/bank/bank.component';
import { AdminEditBankComponent } from '../admin/components/bank/edit-bank.component';
import { AdminFaqComponent } from '../admin/components/faq/faq.component';
import { AdminEditFaqComponent } from '../admin/components/faq/edit-faq.component';
import { AdminBlogCategoryComponent } from '../admin/components/blog-category/blog-category.component';
import { AdminEditBlogCategoryComponent } from '../admin/components/blog-category/edit-blog-category.component';
import { AdminAppointmentComponent } from '../admin/components/appointment/appointment.component';
import { AdminAmenityComponent } from '../admin/components/amenity/amenity.component';
import { AdminEditAmenityComponent } from '../admin/components/amenity/edit-amenity.component';
import { AdminBlogComponent } from '../admin/components/blog/blog.component';
import { AdminBlogDetailComponent } from '../admin/components/blog/blog-detail.component';
import { AdminEditBlogComponent } from '../admin/components/blog/edit-blog.component';
import { AdminReportUserComponent } from '../admin/components/report-user/report-user.component';
import { AdminDevelopmentComponent } from '../admin/components/development/development.component';
import { AdminEditDevelopmentComponent } from '../admin/components/development/edit-development.component';
import { AdminLetterOfIntentComponent } from '../admin/components/letter-of-intent/letter-of-intent.component';
import { AdminLetterOfIntentDetailComponent } from '../admin/components/letter-of-intent/detail.component';
import { AdminTenancyAgreementComponent } from '../admin/components/tenancy-agreement/tenancy-agreement.component';
import { AdminTenancyAgreementDetailComponent } from '../admin/components/tenancy-agreement/detail.component';
import { AdminStampCertificateComponent } from '../admin/components/stamp-certificate/stamp-certificate.component';
import { AdminPropertyComponent } from '../admin/components/property/property.component';
import { AdminCreatePropertyComponent } from '../admin/components/property/create-property/create-property.component';
import { AdminViewPropertyComponent } from '../admin/components/property/view-property/view-property.component';
import { AdminHistoryComponent } from '../admin/components/history/history.component';
import { AdminCertificateComponent } from '../admin/components/certificate/certificate.component';

//Client Side
import { AppComponent }  from '../client/app/app.component';
import { HomeComponent } from '../client/components/home/home.component';
import { CommissionComponent } from '../client/components/commission/commission.component';
import { FooterComponent } from '../client/components/footer/footer.component';
import { CopyrightComponent } from '../client/components/copyright/copyright.component';
import { NavbarComponent }  from '../client/components/navbar/navbar.component';
import { ListPropertyComponent }  from '../client/components/list-property/list-property.component';
import { PropertyComponent }  from '../client/components/property/property.component';
import { PropertyDetailComponent }  from '../client/components/property-detail/property-detail.component';
import { LoiInitiateComponent }  from '../client/components/loi-initiate/loi-initiate.component';
import { LoiViewComponent }  from '../client/components/loi-view/loi-view.component';
import { LoiReuploadComponent }  from '../client/components/loi-reupload/loi-reupload.component';
import { LoiConfirmComponent }  from '../client/components/loi-confirm/loi-confirm.component';
import { TaInitiateComponent }  from '../client/components/ta-initiate/ta-initiate.component';
import { TaViewComponent }  from '../client/components/ta-view/ta-view.component';
import { TaPaymentComponent }  from '../client/components/ta-payment/ta-payment.component';
import { IlViewComponent }  from '../client/components/il-view/il-view.component';
import { ChatComponent }  from '../client/components/chat/chat.component';
import { ScheduleViewingComponent }  from '../client/components/schedule-viewing/schedule-viewing.component';
import { VerificationComponent }  from '../client/components/verification/verification.component';
import { ForgetPasswordEmailComponent }  from '../client/components/forget-password-email/forget-password-email.component';
import { SettingComponent }  from '../client/components/setting/setting.component';
import { AboutUsComponent }  from '../client/components/about-us/about-us.component';
import { PrivacyComponent }  from '../client/components/privacy/privacy.component';
import { TermsConditionComponent }  from '../client/components/terms-condition/terms-condition.component';
import { FeesChargesComponent }  from '../client/components/fees-charges/fees-charges.component';
import { FaqComponent }  from '../client/components/faq/faq.component';
import { BlogComponent }  from '../client/components/blog/blog.component';
import { BlogViewComponent }  from '../client/components/blog-view/blog-view.component';
import { SavingsComponent }  from '../client/components/savings/savings.component';
import { MemberSectionComponent }  from '../client/components/member-section/member-section.component';
import { ListingComponent }  from '../client/components/member-section/listing/listing.component';
import { FavouriteComponent }  from '../client/components/member-section/favourite/favourite.component';
import { ProposedComponent }  from '../client/components/member-section/proposed/proposed.component';
import { UpcomingTemplateComponent }  from '../client/components/member-section/upcoming/upcoming-template.component';
import { UpcomingComponent }  from '../client/components/member-section/upcoming/upcoming.component';
import { UpcomingDetailComponent }  from '../client/components/member-section/upcoming/upcoming-detail.component';
import { ArchivedComponent }  from '../client/components/member-section/archived/archived.component';
import { LetterOfIntentComponent }  from '../client/components/member-section/letter-of-intent/letter-of-intent.component';
import { TenancyAgreementComponent }  from '../client/components/member-section/tenancy-agreement/tenancy-agreement.component';
import { InventoryComponent }  from '../client/components/member-section/inventory/inventory.component';
import { NotFoundComponent }  from '../client/components/404/404.component';

//Moddleware
import { VerifiedMiddleware, LoggedInMiddleware, AsAdminMiddleware }  from '../middleware';

const routes: Routes = [
	{ path: 'admin/login', component: AdminLoginComponent },
	{ path: 'admin', component: AdminComponent, canActivate: [AsAdminMiddleware], children: [
		{ path: '', component: AdminHeaderComponent, canActivate: [AsAdminMiddleware], outlet: 'header' },
		{ path: '', component: AdminSidebarComponent, canActivate: [AsAdminMiddleware], outlet: 'sidebar' },
		{ path: 'dashboard', component: AdminDashboardComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'user', component: AdminUserComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'user/new', component: AdminEditUserComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'user/:id/edit', component: AdminEditUserComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'user/:id/detail', component: AdminUserDetailComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'bank', component: AdminBankComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'bank/new', component: AdminEditBankComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'bank/:id/edit', component: AdminEditBankComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'faq', component: AdminFaqComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'faq/new', component: AdminEditFaqComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'faq/:id/edit', component: AdminEditFaqComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'category', component: AdminBlogCategoryComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'category/new', component: AdminEditBlogCategoryComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'category/:id/edit', component: AdminEditBlogCategoryComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'appointment', component: AdminAppointmentComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'amenity', component: AdminAmenityComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'amenity/new', component: AdminEditAmenityComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'amenity/:id/edit', component: AdminEditAmenityComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'blog', component: AdminBlogComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'blog/new', component: AdminEditBlogComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'blog/:id/edit', component: AdminEditBlogComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'blog/:id/detail', component: AdminBlogDetailComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'report_user', component: AdminReportUserComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'report_user/detail', component: AdminReportUserComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'development', component: AdminDevelopmentComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'development/new', component: AdminEditDevelopmentComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'development/:id/edit', component: AdminEditDevelopmentComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'development/:id/detail', component: AdminDevelopmentComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'development/:id/unit', component: AdminDevelopmentComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'development/:id/unit/new', component: AdminEditDevelopmentComponent, canActivate: [AsAdminMiddleware]},
		{ path: 'development/unit/:id/edit', component: AdminEditDevelopmentComponent, canActivate: [AsAdminMiddleware]},
		{ path: 'letter_of_intent', component: AdminLetterOfIntentComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'letter_of_intent/details/:id', component: AdminLetterOfIntentDetailComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'tenancy_agreement', component: AdminTenancyAgreementComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'tenancy_agreement/details/:id', component: AdminTenancyAgreementDetailComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'tenancy_agreement/:id/stamp_certificate', component: AdminStampCertificateComponent, canActivate: [AsAdminMiddleware]},
		{ path: 'property', component: AdminPropertyComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'property/new', component: AdminCreatePropertyComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'property/:id/edit', component: AdminCreatePropertyComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'property/:id/detail', component: AdminViewPropertyComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'history', component: AdminHistoryComponent, canActivate: [AsAdminMiddleware] },
		{ path: 'certificate-of-stampduty', component: AdminCertificateComponent, canActivate: [AsAdminMiddleware] },
	]},
	{ path: '', children:[
		{ path : '', component: NavbarComponent, outlet: 'navbar', data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : '', component: FooterComponent, outlet: 'footer', data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : '', component: CopyrightComponent, outlet: 'copyright', data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : '', component: HomeComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'property/browse', component: ListPropertyComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'property/new', component: PropertyComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'property/:id/view', component: PropertyDetailComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'property/:id/edit', component: PropertyComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'chat', component: ChatComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'chat/:id', component: ChatComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'loi/:id/initiate', component: LoiInitiateComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'loi/:id/view', component: LoiViewComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'loi/:id/reupload', component: LoiReuploadComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'loi/:id/confirm', component: LoiConfirmComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'ta/:id/initiate', component: TaInitiateComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'ta/:id/view', component: TaViewComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'ta/:id/payment', component: TaPaymentComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'inventory/:id', component: IlViewComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'appointment/:id/initiate-view', component: ScheduleViewingComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'verification/:id', component: VerificationComponent, canActivate: [LoggedInMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'forget-password-email', component: ForgetPasswordEmailComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'reset-password-email/:id', component: ForgetPasswordEmailComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'setting', component: SettingComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'information/about-us', component: AboutUsComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'privacy', component: PrivacyComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'terms-condition', component: TermsConditionComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'fees-charges', component: FeesChargesComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'faq', component: FaqComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'blog', component: BlogComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'blog-view/:slug', component: BlogViewComponent, data: {}
		},
		{ path : 'savings', component: SavingsComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
		{ path : 'dashboard', component: MemberSectionComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware],
	    	children:[
	          	{ path : 'listing', component: ListingComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
						meta: {
							'og:url': window.location.host+location.pathname
						}
					}
				},
	          	{ path : 'favourite', component: FavouriteComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
						meta: {
							'og:url': window.location.host+location.pathname
						}
					}
				},
	          	{ path : 'proposed', component: ProposedComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
						meta: {
							'og:url': window.location.host+location.pathname
						}
					}
				},
	          	{ path : 'upcoming', component: UpcomingTemplateComponent,
			    	children:[
			          	{ path : '', component: UpcomingComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
								meta: {
									'og:url': window.location.host+location.pathname
								}
							}
						},
			          	{ path : ':id', component: UpcomingDetailComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
								meta: {
									'og:url': window.location.host+location.pathname
								}
							}
						},
			    	]},
	          	{ path : 'archived', component: ArchivedComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
						meta: {
							'og:url': window.location.host+location.pathname
						}
					}
				},
	          	{ path : 'letter-of-intent', component: LetterOfIntentComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
						meta: {
							'og:url': window.location.host+location.pathname
						}
					}
				},
	          	{ path : 'tenancy-agreement', component: TenancyAgreementComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
						meta: {
							'og:url': window.location.host+location.pathname
						}
					}
				},
	          	{ path : 'inventory', component: InventoryComponent, canActivate: [LoggedInMiddleware, VerifiedMiddleware], data: {
						meta: {
							'og:url': window.location.host+location.pathname
						}
					}
				},
	        ]
      	},
      	{ path : '**', component: NotFoundComponent, data: {
				meta: {
					'og:url': window.location.host+location.pathname
				}
			}
		},
  	]}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class RoutingModule {}
